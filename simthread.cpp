#include "simThread.h"
#include "AgriPoliS.h"

SimThread::SimThread(QString indir, QString policy): inputDir(indir),
policyFile(policy), QThread() {
  stopped = false;
  zustand = NOTREADY;
}

void SimThread::run(){
  agpinit(inputDir.toStdString(), policyFile.toStdString());
  Manager->getGlobals()->SIM_THREAD=qobject_cast<SimThread*>(currentThread());
  agprun();
  emit simover();
  //cout <<  Manager->FarmList.back()->getFarmId()<< "  Warum ??" << endl;
}

void SimThread::stop(){
    stopped = true;
}

void SimThread::takedecboden(){
  Manager->getGlobals()->SIM_WEITER=true;
}

void SimThread::takedecinvest(){
    Manager->getGlobals()->SIM_WEITER=true;
}

void SimThread::takedecclose(){
    Manager->getGlobals()->SIM_WEITER=true;
}
void SimThread::emitdecclose(){
    emit decclose();
}

void SimThread::emitdecinvest(){
    emit decinvest();
}

void SimThread::emitdecboden(){
    emit decboden();
}

void SimThread::emitInitover(){
    emit initover();
}

void SimThread::emitNewplot(PlA* s){
    emit newplot(s);

}

void SimThread::emitDelplot(PlA* s){
    emit delplot(s);
}

void SimThread::toZustand(SimZustand st){
    zustand = st;
}

void SimThread::emitItOver(int it){
    emit itOver(it);
}

void SimThread::emitItBeginn(){
    emit itBeginn();
}

void SimThread::emitProduced(){
    emit produced();
}

void SimThread::emitLAOver(){
    emit LAOver();
}

void SimThread::emitPeriodResults(){
    emit hasPeriodResults();
}

void SimThread::emitLAsig(int i, int k){
    emit LAsig(i, k);
}

void SimThread::emitLAsig(int p){
    emit LAsig(p);
}

void SimThread::emitBidValid(){
    emit bidValidate();
}

void SimThread::emitFoFsig(int i){
    emit FoFsig(i);
}

void SimThread::emitINVsig(){
    emit INVsig();
}

void SimThread::emitPRODsig(){
    emit PRODsig();
}

void SimThread::emitBusy(){
    emit busy();
}

void SimThread::emitSimOver(){
    emit simOver();
}
