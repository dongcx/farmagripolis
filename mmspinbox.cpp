
#include "mmspinbox.h"

void minSpinBox::updateValue(int i){
    int v= value();
    if (i<v)
        setValue(i);
}

void maxSpinBox::updateValue(int i){
    int v= value();
    if (i>v)
        setValue(i);
}
