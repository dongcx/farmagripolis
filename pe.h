#ifndef PRICEERWARTUNG_H
#define PRICEERWARTUNG_H

#include <QDialog>
#include <QLabel>
#include <QLineEdit>

class RegFarmInfo;
class SpreadSheet2;
class Title02;
class QComboBox;
class QSpinBox;
class QDoubleSpinBox;

using namespace std;
class PEwidget : public QWidget{
Q_OBJECT
public:
    PEwidget(int, int*, QString,vector<QString>, vector<QString>, vector<QString>, vector<double>,vector<double>, vector<QString>, QDialog *parent=0);

    void setInited(bool);
    void closeEvent(QCloseEvent *);
    void enableDSBs(bool);
    void setPeSpiels(vector<bool>);
public slots:
    void updateVar(int);
    void setVarIndex(int);
    void getPeOK(int,bool);

    void updateData(vector<double> ps,
                              vector<double> pes, vector<double> mypes, bool exist=false);
    void updateData1(vector<int> gids, vector<double> gps, vector<double> gpes);
    void updateData1a(vector<double> gps, vector<double> gpes);
    void updateData2(int rpos, vector<double> *mPEs, vector<double> adPEs, vector<double> naPEs);
    void updateData3(vector<int> pIDs, vector<vector<double>  > *allPs);

    void updateGMs(vector<double> gms);
    void updatePrices(vector<double> ps);
    void updateCosts(vector<double> cs);
    void updateAvPrices(vector<double> avps);
    void updateRefPEs(vector<double> pes);
    void updateMyPEs(vector<double> pes, bool exist);

    void updatePE(int);
    void updateIndexLab();

    void savePEs();
    void updateRef(int);
    void updateRefs();

    void updateIter(int);
    void setPeInited(bool);

signals:
    void peSig(int);
    void peVarSig(int);
    void ausBlenden();

private:
    void updateDSBs(vector<double>);
    void calRefPrices();
    void prettyUnits();

    int ITER;
    int nsoils;
    RegFarmInfo *farm0;

    QComboBox *peVarCBox;
    QSpinBox *refzeitSBox;
    int refzeit;
    int *peVar;

    int refpos;
    double preisIndex;
    QLabel *pindexLab;

    int nRowPE, nColPE;
    SpreadSheet2 *peSS;

    vector<QDoubleSpinBox*> peDSBs;

    vector<QString> names;
    vector<QString> units;
    vector<double> prices;
    vector<double> costs;
    vector<double> refPEs;
    vector<double> avprices;
    vector<double> prodPEs;
    vector<double> ertrags;
    vector<double> nebenErtrags;
    vector<QString> gmunits;

    vector<int> prodIDs;
    vector<bool> peSpiels; //aenderbare index
    int numPEs;

    vector<int> getreideIDs;
    vector<double> getreidePrices, getreidePEs;

    vector<double> adaptPEs, naivPEs, *manPEs;

    bool inited;
    double oldvalue;
    vector<vector<double> > *allPrices;

    bool firstGUI;
    QString refname;
    int refprodID;
    int proz0, proz, prozent;
    bool vorhanden;
    bool reakt;
};

class Title02 : public QLabel{
    Q_OBJECT
public: explicit Title02(Qt::AlignmentFlag align, QFont font, QWidget* w=0);

};

#endif // PRICEERWARTUNG_H
