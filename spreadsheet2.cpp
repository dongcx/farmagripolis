#include <QtGui>

#include "cell2.h"
#include "spreadsheet2.h"
#include <iostream>

using namespace std;
//extern int aWidth;

SpreadSheet2::SpreadSheet2(int nrows, int ncols, bool hasHHeader, vector<QString> hheaders, bool hasVHeader, bool b2, bool ac, QWidget *parent)
    :RowCount(nrows),ColumnCount(ncols), hHeader(hasHHeader), hHeaders(hheaders),vHeader(hasVHeader),border2(b2), alignC(ac), inited(false), QTableWidget(parent)
{
    setItemDelegate(new MyDelegate2());
    autoRecalc = true;

    setGridStyle(Qt::SolidLine);
    //setShowGrid(false);
    if (!vHeader) verticalHeader()->hide();
    if (!hHeader) horizontalHeader()->hide();
    setItemPrototype(new Cell2());
    setSelectionMode(ContiguousSelection);

    connect(this, SIGNAL(itemChanged(QTableWidgetItem *)),
            this, SLOT(afterItemChanged(QTableWidgetItem*)));//somethingChanged()));//validateItem(QTableWidgetItem *)));//

    //this->verticalScrollBar()->hide();//setDisabled(true);
    //this->horizontalScrollBar()->hide();//setDisabled(true);
    clear();
    initTab();
    adjSize();
}

void SpreadSheet2::afterItemChanged(QTableWidgetItem* item) {
    //validateItem(item);
    somethingChanged();
}

//todo
void SpreadSheet2::validateItem(QTableWidgetItem *it){
    if (!inited) return;

    QValidator::State ok;
    Cell2 *c = static_cast<Cell2*> (it);
    int rr = c->row();
    int cc = c->column();

    bool todo = false;
    QDoubleValidator *dv = new QDoubleValidator();
    QLocale::setDefault(QLocale::German);
    //dv->setLocale(QLocale(QLocale::German));
    switch (art) {
    case 0:
        break;
    case 1:
        if (rr==0 && cc >1) {
            todo = true;
            dv->setRange(0.5, 300); //todo
        }
        break;
    case 2:
        if (cc==3) {//meine PE
            todo = true;
            double min, max;

            int pcc = cc-2;
            Cell2 * cx = static_cast<Cell2*>(item(rr,pcc));
            int xr = cx->row();
            int xc = cx->column();
            QString xx =cx->data(Qt::EditRole).toString();
            double dx = xx.toDouble();

            if (dx==0){
                min = 0;
                max = 1;
            }
            else if (dx<0) {
                min = 2*dx;
                if (min < -1) min = -1;
                max = 0;
            }else {
                min = 0;
                max = 2*dx;
                if (max<1) max = 1;
            }
            //*/
            dv->setRange(min, max);
        }
        break;
    default: ;
    }

    if (!todo) return;

    QString oldtext = it->text();

    //double d = c->data(Qt::DisplayRole).toDouble();
    c->setDirty();
    viewport()->update();

    int pos = 0;
    QString t = it->text();
    ok = dv->validate(t, pos);

    //d = t.toDouble();
    if (!(ok == QValidator::Acceptable)) {
        //c->setText(oldtext);
        c->setFormula(oldtext);
        QMessageBox::warning(this,QString("? %1").arg(t), QString("Zahlen: %1 %2 %3").arg(art).arg(rr).arg(cc), "ok");
    }
   else {
        //c->setText(t);
        c->setFormula(t);
    }
    //*/
}

void SpreadSheet2::updateColumn(int col, vector<double> ds, vector<double> rats){
    int rows=rowCount();
    for (int i=0;i<rows;++i){
        cell(i, col)->setAlign(Qt::AlignRight, Qt::AlignCenter);
        setFormula(i,col, QLocale().toString(ds[i]/rats[i], 'f', 2)+"  ");//QString::number(ds[i], 'f', 2));//qstr);
    }
}

void SpreadSheet2::updateColumn(int col, vector<double> ds, vector<double> rats, vector<double> nebens){
    int rows=rowCount();
    for (int i=0;i<rows;++i){
        cell(i, col)->setAlign(Qt::AlignRight, Qt::AlignCenter);
        //std::cout<<">>: "<<i<<"\t"<<ds[i]<<"\t"<<nebens[i]<<"\t"<<rats[i]<<"\n";
        setFormula(i,col, QLocale().toString((ds[i]-nebens[i])/rats[i], 'f', 2)+"  ");
    }
}

void SpreadSheet2::updateColumn(int col, vector<double> ds, vector<double> rats, vector<double> nebens, vector<bool> inds){
    int rows=rowCount();
    for (int i=0;i<rows;++i){
        if(inds[i]) {
            cell(i, col)->setAlign(Qt::AlignRight, Qt::AlignCenter);
            setFormula(i,col, QLocale().toString((ds[i]-nebens[i])/rats[i], 'f', 2)+"  ");
        }else
            setFormula(i,col, QString());
    }
}

void SpreadSheet2::updateColumn(int col, vector<double> ds){
    int rows=rowCount();
    for (int i=0;i<rows;++i){
        //cell(i, col)->setAlign(Qt::AlignRight, Qt::AlignCenter);
        setFormula(i,col, QLocale().toString(ds[i], 'f', 2)+"  ");//QString::number(ds[i], 'f', 2));//qstr);
    }
}

void SpreadSheet2::updateColumn(int col, vector<int> ds){
    int rows=rowCount();
    for (int i=0;i<rows;++i){
        setFormula(i,col, QLocale().toString(ds[i])+"  ");//QString::number(ds[i], 'f', 2));//qstr);
        cell(i, col)->setAlign(Qt::AlignRight, Qt::AlignCenter);
    }
}


void SpreadSheet2::updateColumn(int col, vector<double> ds,int x){
    int rows=rowCount();
    for (int i=0;i<rows;++i){
        if (i>=rows-1-x && i<=rows-2)
            setFormula(i,col, QString(""));
        else  {
            setFormula(i,col, QLocale().toString(ds[i], 'f', 2));//QString::number(ds[i], 'f', 2));//qstr);
        }
    }
}

void SpreadSheet2::updateColumn(vector<double> ds, vector<int> is){
    int rows=ds.size();//rowCount();
    for (int i=0;i<rows;++i){
        setFormula(i, is[i], QLocale().toString(ds[i], 'f', 2));// QString::number(ds[i],'f', 2));//QString(ss));
        setFormula(i,is[i]==1?2:1, QString(""));
    }
}

void SpreadSheet2::updateColumn(int col, vector<QString> ds){
    int rows=rowCount();
    for (int i=0;i<rows;++i){
        setFormula(i,col, ds[i]);
    }
}

void SpreadSheet2::setRow(int row, vector<QString> strs){
    int cols= columnCount();
    for (int i=0; i<cols;++i){
        setFormula(row, i, strs[i]);
    }
}

void SpreadSheet2::setRow(int row, vector<double> ds){
    int cols= columnCount();
    for (int i=0; i<cols;++i){
        setFormula(row, i, QLocale().toString(ds[i]));//QString::number(ds[i]));
    }
}

void SpreadSheet2::setRowColor(int r, QColor c){
    int cols = columnCount();
    for (int i=0; i<cols; ++i){
        cell(r,i)->setBackgroundColor(c);
    }
}

void SpreadSheet2::setRowStyleSheet(int r, QFont f){
    int cols = columnCount();
    for (int i=0; i<cols; ++i){
        cell(r,i)->setFont(f);
    }
}

void SpreadSheet2::setRowFrame(int r, int w){
    //aWidth= w;
    int cols = columnCount();
    for (int i=0; i<cols; ++i){
        if (i==0)
            cell(r,i)->setData(Qt::DisplayPropertyRole, -103);
        else if (i==cols-1)
            cell(r,i)->setData(Qt::DisplayPropertyRole, -101);
        else
            cell(r,i)->setData(Qt::DisplayPropertyRole, -102);
    }
    update();
}

void SpreadSheet2::setColStyleSheet(int r, QFont f){
    int rows = columnCount();
    for (int i=0; i<rows; ++i){
        cell(i,r)->setFont(f);
    }
}

void SpreadSheet2::initTab(){
    int rows = rowCount();
    int cols = columnCount();
    for (int i=0;i<rows;++i)
        for (int j=0; j<cols; ++j){
            setFormula(i,j,QString(""));
            //setAligns(i, j);
        }
}

void SpreadSheet2::setAligns(int r, int c){
     ;
}

void SpreadSheet2::adjSize(){
    //resizeColumnToContents(0);
    resizeColumnsToContents();

    int w = 0;
    int count = columnCount();
    for (int i = 0; i < count; i++)
        w += columnWidth(i);

    int maxW = ( w
                 + (count<4 ? 3*count :count)
       + (vHeader?verticalHeader()->width():0) ) ;
       // + (verticalScrollBar()->isHidden()? 0 :verticalScrollBar()->width()));



    int h = 0;
    count = rowCount();
    for (int i = 0; i < count; i++)
        h += rowHeight(i);

    int maxH =( h
                + (count<10 ? (count<=3? 6*count : 3*count): count)
         + (hHeader?horizontalHeader()->height():0) );
        //+ (horizontalScrollBar()->isHidden() ? 0 : horizontalScrollBar()->height()));

    if (maxH > 330 ) {
        maxH = 330;
        horizontalScrollBar()->show();
        maxW += horizontalScrollBar()->height();
        //verticalScrollBar()->show();
        //maxW += verticalScrollBar()->width();
    }

    setMaximumWidth(maxW);
    setMinimumWidth(maxW);

    setMaximumHeight(maxH);
    setMinimumHeight(maxH);
    //*/
}

QString SpreadSheet2::currentLocation() const
{
    return QChar('A' + currentColumn())
           + QString::number(currentRow() + 1);
}

QString SpreadSheet2::currentFormula() const
{
    return formula(currentRow(), currentColumn());
}

QTableWidgetSelectionRange SpreadSheet2::selectedRange() const
{
    QList<QTableWidgetSelectionRange> ranges = selectedRanges();
    if (ranges.isEmpty())
        return QTableWidgetSelectionRange();
    return ranges.first();
}

void SpreadSheet2::setRowCountMy(int nr){
    RowCount = nr;
    QTableWidget::setRowCount(RowCount);
}

void SpreadSheet2::clear()
{
    setRowCount(0);
    setColumnCount(0);
    setRowCount(RowCount);
    setColumnCount(ColumnCount);

    if (hHeader){
    for (int i = 0; i < ColumnCount; ++i) {
        QTableWidgetItem *item = new QTableWidgetItem;
        item->setText(hHeaders[i]);
        setHorizontalHeaderItem(i, item);
    }}
    //*/
    setCurrentCell(0, 0);
}

bool SpreadSheet2::readFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::warning(this, tr("SpreadSheet2"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(file.fileName())
                             .arg(file.errorString()));
        return false;
    }

    QDataStream in(&file);
    in.setVersion(QDataStream::Qt_4_3);

    quint32 magic;
    in >> magic;
    if (magic != MagicNumber) {
        QMessageBox::warning(this, tr("SpreadSheet2"),
                             tr("The file is not a SpreadSheet2 file."));
        return false;
    }

    clear();

    quint16 row;
    quint16 column;
    QString str;

    QApplication::setOverrideCursor(Qt::WaitCursor);
    while (!in.atEnd()) {
        in >> row >> column >> str;
        setFormula(row, column, str);
    }
    QApplication::restoreOverrideCursor();
    return true;
}

bool SpreadSheet2::writeFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::warning(this, tr("SpreadSheet2"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(file.fileName())
                             .arg(file.errorString()));
        return false;
    }

    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_4_3);

    out << quint32(MagicNumber);

    QApplication::setOverrideCursor(Qt::WaitCursor);
    for (int row = 0; row < RowCount; ++row) {
        for (int column = 0; column < ColumnCount; ++column) {
            QString str = formula(row, column);
            if (!str.isEmpty())
                out << quint16(row) << quint16(column) << str;
        }
    }
    QApplication::restoreOverrideCursor();
    return true;
}

void SpreadSheet2::sort(const fwCompare2 &compare)
{
    QList<QStringList> rows;
    QTableWidgetSelectionRange range = selectedRange();
    int i;

    for (i = 0; i < range.rowCount(); ++i) {
        QStringList row;
        for (int j = 0; j < range.columnCount(); ++j)
            row.append(formula(range.topRow() + i,
                               range.leftColumn() + j));
        rows.append(row);
    }

    qStableSort(rows.begin(), rows.end(), compare);

    for (i = 0; i < range.rowCount(); ++i) {
        for (int j = 0; j < range.columnCount(); ++j)
            setFormula(range.topRow() + i, range.leftColumn() + j,
                       rows[i][j]);
    }

    clearSelection();
    somethingChanged();
}

void SpreadSheet2::cut()
{
    copy();
    del();
}

void SpreadSheet2::copy()
{
    QTableWidgetSelectionRange range = selectedRange();
    QString str;

    for (int i = 0; i < range.rowCount(); ++i) {
        if (i > 0)
            str += "\n";
        for (int j = 0; j < range.columnCount(); ++j) {
            if (j > 0)
                str += "\t";
            str += formula(range.topRow() + i, range.leftColumn() + j);
        }
    }
    QApplication::clipboard()->setText(str);
}

void SpreadSheet2::paste()
{
    QTableWidgetSelectionRange range = selectedRange();
    QString str = QApplication::clipboard()->text();
    QStringList rows = str.split('\n');
    int numRows = rows.count();
    int numColumns = rows.first().count('\t') + 1;

    if (range.rowCount() * range.columnCount() != 1
            && (range.rowCount() != numRows
                || range.columnCount() != numColumns)) {
        QMessageBox::information(this, tr("SpreadSheet2"),
                tr("The information cannot be pasted because the copy "
                   "and paste areas aren't the same size."));
        return;
    }

    for (int i = 0; i < numRows; ++i) {
        QStringList columns = rows[i].split('\t');
        for (int j = 0; j < numColumns; ++j) {
            int row = range.topRow() + i;
            int column = range.leftColumn() + j;
            if (row < RowCount && column < ColumnCount)
                setFormula(row, column, columns[j]);
        }
    }
    somethingChanged();
}

void SpreadSheet2::del()
{
    QList<QTableWidgetItem *> items = selectedItems();
    if (!items.isEmpty()) {
        foreach (QTableWidgetItem *item, items)
            delete item;
        somethingChanged();
    }
}

void SpreadSheet2::selectCurrentRow()
{
    selectRow(currentRow());
}

void SpreadSheet2::selectCurrentColumn()
{
    selectColumn(currentColumn());
}

void SpreadSheet2::recalculate()
{
    for (int row = 0; row < RowCount; ++row) {
        for (int column = 0; column < ColumnCount; ++column) {
            if (cell(row, column)){

                cell(row, column)->setDirty();
            }
        }
    }
    viewport()->update();
    for (int row = 0; row < RowCount; ++row) {
        for (int column = 0; column < ColumnCount; ++column) {
            if (cell(row, column)){
                Cell2 *c=cell(row,column);
                /*if (isSum(row,column) && (c->data(Qt::DisplayRole).toDouble()<0))
                    c->setForeground(QBrush(Qt::red));
                else
                //*/
                    c->setForeground(QBrush(Qt::black));

            }
        }
    }
    adjSize();
}

void SpreadSheet2::setAutoRecalculate(bool recalc)
{
    autoRecalc = recalc;
    if (autoRecalc)
        recalculate();
}

void SpreadSheet2::findNext(const QString &str, Qt::CaseSensitivity cs)
{
    int row = currentRow();
    int column = currentColumn() + 1;

    while (row < RowCount) {
        while (column < ColumnCount) {
            if (text(row, column).contains(str, cs)) {
                clearSelection();
                setCurrentCell(row, column);
                activateWindow();
                return;
            }
            ++column;
        }
        column = 0;
        ++row;
    }
    QApplication::beep();
}

void SpreadSheet2::findPrevious(const QString &str,
                               Qt::CaseSensitivity cs)
{
    int row = currentRow();
    int column = currentColumn() - 1;

    while (row >= 0) {
        while (column >= 0) {
            if (text(row, column).contains(str, cs)) {
                clearSelection();
                setCurrentCell(row, column);
                activateWindow();
                return;
            }
            --column;
        }
        column = ColumnCount - 1;
        --row;
    }
    QApplication::beep();
}

void SpreadSheet2::somethingChanged()
{
    if (autoRecalc)
        recalculate();
    emit modified();
}

Cell2 *SpreadSheet2::cell(int row, int column) const
{
    return static_cast<Cell2 *>(item(row, column));
}

void SpreadSheet2::setFormula(int row, int column,
                             const QString &formula)
{
    Cell2 *c = cell(row, column);
    if (!c) {
        if (alignC) c= new Cell2(false, true);
        else if (column==0) c = new Cell2(true, false);
        else c = new Cell2;
        setItem(row, column, c);
    }

   /* if (isSum(row,column) && (c->data(Qt::DisplayRole).toDouble()<0))
        c->setForeground(QBrush(Qt::red));
   //*/
         c->setFormula(formula);
         if (row==rowCount()-1 && border2) {
                c->setData(Qt::DisplayPropertyRole, -1);
                //c->setFont(QFont("times",10,QFont::Bold));
          }
}

bool SpreadSheet2::isSum(int r,int c){
    if (r==RowCount-1) return true;
    return false;
}

QString SpreadSheet2::formula(int row, int column) const
{
    Cell2 *c = cell(row, column);
    if (c) {
        return c->formula();
    } else {
        return "";
    }
}

QString SpreadSheet2::text(int row, int column) const
{
    Cell2 *c = cell(row, column);
    if (c) {
        return c->text();
    } else {
        return "";
    }
}

void SpreadSheet2::setArt(int i){
  art = i;
}

void SpreadSheet2::setInited(bool b){
  inited = b;
}

bool fwCompare2::operator()(const QStringList &row1,
                                    const QStringList &row2) const
{
    for (int i = 0; i < KeyCount; ++i) {
        int column = keys[i];
        if (column != -1) {
            if (row1[column] != row2[column]) {
                if (ascending[i]) {
                    return row1[column] < row2[column];
                } else {
                    return row1[column] > row2[column];
                }
            }
        }
    }
    return false;
}
