#ifndef DATAWINDOW_H
#define DATAWINDOW_H

#include <QMainWindow>
#include <QComboBox>
#include <QLabel>
#include <QGraphicsView>
#include <QGraphicsScene>

#include "AgriPoliS.h"
#include "RegFarm.h"
#include <QGridLayout>
#include <QGroupBox>

#include <QPrinter>
#include "myTypes.h"
#include "cplot.h"

#include <qwt_plot_zoomer.h>
#include "ftplot.h"

#include <qcheckbox.h>
#include <qradiobutton.h>
#include <qpushbutton.h>
#include "mmspinbox.h"

#include "adjDialog.h"

class Mainwindow;

class Datawindow:public QMainWindow {
    Q_OBJECT
public:
    static int const MAXCURVES=12;

    map<int,RegFarmInfo*> mapIDpFarm;
    static const int numPlot = 8;//6;

    int minIter;
    int maxIter;
    double animYrightMax;
    double animYrightMin;

    explicit Datawindow(Mainwindow *parent, QWidget* realpar);
    void updateRegdata();
    void initRegdata();

    void saveConfig();
    void faOutput();

    //max, min X
    vector<int> maxXs, minXs;

    vector<QColor> gridColors, canvasColors;

    //data for drawing curves
    //einkommen pro ha
    vector<double> incXdata;
    vector<vector<double> > incYdatas;
    vector<string> incYtitles;
    vector<QColor> incColors;
    vector<int> incYaxes;
    vector<QwtSymbol::Style> incStyles;

    //gewinn pro betrieb
    vector<double> gewXdata;
    vector<vector<double> > gewYdatas;
    vector<string> gewYtitles;
    vector<QColor> gewColors;
    vector<int> gewYaxes;
    vector<QwtSymbol::Style> gewStyles;

    //Farmgr��e
    vector<double> sizXdata;
    vector<vector<double> > sizYdatas;
    vector<string> sizYtitles;
    vector<QColor> sizColors;
    vector<int> sizYaxes;
    vector<QwtSymbol::Style> sizStyles;

    //Pachtpreis
    vector<double> bodXdata;
    vector<vector<double> > bodYdatas;
    vector<string> bodYtitles;
    vector<QColor> bodColors;
    vector<int> bodYaxes;
    vector<QwtSymbol::Style> bodStyles;

    // Tiere
    vector<double> animXdata;
    vector<vector<double> > animYdatas;
    vector<string> animYtitles;
    vector<QColor> animColors;
    vector<int> animYaxes;
    vector<QwtSymbol::Style> animStyles;

    // EK
    vector<double> ekXdata;
    vector<vector<double> > ekYdatas;
    vector<string> ekYtitles;
    vector<QColor> ekColors;
    vector<int> ekYaxes;
    vector<QwtSymbol::Style> ekStyles;
    vector<int> rankEKs, totEKs;

    //pe-produkte-preise
    vector<QString> prodNames;
    vector<QString> prodUnits;
    vector<vector<double> > prodPricess;
    int parentWidth;

    vector<int> prodIDs;
    vector<double>prodErtrags, prodNertrags;
public slots:
    void initPePrices();
    void updatePePrices();
    void showRunde(int);
    bool plotsCreated();
    void closeEvent(QCloseEvent*);

    void updateGraphic(int);
    void updateGraphic(bool);
    void updateWithBorder();

    void updateCheckLegal(bool);
    void updateCheckSize(bool);
    void updateCheckLegalGew(bool);
    void updateCheckSizeGew(bool);

    void updateLow(int);
    void updateHigh(int);
    void updateLowGew(int);
    void updateHighGew(int);

    void updatePlot();
    void updatePlotGew();
    void updateData();

    void updateAusserB(bool b);
    void updateAusserBGew(bool b);

    void updateLegs(int);
    void updateLegsGew(int);

    void showAdjDialog(bool);
    void updateConfig();

    void updateXmin(int);
    void updateXmax(int);

    void updateDichteMin(double);
    void updateDichteMax(double);

    void updatePlotFarben(QColor,QColor);
    void updateCurveFarben(int,QColor);
    void updateCurveStyle(int,QwtSymbol);

    void dichteEinAusblenden(bool);
    void dichteBoxCheck(bool);

    void drucken();

    void updateNfarms();

signals:
    void hi(bool);

protected:
    QString getFileName();
    void prettyUnits();
    void createActions();
    void createToolBars();
    void makeDocks();

    void createStatusBar();
    void updateStatusBar();

    void createPlots();
    void createConstrWidgets();
    void createFTPlot();

    void enableZoomMode(bool);

    void updateGraphics();
    bool inBereich(double);
    bool inBereichGew(double);
    void updateFarmeinkommen();
    void updateFarmgewinn();
    void updateFarmtypen();

    void constrFarmeinkommen();
    void constrFarmgewinn();

    void updateFarmsize();
    void updatePachtpreis();

    void updateTiere();
    void updateEK();

    void initXs();
    void initHasDialogs();

    void initPlotColors();
    void initStyles();

    void initCurveColors(int);

private:
    bool inited;
    int nFarmtypes;

    QPrinter *printer;
    map<int,string> classnames;

    bool zunah(QColor, int, int, int);
    int picType;
    bool withBorder;

    bool hasPlots;
    FTPlot *tp;

private:
    QToolBar *chooseLayerToolBar;
    QToolBar *zoomToolBar;

    QToolBar *colorToolBar;
    QToolBar *optToolBar;

    QAction *actConfig;
    QAction *actWithBorder;
    QAction *actPrint;
    QAction *actIter;

    QLabel *layerLabel;
public:   QComboBox *layerComboBox;

private:
    QSpinBox* iterSBox;
    int iter;
    QGraphicsItemGroup *gridGroup;
    QColor gridColor;

    QLabel *statusLabel;

    Plot *plot;
    QWidget *cWidget;
    vector<QVBoxLayout *> clouts;
    QVBoxLayout *clout;

    vector<QWidget*> plots;
    vector<QWidget*> constrWidgets;

    int oldlayer;
    QwtPlotZoomer *d_zoomer[2*numPlot];

    vector< vector<DataReg> > regDatavec2;

    vector<string> farmtypeNames;
    vector<QColor> farmtypeColors;
    vector<vector<double> > farmtypeNums;
    QwtPlotGrid *farmtypeGrid;
    vector <vector<double> > farmtypeProzents;

    vector<bool> legalforms, legalformsGew;
    double farmsizeMin, farmsizeMax;
    double farmsizeMinGew, farmsizeMaxGew;
    bool ausserBereich, ausserBereichGew;

    QCheckBox *checkLegal, *checkLegalGew ;
    vector<QCheckBox *>legs, legsGew;

    QCheckBox *checkSize, *checkSizeGew;
    QCheckBox *ausserB, *ausserBGew ;

    minSpinBox *sbLow, *sbLowGew;
    maxSpinBox *sbHigh,*sbHighGew;

    QLabel *bereichLab, *bereichLabGew;
    QLabel *llab, *llabGew;
    QLabel *hlab, *hlabGew;

    QPushButton *repButton, *repButtonGew;
    vector<bool> hasDialogs;
    vector<adjDialog*> dialogs;

    int nSoils;
    vector<string> namessoils;
    int nAnimals;
    double maxFarmsize;
    int nLegaltypes;

    QCheckBox *dichteBox;

    int nFarms;
    QString nFarmsStr;
    QGroupBox *auswahlGBox, *auswahlGBoxGew;

    Mainwindow* par;
};


#endif // DATAWINDOW_H
