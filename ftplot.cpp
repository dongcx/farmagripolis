#include <stdlib.h>
#include <qpen.h>
#include <qwt_plot_layout.h>
#include <qwt_legend.h>
#include <qwt_legend_item.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_histogram.h>
#include <qwt_column_symbol.h>
#include <qwt_series_data.h>
#include "ftplot.h"
#include <qpainter.h>

#include <qwt_plot_picker.h>
#include "Datawindow.h"

using namespace std;

class Histogram: public QwtPlotHistogram
{
public:
    Histogram(const QString &, vector<map <int, double> >*,int ,const QColor &);

    void setColor(const QColor &);
    void setValues(uint numValues, const double *);
    /*
    void m_drawColumns( QPainter *painter,
                    const QwtScaleMap &xMap, const QwtScaleMap &yMap,
                    int from, int to ) const;
    //*/
    QwtColumnRect columnRect(const QwtIntervalSample &sample,
                 const QwtScaleMap &xMap, const QwtScaleMap &yMap) const;

    vector< map<int,double> >  *bases;
    int serie;
};


Histogram::Histogram(const QString &title, vector<map<int,double> > *ba, int s,const QColor &symbolColor):
    bases(ba),serie(s),QwtPlotHistogram(title)
{

    setStyle(QwtPlotHistogram::Columns);

    setColor(symbolColor);
}

void Histogram::setColor(const QColor &symbolColor)
{
    QColor color = symbolColor;
    color.setAlpha(180);

    setPen(QPen(Qt::black));
    setBrush(QBrush(color));

    QwtColumnSymbol *symbol = new QwtColumnSymbol(QwtColumnSymbol::Box);
    symbol->setFrameStyle(QwtColumnSymbol::Raised);//:Plain);//:Raised);
    symbol->setLineWidth(2);
    symbol->setPalette(QPalette(color));
    setSymbol(symbol);
}

void Histogram::setValues(uint numValues, const double *values)
{
    QVector<QwtIntervalSample> samples(numValues);
    map<int,double> aimap,  bimap;
    bool first=(bases->size()==0);
    if (!first) aimap=(*bases)[bases->size()-1];
    for ( uint i = 0; i < numValues; i++ )
    {
        QwtInterval interval(double(i-0.4), i +0.4);
        interval.setBorderFlags(QwtInterval::ExcludeMaximum);

        if (first) bimap[i]=values[i];
        else bimap[i]=aimap[i]+values[i];

        samples[i] = QwtIntervalSample(bimap[i],interval);//values[i], interval);
    }
    bases->push_back(bimap);
    setData(new QwtIntervalSeriesData(samples));
}

QwtColumnRect Histogram::columnRect( const QwtIntervalSample &sample,
    const QwtScaleMap &xMap, const QwtScaleMap &yMap) const
{
    QwtColumnRect rect;

    const QwtInterval &iv = sample.interval;
    int c = static_cast<int>(iv.minValue()+0.6);
    if ( !iv.isValid() )
        return rect;

    if ( orientation() == Qt::Horizontal )
    {
        const double x0 = xMap.transform( baseline());
        const double x  = xMap.transform( sample.value );
        const double y1 = yMap.transform( iv.minValue() );
        const double y2 = yMap.transform( iv.maxValue() );

        rect.hInterval.setInterval( x0, x );
        rect.vInterval.setInterval( y1, y2, iv.borderFlags() );
        rect.direction = ( x < x0 ) ? QwtColumnRect::RightToLeft :
                         QwtColumnRect::LeftToRight;
    }
    else
    {
        const double x1 = xMap.transform( iv.minValue() );
        const double x2 = xMap.transform( iv.maxValue() );
        map<int,double> xmap = (*bases)[serie];
        double x = xmap[c];
        double x0;
     if (serie>0){
        map<int,double> xmap0= (*bases)[serie-1];
        x0=xmap0[c];
     }else x0=0;
        const double y0 = yMap.transform( x0);//-sample.value);//static_cast<int>(iv.minValue())]-sample.value );
        const double y = yMap.transform( x);//sample.value );

        rect.hInterval.setInterval( x1, x2, iv.borderFlags() );
        rect.vInterval.setInterval( y0, y );
        rect.direction = ( y < y0 ) ? QwtColumnRect::BottomToTop :
            QwtColumnRect::TopToBottom;
    }

    return rect;
}


FTPlot::FTPlot(vector<string> ns, vector<QColor> cs, vector< vector<double> > ds,Datawindow *dw, QWidget *parent):
   withGrid(true), serie(0),names(ns),colors(cs),datas(ds),datawin(dw),QwtPlot(parent)
{
    QwtPlotPicker *d_picker;

    setAxisAutoScale(xBottom,false);
    setAxisScale(xBottom,dw->minXs[2],dw->maxXs[2],dw->maxXs[2]-dw->minXs[2]>12?2:1);//2:Farmtype-plot
    setAxisMaxMinor(QwtPlot::xBottom, 1);

    setTitle("Betriebsanteil nach Betriebstypen");

    setCanvasBackground(QColor(datawin->canvasColors[2]));
    plotLayout()->setAlignCanvasToScales(true);

    setAxisTitle(QwtPlot::yLeft, "Anteil, \%");
    setAxisTitle(QwtPlot::xBottom, "Periode");


    QwtLegend *legend = new QwtLegend;
    //legend->setItemMode(QwtLegend::CheckableItem);
    legend->setFrameShape(QFrame::Panel);//Box);
    legend->setFrameShadow(QFrame::Raised);//Sunken);
    insertLegend(legend, QwtPlot::RightLegend);

    grid = new QwtPlotGrid;
    grid->enableX(false);
    grid->enableY(true);
    grid->enableXMin(false);
    grid->enableYMin(false);
    grid->setMajPen(QPen(datawin->gridColors[2], 0, Qt::DotLine));
    grid->attach(this);

    /*
    d_picker = new QwtPlotPicker(QwtPlot::xBottom, QwtPlot::yLeft,
        QwtPlotPicker::CrossRubberBand, QwtPicker::AlwaysOn,
        this->canvas());
    d_picker->setRubberBandPen(QColor(Qt::green));
    d_picker->setRubberBand(QwtPicker::CrossRubberBand);
    d_picker->setTrackerPen(QColor(Qt::white));
    //*/

    populate();

    //connect(this, SIGNAL(legendChecked(QwtPlotItem *, bool)),
      //  SLOT(showItem(QwtPlotItem *, bool)));

    replot();

    setAutoReplot(true);
}

QwtPlotGrid* FTPlot::getGrid(){
    return grid;
}

void FTPlot::populate()
{
    int sz = names.size();
    histogs.clear();//vector<Histogram*> histogs;
    histogs.resize(sz);
    for (int i=0; i<sz; ++i){
        string n = names[i];
        QColor c = colors[i];
        vector<double> d = datas[i];

        histogs[i] = new Histogram(n.c_str(), &bases, serie++, c);
        histogs[i]->setValues( d.size(), &d[0]);
    }

    for (int i=0;i<sz;++i){
        histogs[sz-1-i]->attach(this);
    }
}

void FTPlot::updatePlot(const vector<string> ns, const vector<QColor> cs, const vector< vector<double> > ds){
    names.clear();names=ns;
    colors.clear();colors=cs;
    datas.clear();datas=ds;
    bases.clear();
    serie=0;

    for (int i=0;i<histogs.size();++i){
        histogs[i]->detach();
    }

    populate();
    replot();
}

void FTPlot::updateColor(int i, QColor c){
    colors[i]=c;
    histogs[i]->setColor(c);
}

void FTPlot::updateColors(){
    int sz = histogs.size();
    for (int i=0;i<sz;++i){
        updateColor(i,colors[i]);
    }
}

void FTPlot::showItem(QwtPlotItem *item, bool on)
{
    item->setVisible(on);
}

