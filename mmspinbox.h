#ifndef MMSPINBOX_H
#define MMSPINBOX_H

#include <qspinbox.h>

class minSpinBox:public QSpinBox {
    Q_OBJECT
public:
    minSpinBox(){setMinimum(-1);}
public slots:
    void updateValue(int);
};



class maxSpinBox:public QSpinBox {
    Q_OBJECT
public:
    maxSpinBox(){setMinimum(-1);}
public slots:
    void updateValue(int);
};




#endif // MMSPINBOX_H
