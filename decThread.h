#ifndef DECTHREAD_H
#define DECTHREAD_H

#include <QThread>
class RegFarmInfo;

class DecThread : public QThread
{
    Q_OBJECT

public:
    DecThread();

    void stop();
    void setFarm(RegFarmInfo*);
    void emitAreaOK(int,int);
    void emitAreaOK2B(int,int);
    void invDec2();
    void invDec3();

public slots:
    void simsig();
    void decboden();
    void decinvest();
    void decclose();

    void getLAsig(int, int);
    void getLAsig(int);
    void getBodenOk();
    void getAreaSig(int,int);
    void getAreaSig2(int);
    void getAreaSig2B(int,int);
    void getPeSig(int);
    void getPeVarSig(int);
    void getClosePeSig(int);

    void getClosePeVarSig(int);

    void getFoFsig(int);
    void getCloseSig(bool);

    void getINVsig();
    void getInvestSig(int,int);
    void getInvestPeSig(int);

    void getInvestPeVarSig(int);

signals:
    void odecboden();
    void odecinvest();
    void odecclose();

    void showBodenDialog(int, int);
    void showBodenDialog(int);
    void areaOK(int,int);
    void areaOK2(int);
    void areaOK2B(int,int);
    void peOK(int,bool);
    void peVarOK(int, bool);

    void ClosePeOK(int,bool);
    void ClosePeVarOK(int,bool);

    void showCloseDialog(int);
    void showInvestDialog();

    void investOK(int,bool);
    void investPeOK(int,bool);

    void investPeVarOK(int,bool);

protected:
    void run();

private:
      volatile  bool stopped;
      RegFarmInfo* farm0;
};

#endif // DECTHREAD_H
