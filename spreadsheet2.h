#ifndef SPREADSHEET2_H
#define SPREADSHEET2_H

#include <QTableWidget>
#include <qitemdelegate.h>
#include <vector>
#include <qpainter.h>

class Cell2;
class fwCompare2;
class MyDelegate2;

using namespace std;
//int aWidth = 2;

class SpreadSheet2 : public QTableWidget
{
    Q_OBJECT

public:
    //SpreadSheet2(int, int, bool, vector<QString>, QWidget *parent = 0);
    SpreadSheet2(int, int, bool, vector<QString>, bool vheader=true, bool b2=false, bool align=false, QWidget *parent = 0);

    bool autoRecalculate() const { return autoRecalc; }
    QString currentLocation() const;
    QString currentFormula() const;
    QTableWidgetSelectionRange selectedRange() const;
    void clear();
    bool readFile(const QString &fileName);
    bool writeFile(const QString &fileName);
    void sort(const fwCompare2 &compare);

    void adjSize();
    void updateColumn(int, vector<double>, vector<double>);
    void updateColumn(int, vector<double>, vector<double>, vector<double>);
    void updateColumn(int, vector<double>, vector<double>, vector<double>,vector<bool>);
    void updateColumn(int,vector<double>, int);
    void updateColumn(int,vector<QString>);
    void updateColumn(vector<double>, vector<int>);
    void updateColumn(int, vector<double>);
    void updateColumn(int, vector<int>);
    void setRow(int, vector<QString>);
    void setRow(int, vector<double>);
    void initTab();
    void setAligns(int,int);

    Cell2 *cell(int row, int column) const;
    void setArt(int);
    void setInited(bool);

    void setRowStyleSheet(int,QFont);
    void setColStyleSheet(int,QFont);
    void setRowFrame(int,int);

    void setRowColor(int,QColor);
    void setRowCountMy(int);

public slots:
    void cut();
    void copy();
    void paste();
    void del();
    void selectCurrentRow();
    void selectCurrentColumn();
    void recalculate();
    void setAutoRecalculate(bool recalc);
    void findNext(const QString &str, Qt::CaseSensitivity cs);
    void findPrevious(const QString &str, Qt::CaseSensitivity cs);

    void validateItem(QTableWidgetItem*);
    void afterItemChanged(QTableWidgetItem*);

signals:
    void modified();

private slots:
    void somethingChanged();
    bool isSum(int,int);

private:
    enum { MagicNumber = 0x7F51C883};
    int RowCount;
    int ColumnCount;
    bool hHeader;
    vector<QString> hHeaders;
    bool vHeader;
    bool border2;
    bool alignC;

    //Cell2 *cell(int row, int column) const;
    QString text(int row, int column) const;
    QString formula(int row, int column) const;
    void setFormula(int row, int column, const QString &formula);

    bool autoRecalc;

    int art; //boden: 0B 1F 2PE
    bool inited;
};

class MyDelegate2 : public QItemDelegate {
  public:
    void paint( QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index ) const {
      QItemDelegate::paint( painter, option, index );
      if( index.row()==0 || index.data(Qt::DisplayPropertyRole)==-1 ) {
         QPen pen(Qt::darkGray);
         pen.setWidthF(2.0);
         painter->setPen( pen );
         QRect rect=option.rect;//adjusted(0,-1,0,0);
         painter->drawLine(rect.topLeft(), rect.topRight());
      }
      int role = index.data(Qt::DisplayPropertyRole).toInt();
      if (role<-100) { //mit Rahmen
      //if(index.data(Qt::DisplayPropertyRole)<-100 ) {
         QPen pen(QColor(0,170,0));
         pen.setWidthF(3);
         painter->setPen( pen );
         QRect rect=option.rect;//adjusted(0,-1,0,0);
         painter->drawLine(rect.topLeft(), rect.topRight());
         painter->drawLine(rect.bottomLeft(), rect.bottomRight());
         if (role==-103) //links
            painter->drawLine(rect.topLeft(), rect.bottomLeft());
         if (role == -101 ) //rights
            painter->drawLine(rect.topRight(), rect.bottomRight());
      }
  }
};


class fwCompare2
{
public:
    bool operator()(const QStringList &row1,
                    const QStringList &row2) const;

    enum { KeyCount = 3 };
    int keys[KeyCount];
    bool ascending[KeyCount];
};

#endif
