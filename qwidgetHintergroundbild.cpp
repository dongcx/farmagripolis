    # include <QWidget>
    # include <QBrush>
    # include <QPixmap>
    # include <QColor>
    # include <QPalette>

    void SetBackgroundImage ( QWidget *fp_widget, const QPixmap *fp_image )
    {
       QPalette newPalette = fp_widget->palette ( );
       newPalette.setBrush
       (
          QPalette::Base,
          QBrush ( *fp_image )
       );

       newPalette.setBrush
       (
          QPalette::Background,
          QBrush ( *fp_image )
       );

       fp_widget->setPalette ( newPalette );
    }

    QBrush BrushSetAlpha ( QBrush f_brush, const int f_alpha )
    {
       QColor color = f_brush.color ( );
       color.setAlpha ( f_alpha );
       f_brush.setColor ( color );

       return f_brush;
    }

    void SetTransparency ( QWidget *fp_widget, bool f_transparent )
    {
       int alpha ( 255 );

       if ( f_transparent )
          alpha = 0;

       QPalette newPalette = fp_widget->palette ( );

       newPalette.setBrush ( QPalette::Base, BrushSetAlpha ( newPalette.base ( ), alpha ) ); // sollte rausgeschmissen werden, wenn Textfelder sonst zu unleserlich sind.
       newPalette.setBrush ( QPalette::Background, BrushSetAlpha ( newPalette.background ( ), alpha ) );

       fp_widget->setPalette ( newPalette );
    }
