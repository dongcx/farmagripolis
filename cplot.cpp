#include <qwt_math.h>
#include <qwt_scale_engine.h>
#include <qwt_symbol.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_picker.h>
#include <qwt_plot_curve.h>
#include <qwt_legend.h>
#include <qwt_text.h>
#include <qmath.h>
#include "complexnumber.h"
#include "cplot.h"
#include "Datawindow.h"
#include "qwt_legend_item.h"
#include "Mydatwindow.h"
#include "Datawindow.h"

#if QT_VERSION < 0x040601
#define qExp(x) ::exp(x)
#define qAtan2(y, x) ::atan2(y, x)
#endif

Plot::Plot(int n, QMainWindow *dw, QWidget *parent):
    hasCurves(false),plotn(n), datawin(dw),QwtPlot(parent)
{
  hasMarkers=false;
  setAutoReplot(false);

  setAxisAutoScale(xBottom,false);
  if (n==3)
      setAxisScale(xBottom,static_cast<Mydatwindow*>(dw)->minX,
                   static_cast<Mydatwindow*>(dw)->maxX,
                   static_cast<Mydatwindow*>(dw)->maxX-
                   static_cast<Mydatwindow*>(dw)->minX>12?2:1);
  else if (n==6)
      setAxisScale(xBottom,static_cast<Mydatwindow*>(dw)->minXEK,
                   static_cast<Mydatwindow*>(dw)->maxXEK,
                   static_cast<Mydatwindow*>(dw)->maxXEK-
                   static_cast<Mydatwindow*>(dw)->minXEK>12?2:1);
  else if (n==106) setAxisScale(xBottom,static_cast<Datawindow*>(dw)->minXs[n-100],
               static_cast<Datawindow*>(dw)->maxXs[n-100],
               static_cast<Datawindow*>(dw)->maxXs[n-100]-
               static_cast<Datawindow*>(dw)->minXs[n-100]>12?2:1);
  else setAxisScale(xBottom,static_cast<Datawindow*>(dw)->minXs[n],
               static_cast<Datawindow*>(dw)->maxXs[n],
               static_cast<Datawindow*>(dw)->maxXs[n]-
               static_cast<Datawindow*>(dw)->minXs[n]>12?2:1);

  switch (n) {
  case 0: setTitle("Betriebseinkommen nach Betriebstypen");break;
  case 2: setTitle(tr("Betriebsgröße nach Betriebstypen"));break;
  case 1: setTitle("Betriebsgewinn nach Betriebstypen"); break;
  case 3: setTitle("Einteilung nach Europäischer Größeneinheit (EGE)");break;
  case 4: setTitle("Pachtpreise nach Bodentypen");break;
  case 5: setTitle("Anzahl Tiere und Tierdichte");break;
  case 6:
  case 106: setTitle("Einteilung nach Eigenkapital");break;
  default:;
  }

    legend = new QwtLegend;

    legend->setFrameShape(QFrame::Panel);//Box);
    legend->setFrameShadow(QFrame::Raised);//Sunken);
    legend->setItemMode(QwtLegend::CheckableItem);
    legend->setLineWidth(2);

    withGrid=true;

    // grid 
    grid = new QwtPlotGrid;
    grid->enableXMin(false);//true);
    if (n==3)
        grid->setMajPen(QPen(static_cast<Mydatwindow*>(datawin)->gridColor, 0, Qt::DotLine));
    else if (n==6)
        grid->setMajPen(QPen(static_cast<Mydatwindow*>(datawin)->gridColorEK, 0, Qt::DotLine));
    else if (n==106)
        grid->setMajPen(QPen(static_cast<Datawindow*>(datawin)->gridColors[n-100], 0, Qt::DotLine));
    else grid->setMajPen(QPen(static_cast<Datawindow*>(datawin)->gridColors[n], 0, Qt::DotLine));
    //grid->setMinPen(QPen(Qt::gray, 0 , Qt::DotLine));
    grid->attach(this);

    // axes
    //enableAxis(QwtPlot::yRight);
    setAxisTitle(QwtPlot::xBottom, "Periode");

    //setAxisMaxMajor(QwtPlot::xBottom, 6);
    setAxisMaxMinor(QwtPlot::xBottom, 1);

    setAxisAutoScale(yLeft,true);
    setAxisAutoScale(yRight,true);

    // curves
    makeCurves(n);
    makeMarkers(n);

    connect(this, SIGNAL(legendChecked(QwtPlotItem *, bool)),
            SLOT(showCurve(QwtPlotItem *, bool)));

    /*
    d_pickerL = new QwtPlotPicker(QwtPlot::xBottom, QwtPlot::yLeft,
        QwtPlotPicker::CrossRubberBand, QwtPicker::AlwaysOn,
        this->canvas());
    d_pickerL->setRubberBandPen(QColor(Qt::green));
    d_pickerL->setRubberBand(QwtPicker::CrossRubberBand);
    d_pickerL->setTrackerPen(QColor(Qt::white));

    d_pickerR = new QwtPlotPicker(QwtPlot::xBottom, QwtPlot::yRight,
        QwtPlotPicker::CrossRubberBand, QwtPicker::AlwaysOn,
        this->canvas());
    d_pickerR->setRubberBandPen(QColor(Qt::green));
    d_pickerR->setRubberBand(QwtPicker::CrossRubberBand);
    d_pickerR->setTrackerPen(QColor(Qt::black));

    switch (n) {
    case 0:
    case 1:
    case 3: d_pickerR->setEnabled(false);break;
    case 4: d_pickerL->setEnabled(false);break;
    default:;
    }
    //*/

    setAutoReplot(true);
}

void Plot::showCurve(QwtPlotItem *item, bool on)
{
    item->setVisible(on);
    QWidget *w = legend->find(item);
    if ( w && w->inherits("QwtLegendItem") )
        ((QwtLegendItem *)w)->setChecked(on);

   if(plotn==5) {
     if (static_cast<QwtPlotCurve*>(item)==curves[nCurves-1])
        emit dichteShow(on);
   }
   if (plotn==6|| plotn==106) {
    if (static_cast<QwtPlotCurve*>(item)==curves[nCurves-1])
        makeMarkers(plotn,on);
   }
   replot();
}

void Plot::makeCurves(int n){
    int i,s;
    styles.clear();

    QPen *pen;
    if (!hasCurves) {
        if (n==3)
            setCanvasBackground(QBrush(static_cast<Mydatwindow*>(datawin)->canvasColor));
        else if(n==6)
            setCanvasBackground(QBrush(static_cast<Mydatwindow*>(datawin)->canvasColorEK));
        else if(n==106)
            setCanvasBackground(QBrush(static_cast<Datawindow*>(datawin)->canvasColors[n-100]));
        else setCanvasBackground(QBrush(static_cast<Datawindow*>(datawin)->canvasColors[n]));
    }
    switch(n) {
    case 0:
        if (!hasCurves) {
            insertLegend(legend, QwtPlot::RightLegend);//BottomLegend);
            setAxisTitle(yLeft,(QString("Betriebseinkommen, ")+QChar(8364)+"/ha"));
        }
        xdata=static_cast<Datawindow*>(datawin)->incXdata;
        ydatas=static_cast<Datawindow*>(datawin)->incYdatas;
        yaxes=static_cast<Datawindow*>(datawin)->incYaxes;
        ytitles=static_cast<Datawindow*>(datawin)->incYtitles;
        colors = static_cast<Datawindow*>(datawin)->incColors;

        styles=static_cast<Datawindow*>(datawin)->incStyles;

        break;
    case 1:
        if (!hasCurves) {
            insertLegend(legend, QwtPlot::RightLegend);//BottomLegend);
            setAxisTitle(yLeft,(QString("Gewinn, ")+QChar(8364)+"/Betrieb"));
        }
        xdata=static_cast<Datawindow*>(datawin)->gewXdata;
        ydatas=static_cast<Datawindow*>(datawin)->gewYdatas;
        yaxes=static_cast<Datawindow*>(datawin)->gewYaxes;
        ytitles=static_cast<Datawindow*>(datawin)->gewYtitles;
        colors = static_cast<Datawindow*>(datawin)->gewColors;

        styles=static_cast<Datawindow*>(datawin)->gewStyles;
        //*/
        break;
    case 2:
        if(!hasCurves){
            insertLegend(legend, QwtPlot::RightLegend);
            setAxisTitle(yLeft,(QString("Betriebsgröße")+ " ha"));
        }

        xdata=static_cast<Datawindow*>(datawin)->sizXdata;
        ydatas=static_cast<Datawindow*>(datawin)->sizYdatas;
        yaxes=static_cast<Datawindow*>(datawin)->sizYaxes;
        ytitles=static_cast<Datawindow*>(datawin)->sizYtitles;
        colors = static_cast<Datawindow*>(datawin)->sizColors;

        styles=static_cast<Datawindow*>(datawin)->sizStyles;

        break;
    case 3:
        if(!hasCurves){
            //insertLegend(legend, QwtPlot::RightLegend);
            setAxisTitle(yLeft,(QString("EGE")));
            setAxisTitle(yRight,(QString("Mein Rang(Platz/#Betriebe), ")+"%"));
            insertLegend(legend, QwtPlot::BottomLegend);//:RightLegend);
            enableAxis(QwtPlot::yRight);
            setAxisAutoScale(yRight, false);
            setAxisScale(yRight,100,0); //prozent
        }

        xdata=static_cast<Mydatwindow*>(datawin)->esuXdata;
        ydatas=static_cast<Mydatwindow*>(datawin)->esuYdatas;
        yaxes=static_cast<Mydatwindow*>(datawin)->esuYaxes;
        ytitles=static_cast<Mydatwindow*>(datawin)->esuYtitles;
        colors = static_cast<Mydatwindow*>(datawin)->esuColors;
        styles=static_cast<Mydatwindow*>(datawin)->esuStyles;

        break;
    case 4:
        if (!hasCurves){
            insertLegend(legend, QwtPlot::RightLegend);
            setAxisTitle(yLeft,(QString("Pachpreis ")+QChar(8364)+"/ha"));
        }

        xdata=static_cast<Datawindow*>(datawin)->bodXdata;
        ydatas=static_cast<Datawindow*>(datawin)->bodYdatas;
        yaxes=static_cast<Datawindow*>(datawin)->bodYaxes;
        ytitles=static_cast<Datawindow*>(datawin)->bodYtitles;
        colors = static_cast<Datawindow*>(datawin)->bodColors;

        styles=static_cast<Datawindow*>(datawin)->bodStyles;

        break;
    case 5:
        if(!hasCurves){
            insertLegend(legend, QwtPlot::BottomLegend);//RightLegend);
            enableAxis(QwtPlot::yRight);
            setAxisTitle(yRight,(QString("Tierdichte, GVE/ha")));
            setAxisAutoScale(yRight, false);
            setAxisScale(yRight,0,static_cast<Datawindow*>(datawin)->animYrightMax);

            setAxisMaxMinor(yRight,1);
            setAxisMaxMajor(yRight,5);
            setAxisTitle(yLeft,(QString("Anzahl Tiere")));
        }

        xdata=static_cast<Datawindow*>(datawin)->animXdata;
        ydatas=static_cast<Datawindow*>(datawin)->animYdatas;
        yaxes=static_cast<Datawindow*>(datawin)->animYaxes;
        ytitles=static_cast<Datawindow*>(datawin)->animYtitles;
        colors = static_cast<Datawindow*>(datawin)->animColors;

        styles=static_cast<Datawindow*>(datawin)->animStyles;

        break;
    //*/
    case 6:
        if(!hasCurves){
            insertLegend(legend, QwtPlot::BottomLegend);//:RightLegend);
            enableAxis(QwtPlot::yRight);
            setAxisAutoScale(yRight, false);
            setAxisScale(yRight,100,0); //prozent
            setAxisTitle(yLeft,(QString("Eigenkapital, ")+QChar(0x20ac)+"/Betrieb"));
            setAxisTitle(yRight,(QString("Mein Rang(Platz/#Betriebe), ")+"%"));
        }

        xdata=static_cast<Mydatwindow*>(datawin)->ekXdata;
        ydatas=static_cast<Mydatwindow*>(datawin)->ekYdatas;
        yaxes=static_cast<Mydatwindow*>(datawin)->ekYaxes;
        ytitles=static_cast<Mydatwindow*>(datawin)->ekYtitles;
        colors = static_cast<Mydatwindow*>(datawin)->ekColors;
        styles=static_cast<Mydatwindow*>(datawin)->ekStyles;
        //*/
        break;
    case 106:
        if(!hasCurves){
            insertLegend(legend, QwtPlot::BottomLegend);//:RightLegend);
            enableAxis(QwtPlot::yRight);
            setAxisAutoScale(yRight, false);
            setAxisScale(yRight,100,0); //prozent
            setAxisTitle(yLeft,(QString("Eigenkapital, ")+QChar(0x20ac)+"/Betrieb"));
            setAxisTitle(yRight,(QString("Mein Rang(Platz/#Betriebe), ")+"%"));
        }

        xdata=static_cast<Datawindow*>(datawin)->ekXdata;
        ydatas=static_cast<Datawindow*>(datawin)->ekYdatas;
        yaxes=static_cast<Datawindow*>(datawin)->ekYaxes;
        ytitles=static_cast<Datawindow*>(datawin)->ekYtitles;
        colors = static_cast<Datawindow*>(datawin)->ekColors;
        styles=static_cast<Datawindow*>(datawin)->ekStyles;
        //*/
        break;
    default:;
    }

        //setCanvasBackground(QBrush(QColor(Qt::darkCyan)));
        nCurves=ydatas.size();
        curves.resize(nCurves);
        for (i=0;i<nCurves;++i) {
            s=ydatas[i].size();
            if (!hasCurves){
                curves[i]=new QwtPlotCurve(ytitles[i].c_str());
                curves[i]->setYAxis(yaxes[i]);
                curves[i]->setRenderHint(QwtPlotItem::RenderAntialiased);
                curves[i]->setLegendAttribute( QwtPlotCurve::LegendShowSymbol );

                //curves[i]->setStyle(QwtPlotCurve::Dots);

            }

            pen=new QPen(colors[i]);
                int x=-1;
                switch(n){
                case 0: case 1: case 2: if (i==1) x=1;break;
                case 3: if (i==4) x=4; break;
                case 5: if (i==nCurves-1) x=i; break;
                case  6: if (i==4) x=4; break;
                case  106: if (i==4) x=4; break;
                default: ;
                }

                QPen p(colors[i]);
                if (x==-1){
                    p.setStyle(Qt::DashLine);
                    p.setWidthF(2.0);
                }
                else {
                    p.setStyle(Qt::SolidLine);
                    p.setWidthF(3.0);
                    QwtText aT  = axisTitle(yRight);//
                    aT.setColor(colors[i]);
                    setAxisTitle(yRight,aT);
                    //*/
                }
                curves[i]->setPen(p);

            pen->setWidth(2);
            curves[i]->setSymbol(new QwtSymbol(styles[i],QBrush(),*pen,QSize(12,12)));
            curves[i]->setSamples(&xdata[0],&ydatas[i][0],s);

            if (!hasCurves){
                curves[i]->attach(this);
                showCurve(curves[i],true);
            }
        }

   hasCurves=true;
}

int Plot::numCurves(){
   return nCurves;
}

QwtPlotItem* Plot::getCurve(int i){
    return curves[i-1];
}

void Plot::showHideCurve(int i) {
   curves[i]->setVisible(shows[i]);
}

void Plot::makeMarkers(int n){
// marker
return;
if (n!=6||n!=106) return;//ek
int sz = ydatas[4].size();
markers.resize(sz);
for (int i=0; i<sz; ++i){
   if (markers[i]==0){
        markers[i] = new QwtPlotMarker();
        markers[i]->setYAxis(QwtPlot::yRight);
        markers[i]->setValue(xdata[i], ydatas[4][i]);//rank
        markers[i]->setLabelAlignment(Qt::AlignRight|Qt::AlignTop);
        QwtText text;
        if (n==6)
            text = QString::number(static_cast<Mydatwindow*>(datawin)->rankEKs[i])
                +"/"+QString::number(static_cast<Mydatwindow*>(datawin)->totEKs[i]);
        else
            text = QString::number(static_cast<Datawindow*>(datawin)->rankEKs[i])
                +"/"+QString::number(static_cast<Datawindow*>(datawin)->totEKs[i]);
        text.setFont(QFont("times",10));
        markers[i]->setLabel(text);
        markers[i]->attach(this);
        markers[i]->setVisible(curves[4]->isVisible());
    }
}
hasMarkers=true;
}

void Plot::makeMarkers(int n, bool b){
// marker
return;
if (n!=6||n!=106) return;//ek
if (!hasMarkers) return;
int sz = ydatas[4].size();
for (int i=0; i<sz; ++i){
    markers[i]->setVisible(b);
}
/*
markers.resize(2);
markers[0] = new QwtPlotMarker();
markers[0]->setValue(0.0, 0.0);
markers[0]->setLineStyle(QwtPlotMarker::VLine);
markers[0]->setLabelAlignment(Qt::AlignRight | Qt::AlignBottom);
markers[0]->setLinePen(QPen(Qt::green, 0, Qt::DashDotLine));
markers[0]->attach(this);
//*/
}

void Plot::updatePlot(){
   makeCurves(plotn);
   makeMarkers(plotn);
}

QwtPlotGrid * Plot::getGrid(){
    return grid;
}
