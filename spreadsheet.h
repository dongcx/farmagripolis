#ifndef SPREADSHEET_H
#define SPREADSHEET_H

#include <QTableWidget>
#include <qitemdelegate.h>
#include <vector>
#include <qpainter.h>

class Cell;
class fwCompare;
class MyDelegate;

using namespace std;

class SpreadSheet : public QTableWidget
{
    Q_OBJECT

public:
    SpreadSheet(int, int, bool,vector<QString>, bool vheader=true, bool b2=false, bool align=false, QWidget *parent = 0);

    bool autoRecalculate() const { return autoRecalc; }
    QString currentLocation() const;
    QString currentFormula() const;
    QTableWidgetSelectionRange selectedRange() const;
    void clear();
    bool readFile(const QString &fileName);
    bool writeFile(const QString &fileName);
    void sort(const fwCompare &compare);
    void setMyMaxH(int);
    void setMyMaxW(int);

    void adjSize();
    void updateColumn(int,vector<double>, int);
    void updateColumn(int,vector<double>);
    void updateColumn(int,vector<int>);
    void updateColumn(int,vector<QString>);
    void updateColumn(int,vector<string>);
    void updateColumn(vector<double>, vector<int>);
    void updateColumn(int, vector<double>, bool asInt);
    void setRow(int, vector<QString>, bool center = false);
    void setRow(int, vector<double>);
    void initTab();
    void setAligns(int,int);
    void setRowCountMy(int);
    void setColCountMy(int);
    void setCell(int,int,QString);

public slots:
    void cut();
    void copy();
    void paste();
    void del();
    void selectCurrentRow();
    void selectCurrentColumn();
    void recalculate();
    void setAutoRecalculate(bool recalc);
    void findNext(const QString &str, Qt::CaseSensitivity cs);
    void findPrevious(const QString &str, Qt::CaseSensitivity cs);

signals:
    void modified();

private slots:
    void somethingChanged();
    bool isSum(int,int);

private:
    enum { MagicNumber = 0x7F51C883};
    int RowCount;
    int ColumnCount;
    bool hHeader;
    vector<QString> hHeaders;
    bool vHeader;
    bool border2;
    bool alignC;

    Cell *cell(int row, int column) const;
    QString text(int row, int column) const;
    QString formula(int row, int column) const;
    void setFormula(int row, int column, const QString &formula);

    bool autoRecalc;
    int MyMaxH;
    int MyMaxW;
};

class MyDelegate : public QItemDelegate {
  public:
    void paint( QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index ) const {
      QItemDelegate::paint( painter, option, index );
      if( index.row()==0 || index.data(Qt::DisplayPropertyRole)==-1 ) {
         QPen pen(Qt::darkGray);
         pen.setWidthF(2.0);
         painter->setPen( pen );
         QRect rect=option.rect;//adjusted(0,-1,0,0);
         painter->drawLine(rect.topLeft(), rect.topRight());
      }
    }
};


class fwCompare
{
public:
    bool operator()(const QStringList &row1,
                    const QStringList &row2) const;

    enum { KeyCount = 3 };
    int keys[KeyCount];
    bool ascending[KeyCount];
};

#endif
