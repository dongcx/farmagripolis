#include "adjDialog.h"
#include <qlabel.h>
#include <qcombobox.h>
#include <qgridlayout.h>
#include <qpushbutton.h>
#include <qgroupbox.h>
#include "mmspinbox.h"
#include "curveIcon.h"
#include <qsignalmapper.h>
#include <qcolordialog.h>
#include "legendIcon.h"
#include <qlineedit.h>
#include "Datawindow.h"

static const QwtSymbol::Style STYLES[]={
     QwtSymbol::NoSymbol,
     QwtSymbol::Ellipse,
     QwtSymbol::Rect,
     QwtSymbol::Diamond,
     QwtSymbol::Triangle,
     QwtSymbol::DTriangle,
     QwtSymbol::UTriangle,
     QwtSymbol::LTriangle,
     QwtSymbol::RTriangle,
     QwtSymbol::Cross,
     QwtSymbol::XCross,
     QwtSymbol::HLine,
     QwtSymbol::VLine,
     QwtSymbol::Star1,
     QwtSymbol::Star2,
     QwtSymbol::Hexagon
};

static string symbolNames(QwtSymbol::Style s){
    string sn ;
    switch(s) {
    case    QwtSymbol::NoSymbol: sn="Kein Symbol";break;
    case    QwtSymbol::Ellipse: sn="Kreis" ; break;
    case    QwtSymbol::Rect:       sn="Recteck";break;
    case    QwtSymbol::Diamond:    sn="Diamond" ; break;
    case    QwtSymbol::Triangle:   sn="Dreieck";break;
    case    QwtSymbol::DTriangle:  sn="D-Dreieck" ; break;
    case    QwtSymbol::UTriangle:  sn="U-Dreieck";break;
    case    QwtSymbol::LTriangle:  sn="L-Dreieck" ; break;
    case    QwtSymbol::RTriangle:  sn="R-Dreieck";break;
    case    QwtSymbol::Cross:      sn="Kreuz" ; break;
    case    QwtSymbol::XCross:     sn="X-Kreuz";break;
    case    QwtSymbol::HLine:      sn="H-Linie" ; break;
    case    QwtSymbol::VLine:      sn="V-Linie";break;
    case    QwtSymbol::Star1:      sn="X-Stern" ; break;
    case    QwtSymbol::Star2:      sn="Stern";break;
    case    QwtSymbol::Hexagon:    sn="Hexagon" ; break;
    default: sn="Unbekannt";
    }
    return sn;
}

void adjDialog::updateColor(int i){
    QColor c= colors[i];
    QColor newC = QColorDialog::getColor(c);
    if (!newC.isValid()) return;

    vector<QwtSymbol> tsyms;
    for (int j=0; j<NSYMBOLS; ++j){
        tsyms.push_back(QwtSymbol(STYLES[j]));
    }

    if (c!=newC) {
        colors[i]=newC;
        QComboBox *cb = styleCBs[i]; ;
        for (int j=0; j<NSYMBOLS; ++j){
            cb->setItemIcon(j,curveSymbolIcon(colors[i],tsyms[j]));
        }
        int x = styleCBs[i]->currentIndex();
        QwtSymbol s= QwtSymbol(STYLES[x]);
        iconLabs[i]->setPixmap(curveIcon(colors[i], s));
    }

    emit curveFarben(i,colors[i]);
}

void adjDialog::updateIcon(int i){
    int x=styleCBs[i]->currentIndex();

    QwtSymbol s= QwtSymbol(STYLES[x]);
    iconLabs[i]->setPixmap(curveIcon(colors[i], s));

    emit curveStyle(i, s);
}

void adjDialog::updatePlot(QString str){
    QColor c, cnew;
    if (str==QString("GridColor")) {
        c= gridColor;
        cnew = QColorDialog::getColor(c);
        if (!cnew.isValid()) return;
        if (c!=cnew) {
            gridColor= cnew;
            plotIconLabs[0]->setPixmap(gridIcon(gridColor));
        }
    }
    else if(str==QString("CanvasColor")) {
        c= canvasColor;
        cnew= QColorDialog::getColor(c);
        if (!cnew.isValid()) return;
        if (c!=cnew) {
            canvasColor=cnew;
            plotIconLabs[1]->setPixmap(normalIcon(canvasColor));
        }
    }

    emit plotFarben(gridColor,canvasColor);
}

void adjDialog::updateFT(int i){
    QColor c, cnew;
    int ncurves = names.size();
    c= colors[i];
    cnew = QColorDialog::getColor(c);
    if (!cnew.isValid()) return;
    if (c!=cnew) {
            colors[i]= cnew;
            ftIconLabs[ncurves-1-i]->setPixmap(normalIcon(colors[i]));
        }

    emit curveFarben(i, colors[i]);
}


adjDialog::adjDialog(vector<string> ns, vector<QColor> cs, vector<QwtSymbol> ss, QColor gc, QColor cvs, int max, int min, int t, QWidget *parent):
        names(ns),colors(cs),symbols(ss),gridColor(gc),canvasColor(cvs),maxX(max),minX(min),type(t),QDialog(parent){

    QSignalMapper *sigmapperColor= new QSignalMapper(this);
    int ncurves = names.size();
    QGridLayout *gout=new QGridLayout;
    QGroupBox *gb1;

 if (type!=3) {
    QSignalMapper *sigmapper= new QSignalMapper(this);

    vector<QwtSymbol> tsyms;
    for (int j=0; j<NSYMBOLS; ++j){
        tsyms.push_back(QwtSymbol(STYLES[j]));
    }

    syms.push_back(ss);

    for (int i= 0; i<ncurves; ++i){
        QLabel *l1=new QLabel();
        l1->setPixmap(curveIcon(colors[i], symbols[i]));
        l1->setAlignment(Qt::AlignCenter);
        iconLabs.push_back(l1);
        QLabel *l2=new QLabel(names[i].c_str());
        QPushButton *pb= new QPushButton("Farbe �ndern");
        QComboBox *cb=new QComboBox();

        int x=0;

        for (int j=0; j<NSYMBOLS; ++j){
            if(STYLES[j]==symbols[i].style()) x=j;
            cb->addItem(curveSymbolIcon(colors[i],tsyms[j]), symbolNames(STYLES[j]).c_str() );
        }

        cb->setCurrentIndex(x);
        styleCBs.push_back(cb);

        colorPBs.push_back(pb);

        sigmapperColor->setMapping(pb, i);
        connect(pb,SIGNAL(clicked()), sigmapperColor, SLOT(map()));
        sigmapper->setMapping(cb, i);
        connect(cb,SIGNAL(currentIndexChanged(int)), sigmapper, SLOT(map()));

        gout->addWidget(l1,i,0);
        gout->addWidget(l2,i,1);
        gout->addWidget(pb,i,2);
        gout->addWidget(cb,i,3);

    }
    connect(sigmapper,SIGNAL(mapped(int)),this, SLOT(updateIcon(int)));
    connect(sigmapperColor,SIGNAL(mapped(int)),this, SLOT(updateColor(int)));

    gb1 = new QGroupBox("Kurvenfarbe und -symbol");
    gb1->setLayout(gout);

}else { //Farmtype
    for (int i=ncurves-1; i>=0;--i){
        QLabel *la=new QLabel();
        la->setPixmap(normalIcon(colors[i]));
        ftIconLabs.push_back(la);
        QLabel *lb=new QLabel(names[i].c_str());
        ftLabs.push_back(lb);

        QPushButton *p= new QPushButton("Farbe �ndern");
        sigmapperColor->setMapping(p,i);

        connect(p,SIGNAL(clicked()),sigmapperColor,SLOT(map()));

        gout->addWidget(la,ncurves-i-1,0);
        gout->addWidget(lb,ncurves-i-1,1);
        gout->addWidget(p,ncurves-i-1,2);
    }

    connect(sigmapperColor,SIGNAL(mapped(int)), this, SLOT(updateFT(int)));

    QHBoxLayout *h2 = new QHBoxLayout;
    h2->addStretch();
    h2->addLayout(gout);
    h2->addStretch();

    gb1 = new QGroupBox("Balkenfarben");
    gb1->setLayout(h2);
}


    QGroupBox *gb2 = new QGroupBox("Ploteigenschaften");
    QGridLayout *g2out = new QGridLayout;

    QSignalMapper *sigmapperPlot = new QSignalMapper(this);
    for (int i=0; i<2;++i){
        QLabel *la=new QLabel();
        if (i==0)
            la->setPixmap(gridIcon(gridColor));
        else
            la->setPixmap(normalIcon(canvasColor));

        plotIconLabs.push_back(la);
        QLabel *lb;
        if (i==0) lb=new QLabel("Gitterliniefarbe");
        else lb = new QLabel("Hintergrundfarbe");
        plotLabs.push_back(lb);

        QPushButton *p= new QPushButton("Farbe �ndern");
        if (i==0) sigmapperPlot->setMapping(p,QString("GridColor"));
        else sigmapperPlot->setMapping(p,QString("CanvasColor"));

        connect(p,SIGNAL(clicked()),sigmapperPlot,SLOT(map()));

        g2out->addWidget(la,i,0);
        g2out->addWidget(lb,i,1);
        g2out->addWidget(p,i,2);
    }

    connect(sigmapperPlot,SIGNAL(mapped(QString)), this, SLOT(updatePlot(QString)));

    QHBoxLayout *h2 = new QHBoxLayout;
    h2->addStretch();
    h2->addLayout(g2out);
    h2->addStretch();

    gb2->setLayout(h2);

    //periodenbereich

    QGroupBox *gb3 = new QGroupBox("Periodenbereich");
    QLabel *lmin = new QLabel("Min");
    QLabel *lmax = new QLabel ("Max");

    minSpinBox *smin= new minSpinBox;
    maxSpinBox *smax = new maxSpinBox;

    smin->setMaximum(maxX);
    smin->setValue(minX);
    smax->setMaximum(maxX);
    smax->setValue(maxX);

    connect(smin, SIGNAL(valueChanged(int)), smax, SLOT(updateValue(int)));
    connect(smax, SIGNAL(valueChanged(int)), smin, SLOT(updateValue(int)));

    connect(smin, SIGNAL(valueChanged(int)), this, SLOT(xMinChanged(int)));
    connect(smax, SIGNAL(valueChanged(int)), this, SLOT(xMaxChanged(int)));

    QGridLayout *g3out  =new QGridLayout;
    g3out->addWidget(lmin,0,0);
    g3out->addWidget(smin,0,1);
    g3out->addWidget(lmax,0,4);
    g3out->addWidget(smax,0,5);

    QHBoxLayout *h3 = new QHBoxLayout;
    h3->addStretch();
    h3->addWidget(lmin);
    h3->addWidget(smin);
    h3->addSpacing(50);
    h3->addWidget(lmax);
    h3->addWidget(smax);
    h3->addStretch();

    gb3->setLayout(h3);

    //tierdichte: y-Axis
    QGroupBox *gb4;
 if(type==5) {
    gb4 = new QGroupBox("Dichtebereich");
    QLabel *lmin4 = new QLabel("Untergrenze ");
    QLabel *lmax4 = new QLabel ("Obergrenze ");

    minDichteEdit= new mmLineEdit(QString::number(static_cast<Datawindow*>(parent)->animYrightMin));
    maxDichteEdit= new mmLineEdit(QString::number(static_cast<Datawindow*>(parent)->animYrightMax));

    minDichteEdit->setFixedWidth(30);
    maxDichteEdit->setFixedWidth(30);

    connect(minDichteEdit,SIGNAL(textChanged(QString)), this, SLOT(dichteXMinChanged(QString)));
    connect(maxDichteEdit,SIGNAL(textChanged(QString)), this,SLOT(dichteXMaxChanged(QString)));

    connect(maxDichteEdit,SIGNAL(textChanged(QString)), minDichteEdit,SLOT(updateMin(QString)));
    connect(minDichteEdit,SIGNAL(textChanged(QString)), maxDichteEdit,SLOT(updateMax(QString)));

    QHBoxLayout *h4 = new QHBoxLayout;
    h4->addStretch(2);
    h4->addWidget(lmin4);
    h4->addWidget(minDichteEdit);
    h4->addSpacing(20);
    h4->addWidget(lmax4);
    h4->addWidget(maxDichteEdit);
    h4->addStretch(2);

    gb4->setLayout(h4);
}

    QVBoxLayout *vout = new QVBoxLayout;
    vout->addWidget(gb1);
    vout->addWidget(gb2);
    vout->addWidget(gb3);
    if (type==5) vout->addWidget(gb4);

    QPushButton *pbOK = new QPushButton("OK");
    QHBoxLayout *hout= new QHBoxLayout;
    hout->addStretch();
    hout->addWidget(pbOK);

    connect(pbOK, SIGNAL(clicked()), this, SLOT(close()));

    hout->addStretch();

    vout->addLayout(hout);

    setLayout(vout);

    resize(400,500);
}

void adjDialog::dichteXMinChanged(QString s ){
    emit(minDichte(s.toDouble()));
}

void adjDialog::dichteXMaxChanged(QString s ){
    emit(maxDichte(s.toDouble()));
}

void adjDialog::emitPlotFarben(QString str){
    QColor gridC, canvasC;
    gridC=gridColor;
    canvasC=canvasColor;
    emit plotFarben(gridC,canvasC);
}

void adjDialog::emitCurveFarben(int i){
    QColor c = colors[i];
    emit curveFarben(i,c);
}

void adjDialog::xMinChanged(int min){
    emit minValue(min);
}

void adjDialog::xMaxChanged(int max){
    emit maxValue(max);
}


//===============mmLineEdit
QSize mmLineEdit::sizeHint(){
    return QSize(10,10);
}

void mmLineEdit::updateMin(QString s){
    double d=this->text().toDouble();
    double f=s.toDouble();

    if (f<d) this->setText(s);
}

void mmLineEdit::updateMax(QString s){
    double d=this->text().toDouble();
    double f=s.toDouble();

    if (f>d) this->setText(s);
}
