#include "legendIcon.h"
#include <QPainter>
#include <QPixmapCache>

QPixmap normalIcon(const QColor &color,
                   const QSize &size){
    QString key = QString("NORMALICON:%1:%2x%3")
        .arg(color.name())
        .arg(size.width()).arg(size.height());
        //*/
    QPixmap pixmap(size);
    if (!QPixmapCache::find(key, &pixmap)) {
        pixmap.fill(Qt::transparent);
        QPainter painter(&pixmap);
        painter.setRenderHint(QPainter::Antialiasing);
        painter.setPen(Qt::NoPen);
        painter.setBrush(QBrush(color));
        painter.drawRect(0, 0, size.width(), size.height());
        painter.end();
        QPixmapCache::insert(key, pixmap);
    }
    return pixmap;
}

QPixmap freeIcon(const QColor &color,
                 const QSize &size){
    QString key = QString("FREEICON:%1:%2x%3")
        .arg(color.name())
        .arg(size.width()).arg(size.height());
    QPixmap pixmap(size);
    if (!QPixmapCache::find(key, &pixmap)) {
        pixmap.fill(Qt::transparent);
        QPainter painter(&pixmap);
        painter.setRenderHint(QPainter::Antialiasing);
        QPen pen;
        pen.setColor(color);
        pen.setWidth(4);
        painter.setPen(pen);
        painter.setBrush(Qt::NoBrush);//QBrush(color));
        painter.drawRect(1, 1, size.width()-1, size.height()-1);
        painter.end();
        QPixmapCache::insert(key, pixmap);
    }
    return pixmap;
}

QPixmap farmSitzIcon(const QColor &color, const QSize &size){
    QString key = QString("FARMSITZICON:%1:%2x%3")
        .arg(color.name())
        .arg(size.width()).arg(size.height());
    QPixmap pixmap(size);
    if (!QPixmapCache::find(key, &pixmap)) {
        pixmap.fill(Qt::transparent);
        QPainter painter(&pixmap);
        painter.setRenderHint(QPainter::Antialiasing);
        QPen pen;
        pen.setColor(Qt::black);
        pen.setWidth(2);
        painter.setPen(pen);
        //painter.fillRect(QRect(0,0,size.width(),size.height()),color);
        //*/
        painter.setBrush(Qt::NoBrush);
        painter.drawLine(1,1, size.width()-1,size.height()-1);
        painter.drawLine(size.width()-1,1,1,size.height()-1);

        painter.end();
        QPixmapCache::insert(key, pixmap);
    }
    return pixmap;
}

QPixmap mySitzIcon(const QColor &color,
               const QSize &size){

    QString key = QString("MYSITZICON:%1:%2x%3")
        .arg(color.name())
        .arg(size.width()).arg(size.height());
    QPixmap pixmap(size);
    if (!QPixmapCache::find(key, &pixmap)) {
        pixmap.fill(Qt::transparent);
        QPainter painter(&pixmap);
        painter.setRenderHint(QPainter::Antialiasing);
        QPen pen;
        pen.setColor(Qt::black);
        pen.setWidth(2);
        painter.setPen(pen);
        //painter.setBrush(QBrush(color));

        painter.fillRect(QRect(0,0,size.width(),size.height()),color);
        painter.setBrush(Qt::NoBrush);
        painter.drawLine(1,1, size.width()-1,size.height()-1);
        painter.drawLine(size.width()-1,1,1,size.height()-1);

        pen.setColor(QColor(255-qRed(color.rgb()), 255-qGreen(color.rgb()),
                      255-qBlue(color.rgb())));
        pen.setWidth(5);
        painter.setPen(pen);
        painter.drawEllipse(size.width()/4,size.height()/4,
                            size.width()/2-1, size.height()/2-1);
        painter.end();
        QPixmapCache::insert(key, pixmap);
    }
    return pixmap;
}

QPixmap ownlandIcon(const QColor &color,
               const QSize &size){

    QString key = QString("OWNLANDICON:%1:%2x%3")
        .arg(color.name())
        .arg(size.width()).arg(size.height());
    QPixmap pixmap(size);
    if (!QPixmapCache::find(key, &pixmap)) {
        pixmap.fill(Qt::transparent);
        QPainter painter(&pixmap);
        painter.setRenderHint(QPainter::Antialiasing);

        QPen pen;
        pen.setColor(Qt::black);//QColor(255-qRed(color.rgb()), 255-qGreen(color.rgb()),
                      //255-qBlue(color.rgb())));
        pen.setWidth(2);
        painter.setPen(pen);
        painter.drawEllipse(QRect(2,2,size.width()-3,size.height()-3));
        painter.end();
        QPixmapCache::insert(key, pixmap);
    }
    return pixmap;
}

QPixmap gridIcon(const QColor &color,
               const QSize &size){

    QString key = QString("GRIDICON:%1:%2x%3")
        .arg(color.name())
        .arg(size.width()).arg(size.height());
    QPixmap pixmap(size);
    if (!QPixmapCache::find(key, &pixmap)) {
        pixmap.fill(Qt::transparent);
        QPainter painter(&pixmap);
        painter.setRenderHint(QPainter::Antialiasing);

        QPen pen;
        pen.setColor(color);//QColor(255-qRed(color.rgb()), 255-qGreen(color.rgb()),
                      //255-qBlue(color.rgb())));
        pen.setWidth(2);
        painter.setPen(pen);
        int w= size.width();
        int h= size.height();
        painter.drawLine(QPoint(w/3,0),QPoint(w/3,h));
        painter.drawLine(QPoint(2*w/3,0),QPoint(2*w/3,h));
        painter.drawLine(QPoint(0,h/3),QPoint(w,h/3));
        painter.drawLine(QPoint(0,2*h/3),QPoint(w,2*h/3));

        painter.end();
        QPixmapCache::insert(key, pixmap);
    }
    return pixmap;
}
