#ifndef BodenDialog1_H
#define BodenDialog1_H

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QDoubleValidator>
#include <QTimer>
#include <QFile>


#include "RegPlot.h"

class RegFarmInfo;
class SpreadSheet2;
class Title22;
class QComboBox;
class QSpinBox;
class QDoubleSpinBox;
class Mainwindow;
class PEwidget;

class BodenDialog1 : public QDialog {
Q_OBJECT
public:
    BodenDialog1(Mainwindow* parent=0);

    void updateFreePlots();
    void updateOffers();

    void closeEvent(QCloseEvent*);
    void enableBSbutton(bool);
    bool isInited();
    void setShowBSpiegel(bool);
    vector<double> getManPEs() const;
    vector<double> getEffPEs() ;

public slots:
    void showBoden(int, int);
    void showBoden(int);

    void updateGUI();
    void updateGUI2();
    void updateGUIz();

    void getBids();
    void getVorschlag();
    void getBids0();
    bool validBids();
    bool validSoilType(int);
    void validSoilType0(int);
    void validBidDSB0(int);
    void validBidDSB(int);

    void showHilfe();
    void hilfeClosed();
    void weiter();
    void weiter1();
    void neueGebote();

    void makePeWidget();
    void makeAreaWidget();

    void makePeWidget2();
    void makeAreaWidget2();

    void blendenPE();
    void updateSoilType(int);

    void updateArea(int);
    void updateAreas();
    void updateArea2(int);
    void updateArea2B(int);
    void getAreaOK(int,int);
    void getAreaOK2(int);
    void getAreaOK2B(int,int);
    void getPeOK(int,bool);
    void getPeVarOK(int, bool);

    void getPeSig(int);
    void getPeVarSig(int);
    void updateGDBs(int);
    void updateGDBs();

    void savePEs();

    void updateFreeArea(int);
    void updateErg();

    void blendenBSpiegel();
    void getMydatwindowHi(bool);

    void updateVorschlags();
    void updateVorschlags(int);
    void abspeichern();
    void updatePEs();
    void getLAsig(int,int);

signals:
    void ok(int);
    void fertig();
    void firstBids();

    void areaSig(int,int);
    void areaSig2(int);
    void areaSig2B(int,int);
    void peSig(int);
    void peVarSig(int);
    void peOK(int,bool);

    void LAsig(int,int);

private:
    void setPEs();
    bool hasPEwidget;
    void makeEditable(SpreadSheet2 *);

    vector<vector<pair<int,double> > > offers_of_type;
    vector<vector<double> > vorschlags_of_type;
    vector<SpreadSheet2*> gebotstables;
    vector<QPushButton*> vButtons;

    int prozent;
    vector<SpreadSheet2*> bidSheets;
    int nsoils;
    vector<string> soilNames;
    RegFarmInfo *farm0;
    vector<int> maxRows;
    vector<double> landinputs;
    vector<QLabel*> labLandinputs;

    int nRowBid, nColBid;

    Title22 *prozentLab;
    Title22 *prozentLab1;
    Title22 *ergLab;
    QString prozentStr;
    QWidget *bidsWidget, *weiterWidget;
    QPushButton *neuBidsButton, *weiterButton;
    QPushButton *weiterZbutton;
    QWidget *weiterWidget1;

    QPushButton *hilfeButton;
    QDialog *dialBodenHilfe;

    QWidget *areaWidget;
    PEwidget *peWidget;
    QComboBox *soiltypeCBox;
    int soiltype;
    int refzeit;
    int ITER;  //max refzeit
    int peVar;
    QString refname;
    int refprodID;
    int refpos;
    double preisIndex;

    QPushButton *peButton;
    bool showPEwidget;

    int nRowArea, nColArea;
    SpreadSheet2 *areaSS;

    vector<vector<QSpinBox*> > areaDSBs; //nach bodentyp

    QColor cellBColor;
    vector<QString> prodNames;
    vector<int> prodIDs;
    vector<double> prodPrices;
    vector<double> prodCosts;
    vector<double> prodRefPEs;
    vector<double> prodRefPrices;
    vector<double> prodPEs;
    int numPEs;

    vector<int> getreideIDs;
    vector<double> getreidePrices, getreidePEs;

    vector<double> adaptPEs, naivPEs, manPEs;
    vector<vector<double> > allPrices;  //PEs for all iterations

    vector<vector<int> > varAreas;  //nach soiltype
    bool inited;
    double oldvalue;

    bool firstGUI;
    double proz0, proz;

    //it, proz
    vector<map<double, vector<double> > > all_myPEs;

    map<double, vector<double> > it_myPEs;

    //it, proz, soil, bids
    vector<map<int, vector<vector<pair<int, double> > > > > all_bids;

    //proz, soil, bids
    map<int, vector<vector<pair<int, double> > > > it_bids;

    vector<vector<QSpinBox*> > areaDSBs_of_type;
    vector<vector<QDoubleSpinBox*> > bidDSBs_of_type;
    bool BidIsValid;

    Mainwindow *parent;
    QSpinBox *radSBox;
    vector<int> freeAreas, radFreeAreas;

    SpreadSheet2 *freeAreaSS;
    SpreadSheet2 *ergSS;
    QWidget *ergWidget;
    vector<int> ergAreas;

    vector<QString> prodUnits;
    vector<QString> prodGMunits;
    vector<double> prodErtrags, prodNertrags;

    bool showBSpiegel;
    QPushButton *spiegelButton;
    bool toClose;
};

class Title22 : public QLabel{
    Q_OBJECT
public: explicit Title22(Qt::AlignmentFlag align, QFont font, QWidget* w=0);

};

#endif // BodenDialog1_H
