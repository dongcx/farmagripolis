#include "curveIcon.h"
#include <QPainter>
#include <QPixmapCache>

/*
QPixmap canvasIcon(const QColor &color,
                 const QSize &size=DefaultSize);
QPixmap gridIcon(const QColor &color,
                 const QSize &size=DefaultSize);
QPixmap curveIcon(const QColor &color, QwtSymbol &symbol,
                 const QSize &size=DefaultSize);
//*/

QPixmap canvasIcon(const QColor &color,
                   const QSize &size){
    QString key = QString("CANVASICON:%1:%2x%3")
        .arg(color.name())
        .arg(size.width()).arg(size.height());
    QPixmap pixmap(size);
    if (!QPixmapCache::find(key, &pixmap)) {
        pixmap.fill(Qt::transparent);
        QPainter painter(&pixmap);
        painter.setRenderHint(QPainter::Antialiasing);
        painter.setPen(Qt::NoPen);
        painter.setBrush(QBrush(color));
        painter.drawRect(0, 0, size.width(), size.height());
        painter.end();
        QPixmapCache::insert(key, pixmap);
    }
    return pixmap;
}

QPixmap gridlineIcon(const QColor &color,
                 const QSize &size){
    QString key = QString("GRIDICON:%1:%2x%3")
        .arg(color.name())
        .arg(size.width()).arg(size.height());
    QPixmap pixmap(size);
    if (!QPixmapCache::find(key, &pixmap)) {
        pixmap.fill(Qt::transparent);
        QPainter painter(&pixmap);
        painter.setRenderHint(QPainter::Antialiasing);
        QPen pen;
        pen.setColor(color);
        pen.setWidth(1);
        pen.setStyle(Qt::DotLine);
        painter.setPen(pen);
        painter.setBrush(Qt::NoBrush);//QBrush(color));
        painter.drawLine(0,size.height()/2,size.width(), size.height()/2);
        //painter.drawRect(1, 1, size.width()-1, size.height()-1);
        painter.end();
        QPixmapCache::insert(key, pixmap);
    }
    return pixmap;
}

QPixmap curveIcon(const QColor &color, QwtSymbol &symbol,
                 const QSize &size){
    /*QString key = QString("CURVEICON:%1:%2x%3")
        .arg(color.name())
        .arg(size.width()).arg(size.height());
    //*/
    QPixmap pixmap(size);
    //if (!QPixmapCache::find(key, &pixmap)) {
        pixmap.fill(Qt::transparent);
        QPainter painter(&pixmap);
        painter.setRenderHint(QPainter::Antialiasing);
        QPen pen;
        pen.setColor(color);
        pen.setWidth(2);
        painter.setPen(pen);
        //painter.fillRect(QRect(0,0,size.width(),size.height()),color);
        //*/
        painter.setBrush(Qt::NoBrush);
        int y= size.height()/2;
        painter.drawLine(0,y, size.width(),y);//size.height()/2, size.width(), size.height()/2);       painter.drawLine(size.width()-1,1,1,size.height()-1);

        QPointF *point= new QPointF(size.width()/2., size.height()/2.);
        symbol.setSize(size.width()/3);//size);
        symbol.setBrush(Qt::NoBrush);
        symbol.setPen(QPen(color));
        symbol.drawSymbol(&painter, *point);
        painter.end();
      //  QPixmapCache::insert(key, pixmap);
    //}
    return pixmap;
}

QPixmap curveSymbolIcon(const QColor &color, QwtSymbol &symbol,
                 const QSize &size){
    /*QString key = QString("CURVESYMICON:%1:%2x%3")
        .arg(color.name())
        .arg(size.width()).arg(size.height());
      //*/
    QPixmap pixmap(size);
    //if (!QPixmapCache::find(key, &pixmap)) {
        pixmap.fill(Qt::transparent);
        QPainter painter(&pixmap);
        painter.setRenderHint(QPainter::Antialiasing);

        QPointF *point= new QPointF(size.width()/2., size.height()/2.);
        symbol.setSize(size.width()*3/4);
        //symbol.setColor(color);
        symbol.setBrush(Qt::NoBrush);
        symbol.setPen(QPen(color));
        symbol.drawSymbol(&painter, *point);
        painter.end();
      //  QPixmapCache::insert(key, pixmap);
    //}
    return pixmap;
}
