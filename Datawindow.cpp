#include "Datawindow.h"
#include <QDockWidget>
#include <QAction>
#include <QToolBar>
#include <QGraphicsView>
#include <QGraphicsScene>
#include "RegMessages.h"
#include "RegPlot.h"
#include "Mainwindow.h"

#include <QVBoxLayout>
#include <QLineEdit>
#include <QPicture>
#include <QCoreApplication>

#include <QColorDialog>
#include "legendIcon.h"

#include <QPushButton>
#include <QSignalMapper>
#include <QGroupBox>
#include <QDialogButtonBox>

#include <QSettings>
#include <QPrintDialog>
#include <QList>
#include <QDir>

#include <QStatusBar>
#include <qwt_plot_zoomer.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_picker.h>

#include <qcheckbox.h>
#include <qradiobutton.h>

#include <qspinbox.h>
#include "mmspinbox.h"


class Zoomer: public QwtPlotZoomer
{
public:
    Zoomer(int xAxis, int yAxis, QwtPlotCanvas *canvas):
        QwtPlotZoomer(xAxis, yAxis, canvas)
    {
        setTrackerMode(QwtPicker::AlwaysOff);
        setRubberBand(QwtPicker::NoRubberBand);

        // RightButton: zoom out by 1
        // Ctrl+RightButton: zoom out to full size

        setMousePattern(QwtEventPattern::MouseSelect2,
            Qt::RightButton, Qt::ControlModifier);
        setMousePattern(QwtEventPattern::MouseSelect3,
            Qt::RightButton);
    }
};

void Datawindow::initXs(){
    maxIter=gg->RUNS;
    minIter=-1;
    for (int i=0; i<numPlot; ++i){
        maxXs.push_back(maxIter);
        minXs.push_back(minIter);
    }

}

void Datawindow::initHasDialogs(){
    for (int i=0; i<numPlot; ++i){
       hasDialogs.push_back(false);;
    }
}

Datawindow::Datawindow(Mainwindow *parent, QWidget* p):gridGroup(0),hasPlots(false), par(parent),
        QMainWindow(p) {
    setWindowTitle(tr("Daten Region"));

    printer = new QPrinter(QPrinter::HighResolution);
    //printer->setOutputFileName("tregd.pdf");

    createActions();
    createToolBars();
    //createStatusBar();
    nLegaltypes=4;
    nFarmtypes=4;

    minIter=-1;
    maxIter=25;

    animYrightMax=0.5;
    initHasDialogs();
    initPlotColors();

    dialogs.resize(numPlot);
    cWidget=new QWidget(this);
    clout = new QVBoxLayout(cWidget);

    oldlayer= picType = layerComboBox->currentIndex();
    inited=false;

    cWidget->setLayout(clout);
    setCentralWidget(cWidget);

    //withBorder=plots[picType]->withGrid;
    resize(800,500);

    nSoils=0;
    maxFarmsize=10;
    setWindowIcon(QPixmap(":/images/statistics.png"));
}

void Datawindow::initPlotColors(){
    gridColors.resize(numPlot);
    canvasColors.resize(numPlot);

    QSettings dataSettings("IAMO, BSE", "Farm AgriPoliS");

    for (int i=0; i<numPlot;++i){
        gridColors[i]=dataSettings.value(QString("gridColor_")+i, Qt::gray).value<QColor>();
        canvasColors[i]=dataSettings.value(QString("canvasColor_")+i, Qt::darkCyan).value<QColor>();
    }
}

void Datawindow::initStyles(){
    int k= QwtSymbol::NoSymbol;
    k++;

    vector<QwtSymbol::Style> vs;
    for (int i=0;i<MAXCURVES;++i){
        vs.push_back(static_cast<QwtSymbol::Style>(k++));
    }

    QSettings dataSettings("IAMO, BSE", "Farm AgriPoliS");

    incStyles.resize(nFarmtypes+2);
    gewStyles.resize(nFarmtypes+2);
    sizStyles.resize(nFarmtypes+2);
    bodStyles.resize(2*nSoils);
    animStyles.resize(nAnimals+1);
    ekStyles.resize(5);
    for (int i=0;i<MAXCURVES;++i){
        if (i<5)
            ekStyles[i]=static_cast<QwtSymbol::Style>(dataSettings.value(QString("ekRegStyle_")+i, i+1).value<int>());
        if (i<nFarmtypes+2){
            incStyles[i]=static_cast<QwtSymbol::Style>(dataSettings.value(QString("incStyle_")+i, i+1).value<int>());
            gewStyles[i]=static_cast<QwtSymbol::Style>(dataSettings.value(QString("gewStyle_")+i, i+1).value<int>());
            sizStyles[i]=static_cast<QwtSymbol::Style>(dataSettings.value(QString("sizStyle_")+i, i+1).value<int>());
        }
        if (i<2*nSoils)
            bodStyles[i]=static_cast<QwtSymbol::Style>(dataSettings.value(QString("bodStyle_")+i, i+1).value<int>());
        if (i<nAnimals+1)
            animStyles[i]=static_cast<QwtSymbol::Style>(dataSettings.value(QString("animStyle_")+i, i+1).value<int>());
    }
}

void Datawindow::initCurveColors(int i){
    vector<QColor> colors;
    for (int k=0;k<MAXCURVES;++k){
        colors.push_back(QColor((k%2)*255, ((k>>1)%2)*255, ((k>>2)%2)*255));
    }
    QSettings dataSettings("IAMO, BSE", "Farm AgriPoliS");
    int t,sz;
    switch(i){
    case 0:
        sz=nFarmtypes+2;
        incColors.resize(sz);
        for(t=0;t<sz;++t){
            incColors[t]=dataSettings.value(QString("incColor_")+t,colors[t]).value<QColor>();
        }
        break;
    case 1:
        sz=nFarmtypes+2;
        gewColors.resize(sz);
        for(t=0;t<sz;++t){
            gewColors[t]=dataSettings.value(QString("gewColor_")+t,colors[t]).value<QColor>();
        }
        break;
    case 2:
        sz=nFarmtypes+2;
        sizColors.resize(sz);
        for(t=0;t<sz;++t){
            sizColors[t]=dataSettings.value(QString("sizColor_")+t,colors[t]).value<QColor>();
        }
        break;
    case 3:
        farmtypeColors.resize(nFarmtypes);
        for(t=0;t<nFarmtypes;++t){
            farmtypeColors[t]=dataSettings.value(QString("farmtypeColor_")+t,colors[t]).value<QColor>();
        }
        break;
    case 4:
        bodColors.resize(2*nSoils);
        for(t=0;t<2*nSoils;++t){
            bodColors[t]=dataSettings.value(QString("bodColor_")+t,colors[t]).value<QColor>();
        }
        break;
    case 5:
        sz=nAnimals+1;
        animColors.resize(sz);
        for(t=0;t<sz;++t){
            animColors[t]=dataSettings.value(QString("animColor_")+t,colors[t]).value<QColor>();
        }
        break;
    case 6:
        sz=5;
        ekColors.resize(sz);//+ranking
        for(t=0;t<sz;++t){
               ekColors[t]=dataSettings.value(QString("ekRegColor_")+t,colors[t]).value<QColor>();
        }
        break;
    default:;
    }

}

/*
struct DataReg {
    int farmid;
    int rechtsform;
    int farmtype;
    double esusize;
    double einkommen;
    double totalarea;
    double landused;
    double lu;
    vector <double> areasoils;
    vector <double> pricesoils;
    vector <double> animals;
    double prof
};
//*/
bool sizeCompare(DataReg x1, DataReg x2){
    return x1.totalarea<x2.totalarea;
}

void Datawindow::initRegdata(){
    iter=0;

   farmsizeMin=0;
   farmsizeMax=1000;

   classnames[1]="Veredlung";//Pig/Poultry";
   classnames[2]="Futterbau";//Greenland";
   classnames[3]="Marktfruchtbau";//Arable";
   classnames[4]="Gemischt";//Mixed";
   classnames[5]="Geschloßen";//Mixed";

   //einkommen
   incYtitles.push_back("Ausgewählte Betriebe");
   incYtitles.push_back("Mein Betrieb");

   for (int i=1;i<=nFarmtypes;++i){
        farmtypeNames.push_back(classnames[i]);
        incYtitles.push_back(classnames[i]);
    }

   incYaxes.push_back(QwtPlot::yLeft);
   incYaxes.push_back(QwtPlot::yLeft);

   for (int i=1;i<=nFarmtypes;++i){
        incYaxes.push_back(QwtPlot::yLeft);
    }

    ausserBereich=false;
    legalforms.resize(nLegaltypes);  //Wieviele ??????????
    for (int i=0; i<nLegaltypes; ++i){
        legalforms[i]=true;
    }


    //gewinn
   gewYtitles.push_back("Ausgewählte Betriebe");
   gewYtitles.push_back("Mein Betrieb");

   for (int i=1;i<=nFarmtypes;++i){
        gewYtitles.push_back(classnames[i]);
    }

   gewYaxes.push_back(QwtPlot::yLeft);
   gewYaxes.push_back(QwtPlot::yLeft);

   for (int i=1;i<=nFarmtypes;++i){
        gewYaxes.push_back(QwtPlot::yLeft);
    }

    ausserBereichGew=false;
    legalformsGew.resize(nLegaltypes);  //Wieviele ??????????
    for (int i=0; i<nLegaltypes; ++i){
        legalformsGew[i]=true;
    }


    //==Farmsize=====
   sizYtitles.push_back("Alle Betriebe");
   sizYtitles.push_back("Mein Betrieb");

   for (int i=1;i<=nFarmtypes;++i){
        sizYtitles.push_back(classnames[i]);
    }

   sizYaxes.push_back(QwtPlot::yLeft);
   sizYaxes.push_back(QwtPlot::yLeft);

   for (int i=1;i<=nFarmtypes;++i){
        sizYaxes.push_back(QwtPlot::yLeft);
    }


    //====Pachtpreis=================
    nSoils= gg->NO_OF_SOIL_TYPES;
    namessoils=gg->NAMES_OF_SOIL_TYPES;

    for (int i=0; i<nSoils; ++i){
       bodYtitles.push_back(namessoils[i]);
       bodYtitles.push_back(namessoils[i]+"-Mein Betrieb");
    }

    for (int i=0; i<2*nSoils; ++i){
        bodYaxes.push_back(QwtPlot::yLeft);
    }

    //Tiere
    int nprods= Manager->Market->getNumProducts();
    list<RegFarmInfo*>::iterator it = Manager->FarmList.begin();
        for (int i=0; i<nprods; ++i){
            if ((*it)->getLU(i)!=0)
                animYtitles.push_back(Manager->Market->getNameOfProduct(i));
        }
        animYtitles.push_back("Tierdichte(GVE/ha)");

    nAnimals=animYtitles.size()-1;
    int i;
    for (i=0; i<nAnimals; ++i){
        animYaxes.push_back(QwtPlot::yLeft);
    }
    animYaxes.push_back(QwtPlot::yRight);

    //ek
    ekYtitles=par->mydatWindow->ekYtitles;
    ekYaxes = par->mydatWindow->ekYaxes;
    initPePrices(); //prices

    for (int i=0;i<numPlot;++i)
         initCurveColors(i);
    initStyles();
    initXs();
    inited=true;
}

void Datawindow::initPePrices(){
    int xcount = 0;
    int refpos;
    vector<RegProductInfo> pcat = Manager->Market->getProductCat();
    int sz = pcat.size();
    bool refnameFound = false;
    for (int i=0; i<sz; ++i){
        //cout<<pcat[i].getName().c_str()<<"\t"<<pcat[i].getProductGroup()<<"\t"<<pcat[i].getPreisSchwankung()<<endl;
        if (!pcat[i].getPreisSchwankung()) continue;
        //if (pcat[i].getProductGroup()<0) continue;
        if (pcat[i].getProductGroup()== 0  && !refnameFound) {
            //refname = QString(pcat[i].getName().c_str());
            refpos = xcount;
            refnameFound = true;
        }else if (pcat[i].getProductGroup()==0){
            continue;
        }else if (pcat[i].getProductGroup()==-1 && pcat[i].getClass().compare("GRASSLAND")!=0) // nur f. Altmark ??
             continue;

        ++xcount;
        prodIDs.push_back(i);
        prodUnits.push_back(QString(pcat[i].getUnit().c_str()));
        prodNames.push_back(QString(pcat[i].getName().c_str()));
        //prodPrices.push_back(pcat[i].getPrice());
        prodErtrags.push_back(pcat[i].getErtrag());
        prodNertrags.push_back(pcat[i].getNebenErtrag());
    }
    prettyUnits();
}
void Datawindow::updatePePrices(){
    parentWidth = par->size().width()-300;//static_cast<Mainwindow*>(parent())->size().width();
    vector<RegProductInfo> pcat = Manager->Market->getProductCat();
    int sz = prodIDs.size();
    vector<double> prices;
    for (int i=0; i<sz; ++i){
        prices.push_back((pcat[prodIDs[i]].getPrice()-prodNertrags[i])/prodErtrags[i]);
    }
    prodPricess.push_back(prices);
}

void Datawindow::updateRegdata(){
   if (!inited) initRegdata();

   updatePePrices();

   list<RegFarmInfo*>::iterator it = Manager->FarmList.begin();
   vector <DataReg> v;
   DataReg rd;

   int nprods = Manager->Market->getNumProducts();
   while (it != Manager->FarmList.end()){
        rd.farmid=(*it)->getFarmId();
        rd.rechtsform=(*it)->getLegalType();
        rd.farmtype= (*it)->getFarmClass();


        rd.totalarea = (*it)->getLandInput();
        if (rd.totalarea>maxFarmsize) {
            maxFarmsize=rd.totalarea;
            farmsizeMax=maxFarmsize;
            farmsizeMaxGew=maxFarmsize;
        }

        rd.landused = (*it)->getUnitsProducedOfGroup(0);

        vector<double> as,ps;
        for (int i=0; i<nSoils;++i){
            as.push_back((*it)->getRentedLandOfType(i));
            ps.push_back((*it)->getAvRentOfType(i));
        }

        rd.areasoils=as;
        rd.pricesoils=ps;

        vector<double> ts;
        for (int i=0; i<nprods; ++i){
            if ((*it)->getLU(i)!=0)
                ts.push_back((*it)->getUnitsOfProduct(i));
        }
        rd.animals = ts;
        rd.lu = (*it )-> getTotalLU();

        //Farmeinkommen
        double prof= (*it)->getProfit();
        double farm_lab= (*it)->labour->getFamilyLabour();
        double lab_input = (*it)->labour->getLabourInputHours();
        double x = (*it)->FarmInvestList->getAcquisitionCostsOfNumber(gg->FIXED_HIRED_LAB);
        double y = farm_lab>lab_input ? lab_input : farm_lab;

        rd.prof = prof;
        rd.einkommen = prof - 2*x/gg->MAX_H_LU*y;

        rd.esusize = (double)(*it)->getStandardGrossMargin()/gg->ESU;

        v.push_back(rd);

        ++it;
   }
   sort(v.begin(),v.end(),sizeCompare);
   regDatavec2.push_back(v);

   iterSBox->setMaximum(iter);
   iter++;

   updateFarmeinkommen();
   updateFarmgewinn();
   updateFarmsize();
   updatePachtpreis();
   updateTiere();
   updateEK();

   updateFarmtypen();

   updateGraphics();
}

void Datawindow::updateEK(){
    ekYdatas=par->mydatWindow->ekYdatas;
    ekXdata=par->mydatWindow->ekXdata;
    rankEKs=par->mydatWindow->rankEKs;
    totEKs=par->mydatWindow->totEKs;
}

void Datawindow::updateFarmsize(){
   sizYdatas.clear();

   vector<double> v6[nFarmtypes+2];
   vector<double> vx;

   for (int i=0;i<regDatavec2.size();++i){
        vector<DataReg> tv= regDatavec2[i];
        int ts = tv.size();

        double totaln=0;
        double totalArea=0;
        double sizFT[nFarmtypes];
        int sizFTn[nFarmtypes];
        for (int t=0; t<nFarmtypes; ++t){
            sizFTn[t]=0;
            sizFT[t]=0;
        }

        double myArea=0;
        for (int j=0;j<ts;++j){
           if (tv[j].farmid==0){
                myArea= tv[j].totalarea;
           }

           totaln++;
           totalArea+=tv[j].totalarea;
           sizFTn[tv[j].farmtype-1]++;
           sizFT[tv[j].farmtype-1]+=tv[j].totalarea;

        }

        for (int k=0; k<nFarmtypes+2; ++k){ //??????
            switch(nFarmtypes+1-k){
            case 1: v6[k].push_back(totalArea/totaln); break;
            case 0: v6[k].push_back(myArea);break;
            default:
                v6[k].push_back(sizFTn[k]==0? 0: sizFT[k]/sizFTn[k]);
            }
        }

        vx.push_back(i);

    }
    sizXdata=vx;

    sizYdatas.push_back(v6[nFarmtypes]);
    sizYdatas.push_back(v6[nFarmtypes+1]);
    for (int i=0;i<nFarmtypes;++i){
        sizYdatas.push_back(v6[i]);
    }
}

void Datawindow::updatePachtpreis(){
   bodYdatas.clear();

   int s= nSoils;
   vector<double> vt[2*s];

   vector<double> vx;

   for (int i=0;i<regDatavec2.size();++i){
        vector<DataReg> tv= regDatavec2[i];
        int ts = tv.size();

        double a[s],p[s], mp[s];
        for (int t=0; t<s; ++t){
            a[t]=0;
            p[t]=0; mp[t]=0;
        }


        for (int j=0;j<ts;++j){
            for (int k=0;k<s;++k){
                a[k]+=tv[j].areasoils[k];
                p[k]+=tv[j].areasoils[k]*tv[j].pricesoils[k];
                if (tv[j].farmid==0){
                    mp[k]=tv[j].pricesoils[k];
                }
            }
        }

        for (int k=0; k<s; ++k){ //??????
            vt[2*k].push_back(p[k]/a[k]);
            vt[2*k+1].push_back(mp[k]);
        }

        vx.push_back(i);

    }
    bodXdata=vx;

    for (int k=0;k<2*s;++k){
        bodYdatas.push_back(vt[k]);
    }
}


void Datawindow::updateTiere(){
   animYdatas.clear();

   int s= nAnimals;
   vector<double> vt[s];
   vector<double> lus;

   vector<double> vx;

   for (int i=0;i<regDatavec2.size();++i){
        vector<DataReg> tv= regDatavec2[i];
        int ts = tv.size();

        double totalLu=0;
        double totalArea=0;

        double a[s];
        for (int t=0; t<s; ++t){
            a[t]=0;
        }


        for (int j=0;j<ts;++j){
           totalLu+=tv[j].lu;
           totalArea+=tv[j].landused;
           for (int k=0;k<s;++k){
                a[k]+=tv[j].animals[k];
            }
        }

        for (int k=0; k<s; ++k){ //??????
            vt[k].push_back(a[k]);
        }

        lus.push_back(totalLu/totalArea);

        vx.push_back(i);

    }
    animXdata=vx;

    for (int k=0;k<s;++k){
        animYdatas.push_back(vt[k]);
    }
    animYdatas.push_back(lus);
}


bool Datawindow::inBereich(double s){
    if (ausserBereich) {
        return (s<farmsizeMin || s>farmsizeMax);
    }else {
        return (s>=farmsizeMin && s<=farmsizeMax);
    }
}

bool Datawindow::inBereichGew(double s){
    if (ausserBereichGew) {
        return (s<farmsizeMinGew || s>farmsizeMaxGew);
    }else {
        return (s>=farmsizeMinGew && s<=farmsizeMaxGew);
    }
}

void Datawindow::updateGraphics(){
    iterSBox->setValue(iter);
    if (!hasPlots){
        createPlots();
        createConstrWidgets();
        hasPlots=true;

      for (int i=0;i<numPlot;++i){
        QWidget* w=new QWidget();
        QVBoxLayout * lt = new QVBoxLayout();
        lt->addWidget(plots[i]);
        if (i==0 || i==1)
            lt->addWidget(constrWidgets[i]);
        w->setLayout(lt);
        if(i!=picType) w->hide();
        clouts.push_back(lt);
        clout->addWidget(w);
      }
      static_cast<Farmwidget*>(plots[7])->updateData(); // preis
    }
  else {
    sbHigh->setMaximum(maxFarmsize);
    sbHigh->setValue(farmsizeMax);
    sbLow->setMaximum(maxFarmsize);
    farmtypeProzents.clear();
    vector <double> farmtypeTotal;
    for (int i=0; i<regDatavec2.size();++i){
        farmtypeTotal.push_back(regDatavec2[i].size());
    }


    for (int i=0; i<nFarmtypes; ++i){
        vector <double> ftP;
        vector <double> ftN = farmtypeNums[i];
        for (int j=0; j<ftN.size();++j){
            ftP.push_back(100*ftN[j]/farmtypeTotal[j]);
        }
        farmtypeProzents.push_back(ftP);
    }
        FTPlot *t = static_cast<FTPlot*>(clout->itemAt(3)->widget()->layout()->itemAt(0)->widget());
        t->updatePlot(farmtypeNames,farmtypeColors,farmtypeProzents);


    for (int i=0; i<numPlot; ++i){
        if (i==3) continue;
        if(i==7) {
            Farmwidget* fw = static_cast<Farmwidget*>(plots[i]);
            fw->updateData();
            continue;
        }
        Plot* tp;
        if(i!=5)
            tp=static_cast<Plot*>(clout->itemAt(i)->widget()->layout()->itemAt(0)->widget());
        else // tiere
            tp=static_cast<Plot*>(clout->itemAt(i)->widget()->layout()->itemAt(0)
                            ->widget()->layout()->itemAt(0)->widget());
        tp->updatePlot();
    }
  }
}

void Datawindow::showRunde(int it){
  for (int i=1; i<numPlot; ++i){
    if (i!=7) continue; //nur preisübersicht
    Farmwidget *fw= static_cast<Farmwidget*>(plots[i]);
    fw->updateData(it);
  }
}

void Datawindow::prettyUnits(){ //euro symbol
    int sz = prodUnits.size();
    for (int i=0; i<sz; ++i){
        QString str = prodUnits[i];
        str.replace("EuRo",QString("")+QChar(0x20ac), Qt::CaseInsensitive);
        prodUnits[i]=str+"  ";
    }
}

void Datawindow::updateFarmtypen(){
   farmtypeNums.clear();
   int p=regDatavec2.size();
   vector <vector<double> > ftvs;
   ftvs.resize(nFarmtypes);
   for (int i=0;i<p;++i){
       vector<DataReg> r=regDatavec2[i];
       vector<double> fts;
       fts.resize(nFarmtypes);
       for (int k=0;k<nFarmtypes;++k){
            fts[k]=0;
       }
       for (int j=0;j<r.size();++j){
           int s=r[j].farmtype-1;
           fts[s]++;
       }

       for (int k=0;k<nFarmtypes;++k){
            ftvs[k].push_back(fts[k]);
       }
   }

   for (int k=0;k<nFarmtypes;++k)
    farmtypeNums.push_back(ftvs[k]);
}

void Datawindow::updateFarmeinkommen(){
   incYdatas.clear();

   vector<double> v6[nFarmtypes+2];
   vector<double> vx;

   int sz = regDatavec2.size();
   for (int i=0;i<sz;++i){
        vector<DataReg> tv= regDatavec2[i];
        int ts = tv.size();

        double totalInc=0;
        double totalArea=0;
        double incFT[nFarmtypes];
        int incFTn[nFarmtypes];
        for (int t=0; t<nFarmtypes; ++t){
            incFTn[t]=0;
            incFT[t]=0;
        }

        double myInc= 0;
        double myArea=0;

        if (i==sz-1) nFarms=0;
        for (int j=0;j<ts;++j){
           if (tv[j].farmid==0){
                 myInc = tv[j].einkommen;
                 myArea= tv[j].totalarea;
           }
           if(!legalforms[tv[j].rechtsform]) continue;
           if(!inBereich(tv[j].totalarea)) continue;

           if (i==sz-1) ++nFarms;

           totalInc+=tv[j].einkommen;
           totalArea+=tv[j].totalarea;
           incFTn[tv[j].farmtype-1]+=tv[j].totalarea;
           incFT[tv[j].farmtype-1]+=tv[j].einkommen;

        }

        for (int k=0; k<nFarmtypes+2; ++k){ //??????
            switch(nFarmtypes+1-k){
            case 1: v6[k].push_back(totalArea==0? 0: totalInc/totalArea); break;
            case 0: v6[k].push_back(myInc/myArea);break;
            default:v6[k].push_back(incFTn[k]==0? 0: incFT[k]/incFTn[k]);
            }
        }

        vx.push_back(i);

    }
    incXdata=vx;

    incYdatas.push_back(v6[nFarmtypes]);
    incYdatas.push_back(v6[nFarmtypes+1]);
    for (int k=0;k<nFarmtypes;++k)
        incYdatas.push_back(v6[k]);
}

void Datawindow::updateFarmgewinn(){
   gewYdatas.clear();

   vector<double> v6[nFarmtypes+2];
   vector<double> vx;

   int sz = regDatavec2.size();
   for (int i=0;i<sz;++i){
        vector<DataReg> tv= regDatavec2[i];
        int ts = tv.size();

        double totalGew=0;
        double totalFarms=0;
        double gewFT[nFarmtypes];
        int gewFTn[nFarmtypes];
        for (int t=0; t<nFarmtypes; ++t){
            gewFTn[t]=0;
            gewFT[t]=0;
        }

        double myGew= 0;

        if (i==sz-1) nFarms=0;
        for (int j=0;j<ts;++j){
           if (tv[j].farmid==0){
                 myGew = tv[j].prof;
           }
           if(!legalformsGew[tv[j].rechtsform]) continue;
           if(!inBereichGew(tv[j].totalarea)) continue;

           if (i==sz-1) ++nFarms;

           totalGew+=tv[j].prof;
           totalFarms++;
           gewFTn[tv[j].farmtype-1]++;
           gewFT[tv[j].farmtype-1]+=tv[j].prof;

        }

        for (int k=0; k<nFarmtypes+2; ++k){ //??????
            switch(nFarmtypes+1-k){
            case 1: v6[k].push_back(totalFarms==0? 0: totalGew/totalFarms); break;
            case 0: v6[k].push_back(myGew);break;
            default:v6[k].push_back(gewFTn[k]==0? 0: gewFT[k]/gewFTn[k]);
            }
        }

        vx.push_back(i);

    }
    gewXdata=vx;

    gewYdatas.push_back(v6[nFarmtypes]);
    gewYdatas.push_back(v6[nFarmtypes+1]);
    for (int k=0;k<nFarmtypes;++k)
        gewYdatas.push_back(v6[k]);
}


void Datawindow::closeEvent(QCloseEvent* event){
    emit hi(true);
    QMainWindow::closeEvent(event);
}

void Datawindow::createConstrWidgets(){
    constrWidgets.clear();
    for (int i=0; i<numPlot ; ++i){
      switch(i){
      case 0: constrFarmeinkommen();break;
      case 1: constrFarmgewinn();break;
      default:
            QWidget* w= new QWidget();
            constrWidgets.push_back(w);
      }
    }
}

void Datawindow::updateNfarms(){
    nFarms = Manager->getNoOfFarms();
    QString titStr = QString("Auswahl (Betriebszahl der akt. Periode: %1)").arg(nFarms);
    auswahlGBox->setTitle(titStr);
    auswahlGBoxGew->setTitle(titStr);
}

void Datawindow::constrFarmeinkommen(){
        nFarms = Manager->getNoOfFarms();
        QString titStr = QString("Auswahl (Betriebszahl der akt. Periode: %1)").arg(nFarms);
        auswahlGBox = new QGroupBox("Auswahl");
        auswahlGBox->setTitle(titStr);
        QRadioButton *checkAll = new QRadioButton("Alle");
        QVBoxLayout *vl1 = new QVBoxLayout();
        vl1->addWidget(checkAll);

        repButton = new QPushButton("Aktualisieren");
        repButton->setEnabled(false);

        checkLegal = new QCheckBox("Nach Rechtsform");
        checkLegal->setChecked(false);
        legs.resize(nLegaltypes);
        legs[0]=new QCheckBox("Einzelbetrieb im Nebenerwerb");
        legs[1]=new QCheckBox("Einzelbetrieb im Haupterwerb");
        legs[2]=new QCheckBox("Juristische Personen");
        legs[3]=new QCheckBox("Personengesellschaften");

        for (int k=0;k<nLegaltypes;++k){
            legs[k]->setEnabled(false);
            legs[k]->setChecked(true);
        }

        connect(checkLegal, SIGNAL(toggled(bool)),this,SLOT(updateCheckLegal(bool)));

        QSignalMapper* legalMapper = new QSignalMapper();

        for (int k=0;k<nLegaltypes;++k){
            legalMapper->setMapping(legs[k],k);
            connect(legs[k],SIGNAL(toggled(bool)),legalMapper,SLOT(map()));
        }

        connect(legalMapper, SIGNAL(mapped(int)),this,SLOT(updateLegs(int)) );

        QVBoxLayout *vl2 = new QVBoxLayout();
        vl2->addWidget(checkLegal);
        QVBoxLayout *vl21 = new QVBoxLayout();
        vl21->setMargin(10);
        for (int k=0;k<nLegaltypes;++k)
            vl21->addWidget(legs[k]);
        vl21->addStretch();

        vl2->addLayout(vl21);;

        checkSize = new QCheckBox("Nach Betriebsgröße");
        checkSize->setChecked(false);
        QVBoxLayout *vl3 = new QVBoxLayout();

        vl3->addWidget(checkSize);
        vl3->addSpacing(10);;

        ausserB = new QCheckBox("Oder außerhalb des Bereichs");
        ausserB->setEnabled(false);
        ausserB->setChecked(false);

        sbLow = new minSpinBox();
        sbLow->setMinimum(0);
        sbLow->setMaximum(maxFarmsize);
        sbLow->setSuffix(" ha");
        sbLow->setValue((int)farmsizeMin);
        sbLow->setAlignment(Qt::AlignRight);
        sbLow->setEnabled(false);

        sbHigh= new maxSpinBox();
        sbHigh->setSuffix(" ha");
        sbHigh->setMaximum(maxFarmsize);
        sbHigh->setValue((int)farmsizeMax);
        sbHigh->setAlignment(Qt::AlignRight);
        sbHigh->setEnabled(false);

        llab= new QLabel("Untergrenze");
        hlab= new QLabel("Obergrenze");
        llab->setEnabled(false);
        hlab->setEnabled(false);

        QGridLayout *glout=new QGridLayout();
        glout->addWidget(hlab,0,0);
        glout->addWidget(sbHigh, 0,1);
        glout->addWidget(llab,1,0);
        glout->addWidget(sbLow,1,1);

        QVBoxLayout *hlout= new QVBoxLayout();
        //hlout->addSpacing(20);
        hlout->addLayout(glout);
        hlout->addSpacing(15);
        hlout->addWidget(ausserB);
        //hlout->addStretch();

        QHBoxLayout *hout=new QHBoxLayout();
        hout->addSpacing(15);
        hout->addLayout(hlout);

        //vl3->setMargin(10);
        vl3->addLayout(hout);
        //vl3->addStretch();

        QHBoxLayout* checklout=new QHBoxLayout();
        //checklout->addLayout(vl1);
        checklout->addStretch();
        checklout->addLayout(vl2);
        checklout->addSpacing(50);
        checklout->addLayout(vl3);
        checklout->addStretch();

        auswahlGBox->setLayout(checklout);
        auswahlGBox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

        connect(checkSize, SIGNAL(toggled(bool)),this,SLOT(updateCheckSize(bool)));
        connect(ausserB,SIGNAL(clicked(bool)),this,SLOT(updateAusserB(bool)) );
        connect(sbLow,SIGNAL(valueChanged(int)),this,SLOT(updateLow(int)));
        connect(sbHigh,SIGNAL(valueChanged(int)),this,SLOT(updateHigh(int)) );

        connect(repButton, SIGNAL(pressed()), this, SLOT(updatePlot()));

        connect(sbLow,SIGNAL(valueChanged(int)), sbHigh, SLOT(updateValue(int)));
        connect(sbHigh,SIGNAL(valueChanged(int)), sbLow, SLOT(updateValue(int)));


        QHBoxLayout *tlout = new QHBoxLayout();
        tlout->addStretch();
        tlout->addWidget(auswahlGBox);
        tlout->addStretch();
        tlout->addWidget(repButton);
        tlout->addStretch();

        QWidget * w=new QWidget();
        w->setLayout(tlout);
        //gb->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Expanding);
        constrWidgets.push_back(w);
}

void Datawindow::constrFarmgewinn(){
        nFarms = Manager->getNoOfFarms();
        QString titStr = QString("Auswahl (Betriebszahl der akt. Periode: %1)").arg(nFarms);
        auswahlGBoxGew = new QGroupBox("Auswahl");
        auswahlGBoxGew->setTitle(titStr);
        QRadioButton *checkAll = new QRadioButton("Alle");
        QVBoxLayout *vl1 = new QVBoxLayout();
        vl1->addWidget(checkAll);

        repButtonGew = new QPushButton("Aktualisieren");
        repButtonGew->setEnabled(false);

        checkLegalGew = new QCheckBox("Nach Rechtsform");
        checkLegalGew->setChecked(false);
        legsGew.resize(nLegaltypes);
        legsGew[0]=new QCheckBox("Einzelbetrieb im Nebenerwerb");
        legsGew[1]=new QCheckBox("Einzelbetrieb im Haupterwerb");
        legsGew[2]=new QCheckBox("Juristische Personen");
        legsGew[3]=new QCheckBox("Personengesellschaften");

        for (int k=0;k<nLegaltypes;++k){
            legsGew[k]->setEnabled(false);
            legsGew[k]->setChecked(true);
        }

        connect(checkLegalGew, SIGNAL(toggled(bool)),this,SLOT(updateCheckLegalGew(bool)));

        QSignalMapper* legalMapper = new QSignalMapper();

        for (int k=0;k<nLegaltypes;++k){
            legalMapper->setMapping(legsGew[k],k);
            connect(legsGew[k],SIGNAL(toggled(bool)),legalMapper,SLOT(map()));
        }

        connect(legalMapper, SIGNAL(mapped(int)),this,SLOT(updateLegsGew(int)) );

        QVBoxLayout *vl2 = new QVBoxLayout();
        vl2->addWidget(checkLegalGew);
        QVBoxLayout *vl21 = new QVBoxLayout();
        vl21->setMargin(10);
        for (int k=0;k<nLegaltypes;++k)
            vl21->addWidget(legsGew[k]);
        vl21->addStretch();

        vl2->addLayout(vl21);;

        checkSizeGew = new QCheckBox("Nach Betriebsgröße");
        checkSizeGew->setChecked(false);
        QVBoxLayout *vl3 = new QVBoxLayout();

        vl3->addWidget(checkSizeGew);
        vl3->addSpacing(10);;

        ausserBGew = new QCheckBox("Oder außerhalb des Bereichs");
        ausserBGew->setEnabled(false);
        ausserBGew->setChecked(false);

        sbLowGew = new minSpinBox();
        sbLowGew->setMinimum(0);
        sbLowGew->setMaximum(maxFarmsize);
        sbLowGew->setSuffix(" ha");
        sbLowGew->setValue((int)farmsizeMinGew);
        sbLowGew->setAlignment(Qt::AlignRight);
        sbLowGew->setEnabled(false);

        sbHighGew= new maxSpinBox();
        sbHighGew->setSuffix(" ha");
        sbHighGew->setMaximum(maxFarmsize);
        sbHighGew->setValue((int)farmsizeMaxGew);
        sbHighGew->setAlignment(Qt::AlignRight);
        sbHighGew->setEnabled(false);

        llabGew= new QLabel("Untergrenze");
        hlabGew= new QLabel("Obergrenze");
        llabGew->setEnabled(false);
        hlabGew->setEnabled(false);

        QGridLayout *glout=new QGridLayout();
        glout->addWidget(hlabGew,0,0);
        glout->addWidget(sbHighGew, 0,1);
        glout->addWidget(llabGew,1,0);
        glout->addWidget(sbLowGew,1,1);

        QVBoxLayout *hlout= new QVBoxLayout();
        //hlout->addSpacing(20);
        hlout->addLayout(glout);
        hlout->addSpacing(15);
        hlout->addWidget(ausserBGew);
        //hlout->addStretch();

        QHBoxLayout *hout=new QHBoxLayout();
        hout->addSpacing(15);
        hout->addLayout(hlout);

        //vl3->setMargin(10);
        vl3->addLayout(hout);
        //vl3->addStretch();

        QHBoxLayout* checklout=new QHBoxLayout();
        //checklout->addLayout(vl1);
        checklout->addStretch();
        checklout->addLayout(vl2);
        checklout->addSpacing(50);
        checklout->addLayout(vl3);
        checklout->addStretch();

        auswahlGBoxGew->setLayout(checklout);
        auswahlGBoxGew->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

        connect(checkSizeGew, SIGNAL(toggled(bool)),this,SLOT(updateCheckSizeGew(bool)));
        connect(ausserBGew,SIGNAL(clicked(bool)),this,SLOT(updateAusserBGew(bool)) );
        connect(sbLowGew,SIGNAL(valueChanged(int)),this,SLOT(updateLowGew(int)));
        connect(sbHighGew,SIGNAL(valueChanged(int)),this,SLOT(updateHighGew(int)) );

        connect(repButtonGew, SIGNAL(pressed()), this, SLOT(updatePlotGew()));

        connect(sbLowGew,SIGNAL(valueChanged(int)), sbHighGew, SLOT(updateValue(int)));
        connect(sbHighGew,SIGNAL(valueChanged(int)), sbLowGew, SLOT(updateValue(int)));


        QHBoxLayout *tlout = new QHBoxLayout();
        tlout->addStretch();
        tlout->addWidget(auswahlGBoxGew);
        tlout->addStretch();
        tlout->addWidget(repButtonGew);
        tlout->addStretch();

        QWidget * w=new QWidget();
        w->setLayout(tlout);
        //gb->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Expanding);
        constrWidgets.push_back(w);
}


void Datawindow::updateLegs(int i){
    QCheckBox * cb = legs[i];
    bool b= cb->isChecked();
    legalforms[i]=b;
    repButton->setEnabled(true);
}

void Datawindow::updateLegsGew(int i){
    QCheckBox * cb = legsGew[i];
    bool b= cb->isChecked();
    legalformsGew[i]=b;
    repButtonGew->setEnabled(true);
}

void Datawindow::updateAusserB(bool b){
    ausserBereich=b;
    repButton->setEnabled(true);
}

void Datawindow::updateAusserBGew(bool b){
    ausserBereichGew=b;
    repButtonGew->setEnabled(true);
}

void Datawindow::updateData(){
    if (layerComboBox->currentIndex()==0){
        updateFarmeinkommen();
    }
    if (layerComboBox->currentIndex()==1){
        updateFarmgewinn();
    }
}

void Datawindow::updatePlot(){
    updateData();

    nFarmsStr = QString("Auswahl (Betriebszahl der akt. Periode: %1)").arg(nFarms);
    auswahlGBox->setTitle(nFarmsStr);

    updateGraphics();
    repButton->setEnabled(false);
}

void Datawindow::updatePlotGew(){
    updateData();

    nFarmsStr = QString("Auswahl (Betriebszahl der akt. Periode: %1)").arg(nFarms);
    auswahlGBoxGew->setTitle(nFarmsStr);

    updateGraphics();
    repButtonGew->setEnabled(false);
}

void Datawindow::updateLow(int i){
    farmsizeMin=(double)i;
    repButton->setEnabled(true);
}

void Datawindow::updateLowGew(int i){
    farmsizeMinGew=(double)i;
    repButtonGew->setEnabled(true);
}

void Datawindow::updateHigh(int i){
    farmsizeMax=(double)i;
    repButton->setEnabled(true);
}

void Datawindow::updateHighGew(int i){
    farmsizeMaxGew=(double)i;
    repButtonGew->setEnabled(true);
}


void Datawindow::updateCheckLegal(bool b){
    for (int k=0; k<nLegaltypes;++k)
        legs[k]->setEnabled(b);

    if (!b) {
        for (int i=0; i<nLegaltypes; ++i){
            legalforms[i]=!b;
        }
    }

    for (int k=0;k<nLegaltypes;++k)
        legs[k]->setChecked(true);

    repButton->setEnabled(true);
}

void Datawindow::updateCheckLegalGew(bool b){
    for (int k=0; k<nLegaltypes;++k)
        legsGew[k]->setEnabled(b);

    if (!b) {
        for (int i=0; i<nLegaltypes; ++i){
            legalformsGew[i]=!b;
        }
    }

    for (int k=0;k<nLegaltypes;++k)
        legsGew[k]->setChecked(true);

    repButtonGew->setEnabled(true);
}

void Datawindow::updateCheckSize(bool b){
    sbLow->setEnabled(b);
    sbHigh->setEnabled(b);
    llab->setEnabled(b);
    hlab->setEnabled(b);
    ausserB->setEnabled(b);

    if (!b) {
        farmsizeMin=0;
        farmsizeMax=maxFarmsize;
        ausserBereich=false;
    }

    sbLow->setValue(farmsizeMin);
    sbHigh->setValue(farmsizeMax);
    ausserB->setChecked(false);

    repButton->setEnabled(true);
}

void Datawindow::updateCheckSizeGew(bool b){
    sbLowGew->setEnabled(b);
    sbHighGew->setEnabled(b);
    llabGew->setEnabled(b);
    hlabGew->setEnabled(b);
    ausserBGew->setEnabled(b);

    if (!b) {
        farmsizeMinGew=0;
        farmsizeMaxGew=maxFarmsize;
        ausserBereichGew=false;
    }

    sbLowGew->setValue(farmsizeMinGew);
    sbHighGew->setValue(farmsizeMaxGew);
    ausserBGew->setChecked(false);

    repButtonGew->setEnabled(true);
}


void Datawindow::createPlots(){
    plots.clear();
    for (int i=0; i<numPlot ; ++i){
       if(i==3) {
            createFTPlot();
            plots.push_back(tp);
            continue;
        }

       if (i==5) {//tiere
            Plot *p = new Plot(i,this,0);
            connect(p,SIGNAL(dichteShow(bool)),this, SLOT(dichteBoxCheck(bool)));
            QWidget *w = new QWidget();
            dichteBox= new QCheckBox("Viehbesatzdichte \nein-/ausblenden");
            dichteBox->setChecked(true);
            connect(dichteBox, SIGNAL(toggled(bool)),this,SLOT(dichteEinAusblenden(bool)));

            dichteBox->setFont(QFont("Times",12));
            QHBoxLayout *hlout = new QHBoxLayout();
            hlout->addWidget(p,1);

            hlout->addWidget(dichteBox,0,Qt::AlignTop);
            w->setLayout(hlout);
            plots.push_back(w);
        }else if (i==6){ //EK
            Plot *p= new Plot(100+i, this, 0);//par->mydatWindow,0); //+100 unterscheiden zu kopie in mydatwindow
            plots.push_back(p);
        }else if (i==7){ //Preise
            QWidget *p= new Farmwidget(i,this);
            plots.push_back(p);
        }else  {
            Plot *p= new Plot(i, this,0);
            plots.push_back(p);
        }
        /*
        d_zoomer[2*i] = new Zoomer( QwtPlot::xBottom, QwtPlot::yLeft,
            static_cast<QwtPlot*>(plots[i])->canvas());
        d_zoomer[2*i]->setRubberBand(QwtPicker::RectRubberBand);
        d_zoomer[2*i]->setRubberBandPen(QColor(Qt::green));
        d_zoomer[2*i]->setTrackerMode(QwtPicker::ActiveOnly);
        d_zoomer[2*i]->setTrackerPen(QColor(Qt::white));

        if (i==5) {
            d_zoomer[2*i+1] = new Zoomer(QwtPlot::xBottom, QwtPlot::yRight,
                    static_cast<QwtPlot*>(plots[i])->canvas());
        }
        //*/
    }
}

 void Datawindow::createFTPlot(){
    vector <vector<double> > farmtypeProzents;
    vector <double> farmtypeTotal;
    for (int i=0; i<regDatavec2.size();++i){
        farmtypeTotal.push_back(regDatavec2[i].size());
    }


    for (int i=0; i<nFarmtypes; ++i){
        vector <double> ftP;
        vector <double> ftN = farmtypeNums[i];
        for (int j=0; j<ftN.size();++j){
            ftP.push_back(100*ftN[j]/farmtypeTotal[j]);
        }
        farmtypeProzents.push_back(ftP);
    }

    tp = new FTPlot(farmtypeNames,farmtypeColors,farmtypeProzents,this);

    /*
        enableZoomMode(true);
    //*/
}

void Datawindow::enableZoomMode(bool on)
{
    //d_panner->setEnabled(on);
    /*
    for (int i=0; i<numPlot ; ++i){
        d_zoomer[i]->setEnabled(on);
        d_zoomer[i]->zoom(0);
    }
    //*/
    //d_picker->setEnabled(!on);
}

void Datawindow::createActions(){

    layerLabel = new QLabel(tr("Darstellung von "));
    layerComboBox=new QComboBox();
    layerComboBox->addItem(tr("Betriebseinkommen"));
    layerComboBox->addItem(tr("Betriebsgewinn"));
    layerComboBox->addItem(tr("Betriebsgrößen"));
    layerComboBox->addItem(tr("Betriebstypen"));
    layerComboBox->addItem(tr("Pachtpreise"));
    layerComboBox->addItem(tr("Tiere"));
    layerComboBox->addItem(tr("Eigenkapital"));
    layerComboBox->addItem(tr("Preisübersicht"));
    //layerComboBox->addItem(tr("Tierzahlen"));

    layerComboBox->setCurrentIndex(2);
    layerLabel->setBuddy(layerComboBox);

    connect(layerComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(updateGraphic(int)));

    actConfig = new QAction("Anpassen",this);
    actConfig->setIcon(QIcon(":/images/config.png"));

    connect(actConfig, SIGNAL(triggered(bool)), this, SLOT(showAdjDialog(bool)));

    actWithBorder = new QAction("Gitter",this);
    actWithBorder->setIcon(QIcon(":/images/grid.png"));

    actPrint = new QAction("Drucken",this);
    actPrint->setIcon(QIcon(":/images/drucken.png"));

    iterSBox = new QSpinBox();
    iterSBox->setPrefix("Runde:  ");
    connect(iterSBox, SIGNAL(valueChanged(int)), this, SLOT(showRunde(int)));

    connect(actWithBorder, SIGNAL(triggered()), this, SLOT(updateWithBorder()));
    connect(actPrint,SIGNAL(triggered()), this, SLOT(drucken()));
}

void Datawindow::showAdjDialog(bool b){
    int l = layerComboBox->currentIndex();
    if (hasDialogs[l]) {
        dialogs[l]->show();
        actConfig->setEnabled(false);
        layerComboBox->setEnabled(false);
        return;
    }

    vector<QwtSymbol> vs;
    vector<string> titles;
    vector<QColor> colors;
    if(l!=3){
        vector<QwtSymbol::Style> styles;
        switch(l){
        case 0: styles=incStyles;titles=incYtitles;colors=incColors;break;
        case 1: styles=gewStyles;titles=gewYtitles;colors=gewColors;break;
        case 2: styles=sizStyles;titles=sizYtitles;colors=sizColors;break;
        case 4: styles=bodStyles;titles=bodYtitles;colors=bodColors;break;
        case 5: styles=animStyles;titles=animYtitles;colors=animColors;break;
        case 6: styles=ekStyles;titles=ekYtitles;colors=ekColors;break;
        default:;
        }

        for (int i=0;i<styles.size();++i){
            QwtSymbol *ps= new QwtSymbol(styles[i]);
            vs.push_back(*ps);
        }
    }

    adjDialog *dialog;

    if (l!=3) {
         dialog= new adjDialog(titles,colors,vs,gridColors[l], canvasColors[l], maxXs[l],minXs[l],l,this);
    }else
         dialog= new adjDialog(farmtypeNames,farmtypeColors,vs,gridColors[l], canvasColors[l], maxXs[l],minXs[l], l,this);

    hasDialogs[l]=true;
    dialogs[l]=dialog;

    connect(dialog,SIGNAL(accepted()),this, SLOT(updateConfig()));
    connect(dialog,SIGNAL(rejected()),this, SLOT(updateConfig()));

    connect(dialog,SIGNAL(maxValue(int)),this,SLOT(updateXmax(int)));
    connect(dialog,SIGNAL(minValue(int)),this,SLOT(updateXmin(int)));

    connect(dialog,SIGNAL(maxDichte(double)),this,SLOT(updateDichteMax(double)));
    connect(dialog,SIGNAL(minDichte(double)),this,SLOT(updateDichteMin(double)));

    connect(dialog,SIGNAL(plotFarben(QColor,QColor)),this,SLOT(updatePlotFarben(QColor,QColor)));
    connect(dialog,SIGNAL(curveFarben(int,QColor)),this,SLOT(updateCurveFarben(int,QColor)));
    connect(dialog,SIGNAL(curveStyle(int,QwtSymbol)),this,SLOT(updateCurveStyle(int,QwtSymbol)));

    dialog->show();

    actConfig->setEnabled(false);
    layerComboBox->setEnabled(false);
}

void Datawindow::updateCurveStyle(int i, QwtSymbol s){
    int l = layerComboBox->currentIndex();
    if (l!=3) {
        switch(l){
        case 0: incStyles[i]=s.style();break;
        case 1: gewStyles[i]=s.style();break;
        case 2: sizStyles[i]=s.style();break;
        case 4: bodStyles[i]=s.style();break;
        case 5: animStyles[i]=s.style();break;
        case 6: ekStyles[i]=s.style();break;
        default:;
        }
        if(l==5)
            static_cast<Plot*>(plots[l]->layout()->itemAt(0)->widget())->updatePlot();
        else static_cast<Plot*>(plots[l])->updatePlot();
    }
}

void Datawindow::updatePlotFarben(QColor gridC, QColor canvasC){
    int i=layerComboBox->currentIndex();
    canvasColors[i]=canvasC;
    gridColors[i]=gridC;
    if (i!=3&& i!=7) {
        Plot* p;
        if (i==5)
           p=static_cast<Plot*>(plots[i]->layout()->itemAt(0)->widget());
        else p=static_cast<Plot*> (plots[i]);
        p->setCanvasBackground(canvasC);
        p->getGrid()->setPen(QPen(gridC,0,Qt::DotLine));
        p->updatePlot();
    } else if (i==3) {
        static_cast<FTPlot*> (tp)->setCanvasBackground(canvasC);
        static_cast<FTPlot*> (tp)->getGrid()->setPen(QPen(gridC,0,Qt::DotLine));
        static_cast<FTPlot*> (tp)->updateColors();
        //Plot(farmtypeNames,farmtypeColors,farmtypeProzents);
    }
}

void Datawindow::updateCurveFarben(int i, QColor c){
    int l = layerComboBox->currentIndex();
    if (l!=3 && l!=7) {
        switch(l) {
        case 0: incColors[i]=c;break;
        case 1: gewColors[i]=c;break;
        case 2: sizColors[i]=c;break;
        case 4: bodColors[i]=c;break;
        case 5: animColors[i]=c;break;
        case 6: ekColors[i]=c;break;
        default:;
        }
        if (l==5) static_cast<Plot*>(plots[l]->layout()->itemAt(0)->widget())->updatePlot();
        else static_cast<Plot*>(plots[l])->updatePlot();
    }
    if(l==3) {
        farmtypeColors[i]=c;
        static_cast<FTPlot*>(tp)->updateColor(i,c);//updatePlot(farmtypeNames,farmtypeColors,farmtypeProzents);
    }
}

void Datawindow::updateXmax(int m){
    int l=layerComboBox->currentIndex();
    maxXs[l]=m;
    if(l==3) { //farmtype
        tp->setAxisScale(QwtPlot::xBottom,minXs[l],maxXs[l],maxXs[l]-minXs[l]<12?1:2);
    }
    else if (l==5)static_cast<Plot*>(plots[l]->layout()->itemAt(0)->widget())->setAxisScale(QwtPlot::xBottom, minXs[l], maxXs[l], maxXs[l]-minXs[l]<12?1:2);
    else static_cast<Plot*>(plots[l])->setAxisScale(QwtPlot::xBottom, minXs[l], maxXs[l], maxXs[l]-minXs[l]<12?1:2);
}

void Datawindow::updateXmin(int m){
    int l=layerComboBox->currentIndex();
    minXs[l]= m;
    if(l==3) { //farmtype
        tp->setAxisScale(QwtPlot::xBottom,minXs[l],maxXs[l],maxXs[l]-minXs[l]<12?1:2);
    }
    else if (l==5) static_cast<Plot*>(plots[l]->layout()->itemAt(0)->widget())->setAxisScale(QwtPlot::xBottom, minXs[l], maxXs[l], maxXs[l]-minXs[l]<12?1:2);
    else static_cast<Plot*>(plots[l])->setAxisScale(QwtPlot::xBottom, minXs[l], maxXs[l], maxXs[l]-minXs[l]<12?1:2);
}

void Datawindow::updateDichteMax(double d){ //tiere 5
    animYrightMax=d;
    Plot* p= static_cast<Plot*>(plots[5]->layout()->itemAt(0)->widget());
    p->setAxisScale(QwtPlot::yRight, animYrightMin, animYrightMax);
}


void Datawindow::updateDichteMin(double d){ //tiere 5
    animYrightMin=d;
    Plot* p= static_cast<Plot*>(plots[5]->layout()->itemAt(0)->widget());
    p->setAxisScale(QwtPlot::yRight, animYrightMin, animYrightMax);
}

void Datawindow::updateConfig(){
    actConfig->setEnabled(true);
    layerComboBox->setEnabled(true);
}

void Datawindow::createStatusBar(){
    statusLabel = new QLabel();
    statusLabel->setIndent(10);
    statusLabel->setTextFormat(Qt::RichText);
    statusLabel->setText("HI");
    statusBar()->addWidget(statusLabel);
}

void Datawindow::updateStatusBar(){
    statusLabel->setText(tr("HALLO"));
}

void Datawindow::createToolBars(){
    chooseLayerToolBar = addToolBar("Auswahl");

    chooseLayerToolBar->addWidget(layerLabel);
    chooseLayerToolBar->addWidget(layerComboBox);

    chooseLayerToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    chooseLayerToolBar->addAction(actConfig);
    actIter=chooseLayerToolBar->addWidget(iterSBox);

    bool b;
    int l= layerComboBox->currentIndex();
    b= (l==7); //preistab
    actIter->setVisible(b);

    zoomToolBar = addToolBar("Zoom");
    zoomToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

    zoomToolBar->addAction(actWithBorder);
    zoomToolBar->addAction(actPrint);

    optToolBar = addToolBar("showhide");
    optToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
}


void Datawindow::updateGraphic(int id){
layerComboBox->setCurrentIndex(id);
bool b=false;
b=(id==7); ;
actIter->setVisible(false);//>setEnabled(b);
actConfig->setVisible(!b);
actWithBorder->setVisible(!b);

QVBoxLayout *lout = static_cast<QVBoxLayout*>(centralWidget()->layout());
//clout->itemAt(0)->widget()->layout()->itemAt(0)->widget())
 lout->itemAt(oldlayer)->widget()->hide();
 lout->itemAt(id)->widget()->show();

 /*if (id!=0 && id!=1) resize(800,500);
 else actConfig->setEnabled(true);
 //*/
 if (id==0 || id==1) actConfig->setEnabled(true);
 oldlayer=id;
}

void Datawindow::updateWithBorder(){
   int id = layerComboBox->currentIndex();
 if (id!=3 && id!=7){
    if(id!=5)
        withBorder = static_cast<Plot*>(plots[id])->withGrid=!static_cast<Plot*>(plots[id])->withGrid;
    else
        withBorder = static_cast<Plot*>(plots[id]->layout()->itemAt(0)->widget())->withGrid
            =!static_cast<Plot*>(plots[id]->layout()->itemAt(0)->widget())->withGrid;
 }else if(id==3)
   withBorder=static_cast<FTPlot*>(plots[id])->withGrid=!static_cast<FTPlot*>(plots[id])->withGrid;

   updateGraphic(withBorder);
}

void Datawindow::updateGraphic(bool b) {
 int id = layerComboBox->currentIndex();

 QwtPlotGrid* g;
 if (id==3) {
    FTPlot* p= static_cast<FTPlot*>(plots[id]);
    g=p->getGrid();
 }
 else {
   if (id!=5)
        g=static_cast<Plot*>(plots[id])->getGrid();
   else if (id!=7)
      g=static_cast<Plot*>(plots[id]->layout()->itemAt(0)->widget())->getGrid();
 }

 b?g->show():g->hide();

}

QString Datawindow::getFileName(){
    QString name;
    int i = layerComboBox->currentIndex();
    switch(i){
    case 0: name="R-eink";break;
    case 1: name="R-gew";break;
    case 2: name="R-grosse";break;
    case 3: name="R-btyp";break;
    case 4: name="R-pacht";break;
    case 5: name="R-tiere";break;
    case 6: name="R-ek";break;
    case 7: name="R-prodpreis";break;
    default:;
    }
    return name;
}

void Datawindow::drucken(){
   QString opstr = QString(gg->OUTPUTFILEdir.c_str());
   printer->setOutputFileName(opstr+"DataRegion/"+getFileName()+".pdf");
    /*QPrintDialog dialog(printer);
    if (dialog.exec()){
    {
        //*/
        QPainter painter(printer);
        /*QPainter painter;
        QPrinter printer;
        painter.begin(&printer);
        //*/

        /* QFont textFont("Times", 20, QFont::Bold);

        painter.setFont(textFont);
        //QRect textRect = painter.boundingRect(QRectF(0,0,9600,500),Qt::AlignCenter,
           // "Das ist eine Graphik");

        painter.drawText(QRect(0,0,painter.window().width(),500),Qt::AlignCenter,
            QString("Graphik nach %1").arg(layerComboBox->currentText()));
            //Das ist eine Graphik");
        painter.translate(0,700);
        scene->render(&painter);

        painter.translate(0,scene->sceneRect().height()*11+50);
        painter.scale(10,10);
        legendWidgets[layerComboBox->currentIndex()]->render(&painter);
       //*/

        painter.scale(8,8);
        this->centralWidget()->render(&painter);
    //}
}

bool Datawindow::zunah(QColor c, int r, int g, int b){
   return (abs(c.red()-r) < 50 && c.red()-r == c.green()-g
            && c.red()-r == c.blue()-b);
}

void Datawindow::saveConfig(){
    QSettings dataSettings("IAMO, BSE", "Farm AgriPoliS");

    for (int i=0; i<incYtitles.size();++i){
        dataSettings.setValue(QString("incColor_")+i, incColors[i]);
    }

    for (int i=0; i<incYtitles.size();++i){
        dataSettings.setValue(QString("incStyle_")+i, incStyles[i]);
    }

    for (int i=0; i<gewYtitles.size();++i){
        dataSettings.setValue(QString("gewColor_")+i, gewColors[i]);
    }

    for (int i=0; i<gewYtitles.size();++i){
        dataSettings.setValue(QString("gewStyle_")+i, gewStyles[i]);
    }

    for (int i=0; i<sizYtitles.size();++i){
        dataSettings.setValue(QString("sizColor_")+i, sizColors[i]);
    }
    for (int i=0; i<sizYtitles.size();++i){
        dataSettings.setValue(QString("sizStyle_")+i, sizStyles[i]);
    }

    for (int i=0; i<bodYtitles.size();++i){
        dataSettings.setValue(QString("bodColor_")+i, bodColors[i]);
    }
    for (int i=0; i<bodYtitles.size();++i){
        dataSettings.setValue(QString("bodStyle_")+i, bodStyles[i]);
    }

    for (int i=0; i<nAnimals+1;++i){
        dataSettings.setValue(QString("animColor_")+i, animColors[i]);
    }
    for (int i=0; i<nAnimals+1;++i){
        dataSettings.setValue(QString("animStyle_")+i, animStyles[i]);
    }

    for (int i=0; i<ekYtitles.size();++i){
        dataSettings.setValue(QString("ekRegColor_")+i, ekColors[i]);
    }
    for (int i=0; i<ekYtitles.size(); ++i){
        dataSettings.setValue(QString("ekRegStyle_")+i, ekStyles[i]);
    }

    for (int i=0; i<farmtypeNames.size();++i){
        dataSettings.setValue(QString("farmtypeColor_")+i, farmtypeColors[i]);
    }

    for (int i=0;i<numPlot;++i){
        dataSettings.setValue(QString("gridColor_")+i, gridColors[i]);
        dataSettings.setValue(QString("canvasColor_")+i, canvasColors[i]);
    }
}

bool Datawindow::plotsCreated(){
    return hasPlots;
}

void Datawindow::dichteBoxCheck(bool b){
    if (dichteBox->isChecked()==b) return;
    dichteBox->setChecked(b);
}

void Datawindow::dichteEinAusblenden(bool b){
    QWidget *w=plots[5];
    Plot *p= static_cast<Plot*>(w->layout()->itemAt(0)->widget());
    p->showCurve(p->getCurve(p->numCurves()), b);
}

void Datawindow::faOutput(){

      for (int i=0;i<numPlot-1;++i){ //preisübersicht nicht drucken
        updateGraphic(i);
        layerComboBox->setCurrentIndex(i);
        //QString str = layerComboBox->currentText();
        //printer->setOutputFileName(str+".pdf");
        drucken();
        QCoreApplication::processEvents();
      }
      //*/
}
