/* glpk.h */

/***********************************************************************
*  This code is part of GLPK (GNU Linear Programming Kit).
*
*  Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
*  2009, 2010 Andrew Makhorin, Department for Applied Informatics,
*  Moscow Aviation Institute, Moscow, Russia. All rights reserved.
*  E-mail: <mao@gnu.org>.
*
*  GLPK is free software: you can redistribute it and/or modify it
*  under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  GLPK is distributed in the hope that it will be useful, but WITHOUT
*  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
*  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
*  License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with GLPK. If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#ifndef DGLPK_H
#define DGLPK_H

#include <stdarg.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* library version numbers: */
#define GLP_MAJOR_VERSION  4
#define GLP_MINOR_VERSION  45

#ifndef DGLP_PROB_DEFINED
#define DGLP_PROB_DEFINED
typedef struct { double _opaque_prob[100]; } dglp_prob;
/* LP/MIP problem object */
#endif

/* optimization direction flag: */
#define GLP_MIN            1  /* minimization */
#define GLP_MAX            2  /* maximization */

/* kind of structural variable: */
#define GLP_CV             1  /* continuous variable */
#define GLP_IV             2  /* integer variable */
#define GLP_BV             3  /* binary variable */

/* type of auxiliary/structural variable: */
#define GLP_FR             1  /* free variable */
#define GLP_LO             2  /* variable with lower bound */
#define GLP_UP             3  /* variable with upper bound */
#define GLP_DB             4  /* double-bounded variable */
#define GLP_FX             5  /* fixed variable */

/* status of auxiliary/structural variable: */
#define GLP_BS             1  /* basic variable */
#define GLP_NL             2  /* non-basic variable on lower bound */
#define GLP_NU             3  /* non-basic variable on upper bound */
#define GLP_NF             4  /* non-basic free variable */
#define GLP_NS             5  /* non-basic fixed variable */

/* scaling options: */
#define GLP_SF_GM       0x01  /* perform geometric mean scaling */
#define GLP_SF_EQ       0x10  /* perform equilibration scaling */
#define GLP_SF_2N       0x20  /* round scale factors to power of two */
#define GLP_SF_SKIP     0x40  /* skip if problem is well scaled */
#define GLP_SF_AUTO     0x80  /* choose scaling options automatically */

/* solution indicator: */
#define GLP_SOL            1  /* basic solution */
#define GLP_IPT            2  /* interior-point solution */
#define GLP_MIP            3  /* mixed integer solution */

/* solution status: */
#define GLP_UNDEF          1  /* solution is undefined */
#define GLP_FEAS           2  /* solution is feasible */
#define GLP_INFEAS         3  /* solution is infeasible */
#define GLP_NOFEAS         4  /* no feasible solution exists */
#define GLP_OPT            5  /* solution is optimal */
#define GLP_UNBND          6  /* solution is unbounded */

typedef struct
{     /* basis factorization control parameters */
      int msg_lev;            /* (reserved) */
      int type;               /* factorization type: */
#define GLP_BF_FT          1  /* LUF + Forrest-Tomlin */
#define GLP_BF_BG          2  /* LUF + Schur compl. + Bartels-Golub */
#define GLP_BF_GR          3  /* LUF + Schur compl. + Givens rotation */
      int lu_size;            /* luf.sv_size */
      double piv_tol;         /* luf.piv_tol */
      int piv_lim;            /* luf.piv_lim */
      int suhl;               /* luf.suhl */
      double eps_tol;         /* luf.eps_tol */
      double max_gro;         /* luf.max_gro */
      int nfs_max;            /* fhv.hh_max */
      double upd_tol;         /* fhv.upd_tol */
      int nrs_max;            /* lpf.n_max */
      int rs_size;            /* lpf.v_size */
      double foo_bar[38];     /* (reserved) */
} dglp_bfcp;

typedef struct
{     /* simplex method control parameters */
      int msg_lev;            /* message level: */
#define GLP_MSG_OFF        0  /* no output */
#define GLP_MSG_ERR        1  /* warning and error messages only */
#define GLP_MSG_ON         2  /* normal output */
#define GLP_MSG_ALL        3  /* full output */
#define GLP_MSG_DBG        4  /* debug output */
      int meth;               /* simplex method option: */
#define GLP_PRIMAL         1  /* use primal simplex */
#define GLP_DUALP          2  /* use dual; if it fails, use primal */
#define GLP_DUAL           3  /* use dual simplex */
      int pricing;            /* pricing technique: */
#define GLP_PT_STD      0x11  /* standard (Dantzig rule) */
#define GLP_PT_PSE      0x22  /* projected steepest edge */
      int r_test;             /* ratio test technique: */
#define GLP_RT_STD      0x11  /* standard (textbook) */
#define GLP_RT_HAR      0x22  /* two-pass Harris' ratio test */
      double tol_bnd;         /* spx.tol_bnd */
      double tol_dj;          /* spx.tol_dj */
      double tol_piv;         /* spx.tol_piv */
      double obj_ll;          /* spx.obj_ll */
      double obj_ul;          /* spx.obj_ul */
      int it_lim;             /* spx.it_lim */
      int tm_lim;             /* spx.tm_lim (milliseconds) */
      int out_frq;            /* spx.out_frq */
      int out_dly;            /* spx.out_dly (milliseconds) */
      int presolve;           /* enable/disable using LP presolver */
      double foo_bar[36];     /* (reserved) */
} dglp_smcp;

typedef struct
{     /* interior-point solver control parameters */
      int msg_lev;            /* message level (see dglp_smcp) */
      int ord_alg;            /* ordering algorithm: */
#define GLP_ORD_NONE       0  /* natural (original) ordering */
#define GLP_ORD_QMD        1  /* quotient minimum degree (QMD) */
#define GLP_ORD_AMD        2  /* approx. minimum degree (AMD) */
#define GLP_ORD_SYMAMD     3  /* approx. minimum degree (SYMAMD) */
      double foo_bar[48];     /* (reserved) */
} dglp_iptcp;

#ifndef DGLP_TREE_DEFINED
#define DGLP_TREE_DEFINED
typedef struct { double _opaque_tree[100]; } dglp_tree;
/* branch-and-bound tree */
#endif

typedef struct
{     /* integer optimizer control parameters */
      int msg_lev;            /* message level (see dglp_smcp) */
      int br_tech;            /* branching technique: */
#define GLP_BR_FFV         1  /* first fractional variable */
#define GLP_BR_LFV         2  /* last fractional variable */
#define GLP_BR_MFV         3  /* most fractional variable */
#define GLP_BR_DTH         4  /* heuristic by Driebeck and Tomlin */
#define GLP_BR_PCH         5  /* hybrid pseudocost heuristic */
      int bt_tech;            /* backtracking technique: */
#define GLP_BT_DFS         1  /* depth first search */
#define GLP_BT_BFS         2  /* breadth first search */
#define GLP_BT_BLB         3  /* best local bound */
#define GLP_BT_BPH         4  /* best projection heuristic */
      double tol_int;         /* mip.tol_int */
      double tol_obj;         /* mip.tol_obj */
      int tm_lim;             /* mip.tm_lim (milliseconds) */
      int out_frq;            /* mip.out_frq (milliseconds) */
      int out_dly;            /* mip.out_dly (milliseconds) */
      void (*cb_func)(dglp_tree *T, void *info);
                              /* mip.cb_func */
      void *cb_info;          /* mip.cb_info */
      int cb_size;            /* mip.cb_size */
      int pp_tech;            /* preprocessing technique: */
#define GLP_PP_NONE        0  /* disable preprocessing */
#define GLP_PP_ROOT        1  /* preprocessing only on root level */
#define GLP_PP_ALL         2  /* preprocessing on all levels */
      double mip_gap;         /* relative MIP gap tolerance */
      int mir_cuts;           /* MIR cuts       (GLP_ON/GLP_OFF) */
      int gmi_cuts;           /* Gomory's cuts  (GLP_ON/GLP_OFF) */
      int cov_cuts;           /* cover cuts     (GLP_ON/GLP_OFF) */
      int clq_cuts;           /* clique cuts    (GLP_ON/GLP_OFF) */
      int presolve;           /* enable/disable using MIP presolver */
      int binarize;           /* try to binarize integer variables */
      int fp_heur;            /* feasibility pump heuristic */
#if 1 /* 28/V-2010 */
      int alien;              /* use alien solver */
#endif
      double foo_bar[29];     /* (reserved) */
} dglp_iocp;

typedef struct
{     /* additional row attributes */
      int level;
      /* subproblem level at which the row was added */
      int origin;
      /* row origin flag: */
#define GLP_RF_REG         0  /* regular constraint */
#define GLP_RF_LAZY        1  /* "lazy" constraint */
#define GLP_RF_CUT         2  /* cutting plane constraint */
      int klass;
      /* row class descriptor: */
#define GLP_RF_GMI         1  /* Gomory's mixed integer cut */
#define GLP_RF_MIR         2  /* mixed integer rounding cut */
#define GLP_RF_COV         3  /* mixed cover cut */
#define GLP_RF_CLQ         4  /* clique cut */
      double foo_bar[7];
      /* (reserved) */
} dglp_attr;

/* enable/disable flag: */
#define GLP_ON             1  /* enable something */
#define GLP_OFF            0  /* disable something */

/* reason codes: */
#define GLP_IROWGEN     0x01  /* request for row generation */
#define GLP_IBINGO      0x02  /* better integer solution found */
#define GLP_IHEUR       0x03  /* request for heuristic solution */
#define GLP_ICUTGEN     0x04  /* request for cut generation */
#define GLP_IBRANCH     0x05  /* request for branching */
#define GLP_ISELECT     0x06  /* request for subproblem selection */
#define GLP_IPREPRO     0x07  /* request for preprocessing */

/* branch selection indicator: */
#define GLP_NO_BRNCH       0  /* select no branch */
#define GLP_DN_BRNCH       1  /* select down-branch */
#define GLP_UP_BRNCH       2  /* select up-branch */

/* return codes: */
#define GLP_EBADB       0x01  /* invalid basis */
#define GLP_ESING       0x02  /* singular matrix */
#define GLP_ECOND       0x03  /* ill-conditioned matrix */
#define GLP_EBOUND      0x04  /* invalid bounds */
#define GLP_EFAIL       0x05  /* solver failed */
#define GLP_EOBJLL      0x06  /* objective lower limit reached */
#define GLP_EOBJUL      0x07  /* objective upper limit reached */
#define GLP_EITLIM      0x08  /* iteration limit exceeded */
#define GLP_ETMLIM      0x09  /* time limit exceeded */
#define GLP_ENOPFS      0x0A  /* no primal feasible solution */
#define GLP_ENODFS      0x0B  /* no dual feasible solution */
#define GLP_EROOT       0x0C  /* root LP optimum not provided */
#define GLP_ESTOP       0x0D  /* search terminated by application */
#define GLP_EMIPGAP     0x0E  /* relative mip gap tolerance reached */
#define GLP_ENOFEAS     0x0F  /* no primal/dual feasible solution */
#define GLP_ENOCVG      0x10  /* no convergence */
#define GLP_EINSTAB     0x11  /* numerical instability */
#define GLP_EDATA       0x12  /* invalid data */
#define GLP_ERANGE      0x13  /* result out of range */

/* condition indicator: */
#define GLP_KKT_PE         1  /* primal equalities */
#define GLP_KKT_PB         2  /* primal bounds */
#define GLP_KKT_DE         3  /* dual equalities */
#define GLP_KKT_DB         4  /* dual bounds */
#define GLP_KKT_CS         5  /* complementary slackness */

/* MPS file format: */
#define GLP_MPS_DECK       1  /* fixed (ancient) */
#define GLP_MPS_FILE       2  /* free (modern) */

typedef struct
{     /* MPS format control parameters */
      int blank;
      /* character code to replace blanks in symbolic names */
      char *obj_name;
      /* objective row name */
      double tol_mps;
      /* zero tolerance for MPS data */
      double foo_bar[17];
      /* (reserved for use in the future) */
} dglp_mpscp;

typedef struct
{     /* CPLEX LP format control parameters */
      double foo_bar[20];
      /* (reserved for use in the future) */
} dglp_cpxcp;

#ifndef DGLP_TRAN_DEFINED
#define DGLP_TRAN_DEFINED
typedef struct { double _opaque_tran[100]; } dglp_tran;
/* MathProg translator workspace */
#endif

dglp_prob *dglp_create_prob(void);
/* create problem object */

void dglp_set_prob_name(dglp_prob *P, const char *name);
/* assign (change) problem name */

void dglp_set_obj_name(dglp_prob *P, const char *name);
/* assign (change) objective function name */

void dglp_set_obj_dir(dglp_prob *P, int dir);
/* set (change) optimization direction flag */

int dglp_add_rows(dglp_prob *P, int nrs);
/* add new rows to problem object */

int dglp_add_cols(dglp_prob *P, int ncs);
/* add new columns to problem object */

void dglp_set_row_name(dglp_prob *P, int i, const char *name);
/* assign (change) row name */

void dglp_set_col_name(dglp_prob *P, int j, const char *name);
/* assign (change) column name */

void dglp_set_row_bnds(dglp_prob *P, int i, int type, double lb,
      double ub);
/* set (change) row bounds */

void dglp_set_col_bnds(dglp_prob *P, int j, int type, double lb,
      double ub);
/* set (change) column bounds */

void dglp_set_obj_coef(dglp_prob *P, int j, double coef);
/* set (change) obj. coefficient or constant term */

void dglp_set_mat_row(dglp_prob *P, int i, int len, const int ind[],
      const double val[]);
/* set (replace) row of the constraint matrix */

void dglp_set_mat_col(dglp_prob *P, int j, int len, const int ind[],
      const double val[]);
/* set (replace) column of the constraint matrix */

void dglp_load_matrix(dglp_prob *P, int ne, const int ia[],
      const int ja[], const double ar[]);
/* load (replace) the whole constraint matrix */

int dglp_check_dup(int m, int n, int ne, const int ia[], const int ja[]);
/* check for duplicate elements in sparse matrix */

void dglp_sort_matrix(dglp_prob *P);
/* sort elements of the constraint matrix */

void dglp_del_rows(dglp_prob *P, int nrs, const int num[]);
/* delete specified rows from problem object */

void dglp_del_cols(dglp_prob *P, int ncs, const int num[]);
/* delete specified columns from problem object */

void dglp_copy_prob(dglp_prob *dest, dglp_prob *prob, int names);
/* copy problem object content */

void dglp_erase_prob(dglp_prob *P);
/* erase problem object content */

void dglp_delete_prob(dglp_prob *P);
/* delete problem object */

const char *dglp_get_prob_name(dglp_prob *P);
/* retrieve problem name */

const char *dglp_get_obj_name(dglp_prob *P);
/* retrieve objective function name */

int dglp_get_obj_dir(dglp_prob *P);
/* retrieve optimization direction flag */

int dglp_get_num_rows(dglp_prob *P);
/* retrieve number of rows */

int dglp_get_num_cols(dglp_prob *P);
/* retrieve number of columns */

const char *dglp_get_row_name(dglp_prob *P, int i);
/* retrieve row name */

const char *dglp_get_col_name(dglp_prob *P, int j);
/* retrieve column name */

int dglp_get_row_type(dglp_prob *P, int i);
/* retrieve row type */

double dglp_get_row_lb(dglp_prob *P, int i);
/* retrieve row lower bound */

double dglp_get_row_ub(dglp_prob *P, int i);
/* retrieve row upper bound */

int dglp_get_col_type(dglp_prob *P, int j);
/* retrieve column type */

double dglp_get_col_lb(dglp_prob *P, int j);
/* retrieve column lower bound */

double dglp_get_col_ub(dglp_prob *P, int j);
/* retrieve column upper bound */

double dglp_get_obj_coef(dglp_prob *P, int j);
/* retrieve obj. coefficient or constant term */

int dglp_get_num_nz(dglp_prob *P);
/* retrieve number of constraint coefficients */

int dglp_get_mat_row(dglp_prob *P, int i, int ind[], double val[]);
/* retrieve row of the constraint matrix */

int dglp_get_mat_col(dglp_prob *P, int j, int ind[], double val[]);
/* retrieve column of the constraint matrix */

void dglp_create_index(dglp_prob *P);
/* create the name index */

int dglp_find_row(dglp_prob *P, const char *name);
/* find row by its name */

int dglp_find_col(dglp_prob *P, const char *name);
/* find column by its name */

void dglp_delete_index(dglp_prob *P);
/* delete the name index */

void dglp_set_rii(dglp_prob *P, int i, double rii);
/* set (change) row scale factor */

void dglp_set_sjj(dglp_prob *P, int j, double sjj);
/* set (change) column scale factor */

double dglp_get_rii(dglp_prob *P, int i);
/* retrieve row scale factor */

double dglp_get_sjj(dglp_prob *P, int j);
/* retrieve column scale factor */

void dglp_scale_prob(dglp_prob *P, int flags);
/* scale problem data */

void dglp_unscale_prob(dglp_prob *P);
/* unscale problem data */

void dglp_set_row_stat(dglp_prob *P, int i, int stat);
/* set (change) row status */

void dglp_set_col_stat(dglp_prob *P, int j, int stat);
/* set (change) column status */

void dglp_std_basis(dglp_prob *P);
/* construct standard initial LP basis */

void dglp_adv_basis(dglp_prob *P, int flags);
/* construct advanced initial LP basis */

void dglp_cpx_basis(dglp_prob *P);
/* construct Bixby's initial LP basis */

int dglp_simplex(dglp_prob *P, const dglp_smcp *parm);
/* solve LP problem with the simplex method */

int dglp_exact(dglp_prob *P, const dglp_smcp *parm);
/* solve LP problem in exact arithmetic */

void dglp_init_smcp(dglp_smcp *parm);
/* initialize simplex method control parameters */

int dglp_get_status(dglp_prob *P);
/* retrieve generic status of basic solution */

int dglp_get_prim_stat(dglp_prob *P);
/* retrieve status of primal basic solution */

int dglp_get_dual_stat(dglp_prob *P);
/* retrieve status of dual basic solution */

double dglp_get_obj_val(dglp_prob *P);
/* retrieve objective value (basic solution) */

int dglp_get_row_stat(dglp_prob *P, int i);
/* retrieve row status */

double dglp_get_row_prim(dglp_prob *P, int i);
/* retrieve row primal value (basic solution) */

double dglp_get_row_dual(dglp_prob *P, int i);
/* retrieve row dual value (basic solution) */

int dglp_get_col_stat(dglp_prob *P, int j);
/* retrieve column status */

double dglp_get_col_prim(dglp_prob *P, int j);
/* retrieve column primal value (basic solution) */

double dglp_get_col_dual(dglp_prob *P, int j);
/* retrieve column dual value (basic solution) */

int dglp_get_unbnd_ray(dglp_prob *P);
/* determine variable causing unboundedness */

int dglp_interior(dglp_prob *P, const dglp_iptcp *parm);
/* solve LP problem with the interior-point method */

void dglp_init_iptcp(dglp_iptcp *parm);
/* initialize interior-point solver control parameters */

int dglp_ipt_status(dglp_prob *P);
/* retrieve status of interior-point solution */

double dglp_ipt_obj_val(dglp_prob *P);
/* retrieve objective value (interior point) */

double dglp_ipt_row_prim(dglp_prob *P, int i);
/* retrieve row primal value (interior point) */

double dglp_ipt_row_dual(dglp_prob *P, int i);
/* retrieve row dual value (interior point) */

double dglp_ipt_col_prim(dglp_prob *P, int j);
/* retrieve column primal value (interior point) */

double dglp_ipt_col_dual(dglp_prob *P, int j);
/* retrieve column dual value (interior point) */

void dglp_set_col_kind(dglp_prob *P, int j, int kind);
/* set (change) column kind */

int dglp_get_col_kind(dglp_prob *P, int j);
/* retrieve column kind */

int dglp_get_num_int(dglp_prob *P);
/* retrieve number of integer columns */

int dglp_get_num_bin(dglp_prob *P);
/* retrieve number of binary columns */

int dglp_intopt(dglp_prob *P, const dglp_iocp *parm);
/* solve MIP problem with the branch-and-bound method */

void dglp_init_iocp(dglp_iocp *parm);
/* initialize integer optimizer control parameters */

int dglp_mip_status(dglp_prob *P);
/* retrieve status of MIP solution */

double dglp_mip_obj_val(dglp_prob *P);
/* retrieve objective value (MIP solution) */

double dglp_mip_row_val(dglp_prob *P, int i);
/* retrieve row value (MIP solution) */

double dglp_mip_col_val(dglp_prob *P, int j);
/* retrieve column value (MIP solution) */

int dglp_print_sol(dglp_prob *P, const char *fname);
/* write basic solution in printable format */

int dglp_read_sol(dglp_prob *P, const char *fname);
/* read basic solution from text file */

int dglp_write_sol(dglp_prob *P, const char *fname);
/* write basic solution to text file */

int dglp_print_ranges(dglp_prob *P, int len, const int list[],
      int flags, const char *fname);
/* print sensitivity analysis report */

int dglp_print_ipt(dglp_prob *P, const char *fname);
/* write interior-point solution in printable format */

int dglp_read_ipt(dglp_prob *P, const char *fname);
/* read interior-point solution from text file */

int dglp_write_ipt(dglp_prob *P, const char *fname);
/* write interior-point solution to text file */

int dglp_print_mip(dglp_prob *P, const char *fname);
/* write MIP solution in printable format */

int dglp_read_mip(dglp_prob *P, const char *fname);
/* read MIP solution from text file */

int dglp_write_mip(dglp_prob *P, const char *fname);
/* write MIP solution to text file */

int dglp_bf_exists(dglp_prob *P);
/* check if the basis factorization exists */

int dglp_factorize(dglp_prob *P);
/* compute the basis factorization */

int dglp_bf_updated(dglp_prob *P);
/* check if the basis factorization has been updated */

void dglp_get_bfcp(dglp_prob *P, dglp_bfcp *parm);
/* retrieve basis factorization control parameters */

void dglp_set_bfcp(dglp_prob *P, const dglp_bfcp *parm);
/* change basis factorization control parameters */

int dglp_get_bhead(dglp_prob *P, int k);
/* retrieve the basis header information */

int dglp_get_row_bind(dglp_prob *P, int i);
/* retrieve row index in the basis header */

int dglp_get_col_bind(dglp_prob *P, int j);
/* retrieve column index in the basis header */

void dglp_ftran(dglp_prob *P, double x[]);
/* perform forward transformation (solve system B*x = b) */

void dglp_btran(dglp_prob *P, double x[]);
/* perform backward transformation (solve system B'*x = b) */

int dglp_warm_up(dglp_prob *P);
/* "warm up" LP basis */

int dglp_eval_tab_row(dglp_prob *P, int k, int ind[], double val[]);
/* compute row of the simplex tableau */

int dglp_eval_tab_col(dglp_prob *P, int k, int ind[], double val[]);
/* compute column of the simplex tableau */

int dglp_transform_row(dglp_prob *P, int len, int ind[], double val[]);
/* transform explicitly specified row */

int dglp_transform_col(dglp_prob *P, int len, int ind[], double val[]);
/* transform explicitly specified column */

int dglp_prim_rtest(dglp_prob *P, int len, const int ind[],
      const double val[], int dir, double eps);
/* perform primal ratio test */

int dglp_dual_rtest(dglp_prob *P, int len, const int ind[],
      const double val[], int dir, double eps);
/* perform dual ratio test */

void dglp_analyze_bound(dglp_prob *P, int k, double *value1, int *var1,
      double *value2, int *var2);
/* analyze active bound of non-basic variable */

void dglp_analyze_coef(dglp_prob *P, int k, double *coef1, int *var1,
      double *value1, double *coef2, int *var2, double *value2);
/* analyze objective coefficient at basic variable */

int dglp_ios_reason(dglp_tree *T);
/* determine reason for calling the callback routine */

dglp_prob *dglp_ios_get_prob(dglp_tree *T);
/* access the problem object */

void dglp_ios_tree_size(dglp_tree *T, int *a_cnt, int *n_cnt,
      int *t_cnt);
/* determine size of the branch-and-bound tree */

int dglp_ios_curr_node(dglp_tree *T);
/* determine current active subproblem */

int dglp_ios_next_node(dglp_tree *T, int p);
/* determine next active subproblem */

int dglp_ios_prev_node(dglp_tree *T, int p);
/* determine previous active subproblem */

int dglp_ios_up_node(dglp_tree *T, int p);
/* determine parent subproblem */

int dglp_ios_node_level(dglp_tree *T, int p);
/* determine subproblem level */

double dglp_ios_node_bound(dglp_tree *T, int p);
/* determine subproblem local bound */

int dglp_ios_best_node(dglp_tree *T);
/* find active subproblem with best local bound */

double dglp_ios_mip_gap(dglp_tree *T);
/* compute relative MIP gap */

void *dglp_ios_node_data(dglp_tree *T, int p);
/* access subproblem application-specific data */

void dglp_ios_row_attr(dglp_tree *T, int i, dglp_attr *attr);
/* retrieve additional row attributes */

int dglp_ios_pool_size(dglp_tree *T);
/* determine current size of the cut pool */

int dglp_ios_add_row(dglp_tree *T,
      const char *name, int klass, int flags, int len, const int ind[],
      const double val[], int type, double rhs);
/* add row (constraint) to the cut pool */

void dglp_ios_del_row(dglp_tree *T, int i);
/* remove row (constraint) from the cut pool */

void dglp_ios_clear_pool(dglp_tree *T);
/* remove all rows (constraints) from the cut pool */

int dglp_ios_can_branch(dglp_tree *T, int j);
/* check if can branch upon specified variable */

void dglp_ios_branch_upon(dglp_tree *T, int j, int sel);
/* choose variable to branch upon */

void dglp_ios_select_node(dglp_tree *T, int p);
/* select subproblem to continue the search */

int dglp_ios_heur_sol(dglp_tree *T, const double x[]);
/* provide solution found by heuristic */

void dglp_ios_terminate(dglp_tree *T);
/* terminate the solution process */

void dglp_init_mpscp(dglp_mpscp *parm);
/* initialize MPS format control parameters */

int dglp_read_mps(dglp_prob *P, int fmt, const dglp_mpscp *parm,
      const char *fname);
/* read problem data in MPS format */

int dglp_write_mps(dglp_prob *P, int fmt, const dglp_mpscp *parm,
      const char *fname);
/* write problem data in MPS format */

void dglp_init_cpxcp(dglp_cpxcp *parm);
/* initialize CPLEX LP format control parameters */

int dglp_read_lp(dglp_prob *P, const dglp_cpxcp *parm, const char *fname);
/* read problem data in CPLEX LP format */

int dglp_write_lp(dglp_prob *P, const dglp_cpxcp *parm, const char *fname);
/* write problem data in CPLEX LP format */

int dglp_read_prob(dglp_prob *P, int flags, const char *fname);
/* read problem data in GLPK format */

int dglp_write_prob(dglp_prob *P, int flags, const char *fname);
/* write problem data in GLPK format */

dglp_tran *dglp_mpl_alloc_wksp(void);
/* allocate the MathProg translator workspace */

int dglp_mpl_read_model(dglp_tran *tran, const char *fname, int skip);
/* read and translate model section */

int dglp_mpl_read_data(dglp_tran *tran, const char *fname);
/* read and translate data section */

int dglp_mpl_generate(dglp_tran *tran, const char *fname);
/* generate the model */

void dglp_mpl_build_prob(dglp_tran *tran, dglp_prob *prob);
/* build LP/MIP problem instance from the model */

int dglp_mpl_postsolve(dglp_tran *tran, dglp_prob *prob, int sol);
/* postsolve the model */

void dglp_mpl_free_wksp(dglp_tran *tran);
/* free the MathProg translator workspace */

int dglp_main(int argc, const char *argv[]);
/* stand-alone LP/MIP solver */

/**********************************************************************/

#ifndef DGLP_LONG_DEFINED
#define DGLP_LONG_DEFINED
typedef struct { int lo, hi; } dglp_long;
/* long integer data type */
#endif

int dglp_init_env(void);
/* initialize GLPK environment */

const char *dglp_version(void);
/* determine library version */

int dglp_free_env(void);
/* free GLPK environment */

void dglp_printf(const char *fmt, ...);
/* write formatted output to terminal */

void dglp_vprintf(const char *fmt, va_list arg);
/* write formatted output to terminal */

int dglp_term_out(int flag);
/* enable/disable terminal output */

void dglp_term_hook(int (*func)(void *info, const char *s), void *info);
/* install hook to intercept terminal output */

int dglp_open_tee(const char *fname);
/* start copying terminal output to text file */

int dglp_close_tee(void);
/* stop copying terminal output to text file */

#ifndef DGLP_ERROR_DEFINED
#define DGLP_ERROR_DEFINED
typedef void (*_dglp_error)(const char *fmt, ...);
#endif

#define dglp_error dglp_error_(__FILE__, __LINE__)
_dglp_error dglp_error_(const char *file, int line);
/* display error message and terminate execution */

#define dglp_assert(expr) \
      ((void)((expr) || (dglp_assert_(#expr, __FILE__, __LINE__), 1)))
void dglp_assert_(const char *expr, const char *file, int line);
/* check for logical condition */

void dglp_error_hook(void (*func)(void *info), void *info);
/* install hook to intercept abnormal termination */

void *dglp_malloc(int size);
/* allocate memory block */

void *dglp_calloc(int n, int size);
/* allocate memory block */

void dglp_free(void *ptr);
/* free memory block */

void dglp_mem_limit(int limit);
/* set memory usage limit */

void dglp_mem_usage(int *count, int *cpeak, dglp_long *total,
      dglp_long *tpeak);
/* get memory usage information */

dglp_long dglp_time(void);
/* determine current universal time */

double dglp_difftime(dglp_long t1, dglp_long t0);
/* compute difference between two time values */

/**********************************************************************/

#ifndef DGLP_DATA_DEFINED
#define DGLP_DATA_DEFINED
typedef struct { double _opaque_data[100]; } dglp_data;
/* plain data file */
#endif

dglp_data *dglp_sdf_open_file(const char *fname);
/* open plain data file */

void dglp_sdf_set_jump(dglp_data *data, void *jump);
/* set up error handling */

void dglp_sdf_error(dglp_data *data, const char *fmt, ...);
/* print error message */

void dglp_sdf_warning(dglp_data *data, const char *fmt, ...);
/* print warning message */

int dglp_sdf_read_int(dglp_data *data);
/* read integer number */

double dglp_sdf_read_num(dglp_data *data);
/* read floating-point number */

const char *dglp_sdf_read_item(dglp_data *data);
/* read data item */

const char *dglp_sdf_read_text(dglp_data *data);
/* read text until end of line */

int dglp_sdf_line(dglp_data *data);
/* determine current line number */

void dglp_sdf_close_file(dglp_data *data);
/* close plain data file */

/**********************************************************************/

typedef struct _dglp_graph dglp_graph;
typedef struct _dglp_vertex dglp_vertex;
typedef struct _dglp_arc dglp_arc;

struct _dglp_graph
{     /* graph descriptor */
      void *pool; /* DMP *pool; */
      /* memory pool to store graph components */
      char *name;
      /* graph name (1 to 255 chars); NULL means no name is assigned
         to the graph */
      int nv_max;
      /* length of the vertex list (enlarged automatically) */
      int nv;
      /* number of vertices in the graph, 0 <= nv <= nv_max */
      int na;
      /* number of arcs in the graph, na >= 0 */
      dglp_vertex **v; /* dglp_vertex *v[1+nv_max]; */
      /* v[i], 1 <= i <= nv, is a pointer to i-th vertex */
      void *index; /* AVL *index; */
      /* vertex index to find vertices by their names; NULL means the
         index does not exist */
      int v_size;
      /* size of data associated with each vertex (0 to 256 bytes) */
      int a_size;
      /* size of data associated with each arc (0 to 256 bytes) */
};

struct _dglp_vertex
{     /* vertex descriptor */
      int i;
      /* vertex ordinal number, 1 <= i <= nv */
      char *name;
      /* vertex name (1 to 255 chars); NULL means no name is assigned
         to the vertex */
      void *entry; /* AVLNODE *entry; */
      /* pointer to corresponding entry in the vertex index; NULL means
         that either the index does not exist or the vertex has no name
         assigned */
      void *data;
      /* pointer to data associated with the vertex */
      void *temp;
      /* working pointer */
      dglp_arc *in;
      /* pointer to the (unordered) list of incoming arcs */
      dglp_arc *out;
      /* pointer to the (unordered) list of outgoing arcs */
};

struct _dglp_arc
{     /* arc descriptor */
      dglp_vertex *tail;
      /* pointer to the tail endpoint */
      dglp_vertex *head;
      /* pointer to the head endpoint */
      void *data;
      /* pointer to data associated with the arc */
      void *temp;
      /* working pointer */
      dglp_arc *t_prev;
      /* pointer to previous arc having the same tail endpoint */
      dglp_arc *t_next;
      /* pointer to next arc having the same tail endpoint */
      dglp_arc *h_prev;
      /* pointer to previous arc having the same head endpoint */
      dglp_arc *h_next;
      /* pointer to next arc having the same head endpoint */
};

dglp_graph *dglp_create_graph(int v_size, int a_size);
/* create graph */

void dglp_set_graph_name(dglp_graph *G, const char *name);
/* assign (change) graph name */

int dglp_add_vertices(dglp_graph *G, int nadd);
/* add new vertices to graph */

void dglp_set_vertex_name(dglp_graph *G, int i, const char *name);
/* assign (change) vertex name */

dglp_arc *dglp_add_arc(dglp_graph *G, int i, int j);
/* add new arc to graph */

void dglp_del_vertices(dglp_graph *G, int ndel, const int num[]);
/* delete vertices from graph */

void dglp_del_arc(dglp_graph *G, dglp_arc *a);
/* delete arc from graph */

void dglp_erase_graph(dglp_graph *G, int v_size, int a_size);
/* erase graph content */

void dglp_delete_graph(dglp_graph *G);
/* delete graph */

void dglp_create_v_index(dglp_graph *G);
/* create vertex name index */

int dglp_find_vertex(dglp_graph *G, const char *name);
/* find vertex by its name */

void dglp_delete_v_index(dglp_graph *G);
/* delete vertex name index */

int dglp_read_graph(dglp_graph *G, const char *fname);
/* read graph from plain text file */

int dglp_write_graph(dglp_graph *G, const char *fname);
/* write graph to plain text file */

void dglp_mincost_lp(dglp_prob *P, dglp_graph *G, int names, int v_rhs,
      int a_low, int a_cap, int a_cost);
/* convert minimum cost flow problem to LP */

int dglp_mincost_okalg(dglp_graph *G, int v_rhs, int a_low, int a_cap,
      int a_cost, double *sol, int a_x, int v_pi);
/* find minimum-cost flow with out-of-kilter algorithm */

void dglp_maxflow_lp(dglp_prob *P, dglp_graph *G, int names, int s,
      int t, int a_cap);
/* convert maximum flow problem to LP */

int dglp_maxflow_ffalg(dglp_graph *G, int s, int t, int a_cap,
      double *sol, int a_x, int v_cut);
/* find maximal flow with Ford-Fulkerson algorithm */

int dglp_check_asnprob(dglp_graph *G, int v_set);
/* check correctness of assignment problem data */

/* assignment problem formulation: */
#define GLP_ASN_MIN        1  /* perfect matching (minimization) */
#define GLP_ASN_MAX        2  /* perfect matching (maximization) */
#define GLP_ASN_MMP        3  /* maximum matching */

int dglp_asnprob_lp(dglp_prob *P, int form, dglp_graph *G, int names,
      int v_set, int a_cost);
/* convert assignment problem to LP */

int dglp_asnprob_okalg(int form, dglp_graph *G, int v_set, int a_cost,
      double *sol, int a_x);
/* solve assignment problem with out-of-kilter algorithm */

int dglp_asnprob_hall(dglp_graph *G, int v_set, int a_x);
/* find bipartite matching of maximum cardinality */

double dglp_cpp(dglp_graph *G, int v_t, int v_es, int v_ls);
/* solve critical path problem */

int dglp_read_mincost(dglp_graph *G, int v_rhs, int a_low, int a_cap,
      int a_cost, const char *fname);
/* read min-cost flow problem data in DIMACS format */

int dglp_write_mincost(dglp_graph *G, int v_rhs, int a_low, int a_cap,
      int a_cost, const char *fname);
/* write min-cost flow problem data in DIMACS format */

int dglp_read_maxflow(dglp_graph *G, int *s, int *t, int a_cap,
      const char *fname);
/* read maximum flow problem data in DIMACS format */

int dglp_write_maxflow(dglp_graph *G, int s, int t, int a_cap,
      const char *fname);
/* write maximum flow problem data in DIMACS format */

int dglp_read_asnprob(dglp_graph *G, int v_set, int a_cost, const char
      *fname);
/* read assignment problem data in DIMACS format */

int dglp_write_asnprob(dglp_graph *G, int v_set, int a_cost, const char
      *fname);
/* write assignment problem data in DIMACS format */

int dglp_read_ccdata(dglp_graph *G, int v_wgt, const char *fname);
/* read graph in DIMACS clique/coloring format */

int dglp_write_ccdata(dglp_graph *G, int v_wgt, const char *fname);
/* write graph in DIMACS clique/coloring format */

int dglp_netgen(dglp_graph *G, int v_rhs, int a_cap, int a_cost,
      const int parm[1+15]);
/* Klingman's network problem generator */

int dglp_gridgen(dglp_graph *G, int v_rhs, int a_cap, int a_cost,
      const int parm[1+14]);
/* grid-like network problem generator */

int dglp_rmfgen(dglp_graph *G, int *s, int *t, int a_cap,
      const int parm[1+5]);
/* Goldfarb's maximum flow problem generator */

int dglp_weak_comp(dglp_graph *G, int v_num);
/* find all weakly connected components of graph */

int dglp_strong_comp(dglp_graph *G, int v_num);
/* find all strongly connected components of graph */

int dglp_top_sort(dglp_graph *G, int v_num);
/* topological sorting of acyclic digraph */

int dglp_wclique_exact(dglp_graph *G, int v_wgt, double *sol, int v_set);
/* find maximum weight clique with exact algorithm */

/***********************************************************************
*  NOTE: All symbols defined below are obsolete and kept here only for
*        backward compatibility.
***********************************************************************/

//#define LPX dglp_prob

/* problem class: */
//#define LPX_LP          100   /* linear programming (LP) */
//#define LPX_MIP         101   /* mixed integer programming (MIP) */

/* type of auxiliary/structural variable: */
//#define LPX_FR          110   /* free variable */
//#define LPX_LO          111   /* variable with lower bound */
//#define LPX_UP          112   /* variable with upper bound */
//#define LPX_DB          113   /* double-bounded variable */
//#define LPX_FX          114   /* fixed variable */

/* optimization direction flag: */
//#define LPX_MIN         120   /* minimization */
//#define LPX_MAX         121   /* maximization */

/* status of primal basic solution: */
//#define LPX_P_UNDEF     132   /* primal solution is undefined */
//#define LPX_P_FEAS      133   /* solution is primal feasible */
//#define LPX_P_INFEAS    134   /* solution is primal infeasible */
//#define LPX_P_NOFEAS    135   /* no primal feasible solution exists */

/* status of dual basic solution: */
//#define LPX_D_UNDEF     136   /* dual solution is undefined */
//#define LPX_D_FEAS      137   /* solution is dual feasible */
//#define LPX_D_INFEAS    138   /* solution is dual infeasible */
//#define LPX_D_NOFEAS    139   /* no dual feasible solution exists */

/* status of auxiliary/structural variable: */
//#define LPX_BS          140   /* basic variable */
//#define LPX_NL          141   /* non-basic variable on lower bound */
//#define LPX_NU          142   /* non-basic variable on upper bound */
//#define LPX_NF          143   /* non-basic free variable */
//#define LPX_NS          144   /* non-basic fixed variable */

/* status of interior-point solution: */
//#define LPX_T_UNDEF     150   /* interior solution is undefined */
//#define LPX_T_OPT       151   /* interior solution is optimal */

/* kind of structural variable: */
//#define LPX_CV          160   /* continuous variable */
//#define LPX_IV          161   /* integer variable */

/* status of integer solution: */
//#define LPX_I_UNDEF     170   /* integer solution is undefined */
//#define LPX_I_OPT       171   /* integer solution is optimal */
//#define LPX_I_FEAS      172   /* integer solution is feasible */
//#define LPX_I_NOFEAS    173   /* no integer solution exists */

/* status codes reported by the routine lpx_get_status: */
//#define LPX_OPT         180   /* optimal */
//#define LPX_FEAS        181   /* feasible */
//#define LPX_INFEAS      182   /* infeasible */
//#define LPX_NOFEAS      183   /* no feasible */
//#define LPX_UNBND       184   /* unbounded */
//#define LPX_UNDEF       185   /* undefined */

/* exit codes returned by solver routines: */
//#define LPX_E_OK        200   /* success */
//#define LPX_E_EMPTY     201   /* empty problem */
//#define LPX_E_BADB      202   /* invalid initial basis */
//#define LPX_E_INFEAS    203   /* infeasible initial solution */
//#define LPX_E_FAULT     204   /* unable to start the search */
//#define LPX_E_OBJLL     205   /* objective lower limit reached */
//#define LPX_E_OBJUL     206   /* objective upper limit reached */
//#define LPX_E_ITLIM     207   /* iterations limit exhausted */
//#define LPX_E_TMLIM     208   /* time limit exhausted */
//#define LPX_E_NOFEAS    209   /* no feasible solution */
//#define LPX_E_INSTAB    210   /* numerical instability */
//#define LPX_E_SING      211   /* problems with basis matrix */
//#define LPX_E_NOCONV    212   /* no convergence (interior) */
//#define LPX_E_NOPFS     213   /* no primal feas. sol. (LP presolver) */
//#define LPX_E_NODFS     214   /* no dual feas. sol. (LP presolver) */
//#define LPX_E_MIPGAP    215   /* relative mip gap tolerance reached */

/* control parameter identifiers: */
//#define LPX_K_MSGLEV    300   /* lp->msg_lev */
//#define LPX_K_SCALE     301   /* lp->scale */
//#define LPX_K_DUAL      302   /* lp->dual */
//#define LPX_K_PRICE     303   /* lp->price */
//#define LPX_K_RELAX     304   /* lp->relax */
//#define LPX_K_TOLBND    305   /* lp->tol_bnd */
//#define LPX_K_TOLDJ     306   /* lp->tol_dj */
//#define LPX_K_TOLPIV    307   /* lp->tol_piv */
//#define LPX_K_ROUND     308   /* lp->round */
//#define LPX_K_OBJLL     309   /* lp->obj_ll */
//#define LPX_K_OBJUL     310   /* lp->obj_ul */
//#define LPX_K_ITLIM     311   /* lp->it_lim */
//#define LPX_K_ITCNT     312   /* lp->it_cnt */
//#define LPX_K_TMLIM     313   /* lp->tm_lim */
//#define LPX_K_OUTFRQ    314   /* lp->out_frq */
//#define LPX_K_OUTDLY    315   /* lp->out_dly */
//#define LPX_K_BRANCH    316   /* lp->branch */
//#define LPX_K_BTRACK    317   /* lp->btrack */
//#define LPX_K_TOLINT    318   /* lp->tol_int */
//#define LPX_K_TOLOBJ    319   /* lp->tol_obj */
//#define LPX_K_MPSINFO   320   /* lp->mps_info */
//#define LPX_K_MPSOBJ    321   /* lp->mps_obj */
//#define LPX_K_MPSORIG   322   /* lp->mps_orig */
//#define LPX_K_MPSWIDE   323   /* lp->mps_wide */
//#define LPX_K_MPSFREE   324   /* lp->mps_free */
//#define LPX_K_MPSSKIP   325   /* lp->mps_skip */
//#define LPX_K_LPTORIG   326   /* lp->lpt_orig */
//#define LPX_K_PRESOL    327   /* lp->presol */
//#define LPX_K_BINARIZE  328   /* lp->binarize */
//#define LPX_K_USECUTS   329   /* lp->use_cuts */
//#define LPX_K_BFTYPE    330   /* lp->bfcp->type */
//#define LPX_K_MIPGAP    331   /* lp->mip_gap */

//#define LPX_C_COVER     0x01  /* mixed cover cuts */
//#define LPX_C_CLIQUE    0x02  /* clique cuts */
//#define LPX_C_GOMORY    0x04  /* Gomory's mixed integer cuts */
//#define LPX_C_MIR       0x08  /* mixed integer rounding cuts */
//#define LPX_C_ALL       0xFF  /* all cuts */

//typedef struct
//{     /* this structure contains results reported by the routines which
//         checks Karush-Kuhn-Tucker conditions (for details see comments
//         to those routines) */
//      /*--------------------------------------------------------------*/
//      /* xR - A * xS = 0 (KKT.PE) */
//      double pe_ae_max;
//      /* largest absolute error */
//      int    pe_ae_row;
//      /* number of row with largest absolute error */
//      double pe_re_max;
//      /* largest relative error */
//      int    pe_re_row;
//      /* number of row with largest relative error */
//      int    pe_quality;
//      /* quality of primal solution:
//         'H' - high
//         'M' - medium
//         'L' - low
//         '?' - primal solution is wrong */
//      /*--------------------------------------------------------------*/
//      /* l[k] <= x[k] <= u[k] (KKT.PB) */
//      double pb_ae_max;
//      /* largest absolute error */
//      int    pb_ae_ind;
//      /* number of variable with largest absolute error */
//      double pb_re_max;
//      /* largest relative error */
//      int    pb_re_ind;
//      /* number of variable with largest relative error */
//      int    pb_quality;
//      /* quality of primal feasibility:
//         'H' - high
//         'M' - medium
//         'L' - low
//         '?' - primal solution is infeasible */
//      /*--------------------------------------------------------------*/
//      /* A' * (dR - cR) + (dS - cS) = 0 (KKT.DE) */
//      double de_ae_max;
//      /* largest absolute error */
//      int    de_ae_col;
//      /* number of column with largest absolute error */
//      double de_re_max;
//      /* largest relative error */
//      int    de_re_col;
//      /* number of column with largest relative error */
//      int    de_quality;
//      /* quality of dual solution:
//         'H' - high
//         'M' - medium
//         'L' - low
//         '?' - dual solution is wrong */
//      /*--------------------------------------------------------------*/
//      /* d[k] >= 0 or d[k] <= 0 (KKT.DB) */
//      double db_ae_max;
//      /* largest absolute error */
//      int    db_ae_ind;
//      /* number of variable with largest absolute error */
//      double db_re_max;
//      /* largest relative error */
//      int    db_re_ind;
//      /* number of variable with largest relative error */
//      int    db_quality;
//      /* quality of dual feasibility:
//         'H' - high
//         'M' - medium
//         'L' - low
//         '?' - dual solution is infeasible */
//      /*--------------------------------------------------------------*/
//      /* (x[k] - bound of x[k]) * d[k] = 0 (KKT.CS) */
//      double cs_ae_max;
//      /* largest absolute error */
//      int    cs_ae_ind;
//      /* number of variable with largest absolute error */
//      double cs_re_max;
//      /* largest relative error */
//      int    cs_re_ind;
//      /* number of variable with largest relative error */
//      int    cs_quality;
//      /* quality of complementary slackness:
//         'H' - high
//         'M' - medium
//         'L' - low
//         '?' - primal and dual solutions are not complementary */
//} LPXKKT;
//
//#define lpx_create_prob _dglp_lpx_create_prob
//LPX *lpx_create_prob(void);
///* create problem object */
//
//#define lpx_set_prob_name _dglp_lpx_set_prob_name
//void lpx_set_prob_name(LPX *lp, const char *name);
///* assign (change) problem name */
//
//#define lpx_set_obj_name _dglp_lpx_set_obj_name
//void lpx_set_obj_name(LPX *lp, const char *name);
///* assign (change) objective function name */
//
//#define lpx_set_obj_dir _dglp_lpx_set_obj_dir
//void lpx_set_obj_dir(LPX *lp, int dir);
///* set (change) optimization direction flag */
//
//#define lpx_add_rows _dglp_lpx_add_rows
//int lpx_add_rows(LPX *lp, int nrs);
///* add new rows to problem object */
//
//#define lpx_add_cols _dglp_lpx_add_cols
//int lpx_add_cols(LPX *lp, int ncs);
///* add new columns to problem object */
//
//#define lpx_set_row_name _dglp_lpx_set_row_name
//void lpx_set_row_name(LPX *lp, int i, const char *name);
///* assign (change) row name */
//
//#define lpx_set_col_name _dglp_lpx_set_col_name
//void lpx_set_col_name(LPX *lp, int j, const char *name);
///* assign (change) column name */
//
//#define lpx_set_row_bnds _dglp_lpx_set_row_bnds
//void lpx_set_row_bnds(LPX *lp, int i, int type, double lb, double ub);
///* set (change) row bounds */
//
//#define lpx_set_col_bnds _dglp_lpx_set_col_bnds
//void lpx_set_col_bnds(LPX *lp, int j, int type, double lb, double ub);
///* set (change) column bounds */
//
//#define lpx_set_obj_coef _dglp_lpx_set_obj_coef
//void lpx_set_obj_coef(dglp_prob *lp, int j, double coef);
///* set (change) obj. coefficient or constant term */
//
//#define lpx_set_mat_row _dglp_lpx_set_mat_row
//void lpx_set_mat_row(LPX *lp, int i, int len, const int ind[],
//      const double val[]);
///* set (replace) row of the constraint matrix */
//
//#define lpx_set_mat_col _dglp_lpx_set_mat_col
//void lpx_set_mat_col(LPX *lp, int j, int len, const int ind[],
//      const double val[]);
///* set (replace) column of the constraint matrix */
//
//#define lpx_load_matrix _dglp_lpx_load_matrix
//void lpx_load_matrix(LPX *lp, int ne, const int ia[], const int ja[],
//      const double ar[]);
///* load (replace) the whole constraint matrix */
//
//#define lpx_del_rows _dglp_lpx_del_rows
//void lpx_del_rows(LPX *lp, int nrs, const int num[]);
///* delete specified rows from problem object */
//
//#define lpx_del_cols _dglp_lpx_del_cols
//void lpx_del_cols(LPX *lp, int ncs, const int num[]);
///* delete specified columns from problem object */
//
//#define lpx_delete_prob _dglp_lpx_delete_prob
//void lpx_delete_prob(LPX *lp);
///* delete problem object */
//
//#define lpx_get_prob_name _dglp_lpx_get_prob_name
//const char *lpx_get_prob_name(LPX *lp);
///* retrieve problem name */
//
//#define lpx_get_obj_name _dglp_lpx_get_obj_name
//const char *lpx_get_obj_name(LPX *lp);
///* retrieve objective function name */
//
//#define lpx_get_obj_dir _dglp_lpx_get_obj_dir
//int lpx_get_obj_dir(LPX *lp);
///* retrieve optimization direction flag */
//
//#define lpx_get_num_rows _dglp_lpx_get_num_rows
//int lpx_get_num_rows(LPX *lp);
///* retrieve number of rows */
//
//#define lpx_get_num_cols _dglp_lpx_get_num_cols
//int lpx_get_num_cols(LPX *lp);
///* retrieve number of columns */
//
//#define lpx_get_row_name _dglp_lpx_get_row_name
//const char *lpx_get_row_name(LPX *lp, int i);
///* retrieve row name */
//
//#define lpx_get_col_name _dglp_lpx_get_col_name
//const char *lpx_get_col_name(LPX *lp, int j);
///* retrieve column name */
//
//#define lpx_get_row_type _dglp_lpx_get_row_type
//int lpx_get_row_type(LPX *lp, int i);
///* retrieve row type */
//
//#define lpx_get_row_lb _dglp_lpx_get_row_lb
//double lpx_get_row_lb(LPX *lp, int i);
///* retrieve row lower bound */
//
//#define lpx_get_row_ub _dglp_lpx_get_row_ub
//double lpx_get_row_ub(LPX *lp, int i);
///* retrieve row upper bound */
//
//#define lpx_get_row_bnds _dglp_lpx_get_row_bnds
//void lpx_get_row_bnds(LPX *lp, int i, int *typx, double *lb,
//      double *ub);
///* retrieve row bounds */
//
//#define lpx_get_col_type _dglp_lpx_get_col_type
//int lpx_get_col_type(LPX *lp, int j);
///* retrieve column type */
//
//#define lpx_get_col_lb _dglp_lpx_get_col_lb
//double lpx_get_col_lb(LPX *lp, int j);
///* retrieve column lower bound */
//
//#define lpx_get_col_ub _dglp_lpx_get_col_ub
//double lpx_get_col_ub(LPX *lp, int j);
///* retrieve column upper bound */
//
//#define lpx_get_col_bnds _dglp_lpx_get_col_bnds
//void lpx_get_col_bnds(LPX *lp, int j, int *typx, double *lb,
//      double *ub);
///* retrieve column bounds */
//
//#define lpx_get_obj_coef _dglp_lpx_get_obj_coef
//double lpx_get_obj_coef(LPX *lp, int j);
///* retrieve obj. coefficient or constant term */
//
//#define lpx_get_num_nz _dglp_lpx_get_num_nz
//int lpx_get_num_nz(LPX *lp);
///* retrieve number of constraint coefficients */
//
//#define lpx_get_mat_row _dglp_lpx_get_mat_row
//int lpx_get_mat_row(LPX *lp, int i, int ind[], double val[]);
///* retrieve row of the constraint matrix */
//
//#define lpx_get_mat_col _dglp_lpx_get_mat_col
//int lpx_get_mat_col(LPX *lp, int j, int ind[], double val[]);
///* retrieve column of the constraint matrix */
//
//#define lpx_create_index _dglp_lpx_create_index
//void lpx_create_index(LPX *lp);
///* create the name index */
//
//#define lpx_find_row _dglp_lpx_find_row
//int lpx_find_row(LPX *lp, const char *name);
///* find row by its name */
//
//#define lpx_find_col _dglp_lpx_find_col
//int lpx_find_col(LPX *lp, const char *name);
///* find column by its name */
//
//#define lpx_delete_index _dglp_lpx_delete_index
//void lpx_delete_index(LPX *lp);
///* delete the name index */
//
//#define lpx_scale_prob _dglp_lpx_scale_prob
//void lpx_scale_prob(LPX *lp);
///* scale problem data */
//
//#define lpx_unscale_prob _dglp_lpx_unscale_prob
//void lpx_unscale_prob(LPX *lp);
///* unscale problem data */
//
//#define lpx_set_row_stat _dglp_lpx_set_row_stat
//void lpx_set_row_stat(LPX *lp, int i, int stat);
///* set (change) row status */
//
//#define lpx_set_col_stat _dglp_lpx_set_col_stat
//void lpx_set_col_stat(LPX *lp, int j, int stat);
///* set (change) column status */
//
//#define lpx_std_basis _dglp_lpx_std_basis
//void lpx_std_basis(LPX *lp);
///* construct standard initial LP basis */
//
//#define lpx_adv_basis _dglp_lpx_adv_basis
//void lpx_adv_basis(LPX *lp);
///* construct advanced initial LP basis */
//
//#define lpx_cpx_basis _dglp_lpx_cpx_basis
//void lpx_cpx_basis(LPX *lp);
///* construct Bixby's initial LP basis */
//
//#define lpx_simplex _dglp_lpx_simplex
//int lpx_simplex(LPX *lp);
///* easy-to-use driver to the simplex method */
//
//#define lpx_exact _dglp_lpx_exact
//int lpx_exact(LPX *lp);
///* easy-to-use driver to the exact simplex method */
//
//#define lpx_get_status _dglp_lpx_get_status
//int lpx_get_status(LPX *lp);
///* retrieve generic status of basic solution */
//
//#define lpx_get_prim_stat _dglp_lpx_get_prim_stat
//int lpx_get_prim_stat(LPX *lp);
///* retrieve primal status of basic solution */
//
//#define lpx_get_dual_stat _dglp_lpx_get_dual_stat
//int lpx_get_dual_stat(LPX *lp);
///* retrieve dual status of basic solution */
//
//#define lpx_get_obj_val _dglp_lpx_get_obj_val
//double lpx_get_obj_val(LPX *lp);
///* retrieve objective value (basic solution) */
//
//#define lpx_get_row_stat _dglp_lpx_get_row_stat
//int lpx_get_row_stat(LPX *lp, int i);
///* retrieve row status (basic solution) */
//
//#define lpx_get_row_prim _dglp_lpx_get_row_prim
//double lpx_get_row_prim(LPX *lp, int i);
///* retrieve row primal value (basic solution) */
//
//#define lpx_get_row_dual _dglp_lpx_get_row_dual
//double lpx_get_row_dual(LPX *lp, int i);
///* retrieve row dual value (basic solution) */
//
//#define lpx_get_row_info _dglp_lpx_get_row_info
//void lpx_get_row_info(LPX *lp, int i, int *tagx, double *vx,
//      double *dx);
///* obtain row solution information */
//
//#define lpx_get_col_stat _dglp_lpx_get_col_stat
//int lpx_get_col_stat(LPX *lp, int j);
///* retrieve column status (basic solution) */
//
//#define lpx_get_col_prim _dglp_lpx_get_col_prim
//double lpx_get_col_prim(LPX *lp, int j);
///* retrieve column primal value (basic solution) */
//
//#define lpx_get_col_dual _dglp_lpx_get_col_dual
//double lpx_get_col_dual(dglp_prob *lp, int j);
///* retrieve column dual value (basic solution) */
//
//#define lpx_get_col_info _dglp_lpx_get_col_info
//void lpx_get_col_info(LPX *lp, int j, int *tagx, double *vx,
//      double *dx);
///* obtain column solution information (obsolete) */
//
//#define lpx_get_ray_info _dglp_lpx_get_ray_info
//int lpx_get_ray_info(LPX *lp);
///* determine what causes primal unboundness */
//
//#define lpx_check_kkt _dglp_lpx_check_kkt
//void lpx_check_kkt(LPX *lp, int scaled, LPXKKT *kkt);
///* check Karush-Kuhn-Tucker conditions */
//
//#define lpx_warm_up _dglp_lpx_warm_up
//int lpx_warm_up(LPX *lp);
///* "warm up" LP basis */
//
//#define lpx_eval_tab_row _dglp_lpx_eval_tab_row
//int lpx_eval_tab_row(LPX *lp, int k, int ind[], double val[]);
///* compute row of the simplex table */
//
//#define lpx_eval_tab_col _dglp_lpx_eval_tab_col
//int lpx_eval_tab_col(LPX *lp, int k, int ind[], double val[]);
///* compute column of the simplex table */
//
//#define lpx_transform_row _dglp_lpx_transform_row
//int lpx_transform_row(LPX *lp, int len, int ind[], double val[]);
///* transform explicitly specified row */
//
//#define lpx_transform_col _dglp_lpx_transform_col
//int lpx_transform_col(LPX *lp, int len, int ind[], double val[]);
///* transform explicitly specified column */
//
//#define lpx_prim_ratio_test _dglp_lpx_prim_ratio_test
//int lpx_prim_ratio_test(LPX *lp, int len, const int ind[],
//      const double val[], int how, double tol);
///* perform primal ratio test */
//
//#define lpx_dual_ratio_test _dglp_lpx_dual_ratio_test
//int lpx_dual_ratio_test(LPX *lp, int len, const int ind[],
//      const double val[], int how, double tol);
///* perform dual ratio test */
//
//#define lpx_interior _dglp_lpx_interior
//int lpx_interior(LPX *lp);
///* easy-to-use driver to the interior point method */
//
//#define lpx_ipt_status _dglp_lpx_ipt_status
//int lpx_ipt_status(LPX *lp);
///* retrieve status of interior-point solution */
//
//#define lpx_ipt_obj_val _dglp_lpx_ipt_obj_val
//double lpx_ipt_obj_val(LPX *lp);
///* retrieve objective value (interior point) */
//
//#define lpx_ipt_row_prim _dglp_lpx_ipt_row_prim
//double lpx_ipt_row_prim(LPX *lp, int i);
///* retrieve row primal value (interior point) */
//
//#define lpx_ipt_row_dual _dglp_lpx_ipt_row_dual
//double lpx_ipt_row_dual(LPX *lp, int i);
///* retrieve row dual value (interior point) */
//
//#define lpx_ipt_col_prim _dglp_lpx_ipt_col_prim
//double lpx_ipt_col_prim(LPX *lp, int j);
///* retrieve column primal value (interior point) */
//
//#define lpx_ipt_col_dual _dglp_lpx_ipt_col_dual
//double lpx_ipt_col_dual(LPX *lp, int j);
///* retrieve column dual value (interior point) */
//
//#define lpx_set_class _dglp_lpx_set_class
//void lpx_set_class(LPX *lp, int klass);
///* set problem class */
//
//#define lpx_get_class _dglp_lpx_get_class
//int lpx_get_class(LPX *lp);
///* determine problem klass */
//
//#define lpx_set_col_kind _dglp_lpx_set_col_kind
//void lpx_set_col_kind(LPX *lp, int j, int kind);
///* set (change) column kind */
//
//#define lpx_get_col_kind _dglp_lpx_get_col_kind
//int lpx_get_col_kind(LPX *lp, int j);
///* retrieve column kind */
//
//#define lpx_get_num_int _dglp_lpx_get_num_int
//int lpx_get_num_int(LPX *lp);
///* retrieve number of integer columns */
//
//#define lpx_get_num_bin _dglp_lpx_get_num_bin
//int lpx_get_num_bin(LPX *lp);
///* retrieve number of binary columns */
//
//#define lpx_integer _dglp_lpx_integer
//int lpx_integer(LPX *lp);
///* easy-to-use driver to the branch-and-bound method */
//
//#define lpx_intopt _dglp_lpx_intopt
//int lpx_intopt(LPX *lp);
///* easy-to-use driver to the branch-and-bound method */
//
//#define lpx_mip_status _dglp_lpx_mip_status
//int lpx_mip_status(LPX *lp);
///* retrieve status of MIP solution */
//
//#define lpx_mip_obj_val _dglp_lpx_mip_obj_val
//double lpx_mip_obj_val(LPX *lp);
///* retrieve objective value (MIP solution) */
//
//#define lpx_mip_row_val _dglp_lpx_mip_row_val
//double lpx_mip_row_val(LPX *lp, int i);
///* retrieve row value (MIP solution) */
//
//#define lpx_mip_col_val _dglp_lpx_mip_col_val
//double lpx_mip_col_val(LPX *lp, int j);
///* retrieve column value (MIP solution) */
//
//#define lpx_check_int _dglp_lpx_check_int
//void lpx_check_int(LPX *lp, LPXKKT *kkt);
///* check integer feasibility conditions */
//
//#define lpx_reset_parms _dglp_lpx_reset_parms
//void lpx_reset_parms(LPX *lp);
///* reset control parameters to default values */
//
//#define lpx_set_int_parm _dglp_lpx_set_int_parm
//void lpx_set_int_parm(LPX *lp, int parm, int val);
///* set (change) integer control parameter */
//
//#define lpx_get_int_parm _dglp_lpx_get_int_parm
//int lpx_get_int_parm(LPX *lp, int parm);
///* query integer control parameter */
//
//#define lpx_set_real_parm _dglp_lpx_set_real_parm
//void lpx_set_real_parm(LPX *lp, int parm, double val);
///* set (change) real control parameter */
//
//#define lpx_get_real_parm _dglp_lpx_get_real_parm
//double lpx_get_real_parm(LPX *lp, int parm);
///* query real control parameter */
//
//#define lpx_read_mps _dglp_lpx_read_mps
//LPX *lpx_read_mps(const char *fname);
///* read problem data in fixed MPS format */
//
//#define lpx_write_mps _dglp_lpx_write_mps
//int lpx_write_mps(LPX *lp, const char *fname);
///* write problem data in fixed MPS format */
//
//#define lpx_read_bas _dglp_lpx_read_bas
//int lpx_read_bas(LPX *lp, const char *fname);
///* read LP basis in fixed MPS format */
//
//#define lpx_write_bas _dglp_lpx_write_bas
//int lpx_write_bas(LPX *lp, const char *fname);
///* write LP basis in fixed MPS format */
//
//#define lpx_read_freemps _dglp_lpx_read_freemps
//LPX *lpx_read_freemps(const char *fname);
///* read problem data in free MPS format */
//
//#define lpx_write_freemps _dglp_lpx_write_freemps
//int lpx_write_freemps(LPX *lp, const char *fname);
///* write problem data in free MPS format */
//
//#define lpx_read_cpxlp _dglp_lpx_read_cpxlp
//LPX *lpx_read_cpxlp(const char *fname);
///* read problem data in CPLEX LP format */
//
//#define lpx_write_cpxlp _dglp_lpx_write_cpxlp
//int lpx_write_cpxlp(LPX *lp, const char *fname);
///* write problem data in CPLEX LP format */
//
//#define lpx_read_model _dglp_lpx_read_model
//LPX *lpx_read_model(const char *model, const char *data,
//      const char *output);
///* read LP/MIP model written in GNU MathProg language */
//
//#define lpx_print_prob _dglp_lpx_print_prob
//int lpx_print_prob(LPX *lp, const char *fname);
///* write problem data in plain text format */
//
//#define lpx_print_sol _dglp_lpx_print_sol
//int lpx_print_sol(LPX *lp, const char *fname);
///* write LP problem solution in printable format */
//
//#define lpx_print_sens_bnds _dglp_lpx_print_sens_bnds
//int lpx_print_sens_bnds(LPX *lp, const char *fname);
///* write bounds sensitivity information */
//
//#define lpx_print_ips _dglp_lpx_print_ips
//int lpx_print_ips(LPX *lp, const char *fname);
///* write interior point solution in printable format */
//
//#define lpx_print_mip _dglp_lpx_print_mip
//int lpx_print_mip(LPX *lp, const char *fname);
///* write MIP problem solution in printable format */
//
//#define lpx_is_b_avail _dglp_lpx_is_b_avail
//int lpx_is_b_avail(LPX *lp);
///* check if LP basis is available */
//
//#define lpx_write_pb _dglp_lpx_write_pb
//int lpx_write_pb(LPX *lp, const char *fname, int normalized,
//      int binarize);
///* write problem data in (normalized) OPB format */
//
//#define lpx_main _dglp_lpx_main
//int lpx_main(int argc, const char *argv[]);
///* stand-alone LP/MIP solver */

#ifdef __cplusplus
}
#endif

#endif

/* eof */
