#include <QtGui>

#include "random.h"
#include "farmWidget.h"
#include "spreadsheet.h"
#include <QHBoxLayout>
#include <qlabel.h>
#include <vector>
#include <RegPlot.h>

static QFont font0 = QFont("SansSerif",14, QFont::Bold);
static QFont font1 = QFont("Times",11, QFont::Bold);
static QFont font2 = QFont("Times",10, QFont::Bold);

Farmwidget::Farmwidget(int i,QWidget *par)
    :plotn(i),parent(static_cast<QMainWindow*>(par)),QWidget(par)
{
    curIter=0;
    toSort=true;
    if(i==0) {
        mparent  = static_cast<Mydatwindow*>(parent);
        Title *lab= new Title(Qt::AlignCenter, font0, par);
        lab->setText("Schlussbilanz");
        vector <QString> vs;
        vs.push_back("Position");
        vs.push_back(QString("Betrag (")+QChar(8364)+")");
        int rows= mparent->sAktivas.size();
        SpreadSheet *ss1= new SpreadSheet(rows,2,true,vs,false, true);
        Title *sublab1 = new Title(Qt::AlignCenter, font1, par);
        //QFont("Times",11,QFont::Bold),par);
        sublab1->setText("Aktiva");

        vector<QString> ss=mparent->sAktivas;
        vector<double> ds= mparent->aktivasI[0];
        ss1->updateColumn(0,ss);
        ss1->updateColumn(1,ds,1);

        QVBoxLayout *v1=new QVBoxLayout;
        v1->addWidget(sublab1);
        v1->addWidget(ss1);
        v1->addStretch();

        SpreadSheet *ss2= new SpreadSheet(rows,2,true, vs,false,true);
        Title *sublab2 = new Title(Qt::AlignCenter, font1, par);
        //QFont("Times",11,QFont::Bold),par);
        sublab2->setText("Passiva");

        ds = mparent->passivasI[0];
        ss = mparent->sPassivas;
        ss2->updateColumn(0,ss);
        ss2->updateColumn(1,ds,2);

        spreadSheets.push_back(ss1);
        spreadSheets.push_back(ss2);

        QVBoxLayout *v2=new QVBoxLayout;
        v2->addWidget(sublab2);
        v2->addWidget(ss2);
        v2->addStretch();

        QVBoxLayout *vlout= new QVBoxLayout;
        vlout->addWidget(lab);

        QHBoxLayout *hlout = new QHBoxLayout;
        hlout->addStretch();
        hlout->addLayout(v1);
        hlout->addLayout(v2);
        hlout->addStretch();

        vlout->addSpacing(20);
        vlout->addLayout(hlout);
        vlout->addStretch();
        setLayout(vlout);
    }else if (i==1){
        Title *lab= new Title(Qt::AlignCenter, font0, par);
        //QFont("SansSerif",15, QFont::Bold),par);
        lab->setText("Gewinn-/Verlustrechnung");

        mparent  = static_cast<Mydatwindow*>(parent);
        int rows = mparent->sErtraege.size();
        vector<QString> vs;
        vs.push_back("Position");
        vs.push_back(QString("Betrag (")+QChar(8364)+")");
        SpreadSheet *ss1= new SpreadSheet(rows,2,true, vs,false,true);
        Title *sublab1 = new Title(Qt::AlignCenter,font1, par);
        //QFont("Times",11,QFont::Bold),par);
        sublab1->setText("Erträge");//Betriebliche Erträge");

        QVBoxLayout *v1=new QVBoxLayout;
        v1->addWidget(sublab1);
        v1->addWidget(ss1);
        v1->addStretch();

        SpreadSheet *ss2= new SpreadSheet(rows, 2, true,vs,false,true);
        Title *sublab2 = new Title(Qt::AlignCenter, font1, par);
        //QFont("Times",11,QFont::Bold),par);
        sublab2->setText("Aufwendungen");

        QVBoxLayout *v2=new QVBoxLayout;
        v2->addWidget(sublab2);
        v2->addWidget(ss2);
        v2->addStretch();

        SpreadSheet *ss3= new SpreadSheet(rows, 2,true,vs,false,true);
        Title *sublab3 = new Title(Qt::AlignCenter,font1, par);
        //QFont("Times",11,QFont::Bold),par);
        sublab3->setText("Finanzergebnis");

        spreadSheets.push_back(ss1);
        spreadSheets.push_back(ss2);
        spreadSheets.push_back(ss3);

        ss1->updateColumn(0, mparent->sErtraege);
        ss2->updateColumn(0, mparent->sAufwendungen);
        ss3->updateColumn(0, mparent->sFinanz);

        ss1->updateColumn(1, mparent->ertraeges[0], rows-1-mparent->getGVRsizes()[0]);
        ss2->updateColumn(1, mparent->aufwendungens[0], rows-1-mparent->getGVRsizes()[1]);
        ss3->updateColumn(1, mparent->finanzs[0], rows-1-mparent->getGVRsizes()[2]);

        QVBoxLayout *v3=new QVBoxLayout;
        v3->addWidget(sublab3);
        v3->addWidget(ss3);
        v3->addStretch();

        QVBoxLayout *vlout= new QVBoxLayout;
        vlout->addWidget(lab);
        vlout->addSpacing(20);
        QHBoxLayout *hlout=new QHBoxLayout;
        hlout->addStretch();
        hlout->addLayout(v1);
        hlout->addLayout(v2);
        hlout->addLayout(v3);
        hlout->addStretch();
        vlout->addLayout(hlout);
        //vlout->addStretch();

        Title *sublab= new Title(Qt::AlignCenter, font2, par);
        //QFont("Times",10, QFont::Bold),par);
        sublab->setText("Gewinn/Verlust des Unternehmens:  ");
        gewinnLab = new Title(Qt::AlignCenter, font2, par);
        //QFont("Times",10, QFont::Bold),par);
        gewinnLab->setText(QLocale().toString(mparent->gewinns[0])+" "+QChar(8364));
        gewinnLab->setAlignment(Qt::AlignLeft);

        QHBoxLayout *hout = new QHBoxLayout;
        hout->addStretch();
        hout->addWidget(sublab);
        hout->addSpacing(50);
        hout->addWidget(gewinnLab);
        hout->addStretch();

        vlout->addLayout(hout);
        vlout->addStretch();

        setLayout(vlout);

    }else if (i==2){
        Title *l1=new Title(Qt::AlignCenter, font0, par);
        // QFont("SansSerif",15, QFont::Bold),par);
        l1->setText("Pachtbilanz");

        vector<QString> vs;
        vs.push_back("Runde");
        vs.push_back("ausgelaufene \n Pachtverträge [ha]");
        vs.push_back("zugepachtete \n Fläche [ha]");
        vs.push_back("zur Verfügung \n stehende Fläche [ha]");

        mparent  = static_cast<Mydatwindow*>(parent);
        int rows = mparent->maxIter;
        SpreadSheet *ss1= new SpreadSheet(rows,4,true, vs,false, false, true); //4 Spalten

        //ss1->initTab();
        vector<double> ds = mparent->pbDatasI[0];
        ss1->setRow(0, ds);

        spreadSheets.push_back(ss1);

        QVBoxLayout *vlout= new QVBoxLayout;
        vlout->addWidget(l1);
        vlout->addSpacing(20);

        QHBoxLayout *hout= new QHBoxLayout;
        hout->addStretch();
        hout->addWidget(ss1);
        hout->addStretch();

        vlout->addLayout(hout);
        vlout->addStretch();

        setLayout(vlout);
    }else if (i==3){
        Title *l1=new Title(Qt::AlignCenter, font0, par);
        //QFont("SansSerif",15, QFont::Bold),par);
        l1->setText("Kontoauszug");

        mparent  = static_cast<Mydatwindow*>(parent);
        Title *l2 = new Title(Qt::AlignLeft, font2, par);
        //QFont("Times",10, QFont::Bold),par);
        l2->setText("Alter Kontostand:  ");
        altStand = new Title(Qt::AlignCenter, font2, par);
        //QFont("Times",10, QFont::Bold),par);
        altStand->setText(QLocale().toString(mparent->altKontos[0], 'f' ,2)+" "+QChar(8364));
        QHBoxLayout *hout1 = new QHBoxLayout;
        hout1->addStretch();
        hout1->addWidget(l2);
        hout1->addSpacing(50);
        hout1->addWidget(altStand);
        hout1->addStretch();

        int rows = mparent->sHabens.size();
        vector<QString> vs;
        vs.push_back("Position");
        vs.push_back(QString("Betrag (")+QChar(8364)+")");
        SpreadSheet *ss1= new SpreadSheet(rows,2,true, vs,false,true);
        Title *sublab1 = new Title(Qt::AlignCenter,font1, par);
        //QFont("Times",11,QFont::Bold),par);
        sublab1->setText("Haben");

        QVBoxLayout *v1=new QVBoxLayout;
        v1->addWidget(sublab1);
        v1->addWidget(ss1);
        v1->addStretch();

        SpreadSheet *ss2= new SpreadSheet(rows, 2, true,vs,false,true);
        Title *sublab2 = new Title(Qt::AlignCenter, font1, par);
        //QFont("Times",11,QFont::Bold),par);
        sublab2->setText("Soll");

        QVBoxLayout *v2=new QVBoxLayout;
        v2->addWidget(sublab2);
        v2->addWidget(ss2);
        v2->addStretch();

        spreadSheets.push_back(ss1);
        spreadSheets.push_back(ss2);

        ss1->updateColumn(0, mparent->sHabens);
        ss2->updateColumn(0, mparent->sSolls);

        ss1->updateColumn(1, mparent->habens[0], rows-1-mparent->getKAsizes()[0]);
        ss2->updateColumn(1, mparent->solls[0], rows-1-mparent->getKAsizes()[1]);

        QVBoxLayout *vlout= new QVBoxLayout;
        vlout->addWidget(l1);
        vlout->addSpacing(20);
        vlout->addLayout(hout1);
        vlout->addStretch();

        QHBoxLayout *hlout=new QHBoxLayout;
        hlout->addStretch();
        hlout->addLayout(v1);
        hlout->addLayout(v2);
        hlout->addStretch();
        vlout->addLayout(hlout);
        //vlout->addStretch();

        Title *l3 = new Title(Qt::AlignLeft,font2, par);
        //QFont("Times",10,QFont::Bold),par);
        l3->setText("Neuer Kontostand:  ");
        neuStand = new Title(Qt::AlignCenter, font2, par);
        //QFont("Times",10, QFont::Bold),par);
        neuStand->setText(QLocale().toString(mparent->neuKontos[0], 'f', 2)+" "+QChar(8364));
        QHBoxLayout *hout2 = new QHBoxLayout;
        hout2->addStretch();
        hout2->addWidget(l3);
        hout2->addSpacing(50);
        hout2->addWidget(neuStand);
        hout2->addStretch();

        vlout->addLayout(hout2);

        /*
        vector<QString> vs;
        vs.push_back("Position");
        vs.push_back(QString("Haben (")+QChar(8364)+")");
        vs.push_back(QString("Soll (")+QChar(8364)+")");
        //vs.push_back("aha");

        int rows = parent->sKAs.size();
        SpreadSheet *ss1= new SpreadSheet(rows, 3,true, vs,false);
        vector<double> ds=parent->kaDatasI[0];
        vector<int> is = parent->kaColsI[0];
        ss1->updateColumn(0,parent->sKAs);
        ss1->updateColumn(ds, is);

        spreadSheets.push_back(ss1);

        Title *l3 = new Title(Qt::AlignLeft,font2, par);
        //QFont("Times",10,QFont::Bold),par);
        l3->setText("Neuer Kontostand:  ");
        neuStand = new Title(Qt::AlignCenter, font2, par);
        //QFont("Times",10, QFont::Bold),par);
        neuStand->setText(QLocale().toString(parent->neuKontos[0], 'f', 2)+" "+QChar(8364));
        QHBoxLayout *hout2 = new QHBoxLayout;
        hout2->addWidget(l3);
        hout2->addWidget(neuStand);

        QVBoxLayout *vlout= new QVBoxLayout;
        vlout->addWidget(l1);
        vlout->addSpacing(20);

        QHBoxLayout *hout= new QHBoxLayout;
        QVBoxLayout *vout = new QVBoxLayout;
        vout->addLayout(hout1);//Widget(l2);
        vout->addWidget(ss1);
        vout->addLayout(hout2);//addWidget(l3);
        vout->addStretch();
        hout->addStretch();
        hout->addLayout(vout);
        hout->addStretch();

        vlout->addLayout(hout);
        vlout->addStretch();
        //*/
        vlout->addStretch(3);
        setLayout(vlout);
    } else if (i==4){
            mparent  = static_cast<Mydatwindow*>(parent);
            Title *l1=new Title(Qt::AlignCenter, font0, par);
            //QFont("SansSerif",15, QFont::Bold),par);
            l1->setText("Betriebsspiegel");

            QVBoxLayout *vlout0= new QVBoxLayout;
            vlout0->addWidget(l1);
            vlout0->addSpacing(20);

            vlout= new QVBoxLayout;

            QGridLayout *g1 = new QGridLayout;
            QLabel *bnrNameLab = new QLabel("Betriebsnr.");
            bnrLab = new QLabel("0");
            QLabel *ageNameLab = new QLabel("Alter");
            ageLab = new QLabel("0");
            //g1->addWidget(bnrNameLab, 0, 0);
            //g1->addWidget(bnrLab, 0, 1);
            g1->addWidget(ageNameLab, 1, 0);
            g1->addWidget(ageLab, 1, 1);

            vlout->addLayout(g1);
            vlout->addSpacing(10);

            QGroupBox *generalGBox = new QGroupBox;
            generalGBox->setTitle(tr("Kenndaten"));
            setStyleSheet("QGroupBox {font: bold;}");// color: blue}");
            QLabel *ekNameLab = new QLabel(QString("Eigenkapital(")+QChar(0x20ac)+")");
            ekLab = new QLabel("1000");
            QLabel *ekrankNameLab = new QLabel("Ranking v Eigenkapital");
            ekrankLab = new QLabel(QString("Platz ")+QString::number(ekplatz)+" von "+QString::number(ektotal));
            QLabel *liqNameLab = new QLabel(QString("Liquidität(")+QChar(0x20ac)+")");
            liqLab = new QLabel("2000");
            QLabel *farmtypeNameLab = new QLabel(QString("Betriebstyp"));
            farmtypeLab = new QLabel("WAS");
            QLabel *rechtsformNameLab = new QLabel(QString("Rechtsform"));
            rechtsformLab = new QLabel("WAS");

            QGridLayout *g2 = new QGridLayout;
            g2->setVerticalSpacing(10);
            g2->addWidget(ekNameLab, 0, 0);
            g2->addWidget(ekLab, 0, 1);
            g2->addWidget(ekrankNameLab, 1, 0);
            g2->addWidget(ekrankLab, 1, 1);
            g2->addWidget(liqNameLab, 2, 0);
            g2->addWidget(liqLab, 2, 1);
            g2->addWidget(farmtypeNameLab, 3, 0);
            g2->addWidget(farmtypeLab, 3, 1);
            g2->addWidget(rechtsformNameLab, 4, 0);
            g2->addWidget(rechtsformLab, 4, 1);
            generalGBox->setLayout(g2);
            vlout->addWidget(generalGBox);
            vlout->addStretch();

            QGroupBox *laborGBox = new QGroupBox;
            laborGBox->setTitle(tr("Arbeitskräfte"));
            //setStyleSheet("QGroupBox {font: bold;}");// color: blue}");

            QLabel *sumAKLabName = new QLabel(QString("Gesamt-AK"));
            sumAKLab = new QLabel("100");
            QLabel *famAKLabName = new QLabel("     Familien-AK");
            famAKLab = new QLabel("22");
            QLabel *lohnAKLabName = new QLabel(QString("     Lohn-AK"));
            lohnAKLab = new QLabel("78");

            QGridLayout *g22 = new QGridLayout;
            g22->setVerticalSpacing(10);
            g22->addWidget(sumAKLabName, 0, 0);
            g22->addWidget(sumAKLab, 0, 1);
            g22->addWidget(famAKLabName, 1, 0);
            g22->addWidget(famAKLab, 1, 1);
            g22->addWidget(lohnAKLabName, 2, 0);
            g22->addWidget(lohnAKLab, 2, 1);
            laborGBox->setLayout(g22);
            vlout->addWidget(laborGBox);
            vlout->addStretch();

            QGroupBox *areaGBox = new QGroupBox;
            areaGBox->setTitle(tr("Flächenübersicht"));//Flächen-Restdauerverteilung"));
            {
                nsoils = gg->NO_OF_SOIL_TYPES;
                maxtime = gg->MAX_CONTRACT_LENGTH;
                soilnames = gg->NAMES_OF_SOIL_TYPES;

                QLabel *pachtLab = new QLabel("Restdauerverteilung der Pachtflächen");
                vector<int> restdauers;
                for (int i=1;i<=maxtime;++i)
                    restdauers.push_back(i);
                vector<QString> headers;
                headers.push_back("Restdauer(Jahr)");
                for (int i=0;i<nsoils;++i)
                    headers.push_back(QString("Umfang [ha]\n")+soilnames[i].c_str());
                rdvSS = new SpreadSheet(maxtime,nsoils+1,true,headers,false);
                rdvSS->updateColumn(0, restdauers);
                rdvSS->setMyMaxH(420);
                QHBoxLayout *hout = new QHBoxLayout;
                hout->addStretch();
                hout->addWidget(rdvSS);
                hout->addStretch();

                vector<QLabel*> soilNameLabs;
                QLabel *sumLab= new QLabel(QString("Flächen (ha)"));
                QLabel *ownLab= new QLabel("Eigentumsflächen (ha)");
                QLabel *rentLab= new QLabel("Pachtflächen (ha)");
                for (int i=0; i<nsoils;++i){
                    soilNameLabs.push_back(new QLabel(soilnames[i].c_str()));
                    sumSoilLabs.push_back(new QLabel);
                    ownSoilLabs.push_back(new QLabel);
                    rentSoilLabs.push_back(new QLabel);
                }
                for (int i=0; i<nsoils;++i){
                    sumSoilLabs[i]->setAlignment(Qt::AlignCenter);
                    ownSoilLabs[i]->setAlignment(Qt::AlignCenter);
                    rentSoilLabs[i]->setAlignment(Qt::AlignCenter);
                }

                QVBoxLayout *vout = new QVBoxLayout;
                QGridLayout *gout = new QGridLayout;

                int zz=1;
                gout->addWidget(sumLab, zz++,0);
                gout->addWidget(ownLab, zz++, 0);
                gout->addWidget(rentLab,zz++,0);

                zz=1;
                for (int i=0; i<nsoils;++i){
                    gout->addWidget(soilNameLabs[i], 0, i+1);
                    gout->addWidget(sumSoilLabs[i], zz++, i+1);
                    gout->addWidget(ownSoilLabs[i], zz++, i+1);
                    gout->addWidget(rentSoilLabs[i], zz++, i+1);
                    zz=1;
                }

                vout->addLayout(gout);
                vout->addSpacing(15);
                vout->addWidget(pachtLab);
                vout->addLayout(hout);
                //vout->addStretch();

                /*vout->addSpacing(10);
                QLabel *sumLab = new QLabel("Summe");
                QHBoxLayout *hout2 = new QHBoxLayout;
                hout2->addStretch();
                hout2->addWidget(sumLab);
                hout2->addStretch();
                for (int i=0; i<nsoils; ++i){
                    QLabel *lab = new QLabel("Hallo");
                    landLabs.push_back(lab);
                    hout2->addWidget(lab);
                    hout2->addStretch();
                }
                vout->addLayout(hout2);
                //*/

                areaGBox->setLayout(vout);
                areaGBox->setMaximumHeight(600); //in Programm-Hauptfenster
            }
            //vlout->addWidget(areaGBox);

            QGroupBox *pachtpreisGBox = new QGroupBox;
            pachtpreisGBox->setTitle(tr("Durchschnittliche Pachtpreise"));
            {
                vector<QString> headers;
                headers.push_back("Bodentyp");
                headers.push_back(QString("Preis (")+QChar(0x20ac)+"/ha)");
                pachtPreisSS = new SpreadSheet(nsoils, 2, true, headers,false);
                pachtPreisSS->updateColumn(0, soilnames);
                QHBoxLayout *hout = new QHBoxLayout;
                hout->addStretch();
                hout->addWidget(pachtPreisSS);
                hout->addStretch();
                pachtpreisGBox->setLayout(hout);
            }
            vlout->addWidget(pachtpreisGBox);
            vlout->addStretch();

            stallGBox = new QGroupBox;
            stallGBox->setTitle(tr("Gebäude"));//: Name genutzt (Kapazität) Alter(max. Nutzungsdauer)"));

            vlout->addWidget(stallGBox);

            vlout->addStretch();
            machineGBox = new QGroupBox;
            machineGBox->setTitle(tr("Maschinen"));//: Name Kapazität Alter(max. Nutzungsdauer)"));
            vlout->addWidget(machineGBox);
            vlout->addStretch(1);
            indexStall = vlout->indexOf(stallGBox);
            indexMachine = vlout->indexOf(machineGBox);

            //vlout->addStretch();
            QHBoxLayout *hh = new QHBoxLayout;
            hh->addStretch();
            hh->addLayout(vlout);
            //hh->addStretch();
            hh->addSpacing(15);
            QVBoxLayout *vv = new QVBoxLayout;
            //vv->addStretch();
            vv->addWidget(areaGBox,0, Qt::AlignBottom);
            vv->addStretch();
            hh->addLayout(vv);//addWidget(areaGBox,0,Qt::AlignCenter);//AlignTop);
            hh->addStretch();

            vlout0->addLayout(hh);
            setLayout(vlout0); //vlout0
        }
    else if (i==5) {
            iterAlt=0;
            mparent  = static_cast<Mydatwindow*>(parent);
            int c=Manager->Farm0->getFarmPlot()->PA.col;
            int r=Manager->Farm0->getFarmPlot()->PA.row;

            Title *l1=new Title(Qt::AlignCenter, font0, par);
            //QFont("SansSerif",15, QFont::Bold),par);
            l1->setText("Übersicht Pachtverträge");

            QLabel *sitzLab = new QLabel(QString("Betrieb: (%1, %2)").arg(QString::number(r)).arg(QString::number(c)));

            fwLout= new QVBoxLayout;
            QVBoxLayout *vlout0= fwLout; //new QVBoxLayout;

            vlout0->addWidget(l1);
            vlout0->addSpacing(20);

            sortfeld=FELD;

            sortingCBox=new QComboBox;
            sortingCBox->addItem("Feldlokation");
            sortingCBox->addItem("Bodentyp");
            sortingCBox->addItem("Preis");
            sortingCBox->addItem("Restdauer");

            connect(sortingCBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(updatePV(QString)));

            QLabel *lab = new QLabel("Sortieren nach  ");
            QHBoxLayout *hl = new QHBoxLayout;
            hl->addStretch();
            hl->addWidget(sitzLab);
            hl->addSpacing(20);
            hl->addWidget(lab);
            hl->addSpacing(5);
            hl->addWidget(sortingCBox);
            hl->addStretch();

            vlout0->addLayout(hl);
            vlout0->addSpacing(10);
            {
                nsoils = gg->NO_OF_SOIL_TYPES;
                maxtime = gg->MAX_CONTRACT_LENGTH;
                soilnames = gg->NAMES_OF_SOIL_TYPES;

                pvSS=mkpvSS();

                QHBoxLayout *hout = new QHBoxLayout;
                hout->addStretch();
                hout->addWidget(pvSS);
                hout->addStretch();

                vlout0->addLayout(hout);
                vlout0->addStretch();
                //pvSS->setMaximumHeight(600); //in Programm-Hauptfenster
            }
            //vlout->addWidget(areaGBox);

            setLayout(vlout0); //vlout0
    }else if (i==6){  //liq
        mparent  = static_cast<Mydatwindow*>(parent);
        Title *l1=new Title(Qt::AlignCenter, font0, par);
        // QFont("SansSerif",15, QFont::Bold),par);
        l1->setText("Liquidität");

        vector<QString> vs;
        vs.push_back("Runde");
        vs.push_back(QString("Liquidität (")+QChar(0x20ac)+")");

        int rows = mparent->maxIter;
        SpreadSheet *ss1= new SpreadSheet(rows, 2,true, vs,false, false, true); //4 Spalten

        //ss1->initTab();
        double d = mparent->liqdats[0];
        vector<double> ds;
        ds.push_back(0);
        ds.push_back(d);
        ss1->setRow(0, ds);

        spreadSheets.push_back(ss1);

        QVBoxLayout *vlout= new QVBoxLayout;
        vlout->addWidget(l1);
        vlout->addSpacing(20);

        QHBoxLayout *hout= new QHBoxLayout;
        hout->addStretch();
        hout->addWidget(ss1);
        hout->addStretch();

        vlout->addLayout(hout);
        vlout->addStretch();

        setLayout(vlout);
    }
    else if (i==7){  //
        dparent  = static_cast<Datawindow*>(parent);
        inited = false; //title-zeilen noch nicht da
        Title *l1=new Title(Qt::AlignCenter, font0, par);
        // QFont("SansSerif",15, QFont::Bold),par);
        l1->setText("Preisübersicht");

        vector<QString> vs;
        vs.push_back("Runde");
        vs.push_back(QString("preis (")+QChar(0x20ac)+")");
        for (int i=0;i<18;++i)
            vs.push_back("hallo");
        int refWidth = dparent->parentWidth;
        int rows = dparent->maxIter+1; //einheit
        SpreadSheet *ss1= new SpreadSheet(rows, 20,true, vs,false, false, true);
        spreadSheets.push_back(ss1);

        QVBoxLayout *vlout= new QVBoxLayout;
        vlout->addWidget(l1);
        vlout->addSpacing(20);
        ss1->setMyMaxW(refWidth);
        QHBoxLayout *hout= new QHBoxLayout;
        hout->addStretch();
        hout->addWidget(ss1);
        hout->addStretch();

        vlout->addLayout(hout);
        vlout->addStretch();

        setLayout(vlout);
    }
}

SpreadSheet* Farmwidget::mkpvSS(){
     vector<QString> headers;
     headers.push_back("Lfd-Nr");
     headers.push_back("FeldLokation\n (Zeile, Spalte)");
     headers.push_back("Bodentyp");
     headers.push_back(QString("Pachtpreis\n")+QChar(0x20ac)+"/ha");
     headers.push_back("Restdauer\n Jahr");

     pvSS = new SpreadSheet(50,5,true,headers,false);
     pvSS->setMyMaxH(600);
     return pvSS;
}

void Farmwidget::updatePV(QString str){
    if(!toSort) return;
    if (str=="Feldlokation")
        sortfeld=FELD;
    else if (str=="Bodentyp")
        sortfeld=BODEN;
    else if (str=="Preis")
        sortfeld = PREIS;
    else if (str=="Restdauer")
        sortfeld = RESTDAUER;
    else sortfeld = FELD;

    updatePV(sortfeld);
}

static bool compFeld(PlotData d1, PlotData d2){
    if (d1.row==d2.row)
        return d1.col<d2.col;
    else
        return d1.row<d2.row;
}

static bool compBoden(PlotData d1, PlotData d2){
    return d1.soiltype<d2.soiltype;
}

static bool compPreis(PlotData d1, PlotData d2){
    return d1.price<d2.price;
}

static bool compRestdauer(PlotData d1, PlotData d2){
    return d1.resttime<d2.resttime;
}

static int icompFeld(const void* d1, const void* d2){
    if ((*(PlotData*)d1).row==(*(PlotData*)d2).row)
        return ((*(PlotData*)d1).col-(*(PlotData*)d2).col);
    else
        return (*(PlotData*)d1).row-(*(PlotData*)d2).row;
}

static int icompBoden(const void* d1, const void* d2){
    return (*(PlotData*)d1).soiltype-(*(PlotData*)d2).soiltype;
}

static int icompPreis(const void* d1, const void* d2){
    return (*(PlotData*)d1).price-(*(PlotData*)d2).price;
}

static int icompRestdauer(const void* d1, const void* d2){
    return (*(PlotData*)d1).resttime-(*(PlotData*)d2).resttime;
}

static bool compFeldp(PlotData* d1, PlotData* d2){
    if ((*d1).row==(*d2).row)
        return (*d1).col<(*d2).col;
    else
        return (*d1).row<(*d2).row;
}

static bool compBodenp(PlotData* d1, PlotData* d2){
    return (*d1).soiltype<(*d2).soiltype;
}

static bool compPreisp(PlotData* d1, PlotData* d2){
    return (*d1).price<(*d2).price;
}

static bool compRestdauerp(PlotData* d1, PlotData* d2){
    return (*d1).resttime<(*d2).resttime;
}

void Farmwidget::updatePV(SORTINGFELD f){
        //typedef bool (*fp)(PlotData, PlotData);
        pds=pdss[curIter];
        int sortint;
        switch(f) {
        case FELD:
            sortint=0;
            //sort(pds.begin(),pds.end(),compFeld);
            qsort(&pds[0],pds.size(),sizeof(PlotData),icompFeld);
            break;
        case BODEN:
            sortint=1;
            //sort(pds.begin(),pds.end(),compBoden);
            qsort(&pds[0],pds.size(),sizeof(PlotData),icompBoden);
            break;
        case PREIS:
            sortint=2;
            //sort(pds.begin(),pds.end(),compPreis);
            qsort(&pds[0],pds.size(),sizeof(PlotData),icompPreis);
            break;
        case RESTDAUER:
            sortint=3;
            //sort(pds.begin(),pds.end(),compRestdauer);
            qsort(&pds[0],pds.size(),sizeof(PlotData),icompRestdauer);
            break;
        default:sortint=0;
           //sort(pds.begin(),pds.end(),compFeld);
           qsort(&pds[0],pds.size(),sizeof(PlotData),icompFeld);
        }

       pdss[curIter]=pds;
       pvSSs[curIter].second=sortint;

       int sz  = pvSS->rowCount();
        vector<QString> pvstr;
        for (int i=0; i<sz; ++i){
            pvstr.clear();
            PlotData pd = pds[i];
            pvstr.push_back(QString::number(i+1));
            pvstr.push_back(QString("(")+QString::number(pd.row)+", "+QString::number(pd.col)+")");
            pvstr.push_back(soilnames[pd.soiltype].c_str());
            pvstr.push_back(QLocale().toString(pd.price,'f',2));
            pvstr.push_back(QString::number(pd.resttime));
            pvSSs[curIter].first->setRow(i, pvstr, true);
        }
}

void Farmwidget::updatePV0(SORTINGFELD f){
        //typedef bool (*fp)(PlotData, PlotData);
        ppds=ppdss[curIter];
        int sortint;
        switch(f) {
        case FELD:
            sortint=0;
            sort(ppds.begin(),ppds.end(),compFeldp);
            break;
        case BODEN:
            sortint=1;
            sort(ppds.begin(),ppds.end(),compBodenp);
            break;
        case PREIS:
            sortint=2;
            sort(ppds.begin(),ppds.end(),compPreisp);
            break;
        case RESTDAUER:
            sortint=3;
            sort(ppds.begin(),ppds.end(),compRestdauerp);
            break;
        default:sortint=0;
           sort(ppds.begin(),ppds.end(),compFeldp);
        }

       ppdss[curIter]=ppds;
       pvSSs[curIter].second=sortint;

       int sz  = pvSS->rowCount();
        vector<QString> pvstr;
        for (int i=0; i<sz; ++i){
            pvstr.clear();
            PlotData pd = (*ppds[i]);
            pvstr.push_back(QString::number(i+1));
            pvstr.push_back(QString("(")+QString::number(pd.row)+", "+QString::number(pd.col)+")");
            pvstr.push_back(soilnames[pd.soiltype].c_str());
            pvstr.push_back(QLocale().toString(pd.price,'f',2));
            pvstr.push_back(QString::number(pd.resttime));
            pvSSs[curIter].first->setRow(i, pvstr, true);
        }
}

void Farmwidget::updateData(){
    if(plotn==0){
        SpreadSheet *ss1= spreadSheets[0];
        SpreadSheet *ss2= spreadSheets[1];
        vector<double> ds=mparent->aktivasI[mparent->aktivasI.size()-1];
        ss1->updateColumn(1,ds,1);
        ds=mparent->passivasI[mparent->passivasI.size()-1];
        ss2->updateColumn(1,ds,2);
    }
    else if (plotn==1){
        int iter = mparent->ertraeges.size()-1;
        SpreadSheet *ss1= spreadSheets[0];
        SpreadSheet *ss2= spreadSheets[1];
        SpreadSheet *ss3= spreadSheets[2];
        ss1->updateColumn(1,mparent->ertraeges[iter],ss1->rowCount()-1-mparent->getGVRsizes()[0]);
        ss2->updateColumn(1,mparent->aufwendungens[iter],ss2->rowCount()-1-mparent->getGVRsizes()[1]);
        ss3->updateColumn(1,mparent->finanzs[iter],ss3->rowCount()-1-mparent->getGVRsizes()[2]);
        gewinnLab->setText(QLocale().toString(mparent->gewinns[iter], 'f', 2)+" "+QChar(8364));

    }else if (plotn==2){
        SpreadSheet *ss1= spreadSheets[0];
        vector<double> ds=mparent->pbDatasI[mparent->pbDatasI.size()-1];
        ss1->setRow(mparent->pbDatasI.size()-1, ds);
        ss1->setCell(mparent->pbDatasI.size()-1,0, QString::number(ds[0]));

    } else if (plotn==3){
        /*int r = mparent->kaDatasI.size();
        vector<double> ds= mparent->kaDatasI[r-1];
        vector<int> is = mparent->kaColsI[r-1];
        ss1->updateColumn(ds,is);
        //*/
        int iter = mparent->habens.size()-1;
        SpreadSheet *ss1= spreadSheets[0];
        SpreadSheet *ss2= spreadSheets[1];
        ss1->updateColumn(1,mparent->habens[iter],ss1->rowCount()-1-mparent->getKAsizes()[0]);
        ss2->updateColumn(1,mparent->solls[iter],ss2->rowCount()-1-mparent->getKAsizes()[1]);
        altStand->setText(QLocale().toString(mparent->altKontos[iter], 'f', 2)+" "+QChar(8364));
        neuStand->setText(QLocale().toString(mparent->neuKontos[iter], 'f', 2)+" "+QChar(8364));

    } else if (plotn==4){
        int r = mparent->bsdats.size()-1;
        ekLab->setText(QLocale().toString(mparent->bsdats[r].ek,'f',2)) ;
        liqLab->setText(QLocale().toString(mparent->bsdats[r].liq,'f', 2)) ;
        ekrankLab->setText(QString("Platz ")+ QString::number(mparent->bsdats[r].EKrank+1) + " von "
                +QString::number(mparent->bsdats[r].EKtotal));

        QString cst = mparent->bsdats[r].isClosed ? "geschlossen":mparent->classnames[mparent->bsdats[r].farmtype].c_str() ;
        farmtypeLab->setText(QString("")+cst );
        cst = mparent->bsdats[r].isClosed ? "geschlossen":mparent->legnames[mparent->bsdats[r].legaltype].c_str();
        rechtsformLab->setText(QString("")+ cst);
//*/
        ageLab->setText(QString("")+QString::number(mparent->bsdats[r].farmage)+ " J.");
        //bnrLab->setText(QString::number(0)); //immer 0 spielerbetrieb
        lohnAKLab->setText(QLocale().toString(mparent->bsdats[r].isClosed ?0: mparent->bsdats[r].lohn_AK,'f',2));
        famAKLab->setText(QLocale().toString(mparent->bsdats[r].isClosed ?0:mparent->bsdats[r].fam_AK,'f',2));
        sumAKLab->setText(QLocale().toString(mparent->bsdats[r].lohn_AK+mparent->bsdats[r].fam_AK,'f',2));

        //Flächen
        for (int i=0; i<nsoils; ++i){
            rdvSS->updateColumn(i+1, mparent->bsdats[r].restdauerAreasOfSoils[i], true);
            //landLabs[i]->setText(QString::number(mparent->bsdats[r].landSums[i]));
            sumSoilLabs[i]->setText(QString::number(mparent->bsdats[r].landSums[i]));
            ownSoilLabs[i]->setText(QString::number(mparent->bsdats[r].ownLands[i]));
            rentSoilLabs[i]->setText(QString::number(mparent->bsdats[r].rentLands[i]));
        }


        //pachtpreis
        pachtPreisSS->updateColumn(1, mparent->bsdats[r].pachtPreisOfSoils);

        //stalls & machines
        updateStalls(r);
        updateMachines(r);

        vlout->insertWidget(indexStall,stallGBox);
        //vlout->addWidget(stallGBox);
        //vlout->addStretch();
        //vlout->addWidget(machineGBox);
        //vlout->addStretch(1);
        vlout->insertWidget(indexMachine,machineGBox);
    }
    else if (plotn==5){
        int r = mparent->pvdats.size()-1;
        curIter = r;
        iterAlt=r;
        pds = mparent->pvdats[r];
        sortfeld=FELD;
        toSort=false;
        sortingCBox->setCurrentIndex(0);
        toSort=true;
        //updatePV(sortfeld);

        int sz = pds.size();
        if (r!=0) {
          pvSS->setVisible(false);
          pvSS=mkpvSS();
          QHBoxLayout *hout = new QHBoxLayout;
          hout->addStretch();
          hout->addWidget(pvSS);
          hout->addStretch();
          fwLout->insertLayout(r+2, hout);
          //fwLout->addLayout(hout);
          //fwLout->addStretch();
          pvSS->setVisible(true);
        }
        pvSS->setRowCountMy(sz);

        //ppds.clear();
        vector<QString> pvstr;
        for (int i=0; i<sz; ++i){
            pvstr.clear();
            PlotData pd = pds[i];
            //ppds.push_back(&(mparent->pvdats[r][i]));
            pvstr.push_back(QString::number(i+1));
            pvstr.push_back(QString("(")+QString::number(pd.row)+", "+QString::number(pd.col)+")");
            pvstr.push_back(soilnames[pd.soiltype].c_str());
            pvstr.push_back(QLocale().toString(pd.price,'f',2));
            pvstr.push_back(QString::number(pd.resttime));
            pvSS->setRow(i, pvstr, true);
        }
        if(pvSSs.size()<r+1){
            pvSSs.push_back(pair<SpreadSheet*, int>(pvSS, 0));
            pdss.push_back(pds);
            //ppdss.push_back(ppds);
        }
        //*/
    }
    else if (plotn==6){
        SpreadSheet *ss1= spreadSheets[0];
        int it= mparent->liqdats.size()-1;
        vector<double> ds;
        double d=mparent->liqdats[it];
        ds.push_back(it);
        ds.push_back(d);
        ss1->setRow(mparent->liqdats.size()-1, ds);
        ss1->setCell(mparent->liqdats.size()-1,0, QString::number(ds[0]));
    }
    else if (plotn==7){
        SpreadSheet *ss1= spreadSheets[0];
        if (!inited){
            vector<QString> names = dparent->prodNames;
            vector<QString> units = dparent->prodUnits;
            QStringList nstr;
            nstr<<"Produktname\nPreiseinheit";

            //vector<QString> ustr;
            //ustr.push_back("Preiseinheit");
            int sz = names.size();
            for (int i=0; i<sz; ++i){
                nstr<<names[i]+"\n"+units[i];
                //ustr.push_back(units[i]);
            }
            ss1->setColCountMy(sz+1);
            ss1->setHorizontalHeaderLabels(nstr);
            //ss1->setRow(0, ustr);

            inited=true;
        }
        ss1->setMyMaxW(dparent->parentWidth);
        ss1->adjSize();
        int r = dparent->prodPricess.size()-1;
        vector<double> ps;
        ps.push_back(r+gg->FirstYear);
        vector<double> ps0 = dparent->prodPricess[r];
        for (int i=0;i<ps0.size();++i){
            ps.push_back(ps0[i]);
        }
        ss1->setRow(r,ps);
        ss1->setCell(r,0, QString::number(ps[0]));
        //*/
    }
}

void Farmwidget::updateData(int i){
    if(plotn==2||plotn==6||plotn==7) return;
    if(plotn==0){
        SpreadSheet *ss1= spreadSheets[0];
        SpreadSheet *ss2= spreadSheets[1];
        vector<double> ds=mparent->aktivasI[i];
        ss1->updateColumn(1,ds,1);
        ds=mparent->passivasI[i];
        ss2->updateColumn(1,ds,2);
    }

    else if (plotn==1){
        SpreadSheet *ss1= spreadSheets[0];
        SpreadSheet *ss2= spreadSheets[1];
        SpreadSheet *ss3= spreadSheets[2];
        ss1->updateColumn(1,mparent->ertraeges[i],ss1->rowCount()-1-mparent->getGVRsizes()[0]);
        ss2->updateColumn(1,mparent->aufwendungens[i],ss2->rowCount()-1-mparent->getGVRsizes()[1]);
        ss3->updateColumn(1,mparent->finanzs[i], ss3->rowCount()-1-mparent->getGVRsizes()[2]);
        gewinnLab->setText(QLocale().toString(mparent->gewinns[i], 'f', 2)+" "+QChar(8364));
    }//*/
    else if (plotn==3){
        /*vector<double> ds= parent->kaDatasI[i];
        vector<int> is = parent->kaColsI[i];
        ss1->updateColumn(ds,is);
        //*/
        SpreadSheet *ss1= spreadSheets[0];
        SpreadSheet *ss2= spreadSheets[1];
        ss1->updateColumn(1,mparent->habens[i],ss1->rowCount()-1-mparent->getKAsizes()[0]);
        ss2->updateColumn(1,mparent->solls[i],ss2->rowCount()-1-mparent->getKAsizes()[1]);
        altStand->setText(QLocale().toString(mparent->altKontos[i], 'f', 2)+" "+QChar(8364));
        neuStand->setText(QLocale().toString(mparent->neuKontos[i], 'f', 2)+" "+QChar(8364));
    }
    else if (plotn==4){
        //TODO
        int r = i;
        ekLab->setText(QLocale().toString(mparent->bsdats[r].ek,'f',2)) ;
        liqLab->setText(QLocale().toString(mparent->bsdats[r].liq,'f', 2)) ;
        ekrankLab->setText(QString("Platz ")+ QString::number(mparent->bsdats[r].EKrank+1) + " von "
                +QString::number(mparent->bsdats[r].EKtotal));

        QString cst = mparent->bsdats[r].isClosed ? "geschlossen":mparent->classnames[mparent->bsdats[i].farmtype].c_str() ;
        farmtypeLab->setText(QString("")+cst );
        cst = mparent->bsdats[r].isClosed ? "geschlossen":mparent->legnames[mparent->bsdats[i].legaltype].c_str();
        rechtsformLab->setText(QString("")+ cst);
//*/
        //farmtypeLab->setText(QString("")+parent->classnames[parent->bsdats[r].farmtype].c_str());
        //rechtsformLab->setText(QString("")+parent->legnames[parent->bsdats[r].legaltype].c_str());
        ageLab->setText(QString("")+QString::number(mparent->bsdats[r].farmage)+ " J.");
        //bnrLab->setText(QString::number(0)); //immer 0 spielerbetrieb
        lohnAKLab->setText(QLocale().toString(mparent->bsdats[r].isClosed ? 0: mparent->bsdats[r].lohn_AK,'f',2));
        famAKLab->setText(QLocale().toString(mparent->bsdats[r].isClosed ?0: mparent->bsdats[r].fam_AK,'f',2));
        sumAKLab->setText(QLocale().toString(mparent->bsdats[r].lohn_AK+mparent->bsdats[r].fam_AK,'f',2));

        //Flächen
        for (int x=0; x<nsoils; ++x){
            rdvSS->updateColumn(x+1, mparent->bsdats[r].restdauerAreasOfSoils[x], true);
            //landLabs[x]->setText(QString::number(mparent->bsdats[r].landSums[x]));
            sumSoilLabs[x]->setText(QString::number(mparent->bsdats[r].landSums[x]));
            ownSoilLabs[x]->setText(QString::number(mparent->bsdats[r].ownLands[x]));
            rentSoilLabs[x]->setText(QString::number(mparent->bsdats[r].rentLands[x]));
        }

        //pachtpreis
        pachtPreisSS->updateColumn(1, mparent->bsdats[r].pachtPreisOfSoils);
//*/
        //stalls & machines
        updateStalls(r);
        updateMachines(r);

        vlout->insertWidget(indexStall,stallGBox);
        vlout->insertWidget(indexMachine,machineGBox);
        //vlout->add Widget(stallGBox);
        //vlout->addWidget(machineGBox);
    }
    else if (plotn==5) {
        int r=i;
        curIter = r;
        pvSS=pvSSs[r].first;
        pvSSs[iterAlt].first->setVisible(false);
        pvSSs[r].first->setVisible(true);
        toSort=false;
        sortingCBox->setCurrentIndex(pvSSs[r].second);
        toSort=true;
        //repaint();
        iterAlt=r;
        /*
        pds = mparent->pvdats[r];
        sortfeld=FELD;
        sortingCBox->setCurrentIndex(0);
        //updatePV(sortfeld);

        int sz = pds.size();
        pvSS->setRowCountMy(sz);

        vector<QString> pvstr;
        for (int ii=0; ii<sz; ++ii){
            pvstr.clear();
            PlotData pd = pds[ii];
            pvstr.push_back(QString::number(ii+1));
            pvstr.push_back(QString("(")+QString::number(pd.row)+", "+QString::number(pd.col)+")");
            pvstr.push_back(soilnames[pd.soiltype].c_str());
            pvstr.push_back(QLocale().toString(pd.price,'f',2));
            pvstr.push_back(QString::number(pd.resttime));
            pvSS->setRow(ii, pvstr, true);
        }
        //*/
   }
   //*/
}

void Farmwidget::paintEvent(QPaintEvent *){
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    QPen p = painter.pen();
    p.setWidth(2);
    p.setColor(QColor("gray"));

    painter.setPen(p);
    QSize sz = layout()->sizeHint();
    painter.drawRect((width()-sz.width())/2,0,sz.width(),sz.height());

    QPixmap pixmap;
    int rz = randlong();
    int s = rz%5;
    switch(s){
    case 0: pixmap=QPixmap::fromImage(QImage(":/images/grafiken/Kuh-bg.png"));
            break;
    case 1: pixmap=QPixmap::fromImage(QImage(":/images/grafiken/KuhR-bg.png"));//Schwein.png"));
            break;
    case 2: pixmap=QPixmap::fromImage(QImage(":/images/grafiken/Schwein-bg.png"));//Traktor.png"));
            break;
    case 3: pixmap=QPixmap::fromImage(QImage(":/images/grafiken/Traktor-bg.png"));
            break;
    default:pixmap=QPixmap::fromImage(QImage(":/images/grafiken/Geld-bg.png"));//Geld.png"));
    }

  //SetBackgroundImage(mydatWindow->centralWidget(), new QPixmap(QPixmap::fromImage(QImage(":/images/grafiken/Kuh.png"))));
    painter.drawTiledPixmap(QRect(0,0, (width()-sz.width())/2-5, sz.height()+5),pixmap);//QPixmap(QPixmap::fromImage(QImage(":/images/grafiken/Kuh.png"))));
    painter.drawTiledPixmap(QRect((width()+sz.width())/2 +5,0, width(), sz.height()+5),pixmap);//QPixmap(QPixmap::fromImage(QImage(":/images/grafiken/Kuh.png"))));
    painter.drawTiledPixmap(QRect(0,sz.height()+5, width(),height()),pixmap);//QPixmap(QPixmap::fromImage(QImage(":/images/grafiken/Kuh.png"))));
}

void Farmwidget::updateMachines(int x){
    vector<QLabel*> machinenameLabels;
    vector<QLabel*> machineCapLabels;
    vector<QLabel*> machineAgeLabels;

    int sz = mparent->bsdats[x].machines.size();

    for (int i=0; i<sz; ++i){
      QString name = mparent->bsdats[x].machines[i].name.c_str();
      int cap = mparent->bsdats[x].machines[i].capacity;
      double age = mparent->bsdats[x].machines[i].age;
      double life = mparent->bsdats[x].machines[i].life;

      QLabel* lab= new QLabel();
      //lab->setAlignment(Qt::AlignRight);
      lab->setText(name);

        int mal = mparent->bsdats[x].machines[i].num;
        QString malStr;
        if (mal>1)
            malStr = QString("x")+QString::number(mal);
        else malStr = "";
      QLabel* labc= new QLabel();
      labc->setAlignment(Qt::AlignCenter);//:AlignRight);
      labc->setText(QString::number(cap,'f',0)+malStr);

      QLabel* labj= new QLabel();
      labj->setAlignment(Qt::AlignCenter);//Right);
      labj->setText(QString::number(age,'f',0)+ " J."
                + " ("+ QString::number(life, 'f',0)+ " J.)");

      machinenameLabels.push_back(lab);
      machineCapLabels.push_back(labc);
      machineAgeLabels.push_back(labj);
  }

  bool noMachine=false;
    if(machinenameLabels.size()==0){
        noMachine  = true;
        machinenameLabels.push_back(new QLabel("keine"));
        machineCapLabels.push_back(new QLabel());
        machineAgeLabels.push_back(new QLabel());
    }

    QGroupBox *tgbox = machineGBox;
    machineGBox = new QGroupBox;
    machineGBox->setTitle(tr("Maschinen"));//: Name Kapazität Alter(max. Nutzungsdauer)"));
    QGridLayout *machineLayout = new QGridLayout;

    int ii=0;
    if (!noMachine) {
        int zz=0;
        machineLayout->addWidget(new QLabel("Name"),ii, zz++);
        machineLayout->addWidget(new QLabel("Kapazität"),ii, zz++);
        machineLayout->addWidget(new QLabel("Alter(max. Nutzungsdauer)"),ii++, zz++);
    }
    for (int i=0;i<machinenameLabels.size();++i){
        machineLayout->addWidget(machinenameLabels[i],ii,0);
        machineLayout->addWidget(machineCapLabels[i],ii,1);
        machineLayout->addWidget(machineAgeLabels[i],ii,2);
        ++ii;
    }

    machineGBox->setLayout(machineLayout);
    if(tgbox) delete tgbox;
}

void Farmwidget::updateStalls(int x){
    vector<QLabel*> investnameLabels;
    vector<QLabel*> investLabels;
    vector<QLabel*> investCapLabels;
    vector<QLabel*> investAgeLabels;
    //vector<QLabel*> investLifeLabels;

    int sz = mparent->bsdats[x].stalls.size();

    for(int i=0;i<sz; ++i){
        QString name = mparent->bsdats[x].stalls[i].name.c_str();
        int cap = mparent->bsdats[x].stalls[i].capacity;
        double age = mparent->bsdats[x].stalls[i].age;
        double used = mparent->bsdats[x].stalls[i].used;
        double life = mparent->bsdats[x].stalls[i].life;

        QLabel* lab= new QLabel();
        //lab->setAlignment(Qt::AlignRight);
        lab->setText(name);

        QLabel * lab1 = new QLabel();
        lab1->setAlignment(Qt::AlignCenter);//Right);
        lab1->setText(QString::number(used,'f',0));

        QLabel *lab3= new QLabel();
        lab3->setAlignment(Qt::AlignCenter);//Right);
        lab3->setText(QString::number(used, 'f',0)+"(" +QString::number(cap)+")");

        investnameLabels.push_back(lab);
        investLabels.push_back(lab1);
        investCapLabels.push_back(lab3);

        QLabel *lab4 = new QLabel();
        lab4->setAlignment(Qt::AlignCenter);//Right);
        lab4->setText(QString::number(age,'f', 0)+" J." +" ("+QString::number(life,'f', 0)+" J.)");
        investAgeLabels.push_back(lab4);

        /*
        QLabel *lab5 = new QLabel();
        lab5->setAlignment(Qt::AlignRight);
        lab5->setText(QString::number(life,'f', 0)+" J.");
        investLifeLabels.push_back(lab5);
        //*/
      }

    bool noStalls = false;
    if(investnameLabels.size()==0){
        noStalls = true;
        investnameLabels.push_back(new QLabel("keine"));
        investLabels.push_back(new QLabel());
        investCapLabels.push_back(new QLabel());
        investAgeLabels.push_back(new QLabel());
        //investLifeLabels.push_back(new QLabel());
    }

    QGroupBox *tgbox = stallGBox;
    stallGBox = new QGroupBox;
    stallGBox->setTitle(tr("Gebäude"));//: Name genutzt (Kapazität) Alter(max. Nutzungsdauer)"));
    QGridLayout *investLayout = new QGridLayout;

    int ii=0;
    if (!noStalls) {
        int zz=0;
        investLayout->addWidget(new QLabel("Name"),ii, zz++);
        investLayout->addWidget(new QLabel("genutzt(Kapazität)"),ii, zz++);
        investLayout->addWidget(new QLabel("Alter(max. Nutzungsdauer)"),ii++, zz++);
    }

    for (int i=0;i<investnameLabels.size();++i){
        int j=0;
        investLayout->addWidget(investnameLabels[i],ii,j++);
        //investLayout->addWidget(investLabels[i],i,j++);
        investLayout->addWidget(investCapLabels[i],ii,j++);
        investLayout->addWidget(investAgeLabels[i],ii,j);
        //investLayout->addWidget(investLifelabes[i],i,j);
        ++ii;
    }

    stallGBox->setLayout(investLayout);
    if (tgbox) delete tgbox;
}

Title::Title(Qt::AlignmentFlag align, QFont f, QWidget*w):QLabel(w){
    this->setAlignment(align);
    this->setFont(f);
}
