#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDockWidget>
#include <QLineEdit>
#include <QComboBox>
#include <QPrinter>
#include <QUrl>
#include <QFile>
#include <QTime>

#include "simThread.h"
#include "decThread.h"

//#include "bodendialog.h"
#include "bodendialog1.h"
#include "investdialog1.h"
#include "closedialog1.h"
#include "Datawindow.h"
#include "Mydatwindow.h"

class QTextDocument;
class QAction;
class QLabel;
class QToolBar;
class QStatusBar;
class QDir;
class QFile;
class AnalogClock;
class DigitalClock;

class Landwindow;
class decIcon;
class QTextBrowser;
class PEwidget;
class Datawindow;

class Mainwindow : public QMainWindow
{
    Q_OBJECT
    Q_ENUMS(SimZustand);
    Q_ENUMS(DECS);
    Q_ENUMS(CWIDGET);
public:
    Mainwindow();
    void closeEvent(QCloseEvent*);
    enum DECS{PROG, BODDEC, INVDEC, CLODEC, CLODEC0, CLODEC1, ENDE};
    enum CWIDGET{LAND, REG, MY, POL, DEC};

    bool faOver() const;
    friend class InvestDialog;
    friend class BodenDialog1;
    friend class PE;
    friend class CloseDialog;
    friend class Datawindow;

signals:
    void zustand(SimZustand);

private slots:
    void enterEvent(QEvent *);
    void leaveEvent(QEvent *);
    void mouseMoveEvent(QMouseEvent *e);
    void getLWmouseMoveEvent(QMouseEvent *e);
    void raiseDecs();

    void getBusy();
    void faOutput();
    void changeDockColor(int);
    void getTimeout();

    void getShowBoden(int,int);
    void getShowBoden(int);
    void getShowClose(int);
    void getShowInvest();
    void updateMwidget();
    void updatePolitWindow();

    void showLandschaften();
    void showSectorDaten();
    void showMyData();
    void showPolicy();
    void showFA();

    void getdecsig();
    void getdecsigBoden(int);
    void getdecsigInv();
    void getdecsigAus();
    void updateWeiter();

    void getInitover();
    void getItOver(int);
    void getItBeginn();
    void getLAOver();
    void getLAsig(int,int);
    void getINVsig();
    void getProduced();
    void getPRODsig();
    void getFoFsig(int);
    void getPeriodResults();

    void updateToolBar(bool);
    void updateToolBar1(bool);
    void updateToolBar2(bool);

    void about();
    void hilfeFA();
    void hilfeBoden();
    void hilfeInvest();
    void hilfeAusst();
    void hilfeAblauf();
    void hilfeLand();

    void newRegion();
    void startOrPause();
    void stop();

    void beenden();
    void drucken();

    void pausen();
    void getFirstBids();
    void getSimOver();

    void initFiles();
    void closeFiles();
    void closeMessage(QCloseEvent*e=0);

private:
    QString dateTimeString(QDateTime) const;
    QString dateString(QDateTime) const;
    bool filesClosed;
    QFile file;
    QFile fileBoden, fileInv, fileAus, filePe;
    QTextStream tstr;
    QTextStream tstrBoden, tstrInv, tstrAus, tstrPe;
    QString decPEdat, decBodenDat,
            decInvDat,decAusDat, spielDat;
    QTime aTime;
    int dtime;
    vector<bool> spielPeProds;
    void setSpielPeProds(vector<bool>);
    vector<bool> getSpielPeProds() const;
    PEwidget* peWidget;
    void setPeWidget(PEwidget*);
    PEwidget* getPeWidget()const;

    void updateHilfe();
    void createActions();
    void createMenus();

    void createToolBars();
    void createStatusBar();

    void makeDocks();
    void makeCentralWidget();
    void makePolitWindow();

    void saveCWidget(CWIDGET);
    void reActGui(CWIDGET);
    void updateStatusLabel(CWIDGET);

  /*  void readSettings();
    void writeSettings();
//*/
volatile bool stoped;
volatile SimZustand SIM_ZUSTAND;

SimThread *sThread;
DecThread *dThread;

    QString inputDir;
    QString policyFile;

    QLabel *statusLabel;
    QAction *actLandschaften;
    QAction *actAbout;
    QAction *actQuit;
    QAction *actHilfeFA;
    QAction *actHilfeAblauf;
    QAction *actHilfeBoden;
    QAction *actHilfeInvest;
    QAction *actHilfeAusst;
    QAction *actHilfeLand;

    QAction *actNewRegion;
    QAction *actStartOrPause;
    QAction *actStop;

    QAction *actSectorData;
    QAction *actMyData;
    QAction *actPolicy;

    QAction *actPrint;
    QAction *actDec;

    QToolBar *mainToolBar;
    QToolBar *simToolBar;

    QMenu *progMenu;
    QMenu *hilfeMenu;

    //QFrame *dataWidget;
    vector<QWidget*> mwidgets;
    QScrollArea *dataWidget;
    Landwindow *landWindow;
    Datawindow *dataWindow;
    Mydatwindow *mydatWindow;
    QScrollArea *politWindow;
    vector<QString> politStrings;
    QString politHtml;
    QTextBrowser *politBrowser;

    //BodenDialog *bodenDialog;
    BodenDialog1 *bodenDialog1;
    InvestDialog *investDialog;
    CloseDialog *closeDialog;

    QDockWidget *myfarmDock;
    QLineEdit *myIdEdit;
    QComboBox *myCapComboBox;
    QLineEdit *myRentLandEdit;

    QDockWidget *simuDock;
    QLabel *simuIterLab;
    QLabel *yearLab;
    QLabel *bodLab, *invLab, *prodLab, *resLab, *closeLab;
    QLabel *decLab;

    QPrinter *printer;
    DECS decs;
    QWidget *dwidget;//f�r dataWidget
    CWIDGET cwidget;
    //AnalogClock *uhr;
    QString aktDecText;
    vector<decIcon*> simuIcons;
    int whichIcon;

    DigitalClock *dclock;
    QLabel *politItLabel;
    QTextBrowser *hilfeBrowser;
    QTextDocument *hilfeDoc;
    QString hilfeFileName;
    QUrl hilfeUrl;

    bool over;
    bool absicherFertig;
};



#endif // MAINWINDOW_H
