#include "investdialog1.h"
#include "AgriPoliS.h"
#include <QLineEdit>
#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QSignalMapper>
#include <QMessageBox>
#include <QGroupBox>
#include <QScrollArea>
#include <QCoreApplication>

#include "cell2.h"
#include "Mainwindow.h"

static QColor bgFinColor = Qt::cyan;

static QFont font0 = QFont("SansSerif",14, QFont::Bold);
static QFont font1 = QFont("Times",11, QFont::Bold);
static QFont font2 = QFont("Times",11, QFont::Normal);//Bold);
static QFont font3 = QFont("Times",10, QFont::Normal);

static const QColor MachineColor = QColor("red").light(180);
static const QColor StallColor = QColor("blue").light(180);
static const QColor LabColor = QColor("green").light(180);

InvestDialog::InvestDialog(Mainwindow* par):aktComb(0),numComb(1),firstGUI(true),inited(false),showPEwidget(false),parent(par),QDialog(){
    nurHead = false;
    inComb = false;
    setMinimumHeight(400);//920
    setMinimumWidth(600); //850
    setWindowIcon(QPixmap(":/images/grafiken/Schwein.png"));
    //widgetPE=new QWidget;
    toClose=false;
    hasPEwidget=false;
    setWindowTitle("Investition");
}

void InvestDialog::closeEvent(QCloseEvent * event){
    if (!( parent->faOver()||toClose)){
        event->ignore();
        return;
    }
    gg->HasInvestDec=true;
    inited=false;
    //Manager->getGlobals()->SIM_WEITER =true;
    Manager->product_cat0=Manager->Market->getProductCat();
    peWidget->close();
    //savePEs();
    QDialog::closeEvent(event);
}

void InvestDialog::updateComb(int ii){
    inComb = true;
    aktComb = indComb(ii);

    Title4* t4 = titles[ii];
    t4->setStyleSheet("QLabel { background-color : yellow ;}");

    updateHead();

  if (!nurHead) {
    farm0->myInvs = myInvs = combs[combInd(aktComb)].invs;
    int n = prodIDs.size();
    for (int r=0;r<n;++r){
        if (r==refpos) {
            double tind;
            tind = prodPrices[refpos]==0? 1:combs[combInd(aktComb)].pes[r]/prodPrices[refpos];
            getreidePEs.clear();
            int ts = getreideIDs.size();
            for (int i=0; i<ts; ++i){
                getreidePEs.push_back(Manager->product_cat0[getreideIDs[i]].getPriceExpectation());
            }
            for (int i=0; i<ts; ++i){
                Manager->product_cat0[getreideIDs[i]].setPriceExpectation(getreidePrices[i]*tind);
            }
        }
        Manager->product_cat0[prodIDs[r]].setPriceExpectation(combs[combInd(aktComb)].pes[r]);
    }
  }
    invRefs = combs[combInd(aktComb)].invRefs;
    updateFinWidget(aktComb);
    updateInvWidget(aktComb);
    updatePeWidget(aktComb);
    inComb=false;
}

void InvestDialog::updateCombs(){
    vector<double> ps, fs;
    vector<int> is, rs;

    switch(peVar){
    case 0: ps = manPEs; break;
    case 1: ps = naivPEs; break;
    case 2: ps = adaptPEs; break;
    default: ;
    }

    peWidget->updateMyPEs(ps,true);

    fs.push_back(langFK);
    fs.push_back(dispo);
    fs.push_back(saving);
    fs.push_back(GDB);
    fs.push_back(liquidity);
    fs.push_back(profit);
    fs.push_back(income);

    int n = invDSBs.size();
    for (int i=0; i<n; ++i){
        is.push_back(invDSBs[i]->value());
        //rs.push_back(invSS->cell(i,7)->data(Qt::DisplayRole).toInt());
    }
    Komb comb ;
    comb.fins = fs;
    comb.invs = is;
    comb.pes = ps;
    comb.invRefs= invRefs;

    myInvs = is;

    combs.push_back(comb); //ref
    combs.push_back(comb); //1
}

void InvestDialog::saveComb(int ic){
    saveCombPe(ic);
    saveCombInv(ic);
    saveCombFin(ic);
}

void InvestDialog::saveCombPe(int ic){
    vector<double> tvalues;
    switch(peVar){
    case 0: tvalues = manPEs; break;
    case 1: tvalues = naivPEs; break;
    case 2: tvalues = adaptPEs; break;
    default: ;
    }

    combs[combInd(ic)].pes = tvalues;
}

void InvestDialog::saveCombFin(int ic){
    vector<double> fs;
    fs.push_back(langFK);
    fs.push_back(dispo);
    fs.push_back(saving);
    fs.push_back(GDB);
    fs.push_back(liquidity);
    fs.push_back(profit);
    fs.push_back(income);

    combs[combInd(ic)].fins = fs;
}

void InvestDialog::saveCombInv(int ic){
    vector<int> is, rs;
    int n = invDSBs.size();
    for (int i=0; i<n; ++i){
        is.push_back(invDSBs[i]->value());
        //rs.push_back(invSS->cell(i,2)->data(Qt::DisplayRole).toInt());
    }

    combs[combInd(ic)].invs = is;
    combs[combInd(ic)].invRefs = invRefs;
}

void InvestDialog::delKomb(){
    int aktComb0 = aktComb;
    if (aktComb==0) {
        QMessageBox::warning(this, tr("Information"),
                         tr("Vorschlag kann nicht gel�scht werden."));
        return;
    }
    vector<struct Komb>::iterator it = combs.begin();
    combs.erase(it+combInd(aktComb));
    vector<QWidget*>::iterator itt = wins.begin()+combInd(aktComb);
    QWidget *qw = *itt;
    wins.erase(itt);
    delete qw;

    if (combInd(aktComb) == combs.size())
        aktComb=indComb(combInd(aktComb)-1);
    else
        aktComb=indComb(combInd(aktComb)+1);

    titles.erase(titles.begin()+combInd(aktComb0));

    vector<int>::iterator itt2 = plans.begin()+combInd(aktComb0);
    plans.erase(itt2);

    titles[combInd(aktComb)]->setStyleSheet("QLabel { background-color : yellow;}");

    updateHead();
    updateComb(combInd(aktComb));
    sa->widget()->adjustSize();
}

void InvestDialog::neuKomb(){
    int altNr = aktComb;
    titles[combInd(altNr)]->setStyleSheet("QLabel { background-color : lightgray;}");
    combs.push_back(combs[combInd(aktComb)]);
    aktComb = numComb;

    struct Komb cb = combs[combInd(altNr)];
    myInvs = cb.invs;
    invRefs = cb.invRefs;

    //QHBoxLayout *saHLout= static_cast<QHBoxLayout*> (sa->widget()->layout());

    createFinCopy(cb,aktComb);
    saHLout->addWidget(finW);
 
    QWidget *saWidget = new QWidget;
    saWidget->setContentsMargins(0,-5,0,-5);
    saWidget->setLayout(saHLout);
    sa->setWidget(saWidget);

    titles[combInd(aktComb)]->setStyleSheet("QLabel { background-color : yellow;}");

    ++numComb;
    updateHead();

    if (altNr==0) { //ref-plan
        for (int r=0; r<nRowInv;++r){
            QSpinBox *dsb = invDSBs[r];
            dsb->setEnabled(true);
        }
        peWidget->enableDSBs(true);
    }

    updateWeiterButton();
    //saHLout->activate();
    //saWidget->adjustSize();
}

static QString buttonText = QString("Ok && weiter mit Plan ");

void InvestDialog::updateWeiterButton(){
    weiterButton->setText(buttonText+QString::number(aktComb));
}

void InvestDialog::updateGUI(){
    //ITER  = gg->tIter;
    //if (firstGUI) farm0 = Manager->Farm0;

    langFK=farm0->getLangFKinv();
    dispo=farm0->getDispoInv();
    saving=farm0->getSavingInv();
    GDB=farm0->getGDBinv();
    liquidity=farm0->getLiqInv();
    profit=farm0->getProfInv();
    income=farm0->getIncInv();

    if (!firstGUI) {
        updateGUI2();
        return;
    }
    QString title("Entscheidungen �. Investitionen");

    Title4 *titleLab = new Title4(Qt::AlignCenter, font0);
    titleLab->setText(title);

    QVBoxLayout *v0 = new QVBoxLayout; //out
    v0->addWidget(titleLab);

    weiterButton = new QPushButton(buttonText+QString::number(1));
    weiterButton->setFont(font1);
    connect(weiterButton, SIGNAL(clicked()), this, SLOT(weiter()));

    weiterWidget  = new QWidget;

    QHBoxLayout *hh = new QHBoxLayout;
    hh->addStretch();
    hh->addWidget(weiterButton);
    hh->addStretch();
    weiterWidget->setLayout(hh);

    QLabel *anzeigeLab  = new QLabel("akt. Plan");
    aktLE = new QLineEdit;
    aktLE->setMaximumWidth(60);
    aktLE->setAlignment(Qt::AlignRight);

    //nrSBox = new QSpinBox;
    //nrSBox->setMinimum(1);

    //connect(nrSBox,SIGNAL(valueChanged(int)), this, SLOT(updateComb(int)));

    QGroupBox *neuGBox = new QGroupBox;
    neuGBox->setContentsMargins(5,-5,5,-5);
    QHBoxLayout *hl0 = new QHBoxLayout;
    newKombButton = new QPushButton("neuer Plan");
    connect(newKombButton,SIGNAL(clicked()), this, SLOT(neuKomb()));

    delButton = new QPushButton("akt. Plan L�schen");
    connect(delButton,SIGNAL(clicked()), this, SLOT(delKomb()));

    numComb = 2;
    aktComb = 1;
    aktLE->setText(QString::number(aktComb));
    anzKomb = new QLineEdit(QString::number(numComb));
    anzKomb->setMaximumWidth(60);
    anzKomb->setAlignment(Qt::AlignRight);
    anzKomb->setReadOnly(true);
    QLabel *anzLab  = new QLabel("Anzahl Pl�ne:");

    //nrSBox->setValue(aktComb);
    //nrSBox->setMaximum(numComb);

    //hl0->addStretch();
    hl0->addWidget(weiterWidget);
    hl0->addSpacing(100);
    hl0->addStretch();
    hl0->addWidget(anzeigeLab);
    hl0->addWidget(aktLE);
    //hl0->addWidget(nrSBox);
    hl0->addSpacing(50);
    hl0->addStretch();
    hl0->addWidget(newKombButton);
    hl0->addStretch();
    hl0->addSpacing(40);
    hl0->addWidget(delButton);
    hl0->addSpacing(50);
    hl0->addWidget(anzLab);
    hl0->addWidget(anzKomb);
    hl0->addStretch();
    neuGBox->setLayout(hl0);

    v0->addWidget(neuGBox);
    //makePeWidget();
    makeFinWidget();
    makeInvWidget();
    makeVorschlagWidget();

    /*QHBoxLayout *peLout = new QHBoxLayout;
    peLout->addStretch();
    peLout->addWidget(peWidget);
    peLout->addStretch();
    widgetPE->setLayout(peLout);
    //*/

    updateCombs();

    QGroupBox *combGBox = new QGroupBox("Aktueller Plan");
    combGBox->setContentsMargins(0,-5,0,-5);
    QHBoxLayout *hl1 = new QHBoxLayout;

    Title4 *combTit  = new Title4(Qt::AlignCenter, font1);
    combTit->setText(QString("Plan-Nr.")+ QString::number(aktComb));

    hl1->addStretch();
    hl1->addWidget(invWidget);
    QVBoxLayout *vvout = new QVBoxLayout;
    vvout->addStretch();
    vvout->addWidget(finWidget);
    vvout->addWidget(vorWidget);
    vvout->addStretch();
    hl1->addLayout(vvout);
    //hl1->addWidget(peWidget);
    hl1->addStretch();

    QWidget *ssWidget = new QWidget;
    ssWidget->setLayout(hl1);

    peButton = new QPushButton("Preiserwartung anpassen");
    showPEwidget=false;
    peWidget->setVisible(showPEwidget);//hide();
    connect(peButton, SIGNAL(clicked()), this, SLOT(blendenPE()));

    QHBoxLayout *hh5 = new QHBoxLayout;
    hh5->addWidget(peButton);
    hh5->addStretch();

    QWidget *peButtonWidget = new QWidget;
    peButtonWidget->setLayout(hh5);

    QVBoxLayout *vl0 = new QVBoxLayout;
    //vl0->addWidget(combTit);
    vl0->addWidget(ssWidget);
    vl0->addWidget(peButtonWidget);

    combGBox->setLayout(vl0);
    v0->addWidget(combGBox);

    //v0->addWidget(weiterWidget);

    QGroupBox *overviewGBox = new QGroupBox("�bersicht Pl�ne(Finanzen)");
    neuGBox->setContentsMargins(0,-5,0,-5);

    sa = new QScrollArea;
    //sa->setContentsMargins(0,-5,0, -5);
    sa->setMinimumHeight(290); //260
    sa->setMaximumHeight(290); //260
    QWidget *saWidget = new QWidget;
    //saWidget->setContentsMargins(0,-5,0, -5);

    //QHBoxLayout *
    saHLout = new QHBoxLayout;
    saHLout->addWidget(makeFinNamen());
    /*
    for (int i=0; i<numComb; ++i) {
        struct Komb cb=combs[i];
    //*/
    struct Komb cb = combs[0];
    createFinCopy(cb,0);
    titles[combInd(0)]->setStyleSheet("QLabel { background-color : lightgray;}");
    saHLout->addWidget(finW);
    cb=combs[1];
    createFinCopy(cb,1);
    titles[combInd(1)]->setStyleSheet("QLabel { background-color : yellow;}");
    saHLout->addWidget(finW);

    saWidget->setLayout(saHLout);
    sa->setWidget(saWidget);

    updateHead();

/*
    QLabel *anzeigeLab  = new QLabel("Anzeige Nr. ");
    nrSBox = new QSpinBox;
    nrSBox->setValue(1);

    QHBoxLayout *hl2 = new QHBoxLayout;
    hl2->addStretch();
    hl2->addWidget(anzeigeLab);
    hl2->addWidget(nrSBox);
    hl2->addStretch();

    QWidget *anzeigeWidget = new QWidget;
    anzeigeWidget->setLayout(hl2);
//*/
    QVBoxLayout *vl2 = new QVBoxLayout;
    vl2->addWidget(sa);
    //vl2->addWidget(anzeigeWidget);

    overviewGBox->setLayout(vl2);

    v0->addWidget(overviewGBox);
    v0->addStretch(1);

    QWidget *allWidget = new QWidget;
    //allWidget->setContentsMargins(30,-5,-30,-5);
    allWidget->setLayout(v0);
    //allWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QScrollArea *allArea = new QScrollArea;
    //allArea->setContentsMargins(30,-5,-30,-15);
    allArea->setWidget(allWidget);
    allArea->setAlignment(Qt::AlignCenter);
    //allArea->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    //allArea->setMinimumHeight(640);
    //allArea->setMinimumWidth(940);

    QHBoxLayout *allLout = new QHBoxLayout;
    allLout->addWidget(allArea);
    setLayout(allLout);
    resize(1270,750);
    firstGUI = false;
    inited = true;
}

void InvestDialog::updatePeWidget(int i){
   peWidget->updateMyPEs(combs[combInd(i)].pes, true);
   bool t=true;
   if (i==0) {//ref plan
        t=false;
   }
   peWidget->enableDSBs(t);
}

void InvestDialog::makePeWidget2(){
    //return;
    peWidget->updateIter(ITER);

    vector<RegProductInfo> pcat = *(farm0->product_cat);
    int sz = numPEs;//pcat.size();

    for (int i=0; i<sz; ++i){
        int idd = prodIDs[i];
        prodPrices[i]=pcat[idd].getPrice();
        prodPEs[i]=pcat[idd].getPriceExpectation();
        //if (ITER>2) cout << t1 << "\t" << t2 << endl;
    }

    prodRefPEs = prodPEs;
    naivPEs = prodPrices;
    adaptPEs = prodPEs;
    //manPEs = prodPEs;
    //cout << "vor get Eff"<<endl;
    manPEs=parent->bodenDialog1->getEffPEs();
    setPEs();
    //cout << "nach get Eff"<<endl;
    //cout<<"Invest\n1: "<<manPEs[1]<<"\t"<<"5: "<<manPEs[5]<<endl;
    //prodRefPrices = prodPrices;

    vector<double> pes ;
    switch(peVar){
    case 0: pes=manPEs;  break;
    case 1: pes=naivPEs; break;
    case 2: pes=adaptPEs; break;
    default:pes=manPEs;
    }

    allPrices.push_back(prodPrices);
    #define PROCEVENTS
    peWidget->updateData(prodPrices, prodRefPEs, pes, true);//prodPEs, true); PROCEVENTS
    peWidget->updateData1a(getreidePrices, getreidePEs); PROCEVENTS
    peWidget->updateRefs();
    #undef PROCEVENTS

    if (aktComb==0) peWidget->enableDSBs(false);
}

QString InvestDialog::makeInvtypName(QString qstr){
    QString str = qstr.trimmed();
    if (str.contains("Biogas")||str.contains("BIOGAS")) return str; //Biogas als Sonderfall
    int len, ind;
    while (1){
        len = str.length();
        ind = str.indexOf(QRegExp("[0-9]"),len-1);
        if (ind !=-1 && ind ==len-1){
            str.chop(1);
        }else if(str.indexOf('_')==str.length()-1){
            str.chop(1);
        }else
            break;
    }
    return str;
}

void InvestDialog::getInvtypNames(){
    vector<RegInvestObjectInfo> icat = Manager->InvestCatalog;
    int sz = icat.size();

    for (int i=0; i<sz; ++i){
        RegInvestObjectInfo ainv = icat[i];
        QString str = ainv.getName().c_str();

        int typ = ainv.getInvestType();
        if (invtypNames.find(typ)!=invtypNames.end()) continue;
        invtypNames[typ]= makeInvtypName(str);
    }
}

QString InvestDialog::makeKapName(QString qstr, int cc){
    QString str = qstr.trimmed();
    int len, ind;
    bool ja = false;
    while (1){
        len = str.length();
        ind = str.lastIndexOf(QRegExp("[0-9]"),len-1);
        if (ind == len-1){
            ja = true;
            str.chop(1);
        }else if(str.indexOf('_')==str.length()-1){
            ja = true;
            str.chop(1);
        }
        else break;
    }

  if (ja){
    if (str.contains("Biogas") || str.contains("BIOGAS")){
         str=QString::number(cc)+QString("kW ")+str;
    }else{
         str=QString::number(cc)+"er-"+str;
    }
  }
    return str;
}

void InvestDialog::makeInvWidget(){
    invWidget = new QWidget;
    getInvtypNames();

    Title4* invTitle = new Title4(Qt::AlignCenter, font1);
    invTitle->setText("Investitionen");

    vector<QString> vs;
    //vs.push_back("Typ");
    vs.push_back(QString("Name"));
    vs.push_back(QString("Gesamtkosten (")+QChar(0x20ac)+")");
    vs.push_back(QString("Kapazit�t"));
    vs.push_back(QString("Kosten \npro Stallplatz \nod. Maschinenkap.(")+ QChar(0x20ac)+ ")");
    vs.push_back(QString("Arbeitsersparnis \n (Stunden)"));
    vs.push_back(QString("Nutzungs-\ndauer(Jahr)*"));
    vs.push_back(QString("Durchschn. Kosten \n pro Jahr(") + QChar(0x20ac)+")");
    vs.push_back(QString("Vorschlag"));
    vs.push_back(QString("Meine Entsch."));

    invHeader = vs;
    vector<RegInvestObjectInfo> icat = Manager->InvestCatalog;
    int sz = icat.size();

    invRefs=farm0->invRefs;
    //bool newType = true;
    for (int i=0; i<sz; ++i){
        RegInvestObjectInfo ainv = icat[i];
        int cc = static_cast<int>(ainv.getCapacity());
        invNames.push_back(makeKapName(QString(ainv.getName().c_str()), cc));

        invCaps.push_back(cc);//ainv.getCapacity());
        myInvs.push_back(invRefs[i]);
        invKosten.push_back(ainv.getAcquisitionCosts());
        invKosten1.push_back(cc>0? ainv.getAcquisitionCosts()/cc :0);
        invLabsub.push_back(ainv.getLabourSubstitution());
        invTypes.push_back(ainv.getInvestType());
        invLifes.push_back(ainv.getEconomicLife());
        double avcj;
        int lf = ainv.getEconomicLife();
        avcj = ainv.getAcquisitionCosts()*(0.3/lf+ 0.7*(0.055*pow(1.055,lf)/(pow(1.055,lf)-1)))+ainv.getMaintenanceCosts();
        invKostenJahrs.push_back(avcj);
    }

    nRowInv = sz-2;  //ohne AK
    nColInv = 9;//7;//4;
    invSS = new SpreadSheet2(nRowInv, nColInv, true, vs, false); //ohne AK
    invSS->setArt(2);
    invSS->setTabKeyNavigation(false);

    QSignalMapper *sigmapInv = new QSignalMapper;

    for (int r=0; r<nRowInv+2; ++r){
            QSpinBox *dsb = new QSpinBox();

            dsb->setAlignment(Qt::AlignRight);
            dsb->setSuffix("  ");
            //dsb->setLocale(QLocale::German);
            dsb->setValue(myInvs[r]);
            if(r<nRowInv) 
                invSS->setCellWidget(r,nColInv-1,dsb);

            invDSBs.push_back(dsb);
            connect(dsb, SIGNAL(valueChanged(int)), sigmapInv, SLOT(map()));
            sigmapInv->setMapping(dsb, r);
    }

    connect(sigmapInv, SIGNAL(mapped(int)), this, SLOT(updateInv(int)));

    invSS->updateColumn(0, invNames);
    invSS->updateColumn(1, invKosten);
    invSS->updateColumn(2, invCaps);
    invSS->updateColumn(3, invKosten1);
    invSS->updateColumn(4, invLabsub);
    invSS->updateColumn(5, invLifes);
    invSS->updateColumn(6, invKostenJahrs);
    invSS->updateColumn(7, invRefs);

    //mit Hintergrundfarben
    iColors.resize(nRowInv);
    for (int i=0; i<nRowInv;++i){
        QColor c ;
        int n = (invTypes[i]-1)%2;
        switch (n){
        case 0: c=QColor("red").light(180); break;
        //case 1: c=QColor("green").light(180); break;
        case 1: c=QColor("blue").light(180); break;
        default:;
        }
        iColors[i]=c;
    }

    for (int i=0; i<nRowInv;++i){
        QColor c ;
        /*int t = invTypes[i];
        if (t<2) c = LabColor;
        else if (t<7) c = StallColor;
        else c = MachineColor;
        //*/
        c=iColors[i];
        invColors.push_back(c);
    }

    for (int i=0; i<nRowInv;++i){
        invSS->setRowColor(i, invColors[i]);
        QSpinBox *dsb = invDSBs[i];
        QPalette myPalette( dsb->palette() );
        myPalette.setColor(QPalette::Active, QPalette::Base, invColors[i]);
        dsb->setPalette(myPalette);
        dsb->update();
    }

    QVBoxLayout *vl = new QVBoxLayout;
    vl->addWidget(invTitle);
    vl->addSpacing(5);

    QComboBox *invTypCBox = new QComboBox;
    typIndex0 = 0;
    typIndex = 0;
    invTypCBox->addItem("Alle");
    //invTypCBox->addItem("Arbeitskr�fte");
    int szz = invtypNames.size();
    for (int i=2; i<szz; ++i){
        invTypCBox->addItem(invtypNames[i]);
    }
    invTypCBox->setFont(QFont("times",12));
    connect(invTypCBox,SIGNAL(currentIndexChanged(int)),this, SLOT(updateInvtyp(int)));

    QHBoxLayout *cbHout  = new QHBoxLayout;
    cbHout->addStretch();
    QLabel *invTypLab = new QLabel("Investitionstyp: ");
    invTypLab->setFont(QFont("times",12));//courier new",12));
    cbHout->addWidget(invTypLab);
    cbHout->addSpacing(12);
    cbHout->addWidget(invTypCBox);
    cbHout->addStretch();

    //vl->addWidget(invTypCBox);
    vl->addLayout(cbHout);

    makeInvtables();

    int ttt = invtables.size();
    for (int i=0; i<ttt; ++i){
        vl->addStretch();
        vl->addWidget(invtables[i]);
    }
    //vl->addWidget(invSS);

    QLabel *lifeLab = new QLabel("*Nach Ablauf der Nutzungsdauer kann der Stall bzw. die Maschine nicht mehr genutzt werden.");
    lifeLab->setStyleSheet("color: blue");
    vl->addWidget(lifeLab);
    vl->addStretch(2);
    vl->setAlignment(Qt::AlignTop);
    invWidget->setLayout(vl);

   /*QWidget *kw = new QWidget;
   kw->resize(100, 150);

   Title4 *invTit  = new Title4(Qt::AlignCenter, font1);
   invTit->setText(QString("Investitionen"));
   QVBoxLayout *vl = new QVBoxLayout;
   vl->addWidget(invTit);
   vl->addWidget(kw);

   vl->setAlignment(Qt::AlignTop);
   invWidget->setLayout(vl);
   //*/
}

void InvestDialog::makeInvtables(){
    vector<RegInvestObjectInfo> icat = Manager->InvestCatalog;
    int sz = icat.size();
    invSBs.resize(sz);
    int numTyps = invtypNames.size();

    posss.resize(numTyps);
    vector<int> numRows;
    numRows.resize(numTyps);

    invNamess.resize(numTyps);
    invKostens.resize(numTyps),
    invKosten1ss.resize(numTyps),
    invLabsubss.resize(numTyps);
    invCapss.resize(numTyps);
    invRefss.resize(numTyps);
    myInvss.resize(numTyps);
    invLifess.resize(numTyps);
    invKostenJahrss.resize(numTyps);
    invDSBss.resize(numTyps);

  for(int i=0; i<sz; ++i){
    int nnn ;
    if (invTypes[i]<2) continue; //nnn= 1;
    else nnn = invTypes[i]-1;

    posss[nnn].push_back(i);
    numRows[nnn]++;

    pair<int,int> p (nnn, posss[nnn].size()-1);
    subtablerows.push_back(p);

    invNamess[nnn].push_back(invNames[i]);
    invKostens[nnn].push_back(invKosten[i]);
    invKosten1ss[nnn].push_back(invKosten1[i]);
    invCapss[nnn].push_back(invCaps[i]);
    invLabsubss[nnn].push_back(invLabsub[i]);
    invLifess[nnn].push_back(invLifes[i]);
    invKostenJahrss[nnn].push_back(invKostenJahrs[i]);
    invRefss[nnn].push_back(invRefs[i]);
    myInvss[nnn].push_back(myInvs[i]);

 }

  invtables.push_back(invSS);
  int cw = invSS->columnWidth(0);
  int cw2 = invSS->columnWidth(nColInv-1);
  int h = invSS->height();
  for (int i=1; i<numTyps; ++i){
    SpreadSheet2 *ss = new SpreadSheet2(numRows[i], nColInv, true, invHeader, false);
    ss->setArt(2);
    ss->setTabKeyNavigation(false);
    for (int k=0; k<numRows[i]; ++k){
        QSpinBox *sb = new QSpinBox;
        sb->setAlignment(Qt::AlignRight);
        sb->setSuffix("  ");
        ss->setCellWidget(k,nColInv-1,sb);
        invDSBss[i].push_back(sb);
        invSBs[posss[i][k]] =sb;
        QSpinBox *dsb = invDSBs[posss[i][k]];
        //connect(sb, SIGNAL(valueChanged(int)), dsb, SIGNAL(valueChanged(int)));
        connect(sb, SIGNAL(valueChanged(int)), dsb, SLOT(setValue(int)));
        //connect(dsb, SIGNAL(valueChanged(int)), sb, SLOT(setValue(int)));
    }

    connect(invSS, SIGNAL(cellChanged(int,int)), this, SLOT(updateSubtable(int,int)));

    ss->updateColumn(0, invNamess[i]);
    ss->updateColumn(1, invKostens[i]);
    ss->updateColumn(2, invCapss[i]);
    ss->updateColumn(3, invKosten1ss[i]);
    ss->updateColumn(4, invLabsubss[i]);
    ss->updateColumn(5, invLifess[i]);
    ss->updateColumn(6, invKostenJahrss[i]);
    ss->updateColumn(7, invRefss[i]);
    ss->setColumnWidth(0,cw);
    ss->setHidden(i!=typIndex);
    ss->adjSize();

    invtables.push_back(ss);
  }
}

void InvestDialog::updateSubtable(int r, int c){
    pair<int,int> p = subtablerows[r];
    invtables[p.first]->cell(p.second,c)->setFormula(invSS->cell(r,c)->formula());
}

void InvestDialog::updateInvtyp(int x){
    int sz = invNamess.size();
    for (int i=0; i<sz; ++i){
        invtables[typIndex0]->setHidden(true);
        invtables[x]->setHidden(false);
        //invtables[x]->setColumnWidth(nColInv-1,invSS->columnWidth(nColInv-1));
    }
    typIndex0 = x;
    typIndex = x;
}

void InvestDialog::updateFins(int akt){
    //cout << akt <<endl;
    vector<QString> td;
    td.push_back(QLocale().toString(langFK,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(dispo,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(saving,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(GDB,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(liquidity,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(profit,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(income,'f',2)+" "+QChar(0x20ac));

    fins[akt]->updateColumn(0, td);//1,td);
}

QWidget* InvestDialog::makeFinNamen(){
    finWidget = new QWidget;
    finWidget->setContentsMargins(0,-5,0,-15);
    Title4* finTitle = new Title4(Qt::AlignCenter, font1);
    finTitle->setText(QString("Parameter"));

    vector<QString> tv;
    SpreadSheet2 *fss = new SpreadSheet2(7,1,false,tv,false);
    fss->setContentsMargins(0,-5,0,-5);

    tv.push_back(QString("langfristiges FK"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Dispositionskredit"));//+QChar(0x20ac)+")");
    tv.push_back(QString("positive Geldanlagen"));//+QChar(0x20ac)+")");
    tv.push_back(QString("erw. GDB"));//+QChar(0x20ac)+")");
    tv.push_back(QString("erw. Liquidit�t"));//+QChar(0x20ac)+")");
    tv.push_back(QString("erw. Gewinn"));//+QChar(0x20ac)+")");
    tv.push_back(QString("erw. Haushaltseinkommen"));//+QChar(0x20ac)+")");

    fss->updateColumn(0,tv);

    QVBoxLayout *vl = new QVBoxLayout;
    vl->addWidget(finTitle);

    QHBoxLayout *hf = new QHBoxLayout;
    hf->addStretch();
    hf->addWidget(fss);
    hf->addStretch();
    QWidget *hw = new QWidget;
    hw->setLayout(hf);

    vl->addWidget(hw);

    finWidget->setLayout(vl);

    return finWidget;
}

void InvestDialog::updateFins(){
    //QHBoxLayout *
    saHLout = new QHBoxLayout;

    struct Komb cb = combs[0];

    /*
    foreach (SpreadSheet2 *s, fins){
        delete s;
    }
    foreach (QWidget *w, wins){
        delete w;
    }
    //*/

    fins.clear();
    wins.clear();
    plans.clear();
    titles.clear();

    saHLout->addWidget(makeFinNamen());
    createFinCopy(cb,0);
    titles[combInd(0)]->setStyleSheet("QLabel { background-color : lightgray;}");
    saHLout->addWidget(finW);

    //if (ITER>2) cout<<"nach finNamen" <<endl;

    cb = combs[1];
    createFinCopy(cb,1);
    saHLout->addWidget(finW);
    titles[combInd(1)]->setStyleSheet("QLabel { background-color : yellow;}");
    //sw->setLayout(saHLout);

    QWidget *saWidget = new QWidget;
    saWidget->setLayout(saHLout);

    //saHLout->activate();
    //saWidget->adjustSize();
    sa->setWidget(saWidget);
}

void InvestDialog::savePEs(){
    it_myPEs.clear();

    switch(peVar) {
    case 0: it_myPEs = manPEs; break;
    case 1: it_myPEs = naivPEs; break;
    case 2: it_myPEs = adaptPEs; break;
    default: ;
    }

    all_myPEs.push_back(it_myPEs);
}

void InvestDialog::blendenPE(){
    if (!showPEwidget) {
        peButton->setText("Ausblenden Preiserwartung");
    }else{
        peButton->setText("Preiserwartung anpassen");
    }
    peWidget->setVisible(!showPEwidget);
    showPEwidget = !showPEwidget;

    //alle vorg�nger
    /*
    peWidget->parentWidget()->layout()->invalidate();
    QWidget *parent = peWidget->parentWidget();
    while (parent) {
        if (!parent->parentWidget()) break; //mainwindow nicht
        parent->adjustSize();
        parent = parent->parentWidget();
    }
    //*/
}

void InvestDialog::getAusblenden(){
    showPEwidget = false;
    peButton->setText("Preiserwartung anpassen");
    peWidget->setVisible(showPEwidget);
}

void InvestDialog::getINVsig(){
    updatePEs();
    emit INVsig();
}

void InvestDialog::setPEs(){
    int n = prodIDs.size();
    for (int r=0;r<n;++r){
        if (r==refpos) {
            double tind;
            tind = prodPrices[refpos]==0? 1:manPEs[r]/prodPrices[refpos];
            getreidePEs.clear();
            int ts = getreideIDs.size();
            for (int i=0; i<ts; ++i){
               getreidePEs.push_back(Manager->product_cat0[getreideIDs[i]].getPriceExpectation());
            }
            for (int i=0; i<ts; ++i){
                Manager->product_cat0[getreideIDs[i]].setPriceExpectation(getreidePrices[i]*tind);
            }
         }
         Manager->product_cat0[prodIDs[r]].setPriceExpectation(manPEs[r]);
    }
}

void InvestDialog::updatePEs(){
if (!hasPEwidget){
    farm0 = Manager->Farm0;
    ITER=gg->tIter;
    makePeWidget();
    hasPEwidget=true;
}else
    makePeWidget2();
}

void InvestDialog::makePeWidget(){
    /*if(parent->getPeWidget()) {
        peWidget=parent->getPeWidget();

        disconnect(peWidget, SIGNAL(peSig(int)), parent->bodenDialog1, SLOT(getPeSig(int)));
        disconnect(peWidget, SIGNAL(peVarSig(int)), parent->bodenDialog1, SLOT(getPeVarSig(int)));
        disconnect(parent->bodenDialog1, SIGNAL(peOK(int,bool)), peWidget, SLOT(getPeOK(int,bool)));

        connect(peWidget, SIGNAL(peSig(int)), this, SLOT(getPeSig(int)));
        connect(peWidget, SIGNAL(peVarSig(int)), this, SLOT(getPeVarSig(int)));
        connect(this, SIGNAL(peOK(int,bool)), peWidget, SLOT(getPeOK(int,bool)));
        connect(peWidget, SIGNAL(ausBlenden()), this, SLOT(getAusblenden()));

        inited=true;
        return;
    }
    //*/

    vector<QString> vs;
    vs.push_back("Produktname");
    vs.push_back(QString("Einheit "));
    vs.push_back(QString("Preis "));
    vs.push_back(QString("")+QChar(0x0D8)+QString("Preis\nder Ref-Zeit"));
    vs.push_back(QString("PE\nvon Modell"));
    vs.push_back(QString("Meine PE"));
    vs.push_back(QString("Deckungsbeitrag"));
    vs.push_back(QString("Einheit von DB"));
    //vs.push_back(QString("Preisindex"));
    //*/

    int xcount = 0;
    vector<RegProductInfo> pcat = *(farm0->product_cat);
    int sz = pcat.size();
    bool refnameFound = false;
    vector<bool> peSpiels;
    for (int i=0; i<sz; ++i){
        //if (pcat[i].getProductGroup()<0) continue;
        if (!pcat[i].getPreisSchwankung()) continue;
        if (pcat[i].getProductGroup()== 0  && !refnameFound) {
            refname = QString(pcat[i].getName().c_str());
            refprodID = i;
            refpos = xcount;
            refnameFound = true;

        }else if (pcat[i].getProductGroup()==0){
            getreideIDs.push_back(i);
            getreidePEs.push_back(pcat[i].getPriceExpectation());
            getreidePrices.push_back(pcat[i].getPrice());
            continue;
        }else if (pcat[i].getProductGroup()==-1 && pcat[i].getClass().compare("GRASSLAND")!=0) // nur f. Altmark ??
             continue;

        ++xcount;

        if (parent->spielPeProds.size()==0) {
        string aName = pcat[i].getName();
        bool aFind=false;
        for (int ti=0;ti<gg->MyPeProducts.size();++ti){
            if (gg->MyPeProducts[ti]==aName) aFind = true;
        }
        peSpiels.push_back(aFind);
        }

        prodNames.push_back(QString(pcat[i].getName().c_str()));
        prodPrices.push_back(pcat[i].getPrice());
        prodCosts.push_back(pcat[i].getVarCost());
        prodIDs.push_back(i);
        prodPEs.push_back(pcat[i].getPriceExpectation());
        prodUnits.push_back(pcat[i].getUnit().c_str());
        prodGMunits.push_back(pcat[i].getGMunit().c_str());
        prodErtrags.push_back(pcat[i].getErtrag());
        prodNertrags.push_back(pcat[i].getNebenErtrag());
        //prodRefPEs.push_back(100);
    }
    if (parent->spielPeProds.size()==0)
        parent->setSpielPeProds(peSpiels);
    else
        peSpiels = parent->getSpielPeProds();

    prodRefPEs = prodPEs;
    naivPEs = prodPrices;
    adaptPEs = prodPEs;

    if (parent->getPeWidget()) {
        manPEs = parent->bodenDialog1->getEffPEs();
        setPEs();
    } else
        manPEs = prodPEs;

    //cout<<"Invest:  manPEs.size() = "<<manPEs.size()<<endl;
    numPEs = prodIDs.size();
    //prodRefPrices = prodPrices;

    allPrices.push_back(prodPrices);
    //calRefPEs();

    peVar = 0;
    #define PROCEVENTS
    peWidget = new PEwidget(ITER, &peVar, refname, vs, prodNames, prodUnits, prodErtrags, prodNertrags,prodGMunits); PROCEVENTS
    peWidget->setPeSpiels(peSpiels);
    peWidget->updateCosts(prodCosts);
    peWidget->updateData(prodPrices, prodRefPEs, manPEs);//prodPEs);PROCEVENTS
    peWidget->updateData1(getreideIDs,getreidePrices, getreidePEs);PROCEVENTS
    peWidget->updateData2(refpos, &manPEs, adaptPEs, naivPEs);PROCEVENTS
    peWidget->updateData3(prodIDs, &allPrices);PROCEVENTS
    peWidget->updateRefs();
    peWidget->setPeInited(true);
    #undef PROCEVENTS

    connect(peWidget, SIGNAL(peSig(int)), this, SLOT(getPeSig(int)));
    connect(peWidget, SIGNAL(peVarSig(int)), this, SLOT(getPeVarSig(int)));
    connect(this, SIGNAL(peOK(int,bool)), peWidget, SLOT(getPeOK(int,bool)));
    connect(peWidget, SIGNAL(ausBlenden()), this, SLOT(getAusblenden()));

    if (!parent->getPeWidget())
            parent->setPeWidget(peWidget);
    inited = true;
}

void InvestDialog::updateHead(){
    nurHead = true;
    //nrSBox->setMaximum(numComb);
    //nrSBox->setValue(aktComb);
    aktLE->setText(QString::number(aktComb));
    anzKomb->setText(QString::number(combs.size()));
    updateWeiterButton();
    nurHead = false;
}

void InvestDialog::updateGUI2(){
    toClose=false;
    combs.clear();

    aktComb=1;
    numComb=2;

    updateInvWidget();
    updateFinWidget();
    //makePeWidget2();
    updateCombs();

    //fins.clear();
    updateFins();
    updateHead();

    inited = true;
}


void InvestDialog::showInvest(){
    updateGUI();
    getAusblenden();
    show();
    //exec();
    parent->aTime.restart();
}

void InvestDialog::updateVorWidget(){
    vector<int> inds;
    int sz = invRefs.size();
    for (int i=0;i<sz;++i){
        if (invRefs[i]>0 && invTypes[i]>1 ) { //keine AK
            inds.push_back(i);
        }
    }
    sz=inds.size();

    if (sz>0) {
        vorLab->setVisible(false);
        vorSS->setVisible(true);
    }
    else {
        vorLab->setVisible(true);
        vorSS->setVisible(false);
    }

    if (sz==0) return;

    vector<QString> names;
    vector<int> vors;
    for (int i=0;i<sz;++i){
            names.push_back(invNames[inds[i]]);
            vors.push_back(invRefs[inds[i]]);
    }
    vorSS->setRowCount(sz);
    vorSS->updateColumn(0, names);
    vorSS->updateColumn(1, vors);
    vorSS->adjSize();
}

void InvestDialog::makeVorschlagWidget(){
    vorWidget = new QWidget;
    Title4* vorTitle = new Title4(Qt::AlignCenter, font1);
    vorTitle->setText("vorgeschlagene Investitionen");

    vector<QString> tv;

    tv.push_back(QString("Name"));
    tv.push_back(QString("Anzahl"));

    vorLab = new QLabel("keine");
    vorLab->setAlignment(Qt::AlignCenter);

    vorSS = new SpreadSheet2(0,2,true,tv,false,false);

    QVBoxLayout *vf = new QVBoxLayout;
    vf->setAlignment(Qt::AlignCenter);
    vf->addStretch();
    vf->addWidget(vorTitle);
    QHBoxLayout *hlf = new QHBoxLayout;
    hlf->addStretch();
    hlf->addWidget(vorSS);
    hlf->addStretch();

    vf->addWidget(vorLab);
    vf->addLayout(hlf);//Widget(finSS);
    vf->addStretch();

    vorWidget->setLayout(vf);
    updateVorWidget();
}

void InvestDialog::makeFinWidget(){
    finWidget = new QWidget;
    Title4* finTitle = new Title4(Qt::AlignCenter, font1);
    finTitle->setText("Finanzen");

    vector<QString> tv;

    tv.push_back(QString("langfristiges FK"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Dispositionskredit"));//+QChar(0x20ac)+")");
    tv.push_back(QString("positive Geldanlagen"));//+QChar(0x20ac)+")");
    tv.push_back(QString("erwart. GDB"));//+QChar(0x20ac)+")");
    tv.push_back(QString("erwart. Liquidit�t"));//+QChar(0x20ac)+")");
    tv.push_back(QString("erwart. Gewinn"));//+QChar(0x20ac)+")");
    tv.push_back(QString("erwart. Haushaltseinkommen"));//+QChar(0x20ac)+")");

    finSS = new SpreadSheet2(7,2,false,tv,false);

    vector<QString> td;
    td.push_back(QLocale().toString(langFK,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(dispo,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(saving,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(GDB,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(liquidity,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(profit,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(income,'f',2)+" "+QChar(0x20ac));

    finSS->updateColumn(0,tv);
    finSS->updateColumn(1,td);

    QVBoxLayout *vf = new QVBoxLayout;
    vf->setAlignment(Qt::AlignCenter);
    vf->addStretch();
    vf->addWidget(finTitle);
    QHBoxLayout *hlf = new QHBoxLayout;
    hlf->addStretch();
    hlf->addWidget(finSS);
    hlf->addStretch();

    vf->addLayout(hlf);//Widget(finSS);
    vf->addStretch();

    finWidget->setLayout(vf);
}

void InvestDialog::updateFinWidget(int i){
    int x=0;
    langFK=combs[combInd(i)].fins[x++];
    dispo=combs[combInd(i)].fins[x++];
    saving=combs[combInd(i)].fins[x++];
    GDB=combs[combInd(i)].fins[x++];
    liquidity=combs[combInd(i)].fins[x++];
    profit=combs[combInd(i)].fins[x++];
    income=combs[combInd(i)].fins[x];

    vector<QString> td;
    td.push_back(QLocale().toString(langFK,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(dispo,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(saving,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(GDB,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(liquidity,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(profit,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(income,'f',2)+" "+QChar(0x20ac));

    finSS->updateColumn(1,td);
}

void InvestDialog::updateFinWidget(){
    langFK=farm0->getLangFKinv();
    dispo=farm0->getDispoInv();
    saving=farm0->getSavingInv();
    GDB=farm0->getGDBinv();
    liquidity=farm0->getLiqInv();
    profit=farm0->getProfInv();
    income=farm0->getIncInv();

    vector<QString> td;
    td.push_back(QLocale().toString(langFK,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(dispo,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(saving,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(GDB,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(liquidity,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(profit,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(income,'f',2)+" "+QChar(0x20ac));

    finSS->updateColumn(1,td);
}

int InvestDialog::combInd(int comb){
    int sz = plans.size();
    for (int i=0; i<sz; ++i){
        if (plans[i]==comb)
            return i;
    }
    return 0; //fehler
}

int InvestDialog::indComb(int i){
    return plans[i];
}

void InvestDialog::updateComb(QString str){
    int i=0;
    titles[combInd(aktComb)]->setStyleSheet("QLabel { background-color : lightgray;}");
    i=str.toInt();

    updateComb(combInd(i));
}

QWidget* InvestDialog::createFinCopy(Komb comb, int nr){
    //QWidget *
    finW = new QWidget();
    finW->setContentsMargins(0,-5,0,-15);
    Title4* finTitle = new Title4(Qt::AlignCenter, font1);
    if (nr==0)
        finTitle->setText(QString("<a href=0>Vorschlag"));
    else
        finTitle->setText(QString("<a href=%1>Plan %1").arg(QString::number(nr)));

    plans.push_back(nr);
    titles.push_back(finTitle);

    connect(finTitle, SIGNAL(linkActivated(QString)), this, SLOT(updateComb(QString)));

    vector<QString> tv;
    tv.push_back(QString("langfristiges FK"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Dispositionskredit"));//+QChar(0x20ac)+")");
    tv.push_back(QString("positive Geldanlagen"));//+QChar(0x20ac)+")"););
    tv.push_back(QString("GDB"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Liquidit�t"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Gewinn"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Haushaltseinkommen"));//+QChar(0x20ac)+")");
//*/
    SpreadSheet2 *fSS = new SpreadSheet2(7,1,false,tv,false);
    fSS->setContentsMargins(0,-5,0,-5);

    vector<QString> td;
    td.push_back(QLocale().toString(comb.fins[0],'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(comb.fins[1],'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(comb.fins[2],'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(comb.fins[3],'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(comb.fins[4],'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(comb.fins[5],'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(comb.fins[6],'f',2)+" "+QChar(0x20ac));

    int tdsz = td.size();
    for (int i=0; i<tdsz; ++i){
        //if (i>0)
        fSS->cell(i,0)->setAlign(Qt::AlignRight, Qt::AlignCenter);
    }

    fSS->updateColumn(0,td);
//    fSS->updateColumn(1,td);

    QVBoxLayout *vf = new QVBoxLayout;
    vf->addWidget(finTitle);

    QHBoxLayout *hf = new QHBoxLayout;
    hf->addStretch();
    hf->addWidget(fSS);
    hf->addStretch();
    QWidget *hw = new QWidget;
    hw->setLayout(hf);

    vf->addWidget(hw);

    //if (nr!=0)
    fins.push_back(fSS);

    finW->setLayout(vf);
    wins.push_back(finW);
    return finW;
}

void InvestDialog::updateInvWidget(int i){
    invRefs = combs[combInd(i)].invRefs;
    myInvs = combs[combInd(i)].invs;
    invSS->updateColumn(7,invRefs);

    bool ok = inited;
    if (ok) inited = false;
    for (int r=0; r<nRowInv;++r){
        QSpinBox *dsb = invDSBs[r];
        QSpinBox *sb = invSBs[r];
        dsb->setValue(myInvs[r]);
        sb->setValue(myInvs[r]);
        if (i==0){
            dsb->setEnabled(false);
            sb->setEnabled(false);
        }
        else{
            dsb->setEnabled(true);
            sb->setEnabled(true);
        }
    }

    if (ok) inited=true;
    updateVorWidget();
}


void InvestDialog::updateInvWidget(){
    myInvs = invRefs = farm0->invRefs;
    invSS->updateColumn(7,invRefs);
    /*if (numComb==1){ //erste comb
        myInvs = farm0->myInvs;
        invSS->updateColumn(5,invRefs);
    }
    //*/
    bool ok = inited;
    if (ok) inited = false;
    for (int r=0; r<nRowInv;++r){
        QSpinBox *dsb = invDSBs[r];
        dsb->setValue(myInvs[r]);
        invSBs[r]->setValue(myInvs[r]);
        if (aktComb != 0) {
             dsb->setEnabled(true);
             invSBs[r]->setEnabled(true);
        }
    }
    if (ok) inited = true;
    //inited=true;
    updateVorWidget();
}

void InvestDialog::updateRefInvs(int akt){
    combs[combInd(akt)].invRefs = farm0->invRefs;
}

void InvestDialog::getPeOK(int r, bool b){
    emit peOK(r,b);
    if (!b) {
            QMessageBox::warning(this, tr("Warnung"), //Solver Information"),
                             tr("Erwartete Preise zu extrem"));//Bedingungen verletzt"));
    }
    if (!b) return;

    updateRefInvs(aktComb);

    updateFinWidget();
    saveCombFin(aktComb);

    //updateInvWidget(aktComb);
    updateInvWidget();

    updateFins(aktComb);
    saveCombPe(aktComb);
}

void InvestDialog::getPeVarOK(int r, bool b){
    updateRefInvs(aktComb);
    updateFinWidget();
    saveCombFin(aktComb);

    updateInvWidget();
    //updateInvWidget(aktComb);
    updateFins(aktComb);
    saveCombPe(aktComb);
}

void InvestDialog::getPeSig(int r){
    emit investPeSig(r);
}

void InvestDialog::getPeVarSig(int v){
    emit investPeVarSig(v);
}

void InvestDialog::weiter(){
    farm0->doLpInvestEnd(myInvs);
    accept();
    toClose=true;
    savePEs();
    abspeichern();//emit fertig();
    close();
    //inited=false;
}

void InvestDialog::abspeichern(){
    vector<bool> spiels=parent->getSpielPeProds();

    int t=parent->aTime.elapsed();
    if (gg->tIter==1) { // first iter
        parent->tstrInv<<"=\t=\tInvest\tKap\tVorschlag\tEntscheid\n";
    }
    parent->tstrInv<<"\nRunde\t"<<gg->tIter<<":\t"<<QLocale().toString(t/1000.,'f',2)<<" Sekunden\n";
    //gg->sFIXED_OFFFARM_LAB
    //gg->sFIXED_HIRED_LAB
    int sz=invRefs.size();
    for (int i=0;i<sz;++i){
        if (invNames[i]==gg->sFIXED_OFFFARM_LAB.c_str() || invNames[i]==gg->sFIXED_HIRED_LAB.c_str()) continue;
        if (invRefs[i]!=myInvs[i] || invRefs[i]!=0){
            parent->tstrInv<<"\t\t"<<invNames[i]<<"\t"
                <<QLocale().toString(invCaps[i])<<"\t"<<invRefs[i]<<"\t"<<myInvs[i]<<"\n";
        }
    }

    sz=prodIDs.size();
    parent->tstrPe<<"\t Investition    \t";
    for (int i=0;i<sz;++i){
       if (spiels[i]) {
            parent->tstrPe<<QLocale().toString((prodRefPEs[i]-prodNertrags[i])/prodErtrags[i],'f',2)+"\t"
                    <<QLocale().toString((it_myPEs[i]-prodNertrags[i])/prodErtrags[i],'f',2)+"\t";
        }
    }parent->tstrPe<<"\n";
}

void InvestDialog::getInvestOK(int r, bool b){
    if (!b) {
        inited = false;//Achtung!
        invDSBs[r]->setValue(oldvalue);
        invSBs[r]->setValue(oldvalue);
        inited = true;

        QMessageBox::warning(this, tr("Warnung"), //Solver Information"),
                         tr("Die momentane Betriebsaustattung erlaubt das nicht"));
    }
    int sz = invDSBs.size();
    for (int i=0; i<sz; ++i){
        invDSBs[i]->setEnabled(true);
    }

    if (!b) return;
    invSBs[r]->setValue(invDSBs[r]->value()); //subtable
    myInvs = farm0->myInvs;

    updateFinWidget();
    saveCombFin(aktComb);
    updateFins(aktComb);
    saveCombInv(aktComb);
}

void InvestDialog::updateInv(int row){
    if (!inited) return;
    if (inComb) return;

    int sz = invDSBs.size();
    for (int i=0; i<sz; ++i){
        invDSBs[i]->setEnabled(false);
    }

    int d = invDSBs[row]->value();
    oldvalue = myInvs[row];

    //cout<<"in investD"<<endl;
    emit investSig(row,d);
}

Title4::Title4(Qt::AlignmentFlag align, QFont f, QWidget*w):QLabel(w){
    this->setAlignment(align);
    this->setFont(f);
}
