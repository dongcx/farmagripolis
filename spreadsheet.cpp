#include <QtGui>

#include "cell.h"
#include "spreadsheet.h"

using namespace std;

SpreadSheet::SpreadSheet(int nrows, int ncols, bool hasHHeader, vector<QString> hheaders, bool hasVHeader, bool b2, bool ac, QWidget *parent)
    :RowCount(nrows),ColumnCount(ncols), hHeader(hasHHeader), hHeaders(hheaders),vHeader(hasVHeader),border2(b2), alignC(ac), QTableWidget(parent)
{
    MyMaxH=600;
    MyMaxW=0;
    setItemDelegate(new MyDelegate());
    autoRecalc = true;

    setGridStyle(Qt::SolidLine);
    //setShowGrid(false);
    if (!vHeader) verticalHeader()->hide();
    if (!hHeader) horizontalHeader()->hide();
    setItemPrototype(new Cell());
    setSelectionMode(ContiguousSelection);

    connect(this, SIGNAL(itemChanged(QTableWidgetItem *)),
            this, SLOT(somethingChanged()));

    //this->verticalScrollBar()->hide();//setDisabled(true);
    //this->horizontalScrollBar()->hide();//setDisabled(true);
    clear();
    initTab();
    adjSize();
}

void SpreadSheet::setMyMaxH(int mh){
    MyMaxH = mh;
}

void SpreadSheet::setMyMaxW(int mw){
    MyMaxW = mw;
}

void SpreadSheet::updateColumn(int col, vector<double> ds,int x){
    int rows=rowCount();
    for (int i=0;i<rows;++i){
        if (i>=rows-1-x && i<=rows-2)
            setFormula(i,col, QString(""));
        else  {
            setFormula(i,col, QLocale().toString(ds[i], 'f', 2));//QString::number(ds[i], 'f', 2));//qstr);
        }
    }
}

void SpreadSheet::updateColumn(int col, vector<double> ds){
    int rows=rowCount();
    for (int i=0;i<rows;++i){
         setFormula(i,col, QLocale().toString(ds[i], 'f', 2));
    }
}

void SpreadSheet::updateColumn(int col, vector<double> ds, bool asInt){
    int rows=rowCount();
    for (int i=0;i<rows;++i){
         cell(i,col)->setAlign(Qt::AlignCenter,Qt::AlignCenter);
         if (!asInt) setFormula(i,col, QLocale().toString(ds[i], 'f', 2));
         else setFormula(i, col, QLocale().toString(static_cast<int>(ds[i])));
    }
}

void SpreadSheet::updateColumn(int col, vector<int> xs){
    int rows=rowCount();
    for (int i=0;i<rows;++i){
        cell(i,col)->setAlign(Qt::AlignCenter,Qt::AlignCenter);
        setFormula(i,col, QLocale().toString(xs[i]));
    }
}

void SpreadSheet::updateColumn(vector<double> ds, vector<int> is){
    int rows=ds.size();//rowCount();
    for (int i=0;i<rows;++i){
        setFormula(i, is[i], QLocale().toString(ds[i], 'f', 2));// QString::number(ds[i],'f', 2));//QString(ss));
        setFormula(i,is[i]==1?2:1, QString(""));
    }
}

void SpreadSheet::updateColumn(int col, vector<QString> ds){
    int rows=rowCount();
    for (int i=0;i<rows;++i){
        setFormula(i,col, ds[i]);
    }
}

void SpreadSheet::updateColumn(int col, vector<string> ds){
    int rows=rowCount();
    for (int i=0;i<rows;++i){
        setFormula(i,col, QString(ds[i].c_str()));
    }
}

void SpreadSheet::setRow(int row, vector<QString> strs, bool center){
    int cols= columnCount();
    for (int i=0; i<cols;++i){
        setFormula(row, i, strs[i]);
        if (center) {
            cell(row, i)->setAlign(Qt::AlignCenter,Qt::AlignCenter);
        }
    }
}

void SpreadSheet::setRow(int row, vector<double> ds){
    int cols= columnCount();
    for (int i=0; i<cols;++i){
        setFormula(row, i, QLocale().toString(ds[i],'f',2));//QString::number(ds[i]));
    }
}

void SpreadSheet::initTab(){
    int rows = rowCount();
    int cols = columnCount();
    for (int i=0;i<rows;++i)
        for (int j=0; j<cols; ++j){
            setFormula(i,j,QString(""));
            //setAligns(i, j);
        }
}

void SpreadSheet::setAligns(int r, int c){
     ;
}

void SpreadSheet::adjSize(){
    //resizeColumnToContents(0);
    resizeColumnsToContents();

    int w = 0;
    int count = columnCount();
    for (int i = 0; i < count; i++)
        w += columnWidth(i);

    int maxW = ( w
                 + (count<6?2*count :count)
       + (vHeader?verticalHeader()->width():0) ) ;
       // + (verticalScrollBar()->isHidden()? 0 :verticalScrollBar()->width()));
    if (MyMaxW>0)
        if(maxW>MyMaxW) maxW=MyMaxW;

    int h = 0;
    count = rowCount();
    for (int i = 0; i < count; i++)
        h += rowHeight(i);

    int maxH =( h
                + (count < 10 ? count :count/2)
         + (hHeader?horizontalHeader()->height():0) );
        //+ (horizontalScrollBar()->isHidden() ? 0 : horizontalScrollBar()->height()));

    if (maxH > MyMaxH ) {
        maxH = MyMaxH;
        verticalScrollBar()->show();
        maxW += verticalScrollBar()->width();
    }

    setMaximumWidth(maxW);
    setMinimumWidth(maxW);

    setMaximumHeight(maxH);
    setMinimumHeight(maxH);
}

QString SpreadSheet::currentLocation() const
{
    return QChar('A' + currentColumn())
           + QString::number(currentRow() + 1);
}

QString SpreadSheet::currentFormula() const
{
    return formula(currentRow(), currentColumn());
}

QTableWidgetSelectionRange SpreadSheet::selectedRange() const
{
    QList<QTableWidgetSelectionRange> ranges = selectedRanges();
    if (ranges.isEmpty())
        return QTableWidgetSelectionRange();
    return ranges.first();
}

void SpreadSheet::clear()
{
    setRowCount(0);
    setColumnCount(0);
    setRowCount(RowCount);
    setColumnCount(ColumnCount);

    if (hHeader){
    for (int i = 0; i < ColumnCount; ++i) {
        QTableWidgetItem *item = new QTableWidgetItem;
        item->setText(hHeaders[i]);
        setHorizontalHeaderItem(i, item);
    }}
    //*/
    setCurrentCell(0, 0);
}

bool SpreadSheet::readFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::warning(this, tr("SpreadSheet"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(file.fileName())
                             .arg(file.errorString()));
        return false;
    }

    QDataStream in(&file);
    in.setVersion(QDataStream::Qt_4_3);

    quint32 magic;
    in >> magic;
    if (magic != MagicNumber) {
        QMessageBox::warning(this, tr("SpreadSheet"),
                             tr("The file is not a SpreadSheet file."));
        return false;
    }

    clear();

    quint16 row;
    quint16 column;
    QString str;

    QApplication::setOverrideCursor(Qt::WaitCursor);
    while (!in.atEnd()) {
        in >> row >> column >> str;
        setFormula(row, column, str);
    }
    QApplication::restoreOverrideCursor();
    return true;
}

bool SpreadSheet::writeFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::warning(this, tr("SpreadSheet"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(file.fileName())
                             .arg(file.errorString()));
        return false;
    }

    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_4_3);

    out << quint32(MagicNumber);

    QApplication::setOverrideCursor(Qt::WaitCursor);
    for (int row = 0; row < RowCount; ++row) {
        for (int column = 0; column < ColumnCount; ++column) {
            QString str = formula(row, column);
            if (!str.isEmpty())
                out << quint16(row) << quint16(column) << str;
        }
    }
    QApplication::restoreOverrideCursor();
    return true;
}

void SpreadSheet::sort(const fwCompare &compare)
{
    QList<QStringList> rows;
    QTableWidgetSelectionRange range = selectedRange();
    int i;

    for (i = 0; i < range.rowCount(); ++i) {
        QStringList row;
        for (int j = 0; j < range.columnCount(); ++j)
            row.append(formula(range.topRow() + i,
                               range.leftColumn() + j));
        rows.append(row);
    }

    qStableSort(rows.begin(), rows.end(), compare);

    for (i = 0; i < range.rowCount(); ++i) {
        for (int j = 0; j < range.columnCount(); ++j)
            setFormula(range.topRow() + i, range.leftColumn() + j,
                       rows[i][j]);
    }

    clearSelection();
    somethingChanged();
}

void SpreadSheet::cut()
{
    copy();
    del();
}

void SpreadSheet::copy()
{
    QTableWidgetSelectionRange range = selectedRange();
    QString str;

    for (int i = 0; i < range.rowCount(); ++i) {
        if (i > 0)
            str += "\n";
        for (int j = 0; j < range.columnCount(); ++j) {
            if (j > 0)
                str += "\t";
            str += formula(range.topRow() + i, range.leftColumn() + j);
        }
    }
    QApplication::clipboard()->setText(str);
}

void SpreadSheet::paste()
{
    QTableWidgetSelectionRange range = selectedRange();
    QString str = QApplication::clipboard()->text();
    QStringList rows = str.split('\n');
    int numRows = rows.count();
    int numColumns = rows.first().count('\t') + 1;

    if (range.rowCount() * range.columnCount() != 1
            && (range.rowCount() != numRows
                || range.columnCount() != numColumns)) {
        QMessageBox::information(this, tr("SpreadSheet"),
                tr("The information cannot be pasted because the copy "
                   "and paste areas aren't the same size."));
        return;
    }

    for (int i = 0; i < numRows; ++i) {
        QStringList columns = rows[i].split('\t');
        for (int j = 0; j < numColumns; ++j) {
            int row = range.topRow() + i;
            int column = range.leftColumn() + j;
            if (row < RowCount && column < ColumnCount)
                setFormula(row, column, columns[j]);
        }
    }
    somethingChanged();
}

void SpreadSheet::del()
{
    QList<QTableWidgetItem *> items = selectedItems();
    if (!items.isEmpty()) {
        foreach (QTableWidgetItem *item, items)
            delete item;
        somethingChanged();
    }
}

void SpreadSheet::selectCurrentRow()
{
    selectRow(currentRow());
}

void SpreadSheet::selectCurrentColumn()
{
    selectColumn(currentColumn());
}

void SpreadSheet::recalculate()
{
    for (int row = 0; row < RowCount; ++row) {
        for (int column = 0; column < ColumnCount; ++column) {
            if (cell(row, column)){

                cell(row, column)->setDirty();
            }
        }
    }
    viewport()->update();
    for (int row = 0; row < RowCount; ++row) {
        for (int column = 0; column < ColumnCount; ++column) {
            if (cell(row, column)){
                Cell *c=cell(row,column);
                /*if (isSum(row,column) && (c->data(Qt::DisplayRole).toDouble()<0))
                    c->setForeground(QBrush(Qt::red));
                else
                //*/
                    c->setForeground(QBrush(Qt::black));

            }
        }
    }
    adjSize();
}

void SpreadSheet::setAutoRecalculate(bool recalc)
{
    autoRecalc = recalc;
    if (autoRecalc)
        recalculate();
}

void SpreadSheet::findNext(const QString &str, Qt::CaseSensitivity cs)
{
    int row = currentRow();
    int column = currentColumn() + 1;

    while (row < RowCount) {
        while (column < ColumnCount) {
            if (text(row, column).contains(str, cs)) {
                clearSelection();
                setCurrentCell(row, column);
                activateWindow();
                return;
            }
            ++column;
        }
        column = 0;
        ++row;
    }
    QApplication::beep();
}

void SpreadSheet::findPrevious(const QString &str,
                               Qt::CaseSensitivity cs)
{
    int row = currentRow();
    int column = currentColumn() - 1;

    while (row >= 0) {
        while (column >= 0) {
            if (text(row, column).contains(str, cs)) {
                clearSelection();
                setCurrentCell(row, column);
                activateWindow();
                return;
            }
            --column;
        }
        column = ColumnCount - 1;
        --row;
    }
    QApplication::beep();
}

void SpreadSheet::somethingChanged()
{
    if (autoRecalc)
        recalculate();
    emit modified();
}

Cell *SpreadSheet::cell(int row, int column) const
{
    return static_cast<Cell *>(item(row, column));
}

void SpreadSheet::setFormula(int row, int column,
                             const QString &formula)
{
    Cell *c = cell(row, column);
    if (!c) {
        if (alignC) c= new Cell(false, true);
        else if (column==0) c = new Cell(true, false);
        else c = new Cell;
        setItem(row, column, c);
    }

   /* if (isSum(row,column) && (c->data(Qt::DisplayRole).toDouble()<0))
        c->setForeground(QBrush(Qt::red));
   //*/
         c->setFormula(formula);
         if (row==rowCount()-1 && border2) {
                c->setData(Qt::DisplayPropertyRole, -1);
                //c->setFont(QFont("times",10,QFont::Bold));
          }
}

bool SpreadSheet::isSum(int r,int c){
    if (r==RowCount-1) return true;
    return false;
}

QString SpreadSheet::formula(int row, int column) const
{
    Cell *c = cell(row, column);
    if (c) {
        return c->formula();
    } else {
        return "";
    }
}

QString SpreadSheet::text(int row, int column) const
{
    Cell *c = cell(row, column);
    if (c) {
        return c->text();
    } else {
        return "";
    }
}

void SpreadSheet::setRowCountMy(int nr){
    RowCount = nr;
    QTableWidget::setRowCount(RowCount);
}

void SpreadSheet::setColCountMy(int nr){
    ColumnCount = nr;
    QTableWidget::setColumnCount(ColumnCount);
}

void SpreadSheet::setCell(int r, int c, QString str){
    setFormula(r,c,str);
}

bool fwCompare::operator()(const QStringList &row1,
                                    const QStringList &row2) const
{
    for (int i = 0; i < KeyCount; ++i) {
        int column = keys[i];
        if (column != -1) {
            if (row1[column] != row2[column]) {
                if (ascending[i]) {
                    return row1[column] < row2[column];
                } else {
                    return row1[column] > row2[column];
                }
            }
        }
    }
    return false;
}
