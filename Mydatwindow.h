#ifndef MYDATWINDOW_H
#define MYDATWINDOW_H

#include <QMainWindow>
#include <QComboBox>
#include <QLabel>
#include <QGraphicsView>
#include <QGraphicsScene>

#include "AgriPoliS.h"
#include "RegFarm.h"
#include <QGridLayout>
#include <QGroupBox>

#include <QPrinter>
#include "myTypes.h"
#include "cplot.h"

#include <qwt_plot_zoomer.h>

#include <qcheckbox.h>
#include <qradiobutton.h>
#include <qpushbutton.h>
#include "mmspinbox.h"

#include "adjDialog.h"
#include "farmWidget.h"

class QSpinBox;
struct DataReg1;
struct DataReg2;
struct DataMy;
struct BSdat;
struct PlotData;
//class BodenDialog;
class CloseDialog;
class Datawindow;
class Mainwindow;

class Mydatwindow:public QMainWindow {
    Q_OBJECT
public:
    //friend class BodenDialog; //pachtbilanz
    friend class CloseDialog; //Betriebsspiegel
    friend class Datawindow;

    static int const MAXCURVES=5;
    static int const numCWidgets=9;
    static const int numCurves=4;
    map<int,string> classnames;
    map<int,string> legnames;
    int nSoils;

    int minIter;
    int maxIter;

    void faOutput();

    explicit Mydatwindow(QWidget *parent, Mainwindow *p);
    void updateMydata();
    void initMydata();
    //void paintEvent(QPaintEvent *);

    void saveConfig();
    void getLiquidity0();

    //ESU
    //max, min X
    int maxX, minX;
    vector<double> esuXdata;
    vector<vector<double> > esuYdatas;
    vector<string> esuYtitles;
    vector<QColor> esuColors;
    vector<int> esuYaxes;
    vector<QwtSymbol::Style> esuStyles;

    QColor gridColor, canvasColor;
    vector<int> ranks, tots;

    //Eigenkapital
    int maxXEK, minXEK;
    vector<double> ekXdata;
    vector<vector<double> > ekYdatas;
    vector<string> ekYtitles;
    vector<QColor> ekColors;
    vector<int> ekYaxes;
    vector<QwtSymbol::Style> ekStyles;

    QColor gridColorEK, canvasColorEK;
    vector<int> rankEKs, totEKs;


    vector<vector<double> > aktivasI;
    vector<vector<double> > passivasI;

    vector<QString> sAktivas;
    vector<QString> sPassivas;

    vector<vector<double> > pbDatasI;

    vector<QString> sErtraege, sAufwendungen, sFinanz;
    vector<vector<double> > ertraeges, aufwendungens, finanzs;
    vector<double> gewinns;

    /*vector<vector<double> > kaDatasI;
    vector<QString> sKAs;
    vector<vector<int> > kaColsI;
    //*/
    vector<QString> sHabens, sSolls;
    vector<vector<double> > habens, solls;

    vector<double> altKontos, neuKontos;

    vector<int> getGVRsizes();
    vector<int> getKAsizes();

    void getStalls0(RegFarmInfo *farm, DataMy *);
    void getStalls(RegFarmInfo *farm, DataMy *);
    void getMachines0(RegFarmInfo *farm, DataMy *);
    void getMachines(RegFarmInfo *farm, DataMy *);

    vector<BSdat> bsdats;
    vector<vector<PlotData> > pvdats ;
    vector<double>  liqdats ;
  
public slots:
    bool plotsCreated();

    void closeEvent(QCloseEvent*);

    void updateGraphic(int);
    void updateGraphic(bool);
    void updateWithBorder();

    void updatePlot();
    //void updateData();

    void showAdjDialog(bool);
    void updateConfig();

    void updateXmin(int);
    void updateXmax(int);

    void updatePlotFarben(QColor,QColor);
    void updateCurveFarben(int,QColor);
    void updateCurveStyle(int,QwtSymbol);

    void showRunde(int);

    void drucken();

signals:
    void hi(bool);

protected:
    QString getFileName();
    void kurvenOutput();
    void sbilanzOutput();
    void pbilanzOutput();
    void kontoOutput();
    void gvRechnungOutput();
    void bspiegelOutput();

    void collectData(list<RegFarmInfo*> li,  vector<DataReg1> &v,
                     vector<DataReg2> &v2, DataMy &md);
    void createActions();
    void createToolBars();

    void createStatusBar();
    void updateStatusBar();

    void createCWidgets();
  
    void enableZoomMode(bool);

    void updateGraphics();
    void updateESU();
    void updateEK();


    //SB-schlussbilanz, GVR-gewinn-/verlustrechnung,
    //PB-pachtbilanz, KA-kontoauszug, B(etriebs)Spiegel
    //PV-pachtverträge, LIQ-Liquidität
    void updateSB();
    void updateGVR();
    void updatePB();
    void updateKA();
    void updateBSpiegel();
    void updatePV();
    void updateLIQ();
  
    void initXs();
    void initHasDialogs();

    void initPlotColors();
    void initStyles();

    void initCurveColors();

private:
    Mainwindow *par;
    bool inited;
    int nFarmtypes;

    
    QPrinter *printer;

    bool withBorder;
    bool hasCWidgets;

    int farmAge;
    int nInvests;
    RegInvestList invList;

private:    
    QToolBar *chooseLayerToolBar;
    QToolBar *zoomToolBar;

    QToolBar *colorToolBar;
    QToolBar *optToolBar;

    QAction *actConfig;
    QAction *actWithBorder;
    QAction *actPrint;

    QLabel *layerLabel;
public:   QComboBox *layerComboBox;

private:
    QGraphicsItemGroup *gridGroup;

    QLabel *statusLabel;

    Plot *plot;
    QWidget *cWidget;
    QVBoxLayout *clout;
    vector<QWidget*> tabs;

    int oldlayer;
    QwtPlotZoomer *d_zoomer[2*numCWidgets];

    vector< vector<DataReg1> > regDatavec2;
    vector< vector<DataReg2> > regDatavec3;
    vector<DataMy> myDatas;

    vector<bool> hasDialogs;
    adjDialog *anpassenDialog, *anpassenDialogEK;


    vector<string> namessoils;

    QSpinBox *iterSBox;
    QLabel *iterLab;
    int iter;
    int maxRunde;

    QAction *actIter;
    int lenG, lenV, lenF;
    int lenKG, lenKV;

    double liquidity0;
    double resEcShare0;
    bool fwReady;
};

struct DataReg1 {
    int farmid;
    double esusize;
};

struct DataReg2 {
    int farmid;
    double ek;
};

struct stalldata{
    string name;
    double used, capacity;
    int num;
    double age;
    int life;
};

struct machinedata{
    string name;
    double capacity;
    int num;
    double age;
    int life;
};

struct BSdat{
    bool isClosed;
    //Betriebsspiegel
    int farmnr;
    int EKrank, EKtotal;
    int farmage, farmtype;
    int legaltype;
    double liq, ek;
    double fam_AK, lohn_AK;
    vector<vector< double> > restdauerAreasOfSoils;
    vector<double> pachtPreisOfSoils;
    vector<stalldata> stalls;
    vector<machinedata> machines;
    vector<double> landSums;
    vector<double> ownLands;
    vector<double> rentLands;
};

struct PlotData{
    int col, row;
    int soiltype;
    double price;
    int resttime;
};

struct DataMy {
    bool isClosed;

    //Schlussbilanz
    double landAssets;
    double totalFixedAssets;
    double liquidity;
    double equityCapital;
    double borrowedCapital;

    //Gewinn-/Verlustrechnung
    double ackerbauG;
    double milchG;
    double rinderG;
    double schweineG;
    double offfarmlabG;
    double dirZahlungenG;
    double sonstG;

    double ackerbauV;
    double milchV;
    double rinderV;
    double schweineV;
    double loehneV ;
    double unterhaltungV;
    double abschreibungV;
    double gemeinkostenV;
    double zupachtungV;
    double sonstV;
    double tacV;
    double distCostsV;

    double zinsG;
    double zinsV;

    double totalIncome;

    //Pachtbilanz
    double totalLand;
    double zugepachtLand;

    //Kontoauszug
    double withdraw;
    double resEcShare;
    double annuity;
    double st_zins;

    //Betriebsspiegel
    int EKrank, EKtotal;
    int farmage, farmtype;
    int legaltype;
    double fam_AK, lohn_AK;
    vector<vector< double> > restdauerAreasOfSoils;
    vector<double> pachtPreisOfSoils;
    vector<stalldata> stalls;
    vector<machinedata> machines;
    vector<double> landSums;
    vector<double> ownLands, rentLands;
    vector<PlotData> plotDatas;

    DataMy(){
        //Schlussbilanz
         landAssets=0;
         totalFixedAssets=0;
         liquidity=0;
         equityCapital=0;
         borrowedCapital=0;

        //Gewinn-/Verlustrechnung
         ackerbauG=0;
         milchG=0;
         rinderG=0;
         schweineG=0;
         offfarmlabG=0;
         dirZahlungenG=0;
         sonstG=0;

         ackerbauV=0;
         milchV=0;
         rinderV=0;
         schweineV=0;
         loehneV =0;
         unterhaltungV=0;
         abschreibungV=0;
         gemeinkostenV=0;
         zupachtungV=0;
         sonstV=0;
         tacV=0;
         distCostsV=0;

         zinsG=0;
         zinsV=0;

         totalIncome=0;

        //Pachtbilanz
         totalLand=0;
         zugepachtLand=0;

        //Kontoauszug
         withdraw=0;
         resEcShare=0;
    }

};

#endif // Mydatwindow_H
