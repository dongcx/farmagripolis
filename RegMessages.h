//---------------------------------------------------------------------------
#ifndef RegMessagesH
#define RegMessagesH
#define CM_DRAWPLOT  (WM_APP + 400)
#define CM_MANAGER_READY (WM_APP +410)
#define CM_UPDATE_INFO (WM_APP + 420)
#define CM_IMAGE_INIT (WM_APP + 430)
#define CM_STATE_INFO (WM_APP + 440)
#include <string>
//class RegFarmInfo;
using namespace std;
// attributes of plot necessary for plotting
struct PlA {                  // Plot appearance
    int col;
    int row;
    int number;
    int plot_colour;
    int state;
    int soil_type;
    string soil_name;
    int farm_id;
};
/*
struct TCMDrawPlot {
    Cardinal Msg;       // Erster Parameter: die Botschafts-ID
    Word Unused1;      // Der erste wParam
    Word Unused2;
    struct PlA *PlotAppearance;    // Der lParam
    Longint Result;    // Der R�ckgabewert
};
struct TCMStateInfo {
    Cardinal Msg;       // Erster Parameter: die Botschafts-ID
    Word Unused1;      // Der erste wParam
    Word Unused2;
    string *state;    // Der lParam
    Longint Result;    // Der R�ckgabewert
};
//*/
//---------------------------------------------------------------------------
#endif
