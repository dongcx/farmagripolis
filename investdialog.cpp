#include "investdialog.h"
#include "AgriPoliS.h"
#include <QLineEdit>
#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QSignalMapper>
#include <QMessageBox>
#include <QGroupBox>
#include <QScrollArea>

#include <cell2.h>

static QColor bgFinColor = Qt::cyan;

static QFont font0 = QFont("SansSerif",14, QFont::Bold);
static QFont font1 = QFont("Times",11, QFont::Bold);
static QFont font2 = QFont("Times",11, QFont::Normal);//Bold);
static QFont font3 = QFont("Times",10, QFont::Normal);

InvestDialog::InvestDialog(QWidget* parent):aktComb(0),numComb(1),firstGUI(true),inited(false),showPEwidget(false),QDialog(parent){
    nurHead = false;
    setMinimumHeight(650);
    setMinimumWidth(950);
}

void InvestDialog::closeEvent(QCloseEvent * event){
    gg->HasInvestDec=true;
    inited=false;
    Manager->getGlobals()->SIM_WEITER =true;
    Manager->product_cat0=Manager->Market->getProductCat();
    QDialog::closeEvent(event);
}

void InvestDialog::dispInvest(int i, int d){
    l1->setText(QString::number(i));
    l2->setText(QString::number(d));
}

void InvestDialog::okPushed(){
    int d1,d2;
    d1 = le1->text().toInt();
    d2 = le2->text().toInt();
    emit invest(d1,d2);
}

void InvestDialog::updateComb(int ii){
    inComb = true;
    aktComb = indComb(ii);

    Title4* t4 = titles[ii];
    t4->setStyleSheet("QLabel { background-color : yellow ;}");

    updateHead();

  if (!nurHead) {
    farm0->myInvs = myInvs = combs[combInd(aktComb)].invs;
    int n = prodIDs.size();
    for (int r=0;r<n;++r){
        if (r==refpos) {
            double tind =combs[combInd(aktComb)].pes[r]/prodPrices[refpos];
            getreidePEs.clear();
            int ts = getreideIDs.size();
            for (int i=0; i<ts; ++i){
                getreidePEs.push_back(Manager->product_cat0[getreideIDs[i]].getPriceExpectation());
            }
            for (int i=0; i<ts; ++i){
                Manager->product_cat0[getreideIDs[i]].setPriceExpectation(getreidePrices[i]*tind);
            }
        }
        Manager->product_cat0[prodIDs[r]].setPriceExpectation(combs[combInd(aktComb)].pes[r]);
    }
  }
    invRefs = combs[combInd(aktComb)].invRefs;
    updateFinWidget(aktComb);
    updateInvWidget(aktComb);
    updatePeWidget(aktComb);
    inComb=false;
}

void InvestDialog::updateCombs(){
    vector<double> ps, fs;
    vector<int> is, rs;
    int n = peDSBs.size();
    for (int i=0; i<n; ++i){
        ps.push_back(peDSBs[i]->value());
    }

    fs.push_back(langFK);
    fs.push_back(dispo);
    fs.push_back(GDB);
    fs.push_back(liquidity);
    fs.push_back(profit);
    fs.push_back(income);

    n = invDSBs.size();
    for (int i=0; i<n; ++i){
        is.push_back(invDSBs[i]->value());
        rs.push_back(invSS->cell(i,2)->data(Qt::DisplayRole).toInt());
    }
    Komb comb ;
    comb.fins = fs;
    comb.invs = is;
    comb.pes = ps;
    comb.invRefs= rs;

    myInvs = is;

    combs.push_back(comb); //ref
    combs.push_back(comb); //1
}

void InvestDialog::saveComb(int ic){
    saveCombPe(ic);
    saveCombInv(ic);
    saveCombFin(ic);
}

void InvestDialog::saveCombPe(int ic){
    vector<double> ps;
    int n = peDSBs.size();
    for (int i=0; i<n; ++i){
        ps.push_back(peDSBs[i]->value());
    }
    combs[combInd(ic)].pes = ps;
}

void InvestDialog::saveCombFin(int ic){
    vector<double> fs;
    fs.push_back(langFK);
    fs.push_back(dispo);
    fs.push_back(GDB);
    fs.push_back(liquidity);
    fs.push_back(profit);
    fs.push_back(income);

    combs[combInd(ic)].fins = fs;
}

void InvestDialog::saveCombInv(int ic){
    vector<int> is,rs;
    int n = invDSBs.size();
    for (int i=0; i<n; ++i){
        is.push_back(invDSBs[i]->value());
        rs.push_back(invSS->cell(i,2)->data(Qt::DisplayRole).toInt());
    }

    combs[combInd(ic)].invs = is;
    combs[combInd(ic)].invRefs = rs;
}

void InvestDialog::delKomb(){
    int aktComb0 = aktComb;
    if (aktComb==0) {
        QMessageBox::warning(this, tr("Information"),
                         tr("Ref-Plan kann nicht gel�scht werden."));
        return;
    }
    vector<struct Komb>::iterator it = combs.begin();
    combs.erase(it+combInd(aktComb));
    vector<QWidget*>::iterator itt = wins.begin()+combInd(aktComb);
    QWidget *qw = *itt;
    wins.erase(itt);
    delete qw;

    if (combInd(aktComb) == combs.size())
        aktComb=indComb(combInd(aktComb)-1);
    else
        aktComb=indComb(combInd(aktComb)+1);

    titles.erase(titles.begin()+combInd(aktComb0));

    vector<int>::iterator itt2 = plans.begin()+combInd(aktComb0);
    plans.erase(itt2);

    titles[combInd(aktComb)]->setStyleSheet("QLabel { background-color : yellow;}");

    updateHead();
    updateComb(combInd(aktComb));
    sa->widget()->adjustSize();
}

void InvestDialog::neuKomb(){
    //return;

    int altNr = aktComb;
    titles[combInd(altNr)]->setStyleSheet("QLabel { background-color : lightgray;}");
    combs.push_back(combs[combInd(aktComb)]);
    aktComb = numComb;

    struct Komb cb = combs[combInd(altNr)];
    myInvs = cb.invs;
    invRefs = cb.invRefs;

    //QHBoxLayout *saHLout= static_cast<QHBoxLayout*> (sa->widget()->layout());

    createFinCopy(cb,aktComb);
    saHLout->addWidget(finW);
 
    QWidget *saWidget = new QWidget;
    saWidget->setLayout(saHLout);
    sa->setWidget(saWidget);

    titles[combInd(aktComb)]->setStyleSheet("QLabel { background-color : yellow;}");

    ++numComb;
    updateHead();
    
    //saHLout->activate();
    //saWidget->adjustSize();
}

void InvestDialog::updateGUI(){
    ITER  = gg->tIter;
    farm0 = Manager->Farm0;

    langFK=farm0->getLangFKinv();
    dispo=farm0->getDispoInv();
    GDB=farm0->getGDBinv();
    liquidity=farm0->getLiqInv();
    profit=farm0->getProfInv();
    income=farm0->getIncInv();

    if (!firstGUI) {
        updateGUI2();
        return;
    }
    QString title("Entscheidungen �. Investitionen");

    Title4 *titleLab = new Title4(Qt::AlignCenter, font0);
    titleLab->setText(title);

    QVBoxLayout *v0 = new QVBoxLayout; //out
    v0->addWidget(titleLab);

    weiterButton = new QPushButton("Ok && weiter");
    weiterButton->setFont(font1);
    connect(weiterButton, SIGNAL(clicked()), this, SLOT(weiter()));

    weiterWidget  = new QWidget;

    QHBoxLayout *hh = new QHBoxLayout;
    hh->addStretch();
    hh->addWidget(weiterButton);
    hh->addStretch();
    weiterWidget->setLayout(hh);

    QLabel *anzeigeLab  = new QLabel("akt. Plan");
    aktLE = new QLineEdit;
    aktLE->setMaximumWidth(60);
    aktLE->setAlignment(Qt::AlignRight);

    //nrSBox = new QSpinBox;
    //nrSBox->setMinimum(1);

    //connect(nrSBox,SIGNAL(valueChanged(int)), this, SLOT(updateComb(int)));

    QGroupBox *neuGBox = new QGroupBox;
    QHBoxLayout *hl0 = new QHBoxLayout;
    newKombButton = new QPushButton("neuen Plan");
    connect(newKombButton,SIGNAL(clicked()), this, SLOT(neuKomb()));

    delButton = new QPushButton("akt. Plan L�schen");
    connect(delButton,SIGNAL(clicked()), this, SLOT(delKomb()));

    numComb = 2;
    aktComb = 1;
    aktLE->setText(QString::number(aktComb));
    anzKomb = new QLineEdit(QString::number(numComb));
    anzKomb->setMaximumWidth(60);
    anzKomb->setAlignment(Qt::AlignRight);
    anzKomb->setReadOnly(true);
    QLabel *anzLab  = new QLabel("Anzahl Pl�ne:");

    //nrSBox->setValue(aktComb);
    //nrSBox->setMaximum(numComb);

    //hl0->addStretch();
    hl0->addWidget(weiterWidget);
    hl0->addSpacing(100);
    hl0->addStretch();
    hl0->addWidget(anzeigeLab);
    hl0->addWidget(aktLE);
    //hl0->addWidget(nrSBox);
    hl0->addSpacing(50);
    hl0->addStretch();
    hl0->addWidget(newKombButton);
    hl0->addStretch();
    hl0->addSpacing(40);
    hl0->addWidget(delButton);
    hl0->addSpacing(50);
    hl0->addWidget(anzLab);
    hl0->addWidget(anzKomb);
    //hl0->addStretch();
    neuGBox->setLayout(hl0);

    v0->addWidget(neuGBox);

    makePeWidget();
    makeFinWidget();
    makeInvWidget();

    updateCombs();

    QGroupBox *combGBox = new QGroupBox("Aktueller Plan");
    QHBoxLayout *hl1 = new QHBoxLayout;

    Title4 *combTit  = new Title4(Qt::AlignCenter, font1);
    combTit->setText(QString("Plan-Nr.")+ QString::number(aktComb));

    hl1->addStretch();
    hl1->addWidget(invWidget);
    hl1->addWidget(finWidget);
    hl1->addWidget(peWidget);
    hl1->addStretch();

    QWidget *ssWidget = new QWidget;
    ssWidget->setLayout(hl1);

    peButton = new QPushButton("Preiserwartung anpassen");
    showPEwidget=false;
    peWidget->setVisible(showPEwidget);//hide();
    connect(peButton, SIGNAL(clicked()), this, SLOT(blendenPE()));

    QHBoxLayout *hh5 = new QHBoxLayout;
    hh5->addWidget(peButton);
    hh5->addStretch();

    QWidget *peButtonWidget = new QWidget;
    peButtonWidget->setLayout(hh5);

    QVBoxLayout *vl0 = new QVBoxLayout;
    //vl0->addWidget(combTit);
    vl0->addWidget(ssWidget);
    vl0->addWidget(peButtonWidget);

    combGBox->setLayout(vl0);

    v0->addWidget(combGBox);

    //v0->addWidget(weiterWidget);

    QGroupBox *overviewGBox = new QGroupBox("�bersicht Pl�ne(Finanzen)");

    sa = new QScrollArea;
    sa->setMinimumHeight(220);
    QWidget *saWidget = new QWidget;

    //QHBoxLayout *
    saHLout = new QHBoxLayout;

    saHLout->addWidget(makeFinNamen());
    /*
    for (int i=0; i<numComb; ++i) {
        struct Komb cb=combs[i];
    //*/
    struct Komb cb = combs[0];
    createFinCopy(cb,0);
    titles[combInd(0)]->setStyleSheet("QLabel { background-color : lightgray;}");
    saHLout->addWidget(finW);
    cb=combs[1];
    createFinCopy(cb,1);
    titles[combInd(1)]->setStyleSheet("QLabel { background-color : yellow;}");
    saHLout->addWidget(finW);

    saWidget->setLayout(saHLout);
    sa->setWidget(saWidget);

    updateHead();

/*
    QLabel *anzeigeLab  = new QLabel("Anzeige Nr. ");
    nrSBox = new QSpinBox;
    nrSBox->setValue(1);

    QHBoxLayout *hl2 = new QHBoxLayout;
    hl2->addStretch();
    hl2->addWidget(anzeigeLab);
    hl2->addWidget(nrSBox);
    hl2->addStretch();

    QWidget *anzeigeWidget = new QWidget;
    anzeigeWidget->setLayout(hl2);
//*/
    QVBoxLayout *vl2 = new QVBoxLayout;
    vl2->addWidget(sa);
    //vl2->addWidget(anzeigeWidget);

    overviewGBox->setLayout(vl2);

    v0->addWidget(overviewGBox);
    v0->addStretch(1);

    QWidget *allWidget = new QWidget;
    allWidget->setLayout(v0);
    allWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QScrollArea *allArea = new QScrollArea;
    allArea->setWidget(allWidget);
    allArea->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    allArea->setMinimumHeight(640);
    allArea->setMinimumWidth(940);

    QHBoxLayout *allLout = new QHBoxLayout;
    allLout->addWidget(allArea);
    setLayout(allLout);

    resize(950,650);
    firstGUI = false;
}

void InvestDialog::updatePeWidget(int i){
   bool ok = inited;
   if (ok) inited = false;
   for (int r=0; r<nRowPE; ++r){
            QDoubleSpinBox *dsb=peDSBs[r];
            dsb->setValue(combs[combInd(i)].pes[r]);
    }

    if (ok) inited = true;
    preisIndex = combs[combInd(i)].pes[refpos]/prodPrices[refpos];
    pindexLab->setText(QString("Preisindex f�r alle Marktfr�chte (wie der von %1) : %2").arg(refname, QLocale().toString(preisIndex)));
}

void InvestDialog::makePeWidget2(){
    refzeitSBox->setMaximum(ITER);

    vector<RegProductInfo> pcat = *(farm0->product_cat);
    int sz = numPEs;//pcat.size();

    for (int i=0; i<sz; ++i){
        prodPrices[i]=pcat[prodIDs[i]].getPrice();
        prodPEs[i]=pcat[prodIDs[i]].getPriceExpectation();
    }
    prodRefPEs = prodPEs;
    naivPEs = prodPrices;
    adaptPEs = prodPEs;
    manPEs = prodPEs;

    allPEs.push_back(adaptPEs);
    calRefPEs();

    peVar = peVarCBox->currentIndex();
    for (int r=0; r<nRowPE; ++r){
            double max, min;
            max = prodPrices[r]*1.5;
            min = prodPrices[r]*0.7;
            if (max<0) {
                double t = max;
                max = min;
                min = t;
            }
            QDoubleSpinBox *dsb=peDSBs[r]; 
            dsb->setRange(min,max);
            dsb->setValue(prodPEs[r]);
            //setText(QLocale().toString(prodPrices[r],'f',2));
    }

    peSS->updateColumn(1, prodPrices);
    peSS->updateColumn(2, prodRefPEs);
    //peSS->updateColumn(3, prodPEs);

    preisIndex = prodPEs[refpos]/prodPrices[refpos];
    pindexLab->setText(QString("Preisindex f�r alle Marktfr�chte (wie der von %1) : %2").arg(refname, QLocale().toString(preisIndex)));
    //inited = true;
}

void InvestDialog::makeInvWidget(){
   invWidget = new QWidget;

    Title4* invTitle = new Title4(Qt::AlignCenter, font1);
    invTitle->setText("Investitionen");

    vector<QString> vs;
    //vs.push_back("Typ");
    vs.push_back(QString("Name"));
    vs.push_back(QString("Kapazit�t"));
    vs.push_back(QString("Referenz"));
    vs.push_back(QString("Meine Entsch."));

    vector<RegInvestObjectInfo> icat = Manager->InvestCatalog;
    int sz = icat.size();

    invRefs=farm0->invRefs;
    //bool newType = true;
    for (int i=0; i<sz; ++i){
        invNames.push_back(QString(icat[i].getName().c_str()));
        invCaps.push_back(icat[i].getCapacity());
        myInvs.push_back(invRefs[i]);
    }

    nRowInv = sz; //25;
    nColInv = 4;
    invSS = new SpreadSheet2(nRowInv, nColInv, true, vs, false);
    invSS->setArt(2);
    invSS->setTabKeyNavigation(false);

    QSignalMapper *sigmapInv = new QSignalMapper;

    for (int r=0; r<nRowInv; ++r){
            QSpinBox *dsb = new QSpinBox();

            dsb->setAlignment(Qt::AlignRight);
            dsb->setSuffix("  ");
            //dsb->setLocale(QLocale::German);
            dsb->setValue(myInvs[r]);
            invSS->setCellWidget(r,nColInv-1,dsb);

            invDSBs.push_back(dsb);
            connect(dsb, SIGNAL(valueChanged(int)), sigmapInv, SLOT(map()));
            sigmapInv->setMapping(dsb, r);
    }

    connect(sigmapInv, SIGNAL(mapped(int)), this, SLOT(updateInv(int)));

    invSS->updateColumn(0, invNames);
    invSS->updateColumn(1, invCaps);
    invSS->updateColumn(2, invRefs);

    QVBoxLayout *vl = new QVBoxLayout;
    vl->addWidget(invTitle);
    vl->addSpacing(5);

    vl->addWidget(invSS);
    // vl->addStretch(1);
    vl->setAlignment(Qt::AlignTop);
    invWidget->setLayout(vl);

   /*QWidget *kw = new QWidget;
   kw->resize(100, 150);

   Title4 *invTit  = new Title4(Qt::AlignCenter, font1);
   invTit->setText(QString("Investitionen"));
   QVBoxLayout *vl = new QVBoxLayout;
   vl->addWidget(invTit);
   vl->addWidget(kw);

   vl->setAlignment(Qt::AlignTop);
   invWidget->setLayout(vl);
   //*/
}

void InvestDialog::updateFins(int akt){
    //cout << akt <<endl;
    vector<QString> td;
    td.push_back(QLocale().toString(langFK,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(dispo,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(GDB,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(liquidity,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(profit,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(income,'f',2)+" "+QChar(0x20ac));

    fins[akt]->updateColumn(0, td);//1,td);
}

QWidget* InvestDialog::makeFinNamen(){
    finWidget = new QWidget;
    Title4* finTitle = new Title4(Qt::AlignCenter, font1);
    finTitle->setText(QString("Parameter"));

    vector<QString> tv;
    SpreadSheet2 *fss = new SpreadSheet2(6,1,false,tv,false);

    tv.push_back(QString("langfristiges FK"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Dispo"));//+QChar(0x20ac)+")");
    tv.push_back(QString("GDB"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Liquidit�t"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Gewinn"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Haushaltseinkommen"));//+QChar(0x20ac)+")");

    fss->updateColumn(0,tv);

    QVBoxLayout *vl = new QVBoxLayout;
    vl->addWidget(finTitle);

    QHBoxLayout *hf = new QHBoxLayout;
    hf->addStretch();
    hf->addWidget(fss);
    hf->addStretch();
    QWidget *hw = new QWidget;
    hw->setLayout(hf);

    vl->addWidget(hw);

    finWidget->setLayout(vl);

    return finWidget;
}

void InvestDialog::updateFins(){
    //QHBoxLayout *
    saHLout = new QHBoxLayout;

    struct Komb cb = combs[0];

    /*
    foreach (SpreadSheet2 *s, fins){
        delete s;
    }

    foreach (QWidget *w, wins){
        delete w;
    }
    //*/

    fins.clear();
    wins.clear();
    plans.clear();
    titles.clear();

    saHLout->addWidget(makeFinNamen());
    createFinCopy(cb,0);
    titles[combInd(0)]->setStyleSheet("QLabel { background-color : lightgray;}");
    saHLout->addWidget(finW);

    cb = combs[1];
    createFinCopy(cb,1);
    saHLout->addWidget(finW);
    titles[combInd(1)]->setStyleSheet("QLabel { background-color : yellow;}");
    //sw->setLayout(saHLout);

    QWidget *saWidget = new QWidget;
    saWidget->setLayout(saHLout);

    //saHLout->activate();
    //saWidget->adjustSize();
    sa->setWidget(saWidget);
}

void InvestDialog::savePEs(){
    it_myPEs.clear();

    switch(peVar) {
    case 0: it_myPEs = manPEs; break;
    case 1: it_myPEs = naivPEs; break;
    case 2: it_myPEs = adaptPEs; break;
    default: ;
    }

    all_myPEs.push_back(it_myPEs);
}

void InvestDialog::blendenPE(){
    if (!showPEwidget) {
        peButton->setText("Ausblenden Preiserwartung");
    }else{
        peButton->setText("Preiserwartung anpassen");
    }
    peWidget->setVisible(!showPEwidget);
    showPEwidget = !showPEwidget;

    //alle vorg�nger
    peWidget->parentWidget()->layout()->invalidate();
    QWidget *parent = peWidget->parentWidget();
    while (parent) {
        if (!parent->parentWidget()) break; //mainwindow nicht
        parent->adjustSize();
        parent = parent->parentWidget();
    }
    //*/
}


void InvestDialog::makePeWidget(){
    Title4* peTitle = new Title4(Qt::AlignCenter, font1);
    peTitle->setText("Preiserwartung (PE)");

    QLabel *vartype = new QLabel("Variante");
    peVarCBox = new QComboBox;
    peVarCBox->addItem(QString("Manuell"));
    peVarCBox->addItem(QString("Naiv"));
    peVarCBox->addItem(QString("Adaptiv"));

    connect(peVarCBox, SIGNAL(currentIndexChanged(int)), this, SLOT(updateVar(int)));

    QHBoxLayout *hl = new QHBoxLayout;
    hl->addStretch();
    hl->addWidget(vartype);
    hl->addWidget(peVarCBox);
    hl->addStretch();

    QLabel *refLab = new QLabel("Referenz-Zeit");
    refzeitSBox = new QSpinBox;
    refzeitSBox->setMaximum(ITER+1);
    refzeitSBox->setValue(1);
    refzeitSBox->setMinimum(1);
    QLabel *jahrLab = new QLabel("Jahre");

    connect(refzeitSBox, SIGNAL(valueChanged(int)), this, SLOT(updateRef(int)));

    QHBoxLayout *hl1 = new QHBoxLayout;
    hl1->addStretch();
    hl1->addWidget(refLab);
    hl1->addWidget(refzeitSBox);
    hl1->addWidget(jahrLab);
    hl1->addStretch();
    //*/

    vector<QString> vs;
    vs.push_back("Produktname");
    vs.push_back(QString("Preis (")+QChar(0x20ac)+")");
    vs.push_back(QString("Ref. PE (")+QChar(0x20ac)+")");
    vs.push_back(QString("Meine PE (")+QChar(0x20ac)+")");
    vs.push_back(QString("Preisindex"));

    int xcount = 0;
    vector<RegProductInfo> pcat = *(farm0->product_cat);
    int sz = pcat.size();
    bool refnameFound = false;

    for (int i=0; i<sz; ++i){
        //if (pcat[i].getProductGroup()<0) continue;
        if (pcat[i].getProductGroup()== 0  && !refnameFound) {
            refname = QString(pcat[i].getName().c_str());
            refprodID = i;
            refpos = xcount;
            refnameFound = true;

        }else if (pcat[i].getProductGroup()==0){
            getreideIDs.push_back(i);
            getreidePEs.push_back(pcat[i].getPriceExpectation());
            getreidePrices.push_back(pcat[i].getPrice());
            continue;
        }
        ++xcount;
        prodNames.push_back(QString(pcat[i].getName().c_str()));
        prodPrices.push_back(pcat[i].getPrice());
        prodIDs.push_back(i);
        prodPEs.push_back(pcat[i].getPriceExpectation());
        //prodRefPEs.push_back(100);
    }
    prodRefPEs = prodPEs;
    naivPEs = prodPrices;
    adaptPEs = prodPEs;
    manPEs = prodPEs;
    numPEs = prodIDs.size();

    allPEs.push_back(adaptPEs);
    calRefPEs();

    nRowPE = xcount; //25;
    nColPE = 4;
    peSS = new SpreadSheet2(nRowPE, nColPE, true, vs, false);
    peSS->setArt(2);
    peSS->setTabKeyNavigation(false);

    QSignalMapper *sigmapPE = new QSignalMapper;

    peVar = peVarCBox->currentIndex();
    //if (peVar==0) {
        for (int r=0; r<nRowPE; ++r){
            QDoubleSpinBox *dsb = new QDoubleSpinBox();
            //QLineEdit *dsb = new QLineEdit();

            double max, min;
            max = prodPrices[r]*1.5;
            min = prodPrices[r]*0.7;
            if (max<0) {
                double t = max;
                max = min;
                min = t;
            }

           // MyDoubleValidator *dv = new MyDoubleValidator(min,max,10);
           //dv->setNotation(QDoubleValidator::StandardNotation);
           //dsb->setValidator(dv);
            dsb->setRange(min,max);
            dsb->setAlignment(Qt::AlignRight);
            dsb->setSuffix("  ");
            dsb->setLocale(QLocale::German);
            dsb->setValue(prodPrices[r]);//setText(QLocale().toString(prodPrices[r],'f',2));
            peSS->setCellWidget(r,nColPE-1,dsb);

            peDSBs.push_back(dsb);
            connect(dsb, SIGNAL(valueChanged(double)), sigmapPE, SLOT(map()));
            sigmapPE->setMapping(dsb, r);
            //peSS->cell(r,nColPE-1)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable);
            //peSS->cell(r,nColPE-1)->setBackgroundColor(cellBColor);
        }
    //}
    connect(sigmapPE, SIGNAL(mapped(int)), this, SLOT(updatePE(int)));

    peSS->updateColumn(0, prodNames);
    peSS->updateColumn(1, prodPrices);
    peSS->updateColumn(2, prodRefPEs);
    //peSS->updateColumn(3, prodPEs);

    peWidget=new QWidget;

    //refname=QString("Weizen");
    preisIndex = prodPEs[refpos]/prodPrices[refpos];//1.101;
    pindexLab =  new QLabel(QString("Preisindex f�r alle Marktfr�chte (wie der von %1) : %2").arg(refname, QLocale().toString(preisIndex)));

    QVBoxLayout *vl = new QVBoxLayout;
    vl->addWidget(peTitle);
    vl->addSpacing(5);

    QGridLayout *gl = new QGridLayout;
    gl->addLayout(hl, 0, 0);
    gl->addLayout(hl1, 0, 1);
    //vl->addLayout(hl);
    //vl->addLayout(hl1);
    //vl->addStretch();
    vl->addLayout(gl);
    vl->addWidget(peSS);
    vl->addWidget(pindexLab);

    // vl->addStretch(1);
    vl->setAlignment(Qt::AlignTop);
    peWidget->setLayout(vl);
    inited = true;
}

void InvestDialog::updateRef(int x){
    calRefPEs();
    peSS->updateColumn(2, prodRefPEs);
}

void InvestDialog::updateVar(int i){
   peVar = i;

    bool en = false;
    if (i==0) {
        en = true;
    }

    vector<double> tvalues ;
    switch (i) {
    case 0: tvalues = manPEs;break;
    case 1: tvalues = naivPEs; break;
    case 2: tvalues = adaptPEs; break;
    default: ;
    }

    for (int i=0; i<numPEs; ++i){
        peDSBs[i]->setValue(tvalues[i]);

    }

    updateIndexLab();

    for (int r=0; r<nRowPE; ++r){
        static_cast<QDoubleSpinBox*>(peSS->cellWidget(r,nColPE-1))->setEnabled(en);
    }
}

void InvestDialog::updateHead(){
    nurHead = true;
    //nrSBox->setMaximum(numComb);
    //nrSBox->setValue(aktComb);
    aktLE->setText(QString::number(aktComb));
    anzKomb->setText(QString::number(combs.size()));
    nurHead = false;
}

void InvestDialog::updateGUI2(){
    combs.clear();

    aktComb=1;
    numComb=2;

    updateInvWidget();
    updateFinWidget();
    makePeWidget2();

    updateCombs();

    //fins.clear();
    updateFins();

    updateHead();
    inited = true;
}


void InvestDialog::showInvest(){
    updateGUI();
    show();
    //exec();
}

void InvestDialog::makeFinWidget(){
    finWidget = new QWidget;
    Title4* finTitle = new Title4(Qt::AlignCenter, font1);
    finTitle->setText("Finanzen");

    vector<QString> tv;
    finSS = new SpreadSheet2(6,2,false,tv,false);

    tv.push_back(QString("langfristiges FK"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Dispo"));//+QChar(0x20ac)+")");
    tv.push_back(QString("GDB"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Liquidit�t"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Gewinn"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Haushaltseinkommen"));//+QChar(0x20ac)+")");

    vector<QString> td;
    td.push_back(QLocale().toString(langFK,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(dispo,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(GDB,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(liquidity,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(profit,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(income,'f',2)+" "+QChar(0x20ac));

    finSS->updateColumn(0,tv);
    finSS->updateColumn(1,td);

    QVBoxLayout *vf = new QVBoxLayout;
    vf->setAlignment(Qt::AlignCenter);
    vf->addStretch();
    vf->addWidget(finTitle);
    QHBoxLayout *hlf = new QHBoxLayout;
    hlf->addStretch();
    hlf->addWidget(finSS);
    hlf->addStretch();

    vf->addLayout(hlf);//Widget(finSS);
    vf->addStretch();

    finWidget->setLayout(vf);
}

void InvestDialog::updateFinWidget(int i){
    langFK=combs[combInd(i)].fins[0];
    dispo=combs[combInd(i)].fins[1];
    GDB=combs[combInd(i)].fins[2];
    liquidity=combs[combInd(i)].fins[3];
    profit=combs[combInd(i)].fins[4];
    income=combs[combInd(i)].fins[5];

    vector<QString> td;
    td.push_back(QLocale().toString(langFK,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(dispo,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(GDB,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(liquidity,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(profit,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(income,'f',2)+" "+QChar(0x20ac));

    finSS->updateColumn(1,td);
}

void InvestDialog::updateFinWidget(){
    langFK=farm0->getLangFKinv();
    dispo=farm0->getDispoInv();
    GDB=farm0->getGDBinv();
    liquidity=farm0->getLiqInv();
    profit=farm0->getProfInv();
    income=farm0->getIncInv();

    vector<QString> td;
    td.push_back(QLocale().toString(langFK,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(dispo,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(GDB,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(liquidity,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(profit,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(income,'f',2)+" "+QChar(0x20ac));

    finSS->updateColumn(1,td);
}

void InvestDialog::calRefPEs(){
    int sz = prodNames.size();
    int x = refzeitSBox->value();
    prodRefPEs.clear();
    prodRefPEs.resize(sz);
    for (int i=0; i<sz; ++i){
       prodRefPEs[i] = 0;
       for (int k=0; k<x; ++k)  {
            prodRefPEs[i] += allPEs[ITER-k][i];
       }
       prodRefPEs[i] /= x;
    }
}

void InvestDialog::updatePE(int row){
        if (!inited) return;
        if (inComb) return;
        int sz = prodIDs.size();
        for (int i=0; i<sz; ++i){
            peDSBs[i]->setEnabled(false);
        }

        double d = peDSBs[row]->value();
        oldvalue = Manager->product_cat0[prodIDs[row]].getPriceExpectation();
        Manager->product_cat0[prodIDs[row]].setPriceExpectation(d);

        if (row == refpos) {
            double tind = d/prodPrices[refpos];
            getreidePEs.clear();
            int ts = getreideIDs.size();
            for (int i=0; i<ts; ++i){
                getreidePEs.push_back(Manager->product_cat0[getreideIDs[i]].getPriceExpectation());
            }
            for (int i=0; i<ts; ++i){
                Manager->product_cat0[getreideIDs[i]].setPriceExpectation(getreidePrices[i]*tind);
            }
        }

        //cout << "a: row: " << row <<endl;
        emit investPeSig(row);
}

int InvestDialog::combInd(int comb){
    int sz = plans.size();
    for (int i=0; i<sz; ++i){
        if (plans[i]==comb)
            return i;
    }
    return 0; //fehler
}

int InvestDialog::indComb(int i){
    return plans[i];
}

void InvestDialog::updateComb(QString str){
    int i=0;
    titles[combInd(aktComb)]->setStyleSheet("QLabel { background-color : lightgray;}");
    i=str.toInt();
    updateComb(combInd(i));
}

QWidget* InvestDialog::createFinCopy(Komb comb, int nr){
    //QWidget *
    finW = new QWidget();
    Title4* finTitle = new Title4(Qt::AlignCenter, font1);
    if (nr==0)
        finTitle->setText(QString("<a href=0>Ref-Plan"));
    else
        finTitle->setText(QString("<a href=%1>Plan %1").arg(QString::number(nr)));

    plans.push_back(nr);
    titles.push_back(finTitle);

    connect(finTitle, SIGNAL(linkActivated(QString)), this, SLOT(updateComb(QString)));

    vector<QString> tv;
    tv.push_back(QString("langfristiges FK"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Dispo"));//+QChar(0x20ac)+")");
    tv.push_back(QString("GDB"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Liquidit�t"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Gewinn"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Haushaltseinkommen"));//+QChar(0x20ac)+")");
//*/
    SpreadSheet2 *fSS = new SpreadSheet2(6,1,false,tv,false);

    vector<QString> td;
    td.push_back(QLocale().toString(comb.fins[0],'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(comb.fins[1],'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(comb.fins[2],'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(comb.fins[3],'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(comb.fins[4],'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(comb.fins[5],'f',2)+" "+QChar(0x20ac));

    fSS->updateColumn(0,td);
//    fSS->updateColumn(1,td);

    QVBoxLayout *vf = new QVBoxLayout;
    vf->addWidget(finTitle);

    QHBoxLayout *hf = new QHBoxLayout;
    hf->addStretch();
    hf->addWidget(fSS);
    hf->addStretch();
    QWidget *hw = new QWidget;
    hw->setLayout(hf);

    vf->addWidget(hw);

    //if (nr!=0)
    fins.push_back(fSS);

    finW->setLayout(vf);
    wins.push_back(finW);
    return finW;
}

void InvestDialog::updateInvWidget(int i){
    invRefs = combs[combInd(i)].invRefs;
    myInvs = combs[combInd(i)].invs;
    invSS->updateColumn(2,invRefs);

    bool ok = inited;
    if (ok) inited = false;
    for (int r=0; r<nRowInv;++r){
        QSpinBox *dsb = invDSBs[r];
        dsb->setValue(myInvs[r]);
        if (i==0)
            dsb->setEnabled(false);
        else
            dsb->setEnabled(true);
    }

    if (ok) inited=true;
}


void InvestDialog::updateInvWidget(){
    invRefs = farm0->invRefs;
    if (numComb==1){ //erste comb
        myInvs = farm0->myInvs;
        invSS->updateColumn(2,invRefs);
    }
    for (int r=0; r<nRowInv;++r){
        QSpinBox *dsb = invDSBs[r];
        dsb->setValue(myInvs[r]);
    }
    //inited=true;
}

void InvestDialog::updateIndexLab(){
    double refpe = peDSBs[refpos]->value();
    double refp = prodPrices[refpos];
    preisIndex = refpe/refp;
    pindexLab->setText(QString("Preisindex f�r alle Marktfr�chte (wie der von %1) : %2").arg(refname, QLocale().toString(preisIndex,'f',3)));
}

void InvestDialog::getPeOK(int r, bool b){
        if (!b) {
            inited = false;
            peDSBs[r]->setValue(oldvalue);
            inited = true;
            if (r==refpos) {
                int ts = getreideIDs.size();
                for (int i=0; i<ts; ++i){
                    Manager->product_cat0[getreideIDs[i]].setPriceExpectation(getreidePEs[i]);
                }
            }

            Manager->product_cat0[prodIDs[r]].setPriceExpectation(oldvalue);
            QMessageBox::warning(this, tr("Solver Information"),
                             tr("Bedingungen verletzt"));

        }
        int sz = prodIDs.size();
        for (int i=0; i<sz; ++i){
            peDSBs[i]->setEnabled(true);
        }

        if (!b) return;

        if (peVar == 0) manPEs[r] = peDSBs[r]->value();

        updateFinWidget();
        saveCombFin(aktComb);

        if (r==refpos) {
            updateIndexLab();
        }

        updateInvWidget(aktComb);
        updateFins(aktComb);
        saveCombPe(aktComb);
}

void InvestDialog::weiter(){
    farm0->doLpInvestEnd(myInvs);
    accept();
    close();
    //inited=false;
}

void InvestDialog::getInvestOK(int r, bool b){
    if (!b) {
        inited = false;//Achtung!
        invDSBs[r]->setValue(oldvalue);
        inited = true;

        QMessageBox::warning(this, tr("Solver Information"),
                         tr("Bedingungen verletzt"));
    }
    int sz = invRefs.size();
    for (int i=0; i<sz; ++i){
        invDSBs[i]->setEnabled(true);
    }

    if (!b) return;
    myInvs = farm0->myInvs;

    updateFinWidget();
    saveCombFin(aktComb);
    updateFins(aktComb);
    saveCombInv(aktComb);
}

void InvestDialog::updateInv(int row){
    if (!inited) return;
    if (inComb) return;

    int sz = invRefs.size();
    for (int i=0; i<sz; ++i){
        invDSBs[i]->setEnabled(false);
    }

    int d = invDSBs[row]->value();
    oldvalue = myInvs[row];

    emit investSig(row,d);
}

Title4::Title4(Qt::AlignmentFlag align, QFont f, QWidget*w):QLabel(w){
    this->setAlignment(align);
    this->setFont(f);
}
