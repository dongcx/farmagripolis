#include "Landwindow.h"
#include <QDockWidget>
#include <QAction>
#include <QToolBar>
#include <QGraphicsView>
#include <QGraphicsScene>
#include "RegMessages.h"
#include "RegPlot.h"

#include <QVBoxLayout>
#include <QLineEdit>
#include <QPicture>

#include <QColorDialog>
#include "legendIcon.h"

#include <QPushButton>
#include <QSignalMapper>
#include <QGroupBox>
#include <QDialogButtonBox>

#include <QSettings>
#include <QPrintDialog>

#include <strstream>
#include <QList>

#include <QStatusBar>


Q_DECLARE_METATYPE(PlA*);
Q_DECLARE_METATYPE(PlA);

static QString manageCap(double x){
    if (x<0.85) return "sehr gut";
    else if (x<0.95) return "gut";
    else if (x<1.05) return "mittelm��ig";
    else if (x<1.15) return "unterdurchschnittl.";
    else return "schlecht";
}

Landwindow::Landwindow(QWidget *parent): hasScene(false),gridGroup(0),
        modus(0),QMainWindow(parent) {
    setWindowTitle(tr("Landschaften"));

    printer = new QPrinter(QPrinter::HighResolution);
    printer->setOutputFileName("tland.pdf");

    makeDocks();
    //setDockOptions(QMainWindow::ForceTabbedDocks);

    createActions();
    createToolBars();

    createStatusBar();

    info->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    addDockWidget(Qt::RightDockWidgetArea, info);
    addDockWidget(Qt::RightDockWidgetArea, legend);

    scene = new QGraphicsScene();
    //createScene();

    view = new QGraphicsView();
    view->setScene(scene) ;

    setCentralWidget(view);

    lastPlot = 0;
    withBorder=false;
    picType = layerComboBox->currentIndex();

    resize(800,500);

    colorDialog=0;
    nSoils=0;
    nFarmtypes=0;

    chosenFarm=-1;
    modus=0;

    reginfo = new RegionInfo();

    infoWidgetRegion=0;

    //transport cost
    tc=0;
    labTc = new QLabel;
    hasProxy=false;
    firstProxy=true;
    labRent = new QLabel;
    hasProxyRent=false;
    firstProxyRent=true;
    //setMouseTracking(true);
}
/*
void Landwindow::mouseMoveEvent(QMouseEvent *e){
    emit lwMouseMove(e);
}
//*/

void Landwindow::closeEvent(QCloseEvent* event){
    emit hi(true);
    QMainWindow::closeEvent(event);
}

void Landwindow::newplot(PlA * s){
updatePlot(s);
if(!hasScene) return;
double plsz = gg->PLOT_SIZE;
int i=s->soil_type;
reginfo->landSoilsFree[i]--;
freeLandLabels[i]->setText(QString::number(reginfo->landSoilsFree[i]*plsz));
}

void Landwindow::updatePlot(PlA *s){
 if (hasScene) {
   if (hasProxy) {
         scene->removeItem(proxyWidget);
         hasProxy=false;
   }

   if (hasProxyRent) {
         scene->removeItem(proxyWidgetRent);
         hasProxyRent=false;
   }

   PlotItem *item;
   int x=s->col;
   int y= s->row;

   QList<QGraphicsItem*> li = scene->items(QPointF(x*PlotItem::w+PlotItem::w/2,
        y*PlotItem::h+PlotItem::h/2));

    QGraphicsItem *itm;
    QList<QGraphicsItem*>::Iterator it= li.begin();
    while (it != li.end()){
        itm = *it;
        if (itm->pos().x()==0 || itm->pos().y()==0){
            itm=0;
            ++it;

        }else
            break;
    }

    if (!itm) return;
   /*if (gridGroup){
         if (itm == gridGroup) return;
         if (itm->group()== gridGroup)
                return;
   }
   //*/
   item = static_cast<PlotItem*> (itm);

//   cout << "x,y - item->pos.x,y: "<< x << " "<< y <<"-"
  //         << item->pos().x() << " "<< item->pos().y() << endl;
   QColor c;
   if (modus==0)
         c= getColor(picType,s);
   else
         c=getColor(s);

   item->setColor(c);//getColor(picType,s));
   item->setIsFarmstead(getIsFarmstead(s));
   item->setIsFirstFarm(getIsFirstFarm(s));
   item->setFree((*s).state==0);
   item->setModus(modus);
   item->setChosenFarmid(chosenFarmid);

   QVariant var;
   var.setValue(s);
   item->setData(100,var);

   item->update();
 }
}

void Landwindow::delplot(PlA* s){
updatePlot(s);
if(!hasScene) return;
double plsz = gg->PLOT_SIZE;
int i=s->soil_type;
reginfo->landSoilsFree[i]++;
freeLandLabels[i]->setText(QString::number(reginfo->landSoilsFree[i]*plsz));
}

void Landwindow::createActions(){
    layerLabel = new QLabel(tr("Darstellung nach "));
    layerComboBox=new QComboBox();
    layerComboBox->addItem(tr("Betrieben"));
    //layerComboBox->addItem(tr("Agrar ?"));
    layerComboBox->addItem(tr("Bodentypen"));
    //layerComboBox->addItem(tr("Bodentypen (frei?)"));

    layerComboBox->addItem(tr("Betriebstypen"));
    //layerComboBox->addItem(tr("Frei ?"));
    layerComboBox->setCurrentIndex(1);
    layerLabel->setBuddy(layerComboBox);

    connect(layerComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(updateGraphic(int)));

    actZoomIn = new QAction("Zoom In",this);
    actZoomIn->setIcon(QIcon(":/images/zoom-in.png"));

    actZoomOut = new QAction("Zoom Out",this);
    actZoomOut->setIcon(QIcon(":/images/zoom-out.png"));

    connect(actZoomOut, SIGNAL(triggered()), this, SLOT(zoomout()));
    connect(actZoomIn, SIGNAL(triggered()), this, SLOT(zoomin()));

    actColor = new QAction("Farbwahl",this);
    actColor->setIcon(QIcon(":/images/colors.png"));

    connect(actColor,SIGNAL(triggered()), this, SLOT(showColorDialog()));

    actWithBorder = new QAction("Gitter",this);
    actWithBorder->setIcon(QIcon(":/images/grid.png"));

    actPrint = new QAction("Drucken",this);
    actPrint->setIcon(QIcon(":/images/drucken.png"));

    connect(actWithBorder, SIGNAL(triggered()), this, SLOT(updateWithBorder()));
    connect(actPrint,SIGNAL(triggered()), this, SLOT(drucken()));

    actLegende = new QAction("Legende",this);
    actLegende->setIcon(QIcon(":/images/legende.png"));
    actLegende->setEnabled(false);

    actFarminfo = new QAction("Infos",this);
    actFarminfo->setIcon(QIcon(":/images/table_icon.png"));
    actFarminfo->setEnabled(false);

    connect(legend, SIGNAL(visibilityChanged(bool)), this, SLOT(enableLegend(bool)));
    connect(info, SIGNAL(visibilityChanged(bool)),this, SLOT(enableFarminfo(bool)));

    connect(actLegende, SIGNAL(triggered()), this, SLOT(showLegend()));
    connect(actFarminfo, SIGNAL(triggered()), this, SLOT(showFarminfo()));

}

bool Landwindow::sceneCreated(){
    return hasScene ==true;
}

void Landwindow::createStatusBar(){
    statusLabel = new QLabel();
    statusLabel->setIndent(10);
    statusLabel->setTextFormat(Qt::RichText);
    statusLabel->setText("HI");
    statusBar()->addWidget(statusLabel);
}

void Landwindow::updateStatusBar(){
    if (modus==0)
        statusLabel->setText(tr("<b>Info �ber einen Betrieb ansehen:</b> auf dessen X-Feld klicken"));
    else
        statusLabel->setText(tr("<b>Zur�ck zur �bersicht:</b> rechte Maustaste klicken"));
}

void Landwindow::createToolBars(){
    chooseLayerToolBar = addToolBar("Schicht w�hlen");

    chooseLayerToolBar->addWidget(layerLabel);
    chooseLayerToolBar->addWidget(layerComboBox);

    chooseLayerToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    chooseLayerToolBar->addAction(actColor);

    //colorToolBar = addToolBar("Farbwahl");
    //colorToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    //colorToolBar->addAction(actColor);

    zoomToolBar = addToolBar("Zoom");
    zoomToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    zoomToolBar->addAction(actZoomIn);
    zoomToolBar->addAction(actZoomOut);

    zoomToolBar->addAction(actWithBorder);
    zoomToolBar->addAction(actPrint);

    //spaceToolBar = addToolBar("nothing");

    //optToolBar = addToolBar("showhide");
    //optToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    //optToolBar->addAction(actLegende);
    //optToolBar->addAction(actFarminfo);
}

void Landwindow::initScene(){
    int w=PlotItem::w;
    int h=PlotItem::h;

    int nplots=Manager->Region->plots.size();
    for (int i=0;i<nplots;++i){
        PlA *pinfo= &((Manager->Region->plots)[i]->PA);

        //PlotType pt= picType;

        QColor color = getColor(picType,pinfo);
        bool isfarmstead= getIsFarmstead(pinfo);
        bool isfirstfarm =getIsFirstFarm(pinfo);

      PlotItem *item;
      if (!hasScene){
        item = new PlotItem(picType, color, isfarmstead, isfirstfarm,this);
        //if (isfarmstead){

            QVariant var;
            var.setValue(pinfo);
            item->setData(100, var);//pinfo.farm_id);

        //}
        item->setFree(pinfo->state==0);

        //  cout << pinfo->row << " " << pinfo->col << endl;
        item->setPos(pinfo->col*w+w/2, pinfo->row*h+h/2);
        } else {
            QList<QGraphicsItem*> li = scene->items(QPointF(pinfo->col*PlotItem::w+PlotItem::w/2,
                pinfo->row*PlotItem::h+PlotItem::h/2));

            QGraphicsItem *itm;
            QList<QGraphicsItem*>::Iterator it= li.begin();
            while (it != li.end()){
                itm = *it;
                if (itm->pos().x()==0 || itm->pos().y()==0){
                    itm=0;
                    ++it;

                }else
                    break;
            }
            if (!itm) return;
            item = static_cast<PlotItem*>(itm);
            //item = static_cast<PlotItem*>(scene->itemAt(pinfo->col*w+w/2, pinfo->row*h+h/2,QTransform()));
            item->setColor(color);
        }

        item->update();
   if (!hasScene)   scene->addItem(item);

   }
   if (!hasScene) withBorder=true;
   hasScene = true;
   //updateFarms();
   updateStatusBar();
   updateGraphic(withBorder); //with Gitter

   setWindowIcon(QPixmap(":/images/plotscolor.png"));//viewIcon());
}

 QColor Landwindow::getColor(int t,PlA * s){
 //OWNER, SOILTYPEFREE, FARMTYPE
  QColor color ;
  int id;
  if (s->soil_type==nSoils) {
        color = nonAgColor;
        return color;
  }
  int fc,fs,st;
  int r0,g0,b0;
  switch(t){
  case 1: color = soiltypeColors[s->soil_type];
          break;

  case 0: id = s->farm_id;
          if (id==0) color = farm0Color;
          else {
             fc=id%6;
             fs = id%257;
             st=fs%5;
             switch(fc){
             case 0: r0=0;g0=0;b0=1;break;
             case 1: r0=0;g0=1;b0=0;break;
             case 2: r0=1;g0=0;b0=0;break;
             case 3: r0=0;g0=1;b0=1;break;
             case 4: r0=1;g0=0;b0=1;break;
             case 5: r0=1;g0=1;b0=0;break;
             default: r0=0;g0=0;b0=0;
             }

             int r=r0>0?r0*(st+1)*30+75:0;
             int g=g0>0?g0*(st+1)*30+75:0;
             int b=b0>0?b0*(st+1)*30+75:0;

             if (zunah(farm0Color, r,g,b) || zunah(nonAgColor, r,g,b)){

                 r = 255-r; g= 255-g; b=255-b;
                 color = QColor(r,g,b, 50);
             }
             else color = QColor(r,g,b ,50);
          }
          break;
  case 2:
           /*
           //cout << "farm_id  farm_class: " << s->farm_id << " "
             //   << mapIDpFarm[s->farm_id]->getFarmClass() << endl;
            if (mapIDpFarm.find(s->farm_id)==mapIDpFarm.end())
                    cout << s->farm_id << endl;
                //*/
            color = farmtypeColors[mapIDpFarm[s->farm_id]->getFarmClass()-1];
          break;

  default: color =Qt::white;
}
 return color;
 }

 QColor Landwindow::getColor(PlA * s){ //FARM-modus
 //OWNER, SOILTYPEFREE, FARMTYPE
  QColor color, compColor ;

  if (s->soil_type==nSoils) {
        color = nonAgColor;
        return color;
  }
  color = soiltypeColors[s->soil_type];

  if (s->farm_id==chosenFarmid ) return color;
  if (s->farm_id==0 && chosenFarmid==0) return color;
  if (s->farm_id==0) return myColor;//compColor;
  if (s->state==0) return color; //free
  return otherfarmColor;
 }

 bool Landwindow::getIsFarmstead(PlA *s){
        bool isFarmsitz=false;
        if ((*s).state==2) //farmstead
           isFarmsitz= true;
       return isFarmsitz;
}

bool Landwindow::getIsFirstFarm(PlA *s){
        bool first= false;
        if ((*s).farm_id==0)
            first = true;
     return first;
}

void Landwindow::zoomout(){
    view->scale(0.7, 0.7);
    //setWindowIcon(QIcon(viewIcon()));
}

void Landwindow::zoomin(){
    view->scale(1.4,1.4);
    //setWindowIcon(QIcon(viewIcon()));
}

void  Landwindow::enableLegend(bool b){
   if (!b)
       actLegende->setEnabled(true);
}

void Landwindow::enableFarminfo(bool b){
  if (!b)
      actFarminfo->setEnabled(true);
}

void Landwindow::showFarminfo(){
    info->show();
    actFarminfo->setEnabled(false);
}

void Landwindow::showLegend(){
    legend->show();
    actLegende->setEnabled(false);
}


void Landwindow::makeDocks(){
    legend=new QDockWidget("Legende");
    legend->setFeatures(QDockWidget::NoDockWidgetFeatures);
    //legendWidget = new QWidget(legend);
    //legend->setWidget(legendWidget);

    info= new QDockWidget("Infos");
    info->setFeatures(QDockWidget::NoDockWidgetFeatures);
}

void Landwindow::updateFarms (){
    mapIDpFarm.clear();
    list<RegFarmInfo*> farmList = Manager->FarmList;
    list<RegFarmInfo*>::iterator it= farmList.begin();
    while (it!=farmList.end()){
        mapIDpFarm[(*it)->getFarmId()]=(*it);
        ++it;
    }
}

void Landwindow::updateFarminfo(){
    if (modus){
        updateFarminfo(chosenFarm);
    }

}

void Landwindow::updateFarminfo(int fid){
    //idEdit->setText(mapIDpFarm[fid]->getFarmId());
    RegFarmInfo *pfarm = mapIDpFarm[fid];
    idLabel->setText(QString::number(pfarm->getFarmId()));

    kapLabel->setText(QLocale().toString(pfarm->getEquityCapital()));
    //QString::number(pfarm->getEquityCapital()));//+" "+QChar(8364));
    liqLabel->setText(QLocale().toString(pfarm->getLiquidity()));
    //QString::number(pfarm->getLiquidity()));//+" "+QChar(8364));
    farmtypeLabel->setText(classnames[pfarm->getFarmClass()].c_str());
    betriebsalterLabel->setText(QString::number(pfarm->getFarmAge()));
    //managekoeffLabel->setText(QString::number(1/pfarm->getManagementCoefficient()));
    managekoeffLabel->setText(manageCap(pfarm->getManagementCoefficient()));

    for (int i=0; i<nSoils; ++i){
        landLabels[i]->setText(QLocale().toString(pfarm->getLandInputOfType(i)));
        //QString::number(pfarm->getLandInputOfType(i)));//+ " ha");

        landpriceLabels[i]->setText(QLocale().toString(pfarm->getAvRentOfType(i),'g',4));
        //QString::number(pfarm->getAvRentOfType(i),'g',4));
        //+" "+QChar(8364)+"/ha");

    }

    //rentLandEdit->setText(QString::number(total));
    getStallMachine(pfarm);
    getMachines(pfarm);
    //getBuildings(pfarm);

    vlayout->insertWidget(8,investGroupBox);//777777
    vlayout->insertWidget(9,machineGroupBox);
    //vlayout->insertWidget(10,buildingGroupBox);

    //vlayout->addWidget(investGroupBox);
    //vlayout->addStretch(1);
    int ninvs = this->investnameLabels.size()+this->machinenameLabels.size();
    int fh = this->fontMetrics().height();
    //cout << "fh, n:" << fh <<"\t"<< ninvs <<endl;
    infoWidget->widget()->setFixedHeight(2*(ninvs+2)*fh+300);
    infoWidget->widget()->repaint();
}

void Landwindow::updateGraphic(){
    RegFarmInfo *pfarm = mapIDpFarm[0];
    if (pfarm->isClosed()){
        int sz = myFarmSitzIcons.size();
        for (int i=0; i<sz; ++i){
            myFarmSitzIcons[i]->setVisible(false);
            myFarmSitzTexts[i]->setVisible(false);
        }
    }

    if (gridGroup) updateGraphic(withBorder);
    if (modus==0) {
       //updateRegionData();
       updateGraphic(layerComboBox->currentIndex());
       layerComboBox->setEnabled(true);

       return;
    }
    layerComboBox->setEnabled(false);
    //OWNER, SOILTYPEFREE, FARMTYPE
    foreach (QGraphicsItem *item, scene->items()){
       if (!gridGroup || (item != gridGroup && item->group() != gridGroup)){
            PlotItem *it;
            it = static_cast<PlotItem*>(item);
            pInfo =it->data(100).value<PlA*>();
            it->setColor(getColor(pInfo));
            it->setType(1); //SOILTYPEFREE
            it->setIsOwnland(pInfo->state==3);
            it->setModus(modus);
            it->setChosenFarmid(chosenFarmid);
            it->update();
        }
    }
    updateStatusBar();
    updateLegendWidget();
    legend->setWidget(legendWidget);
    info->setWidget(infoWidget);
    info->setWindowTitle(tr("Betriebsdaten"));//Farm info"));

    setWindowIcon(QPixmap(":/images/plotscolor.png"));//QIcon(viewIcon()));
}

QPixmap Landwindow::viewIcon(){
    QPixmap *p=new QPixmap(32,32);
    p->fill();
    QPainter painter(p);
    if (withBorder) {
        updateGraphic(false);
        scene->render(&painter);//view->
        updateGraphic(true);
    }else
        scene->render(&painter);//view->

    return *p;
}

void Landwindow::removeProxies(){
    if (hasProxy){
        scene->removeItem(proxyWidget);
        hasProxy=false; //inScene
    }
    if (hasProxyRent){
        scene->removeItem(proxyWidgetRent);
        hasProxyRent=false; //inScene
    }
}

void Landwindow::updateGraphic(int id){
    removeProxies();
    //*/
    picType = id;
//OWNER, SOILTYPEFREE, FARMTYPE
    PlotType t;
    switch(id){
        //case 0: t=AG_NONAG; break;
        //case 1: t=SOILTYPE; break;
        case 1: t=SOILTYPEFREE; break;//info->setWindowTitle(tr("Bodentypen"));break;
        case 2: t=FARMTYPE; break;//info->setWindowTitle(tr("Farmtypen")); break;
        case 0: t=OWNER;break;// info->setWindowTitle(tr("Betriebe")); break;
        //case 5: t=FREE; break;
        default: t=SOILTYPEFREE;//
     }
    //*/
     info->setWindowTitle(tr("Regionsbeschreibung"));//Region Info"));

     foreach (QGraphicsItem *item, scene->items()){
        if (!gridGroup || (item != gridGroup && item->group() != gridGroup)){
            PlotItem *it;
            it = static_cast<PlotItem*>(item);
            pInfo =it->data(100).value<PlA*>();
            it->setColor(getColor(picType,pInfo));
            it->setType(t);

            it->setModus(0);//REGION;
            it->update();
         }
     }
     if (id == 2)
            updateLegendWidget(id); //farmtype dynamisch anpassen
     legend->setWidget(legendWidgets[id]);
     info->setWidget(infoWidgetRegion);//infoWidgets[id]);
     updateStatusBar();

     setWindowIcon(QPixmap(":/images/plotscolor.png"));//QIcon(viewIcon()));
}

void Landwindow::updateWithBorder(){
   withBorder = !withBorder;
   updateGraphic(withBorder);
}

void Landwindow::updateGraphic(bool b) {
    removeProxies();
    QPen pen(gridColor);
    if (!gridGroup) {
       gridGroup = new QGraphicsItemGroup;
        const int MaxX = gg->NO_COLS;
        const int MaxY = gg->NO_ROWS;
        const int xs = PlotItem::w;
        const int ys = PlotItem::h;
        for (int x = 0; x <= MaxX*xs; x += xs) {
            QGraphicsLineItem *item = new QGraphicsLineItem(x, 0, x,
                                                            MaxY*ys);
            //item->setPos(x,0);
            item->setPen(pen);
            item->setZValue(-100);
            gridGroup->addToGroup(item);
        }
        for (int y = 0; y <= MaxY*ys; y += ys) {
            QGraphicsLineItem *item = new QGraphicsLineItem(0, y,
                                                            MaxX*xs, y);
            //item->setPos(0,y);
            item->setPen(pen);
            item->setZValue(-100);
            gridGroup->addToGroup(item);
        }
        scene->addItem(gridGroup);
    }else{
        foreach (QGraphicsItem *item, gridGroup->childItems()){
             QGraphicsLineItem *litem = static_cast<QGraphicsLineItem*>(item);
             litem->setPen(pen);
        }
    }
    gridGroup->setVisible(b);
}

void Landwindow::drucken(){
  //cout << "Drucken noch nich implementiert" << endl;
    /*QPrintDialog dialog(printer);
    if (dialog.exec()){
    {
    //*/
        QPainter painter(printer);
/*QPainter painter;
QPrinter printer;
painter.begin(&printer);

//*/
     /* QFont textFont("Times", 20, QFont::Bold);

        painter.setFont(textFont);
        //QRect textRect = painter.boundingRect(QRectF(0,0,9600,500),Qt::AlignCenter,
           // "Das ist eine Graphik");

        painter.drawText(QRect(0,0,painter.window().width(),500),Qt::AlignCenter,
            QString("Graphik nach %1").arg(layerComboBox->currentText()));
            //Das ist eine Graphik");
        painter.translate(0,700);
        scene->render(&painter);

        painter.translate(0,scene->sceneRect().height()*11+50);
        painter.scale(10,10);
        legendWidgets[layerComboBox->currentIndex()]->render(&painter);
       //*/
    painter.scale(8,8);
    this->render(&painter);
    //}
}

void Landwindow::showColorDialog(){

  /*
  0 nonag
  1  soil_1
  nSoils soil_nSoils
  nSoils+1 farm_0
  nSoils+2 farmtype_1
  nSoils+nFarmtypes+1 farmtype_nFarmtypes
  nSoils+nFarmtypes+2 otherfarmColor
  nSoils+nFarmtypes+3 myColor
  nSoils+nFarmtypes+4 gridColor
  //*/

  int id = layerComboBox->currentIndex();
  if (modus==1) id = 3;  //Betriebmodus
  QGridLayout *layout = static_cast<QGridLayout*>(colorDialog->layout());
  int total = 1+nSoils+1+nFarmtypes+3;
    bool p;
    QLabel *l1, *l2;
    QPushButton *l3;
  for (int i=0; i<total; ++i){
     l1=static_cast<QLabel*>(layout->itemAtPosition(i,0)->widget());
     l2=static_cast<QLabel*>(layout->itemAtPosition(i,1)->widget());
     l3=static_cast<QPushButton*>(layout->itemAtPosition(i,2)->widget());

  switch(id){
  case 1:
        p=(i>nSoils && i!=nSoils+nFarmtypes+4 ) ;
        break;
  case 0:
        p=(i!=0  && i!= nSoils+1 && i!=nSoils+nFarmtypes+4 ) ;
        break;
  case 2:
        p=(i!=0 && i < nSoils+2 || i>nSoils+nFarmtypes+1 && i!=nSoils+nFarmtypes+4);
        break;

  default: p=(i >nSoils && i< nSoils+nFarmtypes+2|| chosenFarmid==0 && i==nSoils+nFarmtypes+3 );
  }
  if (!withBorder) p = p || i == nSoils+nFarmtypes+4;
     if (p) {
       l1->hide();l2->hide();l3->hide();
     }else {
        l1->show();l2->show();l3->show();
     }
  }

  colorDialog->show();
  actColor->setEnabled(false);
  layerComboBox->setEnabled(false);

}


void Landwindow::showColorDialog2(){

  initColorDialogs2();
  int id = layerComboBox->currentIndex();
  QVBoxLayout *layout = static_cast<QVBoxLayout*>(colorDialog->layout());
  QGroupBox *gbox = static_cast<QGroupBox*>(
        layout->itemAt(id ==2 ? id-1:id)->widget());
  gbox->setHidden(false);
  colorDialog->show();

  actColor->setEnabled(false);

}
//*/

void Landwindow::initColors(){

    nSoils = gg->NO_OF_SOIL_TYPES;
    for (int i=0; i<nSoils; ++i){
        sSoilnames.push_back(gg->NAMES_OF_SOIL_TYPES[i]);
    }

    int num = gg->farm_class.size();
    map<int,int> classmap;
    for (int i=0;i<num; i++){
        classmap[gg->farm_class[i]]=1;
    }
    nFarmtypes = 4; //classmap.size();

   classnames[1]="Veredlung";//Pig/Poultry";
   classnames[2]="Futterbau";//Greenland";
   classnames[3]="Marktfruchtbau";//Arable";
   classnames[4]="Gemischt";//Mixed";
   classnames[5]="Geschlo�en";//Mixed";

    QSettings colorSettings("IAMO, BSE", "Farm AgriPoliS");
    otherfarmColor = colorSettings.value("otherFarmColor", Qt::gray).value<QColor>();
    myColor = colorSettings.value("myColor", Qt::red).value<QColor>();

    gridColor=colorSettings.value("gridColor",QColor(75,75,75,100)).value<QColor>();

    nonAgColor = colorSettings.value("nonAgColor", Qt::black).value<QColor>();
    //agColor = colorSettings.value("agColor", Qt::green).value<QColor>();

    freeColor= colorSettings.value("freeColor", Qt::green).value<QColor>();
    //nonfreeColor= colorSettings.value("nonfreeColor", Qt::yellow).value<QColor>();

    QColor iColors[3] ;
    iColors[0]=QColor(175,160,83,220);
    iColors[1]=QColor(44,167,112,220);
    iColors[2]=QColor(100,100,100,220);
    for (int i=0; i<nSoils; ++i){
        if (i>2) {
            soiltypeColors.push_back(Qt::gray);
            continue;
        }
        /*
        int r = ((i+1) & 4)? 255:0 ;
        int b = ((i+1) & 2)? 255:0;
        int g = ((i+1) & 1)? 255:0;
        //*/
        soiltypeColors.push_back(
            colorSettings.value(sSoilnames[i].c_str(),
                iColors[i]).value<QColor>());
    }

    farm0Color = colorSettings.value("farm0Color", Qt::red).value<QColor>();


    for (int i=0; i<nFarmtypes; ++i){
        if (i>5) {
            farmtypeColors.push_back(Qt::darkGray);
            continue;
        }

        int b = ((i+1) & 4)?  255:0;
        int g = ((i+1) & 2)?  255:0;
        int r = ((i+1) & 1)?  255:0;
        farmtypeColors.push_back(colorSettings.value(
                 classnames[i+1].c_str(),QColor(r,g,b)).value<QColor>());
    }
}

void Landwindow::initLegends(){
    initLegendWidgets();
    legend->setWidget(legendWidgets[layerComboBox->currentIndex()]);
}

void Landwindow::initLegendWidgets() {
  legendWidgets.resize(numPlotType);

  for (int i=0;i<numPlotType;++i){
     legendWidgets[i]=new QWidget();
 }

 layouts.resize(numPlotType);

 {
 //1 SOILTYPEFREE
 QLabel *nonAgIconLab = new QLabel();
 nonAgIconLab->setAlignment(Qt::AlignCenter);
 nonAgIconLab->setPixmap(normalIcon(nonAgColor));
 QLabel *nonAgLab = new QLabel();
 nonAgLab->setText(tr("Siedlungsfl�che"));

 QLabel *farmSitzIconLab = new QLabel();
 farmSitzIconLab->setAlignment(Qt::AlignCenter);
 farmSitzIconLab->setPixmap(farmSitzIcon(Qt::black));
 QLabel *farmSitzLab = new QLabel();
 farmSitzLab->setText(tr("Betrieb"));

 QLabel *myFarmSitzIconLab = new QLabel();
 int c = mapIDpFarm[0]->farm_plot->getSoilType();
 myFarmSitzIconLab->setAlignment(Qt::AlignCenter);
 myFarmSitzIconLab->setPixmap(mySitzIcon(soiltypeColors[c]));

 QLabel *myFarmSitzLab = new QLabel();
 myFarmSitzLab->setText(tr("Mein Betrieb"));

 myFarmSitzIcons.push_back(myFarmSitzIconLab);
 myFarmSitzTexts.push_back(myFarmSitzLab);

 vector<QLabel*> soilIconLabs;
 vector<QLabel*> soilLabs;
 vector<QLabel*> freeSoilIconLabs;
 vector<QLabel*> freeSoilLabs;

 soilIconLabs.resize(nSoils);
 soilLabs.resize(nSoils);
 freeSoilIconLabs.resize(nSoils);
 freeSoilLabs.resize(nSoils);

 for (int i=0; i<nSoils; ++i){
    QLabel *ilab = new QLabel();
    ilab->setAlignment(Qt::AlignCenter);
    ilab->setPixmap(normalIcon(soiltypeColors[i]));
    QLabel *lab = new QLabel();
    lab->setText(sSoilnames[i].c_str());
    soilIconLabs[i]=ilab;
    soilLabs[i]=lab;

    ilab = new QLabel();
    ilab->setPixmap(freeIcon(soiltypeColors[i]));
    ilab->setAlignment(Qt::AlignCenter);
    lab = new QLabel();
    lab->setText(("Freies "+sSoilnames[i]).c_str());
    freeSoilIconLabs[i]=ilab;
    freeSoilLabs[i]=lab;
 }
        layouts[1]=new QGridLayout;
        layouts[1]->addWidget(nonAgIconLab, 0, 0);
        layouts[1]->addWidget(nonAgLab, 0, 1);

        int j;
        for (j=0; j<nSoils;++j){
            layouts[1]->addWidget(soilIconLabs[j], 2*j+1,0);
            layouts[1]->addWidget(soilLabs[j], 2*j+1, 1);
            layouts[1]->addWidget(freeSoilIconLabs[j], 2*j+2,0);
            layouts[1]->addWidget(freeSoilLabs[j], 2*j+2,1);
        }

        layouts[1]->addWidget(farmSitzIconLab, 2*j+1, 0);
        layouts[1]->addWidget(farmSitzLab, 2*j+1, 1);

        layouts[1]->addWidget(myFarmSitzIconLab, 2*j+2,0);
        layouts[1]->addWidget(myFarmSitzLab, 2*j+2, 1);

        QLabel *lab1  = new QLabel();
        lab1->setTextFormat(Qt::RichText);
        lab1->setText("<b> 1 Feld entspricht "+QString::number(gg->PLOT_SIZE)+ " ha </b>");
        layouts[1]->addWidget(lab1,2*j+3,0,1,2,Qt::AlignCenter);

        QVBoxLayout *vl = new QVBoxLayout();
        vl->addStretch(1);
        layouts[1]->addLayout(vl,2*j+4,0,1,2);

        legendWidgets[1]->setLayout(layouts[1]);
    }

 {
 //0 OWNER
        QLabel *nonAgIconLab = new QLabel();
        nonAgIconLab->setAlignment(Qt::AlignCenter);
        nonAgIconLab->setPixmap(normalIcon(nonAgColor));
        QLabel *nonAgLab = new QLabel();
        nonAgLab->setText(tr("Siedlungsfl�che"));

        QLabel *myIconLab = new QLabel();
        myIconLab->setAlignment(Qt::AlignCenter);
        myIconLab->setPixmap(normalIcon(farm0Color));
        QLabel *myLab = new QLabel();
        myLab->setText("Meine Fl�che");

        QLabel *farmSitzIconLab = new QLabel();
        farmSitzIconLab->setAlignment(Qt::AlignCenter);
        farmSitzIconLab->setPixmap(farmSitzIcon(Qt::black));
        QLabel *farmSitzLab = new QLabel();
        farmSitzLab->setText(tr("Betrieb"));

        QLabel *myFarmSitzIconLab = new QLabel();
        myFarmSitzIconLab->setAlignment(Qt::AlignCenter);
        myFarmSitzIconLab->setPixmap(mySitzIcon(farm0Color));
        QLabel *myFarmSitzLab = new QLabel();
        myFarmSitzLab->setText(tr("Mein Betrieb"));

        myFarmSitzIcons.push_back(myFarmSitzIconLab);
        myFarmSitzTexts.push_back(myFarmSitzLab);

        layouts[0]=new QGridLayout;
        layouts[0]->addWidget(nonAgIconLab, 0, 0);
        layouts[0]->addWidget(nonAgLab, 0, 1);

        layouts[0]->addWidget(myIconLab, 1, 0);
        layouts[0]->addWidget(myLab, 1, 1);

        layouts[0]->addWidget(farmSitzIconLab, 2, 0);
        layouts[0]->addWidget(farmSitzLab, 2, 1);

        layouts[0]->addWidget(myFarmSitzIconLab, 3, 0);
        layouts[0]->addWidget(myFarmSitzLab, 3, 1);

       // QLabel *otherFarmsLab = new QLabel();
       // otherFarmsLab->setText(tr("Farben andere Farme sind automatisch zugewiesen"));

       // layouts[0]->addWidget(otherFarmsLab, 2, 1);
        QLabel *lab2  = new QLabel();
        lab2->setTextFormat(Qt::RichText);
        lab2->setText("<b> 1 Feld entspricht "+QString::number(gg->PLOT_SIZE) + " ha<b>");
        layouts[0]->addWidget(lab2,4,0,1,2,Qt::AlignCenter);

        QVBoxLayout *vl = new QVBoxLayout();
        vl->addStretch(1);
        layouts[0]->addLayout(vl,5,0,1,2);

        legendWidgets[0]->setLayout(layouts[0]);
    }

 {
 //2 FARMTYPE
        QLabel *nonAgIconLab = new QLabel();
        nonAgIconLab->setAlignment(Qt::AlignCenter);
        nonAgIconLab->setPixmap(normalIcon(nonAgColor));
        QLabel *nonAgLab = new QLabel();
        nonAgLab->setText(tr("Siedlungsfl�che"));

        QLabel *farmSitzIconLab = new QLabel();
        farmSitzIconLab->setAlignment(Qt::AlignCenter);
        farmSitzIconLab->setPixmap(farmSitzIcon(Qt::black));
        QLabel *farmSitzLab = new QLabel();
        farmSitzLab->setText(tr("Betrieb"));

        QLabel *myFarmSitzIconLab = new QLabel();
        //myFarmSitzIconLab->setPixmap(farmSitzIcon(Qt::black));
        QLabel *myFarmSitzLab = new QLabel();
        myFarmSitzLab->setText(tr("Mein Betrieb"));

        myFarmSitzIcons.push_back(myFarmSitzIconLab);
        myFarmSitzTexts.push_back(myFarmSitzLab);

        vector<QLabel*> farmtypeIconLabs;
        vector<QLabel*> farmtypeLabs;
        farmtypeIconLabs.resize(nFarmtypes);
        farmtypeLabs.resize(nFarmtypes);

        for (int i=0; i<nFarmtypes; ++i){
           QLabel *ilab = new QLabel();
           ilab->setAlignment(Qt::AlignCenter);
           ilab->setPixmap(normalIcon(farmtypeColors[i]));
           QLabel *lab = new QLabel();
           lab->setText(classnames[i+1].c_str());
           farmtypeIconLabs[i]=ilab;
           farmtypeLabs[i]=lab;
        }

        layouts[2]=new QGridLayout;
        layouts[2]->addWidget(nonAgIconLab, 0, 0);
        layouts[2]->addWidget(nonAgLab, 0, 1);

        int j;
        for (j=0; j<nFarmtypes;++j){
            layouts[2]->addWidget(farmtypeIconLabs[j], j+1,0);
            layouts[2]->addWidget(farmtypeLabs[j], j+1, 1);
        }

        layouts[2]->addWidget(farmSitzIconLab, j+1, 0);
        layouts[2]->addWidget(farmSitzLab, j+1, 1);
//my farmtype
        myFarmSitzIconLab->setAlignment(Qt::AlignCenter);
        myFarmSitzIconLab->setPixmap(mySitzIcon(farmtypeColors[mapIDpFarm[0]->getFarmClass()-1]));
        layouts[2]->addWidget(myFarmSitzIconLab, j+2,0);
        layouts[2]->addWidget(myFarmSitzLab, j+2, 1);

        QLabel *lab3  = new QLabel();
        lab3->setTextFormat(Qt::RichText);
        lab3->setText("<b> 1 Feld entspricht "+QString::number(gg->PLOT_SIZE) + " ha</b>");
        layouts[2]->addWidget(lab3,j+3,0,1,2,Qt::AlignCenter);

        QVBoxLayout *vl = new QVBoxLayout();
        vl->addStretch(1);
        layouts[2]->addLayout(vl,j+4,0,1,2);

        legendWidgets[2]->setLayout(layouts[2]);

        }


 {//betriebmodus
 //fast wie 1 SOILTYPEFREE
 QLabel *nonAgIconLab = new QLabel();
 nonAgIconLab->setAlignment(Qt::AlignCenter);
 nonAgIconLab->setPixmap(normalIcon(nonAgColor));
 QLabel *nonAgLab = new QLabel();
 nonAgLab->setText(tr("Siedlungsfl�che"));

 QLabel *otherfarmIconLab = new QLabel();
 otherfarmIconLab->setAlignment(Qt::AlignCenter);
 otherfarmIconLab->setPixmap(normalIcon(otherfarmColor));
 QLabel *otherfarmLab = new QLabel();
 otherfarmLab->setText(tr("Andere Betriebe"));//Andere Farms"));

 /*QLabel *otherfarmFreeIconLab = new QLabel();
 otherfarmFreeIconLab->setAlignment(Qt::AlignCenter);
 otherfarmFreeIconLab->setPixmap(freeIcon(otherfarmColor));
 QLabel *otherfarmFreeLab = new QLabel();
 otherfarmFreeLab->setText(tr("Freie Fl�che"));
 //*/

 QLabel *ownlandIconLab = new QLabel();
 ownlandIconLab->setAlignment(Qt::AlignCenter);
 ownlandIconLab->setPixmap(ownlandIcon(Qt::black));
 QLabel *ownlandLab = new QLabel();
 ownlandLab->setText(tr("Eigentumsfl�che"));

 QLabel *farmSitzIconLab = new QLabel();
 farmSitzIconLab->setAlignment(Qt::AlignCenter);
 farmSitzIconLab->setPixmap(farmSitzIcon(Qt::black));
 QLabel *farmSitzLab = new QLabel();
 farmSitzLab->setText(tr("Betrieb"));

        QLabel *myIconLab = new QLabel();
        myIconLab->setAlignment(Qt::AlignCenter);
        myIconLab->setPixmap(normalIcon(myColor));
        QLabel *myLab = new QLabel();
        myLab->setText("Meine Fl�che");

 QLabel *myFarmSitzIconLab = new QLabel();
 myFarmSitzIconLab->setAlignment(Qt::AlignCenter);
 //int c = mapIDpFarm[0]->farm_plot->getSoilType();
 //myColor = soiltypeColors[c].darker();
 myFarmSitzIconLab->setPixmap(mySitzIcon(myColor));//soiltypeColors[c]));

 QLabel *myFarmSitzLab = new QLabel();
 myFarmSitzLab->setText(tr("Mein Betrieb"));

 myFarmSitzIcons.push_back(myFarmSitzIconLab);
 myFarmSitzTexts.push_back(myFarmSitzLab);

 vector<QLabel*> soilIconLabs;
 vector<QLabel*> soilLabs;
 vector<QLabel*> freeSoilIconLabs;
 vector<QLabel*> freeSoilLabs;

 soilIconLabs.resize(nSoils);
 soilLabs.resize(nSoils);
 freeSoilIconLabs.resize(nSoils);
 freeSoilLabs.resize(nSoils);

 for (int i=0; i<nSoils; ++i){
    QLabel *ilab = new QLabel();
    ilab->setAlignment(Qt::AlignCenter);
    ilab->setPixmap(normalIcon(soiltypeColors[i]));
    QLabel *lab = new QLabel();
    lab->setText(sSoilnames[i].c_str());
    soilIconLabs[i]=ilab;
    soilLabs[i]=lab;

    ilab = new QLabel();
    ilab->setAlignment(Qt::AlignCenter);
    ilab->setPixmap(freeIcon(soiltypeColors[i]));
    lab = new QLabel();
    lab->setText(("Freies "+sSoilnames[i]).c_str());
    freeSoilIconLabs[i]=ilab;
    freeSoilLabs[i]=lab;
 }
        layout=new QGridLayout;
        layout->addWidget(nonAgIconLab, 0, 0);
        layout->addWidget(nonAgLab, 0, 1);

        int j;
        for (j=0; j<nSoils;++j){
            layout->addWidget(soilIconLabs[j], 2*j+1,0);
            layout->addWidget(soilLabs[j], 2*j+1, 1);
            layout->addWidget(freeSoilIconLabs[j], 2*j+2,0);
            layout->addWidget(freeSoilLabs[j], 2*j+2,1);
        }

        int zeile=2*j+1;

        layout->addWidget(ownlandIconLab, zeile, 0);
        layout->addWidget(ownlandLab, zeile++, 1);

        layout->addWidget(otherfarmIconLab, zeile,0);
        layout->addWidget(otherfarmLab, zeile++, 1);

        /*layout->addWidget(otherfarmFreeIconLab, zeile,0);
        layout->addWidget(otherfarmFreeLab, zeile++, 1);
        //*/

        layout->addWidget(farmSitzIconLab, zeile, 0);
        layout->addWidget(farmSitzLab, zeile++, 1);

        layout->addWidget(myIconLab, zeile, 0);
        layout->addWidget(myLab, zeile++, 1);

        layout->addWidget(myFarmSitzIconLab, zeile,0);
        layout->addWidget(myFarmSitzLab, zeile++, 1);

        QLabel *lab1  = new QLabel();
        lab1->setTextFormat(Qt::RichText);
        lab1->setText("<b> 1 Feld entspricht "+QString::number(gg->PLOT_SIZE)+ " ha </b>");
        layout->addWidget(lab1,zeile++,0,1,2,Qt::AlignCenter);

        QVBoxLayout *vl = new QVBoxLayout();
        vl->addStretch(1);
        layout->addLayout(vl,zeile,0,1,2);

        legendWidget = new QWidget();
        legendWidget->setLayout(layout);
    }
}

void Landwindow::initColorDialogs(){
    //OWNER, SOILTYPEFREE, FARMTYPE
    int zeile=0;

    colorDialog = new QDialog(this);

    QPushButton *okButton = new QPushButton(colorDialog);
    okButton->setText("OK");

    int j;
    //QVBoxLayout *mainLayout = new QVBoxLayout;
    QGridLayout *layout = new QGridLayout;
    QSignalMapper *sigmapper= new QSignalMapper(this);


    //0: AGNONAG
     QLabel *nonAgIconLab = new QLabel();
     nonAgIconLab->setPixmap(normalIcon(nonAgColor));
     QLabel *nonAgLab = new QLabel();
     nonAgLab->setText(tr("Siedlungsfl�che"));

            layout->addWidget(nonAgIconLab, zeile, 0);
            layout->addWidget(nonAgLab, zeile, 1);

            QPushButton *nonAgButton0 = new QPushButton(tr("Farbe �ndern"));
            layout->addWidget(nonAgButton0, zeile, 2);
            sigmapper->setMapping(nonAgButton0, QString("nonAgColor"));
            connect(nonAgButton0, SIGNAL(clicked()), sigmapper, SLOT(map()));

    //1 SOILTYPE
     vector<QLabel*> soilIconLabs;
     vector<QLabel*> soilLabs;
     soilIconLabs.resize(nSoils);
     soilLabs.resize(nSoils);

     for (int i=0; i<nSoils; ++i){
        QLabel *ilab = new QLabel();
        ilab->setPixmap(normalIcon(soiltypeColors[i]));
        QLabel *lab = new QLabel();
        lab->setText(sSoilnames[i].c_str());
        soilIconLabs[i]=ilab;
        soilLabs[i]=lab;
     }

            vector<QPushButton*> soilButtons;
            soilButtons.resize(nSoils);
            for (j=0; j<nSoils;++j){
                layout->addWidget(soilIconLabs[j], zeile+j+1,0);
                layout->addWidget(soilLabs[j], zeile+j+1, 1);
                soilButtons[j] = new QPushButton(tr("Farbe �ndern"));
                layout->addWidget(soilButtons[j], zeile+j+1, 2);
                //soilButtons[j]->setEnabled(false);

                sigmapper->setMapping(soilButtons[j], soilLabs[j]->text());
                connect(soilButtons[j], SIGNAL(clicked()), sigmapper, SLOT(map()));
            }
      zeile=zeile+j+1;

     //2 SOILTYPEFREE

     //3 OWNER
        QLabel *myIconLab = new QLabel();
        myIconLab->setPixmap(normalIcon(farm0Color));
        QLabel *myLab = new QLabel();
        myLab->setText("Meine Fl�che");

            layout->addWidget(myIconLab, zeile, 0);
            layout->addWidget(myLab, zeile, 1);

                QPushButton *myFarmButton = new QPushButton(tr("Farbe �ndern"));
                layout->addWidget(myFarmButton, zeile, 2);
                sigmapper->setMapping(myFarmButton, QString("myFarmColor"));
                //myFarmButton->setEnabled(false);
                connect(myFarmButton, SIGNAL(clicked()), sigmapper, SLOT(map()));


   //4 FARMTYPE
            vector<QLabel*> farmtypeIconLabs;
            vector<QLabel*> farmtypeLabs;
            farmtypeIconLabs.resize(nFarmtypes);
            farmtypeLabs.resize(nFarmtypes);

            for (int i=0; i<nFarmtypes; ++i){
               QLabel *ilab = new QLabel();
               ilab->setPixmap(normalIcon(farmtypeColors[i]));
               QLabel *lab = new QLabel();
               lab->setText(classnames[i+1].c_str());
               farmtypeIconLabs[i]=ilab;
               farmtypeLabs[i]=lab;

            }


            vector<QPushButton*> farmtypeButtons;
            farmtypeButtons.resize(nFarmtypes);
            for (j=0; j<nFarmtypes;++j){
                layout->addWidget(farmtypeIconLabs[j], zeile+j+1,0);
                layout->addWidget(farmtypeLabs[j], zeile+j+1, 1);
                farmtypeButtons[j] = new QPushButton(tr("Farbe �ndern"));
                layout->addWidget(farmtypeButtons[j], zeile+j+1, 2);
                //farmtypeButtons[j]->setEnabled(false);
                sigmapper->setMapping(farmtypeButtons[j], farmtypeLabs[j]->text());
                connect(farmtypeButtons[j], SIGNAL(clicked()), sigmapper, SLOT(map()));
            }
        zeile = zeile+j+1;

        QLabel *otherfarmIconLab = new QLabel();
        otherfarmIconLab->setPixmap(normalIcon(otherfarmColor));
        QLabel *otherfarmLab = new QLabel();
        otherfarmLab->setText(tr("Andere Farms"));

        QLabel *myIconLab2 = new QLabel();
        myIconLab2->setPixmap(normalIcon(myColor));
        QLabel *myLab2 = new QLabel();
        myLab2->setText(tr("Meine Fl�chen"));

            layout->addWidget(otherfarmIconLab, zeile, 0);
            layout->addWidget(otherfarmLab, zeile, 1);
            QPushButton *otherfarmButton = new QPushButton(tr("Farbe �ndern"));
            layout->addWidget(otherfarmButton, zeile++, 2);

            sigmapper->setMapping(otherfarmButton, QString("otherfarmColor"));
            connect(otherfarmButton, SIGNAL(clicked()), sigmapper, SLOT(map()));

            layout->addWidget(myIconLab2, zeile, 0);
            layout->addWidget(myLab2, zeile, 1);
            QPushButton *myButton = new QPushButton(tr("Farbe �ndern"));
            layout->addWidget(myButton, zeile++, 2);

            sigmapper->setMapping(myButton, QString("myColor"));
            connect(myButton, SIGNAL(clicked()), sigmapper, SLOT(map()));

       QLabel *gridIconLab = new QLabel();
       gridIconLab->setPixmap(gridIcon(gridColor));
       QLabel *gridLab=new QLabel();
       gridLab->setText(tr("Gitterlinie"));
            layout->addWidget(gridIconLab, zeile, 0);
            layout->addWidget(gridLab, zeile, 1);
            QPushButton *gridButton = new QPushButton(tr("Farbe �ndern"));
            layout->addWidget(gridButton, zeile++, 2);
            sigmapper->setMapping(gridButton, QString("gridColor"));
            connect(gridButton, SIGNAL(clicked()), sigmapper, SLOT(map()));

        connect(sigmapper,SIGNAL(mapped(QString)), this, SLOT(changeColor(QString)));

        connect(okButton, SIGNAL(clicked()), colorDialog, SLOT(close()));
        connect(colorDialog, SIGNAL(finished(int)),this, SLOT(colorDialogClosed(int)));

        layout->addWidget(okButton,zeile,2);

        colorDialog->setLayout(layout);
}


 void Landwindow::initColorDialogs2(){
            //AG_NONAG, SOILTYPE, SOILTYPEFREE,  OWNER, FARMTYPE, FREE
            int zeile=0;

            QDialog *tmpDialog = colorDialog;
            colorDialog = new QDialog(this);

            QPushButton *okButton = new QPushButton(colorDialog);
            okButton->setText("OK");

            int j;

             //0: AGNONAG
             QLabel *nonAgIconLab = new QLabel();
             nonAgIconLab->setPixmap(normalIcon(nonAgColor));
             QLabel *nonAgLab = new QLabel();
             nonAgLab->setText(tr("Siedlungsfl�che"));

             QLabel *agIconLab = new QLabel();
             agIconLab->setPixmap(normalIcon(agColor));
             QLabel *agLab = new QLabel();
             agLab->setText(tr("Agrar-Land"));

             QVBoxLayout *mainLayout = new QVBoxLayout;

             QGridLayout *layout = new QGridLayout;
             QSignalMapper *sigmapper= new QSignalMapper(this);

                    QGroupBox *gbox0 = new QGroupBox("Agr/Non-Agr");

                    layout->addWidget(nonAgIconLab, zeile, 0);
                    layout->addWidget(nonAgLab, zeile, 1);

                    QPushButton *nonAgButton0 = new QPushButton(tr("Farbe �ndern"));
                    layout->addWidget(nonAgButton0, zeile++, 2);
                    sigmapper->setMapping(nonAgButton0, QString("nonAgColor"));
                    connect(nonAgButton0, SIGNAL(clicked()), sigmapper, SLOT(map()));


                    layout->addWidget(agIconLab, zeile, 0);
                    layout->addWidget(agLab, zeile, 1);

                    QPushButton *agButton = new QPushButton(tr("Farbe �ndern"));
                    layout->addWidget(agButton, zeile++, 2);
                    sigmapper->setMapping(agButton, QString("agColor"));
                    connect(agButton, SIGNAL(clicked()), sigmapper, SLOT(map()));

                    gbox0->setLayout(layout);
                    gbox0->setHidden(true);//curId !=0);

                    mainLayout->addWidget(gbox0);;
                    //mainLayout->addSpacing(1);//>addStretch(1);

                    //zeile++;

            //1 SOILTYPE
             nonAgIconLab = new QLabel();
             nonAgIconLab->setPixmap(normalIcon(nonAgColor));
             nonAgLab = new QLabel();
             nonAgLab->setText(tr("Siedlungsfl�che"));

             vector<QLabel*> soilIconLabs;
             vector<QLabel*> soilLabs;
             soilIconLabs.resize(nSoils);
             soilLabs.resize(nSoils);

             for (int i=0; i<nSoils; ++i){
                QLabel *ilab = new QLabel();
                ilab->setPixmap(normalIcon(soiltypeColors[i]));
                QLabel *lab = new QLabel();
                lab->setText(sSoilnames[i].c_str());
                soilIconLabs[i]=ilab;
                soilLabs[i]=lab;
             }

                QGroupBox *gbox1 = new QGroupBox("Bodentypen");
                layout = new QGridLayout;

                    zeile =0;

                    layout->addWidget(nonAgIconLab, zeile, 0);
                    layout->addWidget(nonAgLab, zeile, 1);

                    QPushButton *nonAgButton1 = new QPushButton(tr("Farbe �ndern"));
                    layout->addWidget(nonAgButton1, zeile, 2);
                    sigmapper->setMapping(nonAgButton1, QString("nonAgColor"));
                    connect(nonAgButton1, SIGNAL(clicked()), sigmapper, SLOT(map()));

                    vector<QPushButton*> soilButtons;
                    soilButtons.resize(nSoils);
                    for (j=0; j<nSoils;++j){
                        layout->addWidget(soilIconLabs[j], zeile+j+1,0);
                        layout->addWidget(soilLabs[j], zeile+j+1, 1);
                        soilButtons[j] = new QPushButton(tr("Farbe �ndern"));
                        layout->addWidget(soilButtons[j], zeile+j+1, 2);
                        //soilButtons[j]->setEnabled(false);

                        sigmapper->setMapping(soilButtons[j], soilLabs[j]->text());
                        connect(soilButtons[j], SIGNAL(clicked()), sigmapper, SLOT(map()));
                    }

                gbox1->setLayout(layout);
                gbox1->setHidden(true);//(curId !=1) &&( curId !=2));

                mainLayout->addWidget(gbox1);
               //mainLayout->addSpacing(1);
               // zeile=zeile+j+1;

             //2 SOILTYPEFREE
                mainLayout->addWidget(new QWidget());

             //3 OWNER
             zeile=0;

             nonAgIconLab = new QLabel();
             nonAgIconLab->setPixmap(normalIcon(nonAgColor));
             nonAgLab = new QLabel();
             nonAgLab->setText(tr("Siedlungsfl�che"));

                QLabel *myIconLab = new QLabel();
                myIconLab->setPixmap(normalIcon(farm0Color));
                QLabel *myLab = new QLabel();
                myLab->setText("Meine Fl�che");

                QGroupBox *gbox3 = new QGroupBox("Besitzer");
                layout = new QGridLayout;
                    layout->addWidget(nonAgIconLab, zeile, 0);
                    layout->addWidget(nonAgLab, zeile, 1);

                    QPushButton *nonAgButton3 = new QPushButton(tr("Farbe �ndern"));
                    layout->addWidget(nonAgButton3, zeile++, 2);
                    sigmapper->setMapping(nonAgButton3, QString("nonAgColor"));
                    connect(nonAgButton3, SIGNAL(clicked()), sigmapper, SLOT(map()));

                    layout->addWidget(myIconLab, zeile, 0);
                    layout->addWidget(myLab, zeile, 1);

                        QPushButton *myFarmButton = new QPushButton(tr("Farbe �ndern"));
                        layout->addWidget(myFarmButton, zeile++, 2);
                        sigmapper->setMapping(myFarmButton, QString("myFarmColor"));
                        //myFarmButton->setEnabled(false);
                        connect(myFarmButton, SIGNAL(clicked()), sigmapper, SLOT(map()));

                   gbox3->setLayout(layout);
                   gbox3->setHidden(true);//curId !=3);

                   mainLayout->addWidget(gbox3);
                   //mainLayout->addSpacing(1);

           //4 FARMTYPE
           zeile=0;
           nonAgIconLab = new QLabel();
             nonAgIconLab->setPixmap(normalIcon(nonAgColor));
             nonAgLab = new QLabel();
             nonAgLab->setText(tr("Siedlungsfl�che"));

                    vector<QLabel*> farmtypeIconLabs;
                    vector<QLabel*> farmtypeLabs;
                    farmtypeIconLabs.resize(nFarmtypes);
                    farmtypeLabs.resize(nFarmtypes);

                    for (int i=0; i<nFarmtypes; ++i){
                       QLabel *ilab = new QLabel();
                       ilab->setPixmap(normalIcon(farmtypeColors[i]));
                       QLabel *lab = new QLabel();
                       lab->setText(classnames[i+1].c_str());
                       farmtypeIconLabs[i]=ilab;
                       farmtypeLabs[i]=lab;

                    }

                    QGroupBox *gbox4 = new QGroupBox("Betriebstypen");
                    layout = new QGridLayout;

                    layout->addWidget(nonAgIconLab, zeile, 0);
                    layout->addWidget(nonAgLab, zeile, 1);

                    QPushButton *nonAgButton4 = new QPushButton(tr("Farbe �ndern"));
                    layout->addWidget(nonAgButton4, zeile, 2);
                    sigmapper->setMapping(nonAgButton4, QString("nonAgColor"));
                    connect(nonAgButton4, SIGNAL(clicked()), sigmapper, SLOT(map()));

                    vector<QPushButton*> farmtypeButtons;
                    farmtypeButtons.resize(nFarmtypes);
                    for (j=0; j<nFarmtypes;++j){
                        layout->addWidget(farmtypeIconLabs[j], zeile+j+1,0);
                        layout->addWidget(farmtypeLabs[j], zeile+j+1, 1);
                        farmtypeButtons[j] = new QPushButton(tr("Farbe �ndern"));
                        layout->addWidget(farmtypeButtons[j], zeile+j+1, 2);
                        //farmtypeButtons[j]->setEnabled(false);
                        sigmapper->setMapping(farmtypeButtons[j], farmtypeLabs[j]->text());
                        connect(farmtypeButtons[j], SIGNAL(clicked()), sigmapper, SLOT(map()));
                    }
                //zeile = zeile+j+1;
                gbox4->setLayout(layout);
                gbox4->setHidden(true);//curId !=4);
                mainLayout->addWidget(gbox4);
                //mainLayout->addSpacing(1);

             //5 FREE
             zeile = 0;
             nonAgIconLab = new QLabel();
             nonAgIconLab->setPixmap(normalIcon(nonAgColor));
             nonAgLab = new QLabel();
             nonAgLab->setText(tr("Siedlungsfl�che"));

                    QLabel *freeIconLab = new QLabel();
                    freeIconLab->setPixmap(normalIcon(freeColor));
                    QLabel *freeLab = new QLabel();
                    freeLab->setText(tr("Freie Fl�che"));

                    QLabel *nonFreeIconLab = new QLabel();
                    nonFreeIconLab->setPixmap(normalIcon(nonfreeColor));
                    QLabel *nonFreeLab = new QLabel();
                    nonFreeLab->setText(tr("Nicht-Freie Fl�che"));

                    QGroupBox *gbox5 = new QGroupBox("Freie/non-Freie");
                    layout = new QGridLayout;

                    layout->addWidget(nonAgIconLab, zeile, 0);
                    layout->addWidget(nonAgLab, zeile, 1);

                    QPushButton *nonAgButton5 = new QPushButton(tr("Farbe �ndern"));
                    layout->addWidget(nonAgButton5, zeile++, 2);
                    sigmapper->setMapping(nonAgButton5, QString("nonAgColor"));
                    connect(nonAgButton5, SIGNAL(clicked()), sigmapper, SLOT(map()));

                    layout->addWidget(freeIconLab, zeile, 0);
                    layout->addWidget(freeLab, zeile, 1);
                    QPushButton *freeButton = new QPushButton(tr("Farbe �ndern"));
                    layout->addWidget(freeButton, zeile++, 2);
                    //freeButton->setEnabled(false);
                    sigmapper->setMapping(freeButton, QString("freeColor"));
                    connect(freeButton, SIGNAL(clicked()), sigmapper, SLOT(map()));

                    layout->addWidget(nonFreeIconLab, zeile, 0);
                    layout->addWidget(nonFreeLab, zeile, 1);
                    QPushButton *nonfreeButton = new QPushButton(tr("Farbe �ndern"));
                    layout->addWidget(nonfreeButton, zeile++, 2);
                    //nonfreeButton->setEnabled(false);
                    sigmapper->setMapping(nonfreeButton, QString("nonfreeColor"));
                    connect(nonfreeButton, SIGNAL(clicked()), sigmapper, SLOT(map()));

                    gbox5->setLayout(layout);
                    gbox5->setHidden(true);//curId !=5);
                    mainLayout->addWidget(gbox5);

                connect(sigmapper,SIGNAL(mapped(QString)), this, SLOT(changeColor(QString)));

                connect(okButton, SIGNAL(clicked()), colorDialog, SLOT(close()));
                connect(colorDialog, SIGNAL(finished(int)),this, SLOT(colorDialogClosed(int)));

                mainLayout->addWidget(okButton);

                colorDialog->setLayout(mainLayout);
                if (tmpDialog) delete tmpDialog;
        }
//*/

bool Landwindow::zunah(QColor c, int r, int g, int b){
   return (abs(c.red()-r) < 50 && c.red()-r == c.green()-g
            && c.red()-r == c.blue()-b);
}

void Landwindow::changeColor(QString s){
  /*
  0 nonag
  1  soil_1
  nSoils soil_nSoils
  nSoils+1 farm_0
  nSoils+2 farmtype_1
  nSoils+nFarmtypes+1 farmtype_nFarmtypes
  nSoils+nFarmtypes+2 otherfarmColor
  nSoils+nFarmtypes+3 myColor
  nSoils+nFarmtypes+4 gridColor
  //*/
  bool changed= false;
  QColor c;

  QGridLayout *lout =static_cast<QGridLayout*> (colorDialog->layout());

if (s==QString("nonAgColor")){
  //c = nonAgColor;
  c = QColorDialog::getColor(nonAgColor,this);
  if (c.isValid() && c!=nonAgColor) {
      nonAgColor = c;
      changed=true;
      static_cast<QLabel*>(lout->itemAtPosition(0,0)->widget())->setPixmap(normalIcon(nonAgColor));
  }

}else if (s==QString("myFarmColor")){
  //c=farm0Color;
  c=QColorDialog::getColor(farm0Color,this);
  if (c.isValid() && c!=farm0Color){
       farm0Color = c;
       changed=true;
       static_cast<QLabel*>(lout->itemAtPosition(nSoils+1,0)->widget())->setPixmap(normalIcon(farm0Color));
  }
}

else if (s==QString("otherfarmColor")){
  //c=otherfarmColor;
  c=QColorDialog::getColor(otherfarmColor,this);
  if (c.isValid() && c!=otherfarmColor){
        otherfarmColor=c;
       changed=true;
       static_cast<QLabel*>(lout->itemAtPosition(nSoils+nFarmtypes+2,0)->widget())->setPixmap(normalIcon(otherfarmColor));
  }
}else if (s==QString("myColor")){
  //c=myColor;
  c=QColorDialog::getColor(myColor,this);
  if (c.isValid() && c!=myColor){
        myColor=c;
       changed=true;
       static_cast<QLabel*>(lout->itemAtPosition(nSoils+nFarmtypes+3,0)->widget())->setPixmap(normalIcon(myColor));
  }
}else if (s==QString("gridColor")){
  //c=gridColor;
  c=QColorDialog::getColor(gridColor,this);
  if (c.isValid() && c!=gridColor){
        gridColor=c;
       changed=true;
       static_cast<QLabel*>(lout->itemAtPosition(nSoils+nFarmtypes+4,0)->widget())->setPixmap(gridIcon(gridColor));
  }
}
for (int i=0;i < nSoils; ++i){
  if (s==QString(sSoilnames[i].c_str())) {
  c=QColorDialog::getColor(soiltypeColors[i],this);
  if(!c.isValid())continue;
  if (c!=soiltypeColors[i]){
      soiltypeColors[i]=c;
      changed=true;
      static_cast<QLabel*>(lout->itemAtPosition(i+1,0)->widget())->setPixmap(
           normalIcon(soiltypeColors[i]));
       }
    }
}

for (int i=0; i<classnames.size();++i) {
    if (s==QString(classnames[i+1].c_str())) {
  //c=farmtypeColors[i];
  c=QColorDialog::getColor(farmtypeColors[i],this);
  if(!c.isValid())continue;
  if (c!=farmtypeColors[i]){
      farmtypeColors[i]=c;
      changed=true;
      static_cast<QLabel*>(lout->itemAtPosition(i+nSoils+2,0)->widget())->setPixmap(
           normalIcon(farmtypeColors[i]));
       }
  }
}

 if (changed){
   updateGraphic();//layerComboBox->currentIndex());
   updateLegends();
 }
}

void Landwindow::updateLegends(){
   if (modus == 1) {
        updateLegendWidget();
        return;
   }

   for (int i=0; i<numPlotType; ++i){
        updateLegendWidget(i);
    }
}

void Landwindow::updateLegendWidget(){
     QGridLayout *lout = layout;
     int j;
     static_cast<QLabel*>(lout->itemAtPosition(0,0)->widget())->setPixmap(normalIcon(nonAgColor));
     for (j=0; j<nSoils; ++j){
          static_cast<QLabel*>(lout->itemAtPosition(2*j+1,0)->widget())->setPixmap(normalIcon(soiltypeColors[j]));
          static_cast<QLabel*>(lout->itemAtPosition(2*j+2,0)->widget())->setPixmap(freeIcon(soiltypeColors[j]));
     }
    int zeile=2*j+1;
    QColor c  = chosenFarmid==0 ?soiltypeColors[mapIDpFarm[0]->farm_plot->getSoilType()]: myColor;
    static_cast<QLabel*>(lout->itemAtPosition(zeile+4,0)->widget())->setPixmap(
            mySitzIcon(c));//soiltypeColors[mapIDpFarm[0]->farm_plot->getSoilType()]));
    if(chosenFarmid==0){
        static_cast<QLabel*>(lout->itemAtPosition(zeile+3,0)->widget())->hide();
        static_cast<QLabel*>(lout->itemAtPosition(zeile+3,1)->widget())->hide();
    }
    else {
        static_cast<QLabel*>(lout->itemAtPosition(zeile+3,0)->widget())->setPixmap(
            normalIcon(c));
        static_cast<QLabel*>(lout->itemAtPosition(zeile+3,0)->widget())->show();
        static_cast<QLabel*>(lout->itemAtPosition(zeile+3,1)->widget())->show();
    }

     static_cast<QLabel*>(lout->itemAtPosition(zeile+1,0)->widget())->setPixmap(
            normalIcon(otherfarmColor));
     /*static_cast<QLabel*>(lout->itemAtPosition(zeile+2,0)->widget())->setPixmap(
            freeIcon(otherfarmColor));
            //*/


}

void Landwindow::updateLegendWidget(int id){
QGridLayout *lout = layouts[id];
int j;
switch (id){
case 0:
     static_cast<QLabel*>(lout->itemAtPosition(0,0)->widget())->setPixmap(normalIcon(nonAgColor));

     static_cast<QLabel*>(lout->itemAtPosition(1,0)->widget())->setPixmap(normalIcon(farm0Color));
     static_cast<QLabel*>(lout->itemAtPosition(3,0)->widget())->setPixmap(mySitzIcon(farm0Color));

     break;
case 1:
     static_cast<QLabel*>(lout->itemAtPosition(0,0)->widget())->setPixmap(normalIcon(nonAgColor));
     for (j=0; j<nSoils; ++j){
          static_cast<QLabel*>(lout->itemAtPosition(2*j+1,0)->widget())->setPixmap(normalIcon(soiltypeColors[j]));
          static_cast<QLabel*>(lout->itemAtPosition(2*j+2,0)->widget())->setPixmap(freeIcon(soiltypeColors[j]));
     }
     static_cast<QLabel*>(lout->itemAtPosition(2*j+2,0)->widget())->setPixmap(
            mySitzIcon(soiltypeColors[mapIDpFarm[0]->farm_plot->getSoilType()]));
     break;

case 2:
     static_cast<QLabel*>(lout->itemAtPosition(0,0)->widget())->setPixmap(normalIcon(nonAgColor));
     for (j=0; j<nFarmtypes; ++j){
        static_cast<QLabel*>(lout->itemAtPosition(j+1,0)->widget())->setPixmap(normalIcon(farmtypeColors[j]));
     }
     static_cast<QLabel*>(lout->itemAtPosition(j+2,0)->widget())->setPixmap(
            mySitzIcon(farmtypeColors[mapIDpFarm[0]->getFarmClass()-1]));
     break;

default: ;
}
}

/*
void Landwindow::changeColor(QString s){
  bool changed=false;
  QColor c;
  int id = layerComboBox->currentIndex();
  if (id==2) id =1;
  QVBoxLayout *vlout =static_cast<QVBoxLayout*> (colorDialog->layout());
  QGroupBox *gbox= static_cast<QGroupBox*>(vlout->itemAt(id)->widget());
  QGridLayout *lout = static_cast<QGridLayout*> (gbox->layout());
if (s==QString("nonAgColor")){
  c = nonAgColor;
  nonAgColor = QColorDialog::getColor(nonAgColor,this);
  if (c!=nonAgColor) {
      changed=true;
      static_cast<QLabel*>(lout->itemAtPosition(0,0)->widget())->setPixmap(normalIcon(nonAgColor));
  }
}else if (s==QString("agColor")){
  c= agColor;
  agColor = QColorDialog::getColor(agColor,this);
  if (c!=agColor) {
        changed=true;
      static_cast<QLabel*>(lout->itemAtPosition(1,0)->widget())->setPixmap(normalIcon(agColor));
  }
}else if (s==QString("myFarmColor")){
  c=farm0Color;
  farm0Color=QColorDialog::getColor(farm0Color,this);
  if (c!=farm0Color){
        changed=true;
        static_cast<QLabel*>(lout->itemAtPosition(1,0)->widget())->setPixmap(normalIcon(farm0Color));
  }
}else if (s==QString("freeColor")){
  c=freeColor;
  freeColor=QColorDialog::getColor(freeColor,this);
  if (c!=freeColor){
        changed=true;
      static_cast<QLabel*>(lout->itemAtPosition(1,0)->widget())->setPixmap(normalIcon(freeColor));
      }
}else if (s==QString("nonfreeColor")){
  c=nonfreeColor;
  nonfreeColor=QColorDialog::getColor(nonfreeColor,this);
  if (c!=nonfreeColor){
        changed=true;
      static_cast<QLabel*>(lout->itemAtPosition(2,0)->widget())->setPixmap(normalIcon(nonfreeColor));
      }
}
for (int i=0;i < gg->NO_OF_SOIL_TYPES; ++i){
    if (s==QString(gg->NAMES_OF_SOIL_TYPES[i].c_str())) {
  c=soiltypeColors[i];
  soiltypeColors[i]=QColorDialog::getColor(c,this);
  if (c!=soiltypeColors[i]){
    changed=true;
      static_cast<QLabel*>(lout->itemAtPosition(i+1,0)->widget())->setPixmap(
       normalIcon(soiltypeColors[i]));
       }
    }
}
//*/

void Landwindow::saveColors(){

    QSettings colorSettings("IAMO, BSE", "Farm AgriPoliS");
    colorSettings.setValue("otherFarmColor", otherfarmColor);
    colorSettings.setValue("myColor", myColor);

    colorSettings.setValue("gridColor", gridColor);

    colorSettings.setValue("nonAgColor", nonAgColor);

    //colorSettings.setValue("agColor", agColor);

    colorSettings.setValue("freeColor", freeColor);
    //colorSettings.setValue("nonfreeColor", nonfreeColor);

    colorSettings.setValue("farm0Color", farm0Color);

    colorSettings.setValue("nFarmTypes", nFarmtypes);
    for (int i=0; i<nFarmtypes; ++i){
        colorSettings.setValue(QString(classnames[i+1].c_str()), farmtypeColors[i]);
    }

    colorSettings.setValue("nSoilTypes", nSoils);
    for (int i=0; i<nSoils; ++i){
        colorSettings.setValue(QString(sSoilnames[i].c_str()), soiltypeColors[i]);
    }
}

void Landwindow::colorDialogClosed(int){
    actColor->setEnabled(true);
    layerComboBox->setEnabled(true);
}

QGraphicsView *Landwindow::getView(){
return view;
}

void Landwindow::initInfoWidget(){
    infoWidget= new QScrollArea();
    QWidget *iw = new QWidget();

    RegFarmInfo *farm = mapIDpFarm[0];
    nInvests=Manager->InvestCatalog.size();

    //getFarmInfo(farm);
    QLabel *idnameLabel = new QLabel(tr("Betriebsnummer"));//Farm ID"));
    //idEdit = new QLineEdit(infoWidget);
    //idLabel->setBuddy(idEdit);
    idLabel=new QLabel();
    idLabel->setTextFormat(Qt::RichText);
    idLabel->setAlignment(Qt::AlignCenter);
    idLabel->setText("0");

    QLabel *farmtypenamelab= new QLabel(tr("Betriebstyp"));
    farmtypeLabel=new QLabel();
    farmtypeLabel->setAlignment(Qt::AlignRight);
    farmtypeLabel->setText(classnames[farm->getFarmClass()].c_str());

    QLabel *kapnameLabel= new QLabel(tr("Eigenkapital"));
    kapLabel= new QLabel();

    kapLabel->setTextFormat(Qt::RichText);
    kapLabel->setAlignment(Qt::AlignRight);
    kapLabel->setText("<font color=blue>"+QString::number(farm->getEquityCapital())+"</font>");

    QLabel *managekoeffnameLabel= new QLabel(tr("Managementf�higkeit"));
    managekoeffLabel= new QLabel();
    managekoeffLabel->setTextFormat(Qt::RichText);
    managekoeffLabel->setAlignment(Qt::AlignRight);
    //managekoeffLabel->setText("<color=blue>"+QString::number(1/farm->getManagementCoefficient())+"</font>");//+" "+QChar(8364));
    managekoeffLabel->setText(manageCap(farm->getManagementCoefficient()));

    QLabel *betriebsalternameLabel= new QLabel(tr("Alter des Betriebs"));
    betriebsalterLabel= new QLabel();
    betriebsalterLabel->setTextFormat(Qt::RichText);
    betriebsalterLabel->setAlignment(Qt::AlignRight);
    betriebsalterLabel->setText("<font color=blue>"+QString::number(farm->getFarmAge())+"</font>");


    QLabel *liqnameLabel= new QLabel(tr("Liquidit�t"));
    liqLabel=new QLabel();

    liqLabel->setAlignment(Qt::AlignRight);
    liqLabel->setText(QString::number(farm->getLiquidity()));//+" "+QChar(8364));

    /*QLabel *mLabel=new QLabel();
    mLabel->setText(tr("Maschinen"));
    machineLabel= new QLabel();
    machineLabel->setText("0");
    //*/

       QGridLayout *lout0 = new QGridLayout;
    lout0->addWidget(idnameLabel,0,0);
    lout0->addWidget(idLabel,0,1);

   {
    int i=0;
    int j=0;
    QLabel *tlab1;
    glout=new QGridLayout;

    glout->addWidget(managekoeffnameLabel,i,j++);
    glout->addWidget(managekoeffLabel,i++,j,1,2);
    //tlab1= new QLabel(QChar(8364));
    //tlab1->setAlignment(Qt::AlignCenter);
    //glout->addWidget(tlab1,i++,j);

    j=0;
    glout->addWidget(betriebsalternameLabel,i,j++);
    glout->addWidget(betriebsalterLabel,i,j++);
    tlab1= new QLabel("J");
    tlab1->setAlignment(Qt::AlignCenter);
    glout->addWidget(tlab1,i++, j);

    j=0;
    glout->addWidget(kapnameLabel,i,j++);
    glout->addWidget(kapLabel,i,j++);
    tlab1= new QLabel(QChar(8364));
    tlab1->setAlignment(Qt::AlignCenter);
    glout->addWidget(tlab1,i++, j);

    j=0;
    glout->addWidget(liqnameLabel,i,j++);
    glout->addWidget(liqLabel,i,j++);
    tlab1= new QLabel(QChar(8364));
    tlab1->setAlignment(Qt::AlignCenter);
    glout->addWidget(tlab1,i++,j);

    j=0;
    glout->addWidget(farmtypenamelab, i,j++);
    glout->addWidget(farmtypeLabel,i++,j);

    j=0;
    //glout->addWidget(mLabel,3,0);
    //glout->addWidget(new QWidget(),3,1);
    //glout->addWidget(machineLabel,3,1);
   }

    QGroupBox *generalGBox = new QGroupBox;
    generalGBox->setTitle(tr("Kenndaten"));
    generalGBox->setLayout(glout);

    //QLabel *rentLandLabel = new QLabel(tr("Gepachtete Fl�chen:"));
    //rentLandEdit = new QLineEdit(infoWidget);
    //rentLandLabel->setBuddy(rentLandEdit);
    vector<QLabel*> landnameLabels;
    for (int i=0;i<nSoils;++i){
        QLabel *lab = new QLabel();
        //QLineEdit *ed = new QLineEdit();
        QLabel *lab2= new QLabel();
        lab2->setAlignment(Qt::AlignRight);
        lab2->setText(QString::number(farm->getLandInputOfType(i)));//+ " ha");
        //landEdits.push_back(ed);
        landLabels.push_back(lab2);
        //lab->setBuddy(ed);
        lab->setText(sSoilnames[i].c_str());
        landnameLabels.push_back(lab);
    }

    QGridLayout *landLayout=new QGridLayout;
    for (int i=0;i<nSoils;++i){
        landLayout->addWidget(landnameLabels[i],i,0);
        landLayout->addWidget(landLabels[i],i,1);
        QLabel *tlab= new QLabel("ha");
        tlab->setAlignment(Qt::AlignRight);
        landLayout->addWidget(tlab,i,2);
    }
    QGroupBox *landGroupBox =new QGroupBox();
    landGroupBox->setTitle(tr("Fl�chen"));//Betriesgr��e"));
    landGroupBox->setLayout(landLayout);


    vector<QLabel*> landnameLabs;
    for (int i=0;i<nSoils;++i){
        QLabel *lab = new QLabel();
        //QLineEdit *ed = new QLineEdit();
        QLabel *lab2= new QLabel();
        lab2->setAlignment(Qt::AlignRight);

        lab2->setText(QString::number(farm->getAvRentOfType(i),'g', 4));//
                //+" "+QChar(8364)+"/ha");
        //landEdits.push_back(ed);
        landpriceLabels.push_back(lab2);
        //lab->setBuddy(ed);
        lab->setText(sSoilnames[i].c_str());
        landnameLabs.push_back(lab);
    }

    QGridLayout *landpriceLayout=new QGridLayout;
    for (int i=0;i<nSoils;++i){
        landpriceLayout->addWidget(landnameLabs[i],i,0);
        landpriceLayout->addWidget(landpriceLabels[i],i,1);
        QLabel *tlab = new QLabel(QString(QChar(8364))+"/ha");
        tlab->setAlignment(Qt::AlignRight);
        landpriceLayout->addWidget(tlab,i ,2);
    }
    QGroupBox *landpriceGroupBox =new QGroupBox();
    landpriceGroupBox->setTitle(tr("Durchschnittl. Pachtpreis"));
    landpriceGroupBox->setLayout(landpriceLayout);

    investLayout=0; //new QGridLayout
    investGroupBox =0; //new QGroupBox

    machineLayout=0;
    machineGroupBox =0;

    buildingLayout=0;
    buildingGroupBox =0;

    getStallMachine(farm);
    getMachines(farm);
    //getBuildings(farm);

    //investGroupBox->setLayout(investLayout);

    vlayout = new QVBoxLayout();
    /*vlayout->addWidget(idLabel);
    vlayout->addWidget(idEdit);
    vlayout->addStretch(1);
    //*/
    vlayout->addLayout(lout0);
    vlayout->addStretch(0);

    vlayout->addWidget(generalGBox);
    vlayout->addStretch(0);

    vlayout->addWidget(landGroupBox);
    vlayout->addStretch(0);

    vlayout->addWidget(landpriceGroupBox);
    vlayout->addStretch(0);

    vlayout->addWidget(investGroupBox);
    vlayout->addStretch(0);

    vlayout->addWidget(machineGroupBox);
    vlayout->addStretch(1);

    //vlayout->addWidget(buildingGroupBox);
    //vlayout->addStretch(2);

    /*
    vlayout->addWidget(rentLandLabel);
    vlayout->addWidget(rentLandEdit);
    vlayout->addStretch(1);

    vlayout->addWidget(capLabel);
    vlayout->addWidget(capComboBox);
    //*/
    iw->setLayout(vlayout);
    //iw->setMinimumHeight(500);
    infoWidget->setWidget(iw);

}

void Landwindow::updateRegionData(){
  int nfarms = Manager->getNoOfFarms();
  reginfo->anzFarms = nfarms;
  double plsz = gg->PLOT_SIZE;

  reginfo->landSoilsFree.clear();
  reginfo->landSoilsFree.resize(nSoils);
  reginfo->landFarmtypes.clear();
  reginfo->landFarmtypes.resize(nFarmtypes);

  int nplots = Manager->Region->plots.size();
  for (int i=0; i<nplots; ++i){
        RegPlotInfo *p = Manager->Region->plots[i];
        if (p->getSoilType()==nSoils) continue;
        if (p->getState()==0)
            reginfo->landSoilsFree[p->getSoilType()]++;
        reginfo->landFarmtypes[mapIDpFarm[p->getFarmId()]->getFarmClass()-1]
            ++;
  }

  list<RegFarmInfo*> farmlist = Manager->FarmList;
  list<RegFarmInfo*>::iterator it = farmlist.begin();
  reginfo->anzFarmtypes.clear();
  reginfo->anzFarmtypes.resize(nFarmtypes);

  while (it != farmlist.end()){
    reginfo->anzFarmtypes[(*it)->getFarmClass()-1]++;
    ++it;
  }


    anzLabel->setText(QLocale().toString(reginfo->anzFarms));
    QString d = QString::number(static_cast<int>(reginfo->agland*100./reginfo->anzFarms*plsz+0.5));
    //sizeLabel->setText(d.length()>=3? d.insert(-2,'.'):
      //          (d.length()==2? d.insert(0,"0."): d.insert(0,"0.0")));
    sizeLabel->setText(QLocale().toString(reginfo->agland/reginfo->anzFarms*plsz+0.5,'f',2));
    for (int i=0;i<nSoils;++i){
        QString t = QString::number(static_cast<int>(reginfo->landSoils[i]*10000./reginfo->agland+0.5));
        landLabels1[i]->setText(t.length()>=3? t.insert(-2,'.'):
                (t.length()==2? t.insert(0,"0."): t.insert(0,"0.0")));
        freeLandLabels[i]->setText(QLocale().toString(reginfo->landSoilsFree[i]*plsz));
        //QString::number(reginfo->landSoilsFree[i]*plsz));
    }

    for (int i=0;i<nFarmtypes;++i){
        farmtypeLabels[i]->setText(QString::number(reginfo->anzFarmtypes[i]));
        QString s = QString::number(static_cast<int>(reginfo->landFarmtypes[i]*10000./reginfo->agland+0.5));
        ftAreaLabels[i]->setText(s.length()>=3? s.insert(-2,QChar('.')):
            (s.length()==2 ? s.insert(0,QString("0.")) : s.insert(0,QString("0.0"))));
    }
}

void Landwindow::initRegionData(){
  int nfarms = Manager->getNoOfFarms();
  reginfo->anzFarms = nfarms;

  reginfo->agland=0;
  reginfo->pachtland=0;
  reginfo->anzFarmtypes.clear();
  reginfo->landFarmtypes.clear();
  reginfo->landSoils.clear();
  reginfo->landSoilsFree.clear();
  reginfo->anzFarmtypes.resize(nFarmtypes);
  reginfo->landFarmtypes.resize(nFarmtypes);
  reginfo->landSoils.resize(nSoils);
  reginfo->landSoilsFree.resize(nSoils);

  int nplots = Manager->Region->plots.size();
  for (int i=0; i<nplots; ++i){
         RegPlotInfo *p = Manager->Region->plots[i];
        if (p->getSoilType()==nSoils) continue;
        reginfo->agland++;
        reginfo->landSoils[p->getSoilType()]++;
        if (p->getState()==0)
            reginfo->landSoilsFree[p->getSoilType()]++;
        reginfo->landFarmtypes[mapIDpFarm[p->getFarmId()]->getFarmClass()-1]
            ++;
        if (p->getState()==1)
            reginfo->pachtland++;
  }

}

void Landwindow::initInfoWidgetRegion(){
    anzLabel=new QLabel();
    sizeLabel=new QLabel();
    pachtLabel=new QLabel();
    gesamtLabel=new QLabel();

    initRegionData();


    double plsz = gg->PLOT_SIZE;
    if(!infoWidgetRegion) infoWidgetRegion = new QWidget();

    QLabel *anzNameLabel = new QLabel(tr("Anzahl Betriebe"));//Farm ID"));

    anzLabel->setTextFormat(Qt::RichText);
    anzLabel->setAlignment(Qt::AlignRight);//Center);
    anzLabel->setText(QString::number(reginfo->anzFarms));

    QLabel *sizeNameLab= new QLabel(tr("Durschschn. Gr��e(ha)"));

    sizeLabel->setAlignment(Qt::AlignRight);
    QString d = QString::number(static_cast<int>(reginfo->agland*100./reginfo->anzFarms*plsz+0.5));
    sizeLabel->setText(d.length()>=3? d.insert(-2,'.'):
                (d.length()==2? d.insert(0,"0."): d.insert(0,"0.0")));

    QLabel *pachtNameLab = new QLabel(tr("Pachtfl�chenanteil(\%)"));
    pachtLabel->setAlignment(Qt::AlignRight);
    QString ds = QString::number(static_cast<int>(reginfo->pachtland*10000./reginfo->agland+0.5));
    pachtLabel->setText(ds.length()>=3? ds.insert(-2,'.'):
                (ds.length()==2? ds.insert(0,"0."): ds.insert(0,"0.0")));

    QLabel *gesamtLab = new QLabel(tr("Gesamtfl�chen(ha)"));
    gesamtLabel->setAlignment(Qt::AlignRight);
    QString dss = QLocale().toString(reginfo->agland*plsz);
    //QString::number(reginfo->agland*plsz);
    gesamtLabel->setText(dss);

    QGridLayout *lout0 = new QGridLayout;
    lout0->addWidget(anzNameLabel,0,0);
    lout0->addWidget(anzLabel,0,1);

    lout0->addWidget(sizeNameLab,1,0);
    lout0->addWidget(sizeLabel,1,1);

    lout0->addWidget(gesamtLab,2,0);
    lout0->addWidget(gesamtLabel,2,1);

    lout0->addWidget(pachtNameLab,3,0);
    lout0->addWidget(pachtLabel,3,1);

    QGroupBox *szGBox = new QGroupBox;
    //szGBox->setStyle(Qt::);
    szGBox->setTitle(tr("�berblick"));//Betriebe"));
    szGBox->setLayout(lout0);

    //==============

    vector<QLabel*> landnameLabels;//,landLabels, freeLandLabels;
    for (int i=0;i<nSoils;++i){
        QLabel *lab = new QLabel();
        QLabel *lab2= new QLabel();
        lab2->setAlignment(Qt::AlignRight);
        QString t = QString::number(static_cast<int>(reginfo->landSoils[i]*10000./reginfo->agland+0.5));
        lab2->setText(t.length()>=3? t.insert(-2,'.'):
                (t.length()==2? t.insert(0,"0."): t.insert(0,"0.0")));
        lab2->setIndent(10);
        landLabels1.push_back(lab2);
        QLabel *lab3 = new QLabel();
        lab3->setAlignment(Qt::AlignRight);
        lab3->setIndent(10);
        lab3->setText(QString::number(reginfo->landSoilsFree[i]*plsz));
        freeLandLabels.push_back(lab3);
        lab->setText(sSoilnames[i].c_str());
        landnameLabels.push_back(lab);
    }

    QGridLayout *landLayout=new QGridLayout;
    QLabel *landAnteilLabel = new QLabel("Anteil(\%)");
    landAnteilLabel->setAlignment(Qt::AlignCenter);

    QLabel *landFreeLabel  =new QLabel("Frei(ha)");
    landFreeLabel->setAlignment(Qt::AlignCenter);

    landLayout->addWidget(landAnteilLabel,0, 1);
    landLayout->addWidget(landFreeLabel,0, 2);

    for (int i=0;i<nSoils;++i){
        landLayout->addWidget(landnameLabels[i],i+1,0);
        landLayout->addWidget(landLabels1[i],i+1,1);
        //QLabel *tlab= new QLabel("ha");
        //tlab->setAlignment(Qt::AlignRight);
        landLayout->addWidget(freeLandLabels[i],i+1,2);
    }
    QGroupBox *landGroupBox =new QGroupBox();
    landGroupBox->setTitle(tr("Nach Bodentypen"));
    landGroupBox->setLayout(landLayout);

    //=========

    vector<QLabel*> farmtypenameLabels;//,farmtypeLabels,ftAreaLabels;
    for (int i=0;i<nFarmtypes;++i){
        QLabel *lab = new QLabel();
        QLabel *lab2= new QLabel();
        lab2->setAlignment(Qt::AlignCenter);
        lab2->setText(QString::number(reginfo->anzFarmtypes[i]));
        farmtypeLabels.push_back(lab2);
        QLabel *lab3 = new QLabel();
        QString s = QString::number(static_cast<int>(reginfo->landFarmtypes[i]*10000./reginfo->agland+0.5));
        lab3->setText(s.length()>=3? s.insert(-2,QChar('.')):
            (s.length()==2 ? s.insert(0,QString("0.")) : s.insert(0,QString("0.0"))));
        lab3->setAlignment(Qt::AlignRight);
        lab3->setIndent(10);
        ftAreaLabels.push_back(lab3);
        lab->setText(classnames[i+1].c_str());
        farmtypenameLabels.push_back(lab);
    }

    QGridLayout *farmtypeLayout=new QGridLayout;
    QLabel *ftAnteilLabel = new QLabel("Anz.Betriebe");
    QLabel *ftAreaLabel  =new QLabel("Fl�che(\%)");
    ftAreaLabel->setAlignment(Qt::AlignCenter);

    farmtypeLayout->addWidget(ftAnteilLabel,0, 1);
    farmtypeLayout->addWidget(ftAreaLabel,0, 2);


    for (int i=0;i<nFarmtypes;++i){
        farmtypeLayout->addWidget(farmtypenameLabels[i],i+1,0);
        farmtypeLayout->addWidget(farmtypeLabels[i],i+1,1);
        //QLabel *tlab= new QLabel("ha");
        //tlab->setAlignment(Qt::AlignRight);
        farmtypeLayout->addWidget(ftAreaLabels[i],i+1,2);
    }
    QGroupBox *farmtypeGroupBox =new QGroupBox();
    farmtypeGroupBox->setTitle(tr("Nach Betriebstypen"));
    farmtypeGroupBox->setLayout(farmtypeLayout);

    //===========

    QVBoxLayout *vlayout = new QVBoxLayout();
    vlayout->addWidget(szGBox);
    vlayout->addStretch(0);

    vlayout->addWidget(landGroupBox);
    vlayout->addStretch(0);

    vlayout->addWidget(farmtypeGroupBox);
    vlayout->addStretch(1);

    updateRegionData();
    infoWidgetRegion->setLayout(vlayout);
    //*/
}

void Landwindow::initInfos(){
    /*infoWidgets.resize(numPlotType);
    for (int i=0;i<numPlotType;++i){
        infoWidgets[i] = new QWidget();
        switch(i){
        case 0: infoWidgets[i]->setWindowTitle(tr("Betriebe")); break;
        case 1: infoWidgets[i]->setWindowTitle(tr("Bodentypen"));break;
        case 2: infoWidgets[i]->setWindowTitle(tr("Betriebstypen"));break;
        default:;
        }
    }
    //*/
    initInfoWidget();
    initInfoWidgetRegion();

    info->setWindowTitle(tr("Regionsbeschreibung"));//Region Info"));//Bodentypen"));
    info->setWidget(infoWidgetRegion);//infoWidgets[1]);
}

void Landwindow::getMachines0(RegFarmInfo *farm){
    machinenameLabels.clear();
    machineLabels.clear();
    machineCapLabels.clear();
    machineAgeLabels.clear();

    RegInvestList invList = *(farm->FarmInvestList);
    for(int i=0;i<nInvests;++i){
      int x =farm->getInvestmentsOfCatalogNumber(i);
      if (x<=0) continue;
      RegInvestObjectInfo invobj = Manager->InvestCatalog[i];
      int t = invobj.getAffectsProductGroup();
      if (t!=0) continue; //nur Maschinen

      QString name = invobj.getName().c_str();
      int cap = invobj.getCapacity();
      double age = invList.getAverageAgeOfInvestmentsOfCatalogNumber(i);

      QLabel* lab= new QLabel();
      lab->setAlignment(Qt::AlignRight);
      lab->setText(name);

      QLabel* labn= new QLabel();
      labn->setAlignment(Qt::AlignRight);
      labn->setText(QString::number(x));

      QLabel* labc= new QLabel();
      labc->setAlignment(Qt::AlignRight);
      labc->setText(QString::number(cap,'f',0));

      QLabel* labj= new QLabel();
      labj->setAlignment(Qt::AlignRight);
      labj->setText(QString::number(age,'f',1)+ " J.");

      machinenameLabels.push_back(lab);
      machineLabels.push_back(labn);
      machineCapLabels.push_back(labc);
      machineAgeLabels.push_back(labj);
   }

    if(machinenameLabels.size()==0){
        machinenameLabels.push_back(new QLabel("keine"));
        machineLabels.push_back(new QLabel());
        machineCapLabels.push_back(new QLabel());
        machineAgeLabels.push_back(new QLabel());
    }

    QGridLayout *lout = machineLayout;
    machineLayout = new QGridLayout;

    for (int i=0;i<machinenameLabels.size();++i){
        machineLayout->addWidget(machinenameLabels[i],i,0);
        machineLayout->addWidget(machineLabels[i],i,1);
        machineLayout->addWidget(machineCapLabels[i],i,2);
        machineLayout->addWidget(machineAgeLabels[i],i,3);
    }

    QGroupBox *gb = machineGroupBox;
    machineGroupBox = new QGroupBox;
    machineGroupBox->setLayout(machineLayout);
    machineGroupBox->setTitle(tr("Maschinen (Anzahl, Kapazit�t, Alter)"));
    if (lout) delete lout;
    if (gb) delete gb;
}

void Landwindow::getMachines(RegFarmInfo *farm){
    machinenameLabels.clear();
    machineLabels.clear();
    machineCapLabels.clear();
    machineAgeLabels.clear();

    RegInvestList invList = *(farm->FarmInvestList);
    vector<RegInvestObjectInfo> invs = invList.getInvests();
    int sz = invs.size();
    for(int i=0;i<sz;++i){
      int x = 1;
      RegInvestObjectInfo invobj = invs[i];
      int t = invobj.getAffectsProductGroup();
      if (t!=0) continue; //nur Maschinen

      QString name = invobj.getName().c_str();
      int cap = invobj.getCapacity();
      double age = invobj.getInvestAge();

      QLabel* lab= new QLabel();
      lab->setAlignment(Qt::AlignRight);
      lab->setText(name);

      QLabel* labn= new QLabel();
      labn->setAlignment(Qt::AlignRight);
      labn->setText(QString::number(x));

      QLabel* labc= new QLabel();
      labc->setAlignment(Qt::AlignRight);
      labc->setText(QString::number(cap,'f',0));

      QLabel* labj= new QLabel();
      labj->setAlignment(Qt::AlignRight);
      labj->setText(QString::number(age,'f',0)+ " J.");

      machinenameLabels.push_back(lab);
      machineLabels.push_back(labn);
      machineCapLabels.push_back(labc);
      machineAgeLabels.push_back(labj);
   }

    if(machinenameLabels.size()==0){
        machinenameLabels.push_back(new QLabel("keine"));
        machineLabels.push_back(new QLabel());
        machineCapLabels.push_back(new QLabel());
        machineAgeLabels.push_back(new QLabel());
    }

    QGridLayout *lout = machineLayout;
    machineLayout = new QGridLayout;

    for (int i=0;i<machinenameLabels.size();++i){
        machineLayout->addWidget(machinenameLabels[i],i,0);
        machineLayout->addWidget(machineLabels[i],i,1);
        machineLayout->addWidget(machineCapLabels[i],i,2);
        machineLayout->addWidget(machineAgeLabels[i],i,3);
    }

    QGroupBox *gb = machineGroupBox;
    machineGroupBox = new QGroupBox;
    machineGroupBox->setLayout(machineLayout);
    machineGroupBox->setTitle(tr("Maschinen (Anzahl, Kapazit�t, Alter)"));
    if (lout) delete lout;
    if (gb) delete gb;
}

void Landwindow::getBuildings(RegFarmInfo *farm){
    buildingnameLabels.clear();
    buildingCapLabels.clear();
    buildingLabels.clear();
    buildingAgeLabels.clear();

    RegInvestList invList = *(farm->FarmInvestList);
    for(int i=0;i<nInvests;++i){
      int x =farm->getInvestmentsOfCatalogNumber(i);
      if (x<=0) continue;
      RegInvestObjectInfo invobj = Manager->InvestCatalog[i];
      int t = invobj.getAffectsProductGroup();
      if (t<=0) continue; //nur St�lle

      QString name = invobj.getName().c_str();
      int cap = invobj.getCapacity();
      double age = invList.getAverageAgeOfInvestmentsOfCatalogNumber(i);

      QLabel* lab= new QLabel();
      lab->setText(name);

      QLabel* labn= new QLabel();
      labn->setText(QString::number(x));

      QLabel* labc= new QLabel();
      labc->setText(QString::number(cap,'f',2));

      QLabel* labj= new QLabel();
      labj->setText(QString::number(age,'f',1));

      buildingnameLabels.push_back(lab);
      buildingLabels.push_back(labn);
      buildingCapLabels.push_back(labc);
      buildingAgeLabels.push_back(labj);
   }

    if(buildingnameLabels.size()==0){
        buildingnameLabels.push_back(new QLabel("keine"));
        buildingLabels.push_back(new QLabel());
        buildingCapLabels.push_back(new QLabel());
        buildingAgeLabels.push_back(new QLabel());
    }


    if(buildingnameLabels.size()==0){
        buildingnameLabels.push_back(new QLabel("keine"));
        buildingLabels.push_back(new QLabel());
        buildingCapLabels.push_back(new QLabel());
        buildingAgeLabels.push_back(new QLabel());
    }

    QGridLayout *lout = buildingLayout;
    buildingLayout = new QGridLayout;

    for (int i=0;i<buildingnameLabels.size();++i){
        buildingLayout->addWidget(buildingnameLabels[i],i,0);
        buildingLayout->addWidget(buildingCapLabels[i],i,1);
        buildingLayout->addWidget(buildingLabels[i],i,2);
        buildingLayout->addWidget(buildingAgeLabels[i],i,3);
    }

    QGroupBox *gb = buildingGroupBox;
    buildingGroupBox = new QGroupBox;
    buildingGroupBox->setLayout(buildingLayout);
    buildingGroupBox->setTitle(tr("Geb�ude"));
    if (lout) delete lout;
    if (gb) delete gb;
}

void Landwindow::getStallMachine0(RegFarmInfo *farm){
    investnameLabels.clear();
    investLabels.clear();
    investCapLabels.clear();
    investAgeLabels.clear();

    map <int, int> invtypCap;
    map <int, int> invtypProdtyp;
    map <int, int> invcatNum;

    for(int i=0;i<nInvests;++i){  
      invcatNum[i] = 0;
      if (int x =farm->getInvestmentsOfCatalogNumber(i)!=0){ // TODO nicht AK
        //if (x>1) cout << "x: "<< x << endl;

        //QLabel *lab = new QLabel();
        RegInvestObjectInfo invobj = Manager->InvestCatalog[i];

        invcatNum[i] = x;
        invtypCap[invobj.getInvestType()]=1;//
        //farm->getCapacityOfInvestType(i);//x*invobj.getCapacity();
        invtypProdtyp[invobj.getInvestType()]=invobj.getAffectsProductGroup();
      }
    }

   // RegInvestList invList = farm->FarmInvestList;
   // invList.getInvestmentsOfCatalogNumber();

     map<int,int>::iterator it= invtypCap.begin();

     while (it!=invtypCap.end()){
        stringstream is,is2,is3,is4;
        invtypCap[(*it).first]=farm->getCapacityOfType((*it).first);
        int t = invtypProdtyp[(*it).first];

        if (t<=0)   { //Machinen oder AK
            /*
            is << static_cast<int>((*it).second);
            machineLabel->setText(is.str().c_str());
            //*/
            it++;
            continue;
            //lab->setText(tr("Machinen"));
        }

        QLabel* lab= new QLabel();
        vector<string> ss;
        Manager->Market->getNamesOfGroup(t,ss);

        is2 << ss[0];
        lab->setText(is2.str().c_str());

        //QLineEdit *ed =new QLineEdit();
        QLabel * lab1 = new QLabel();
        lab1->setAlignment(Qt::AlignRight);

        is3<<farm->getUnitsProducedOfGroup1(invtypProdtyp[(*it).first]);
        lab1->setText(QString::number(farm->getUnitsProducedOfGroup1(invtypProdtyp[(*it).first]),'f',0));
            //is3.str().c_str());

        QLabel *lab3= new QLabel();

        is4<< static_cast<int>(farm->getUnitsProducedOfGroup1(invtypProdtyp[(*it).first]))
                 <<"  ("<< static_cast<int>((*it).second)<< ")";

        lab3->setAlignment(Qt::AlignRight);
        //lab3->setText(is4.str().c_str());
        lab3->setText(QString::number((*it).second));

        investnameLabels.push_back(lab);
        investLabels.push_back(lab1);
        investCapLabels.push_back(lab3);

        QLabel *lab4 = new QLabel();
        lab4->setAlignment(Qt::AlignRight);
        double age = farm->getAverageAgeOfInvestmentsOfProdtype(t);
        lab4->setText(QString::number(age,'f', 1)+" J.");
        investAgeLabels.push_back(lab4);

        it++;
      }

    if(investnameLabels.size()==0){
        investnameLabels.push_back(new QLabel("keine"));
        investLabels.push_back(new QLabel());
        investCapLabels.push_back(new QLabel());
        investAgeLabels.push_back(new QLabel());
    }

    QGridLayout *lout = investLayout;
    investLayout = new QGridLayout;

    for (int i=0;i<investnameLabels.size();++i){
        investLayout->addWidget(investnameLabels[i],i,0);
        investLayout->addWidget(investLabels[i],i,1);
        investLayout->addWidget(investCapLabels[i],i,2);
        investLayout->addWidget(investAgeLabels[i],i,3);
    }

    QGroupBox *gb = investGroupBox;
    investGroupBox = new QGroupBox;
    investGroupBox->setLayout(investLayout);
    investGroupBox->setTitle(tr("Tiere  (Anzahl, Stallkapazit�t, Alter)"));
    if (lout) delete lout;
    if (gb) delete gb;
}

static bool invcompare(RegInvestObjectInfo inv1, RegInvestObjectInfo inv2){
    int t1, t2, c1,c2;
    t1=inv1.getAffectsProductGroup();
    t2=inv2.getAffectsProductGroup();
    c1= inv1.getCapacity();
    c2=inv2.getCapacity();
    if (t1<t2) return true;
    else if (t1>t2) return false;
    else return (c1>=c2);
}

void Landwindow::getStallMachine(RegFarmInfo *farm){
    investnameLabels.clear();
    investLabels.clear();
    investCapLabels.clear();
    investAgeLabels.clear();

    RegInvestList invList = *(farm->FarmInvestList);
    vector<RegInvestObjectInfo> invs = invList.getInvests();
    sort(invs.begin(),invs.end(),invcompare);

    map<int,int> rests;
    map<int,bool> assigneds;

    int sz = invs.size();
    for(int i=0;i<sz;++i){
      RegInvestObjectInfo invobj = invs[i];

      int t = invobj.getAffectsProductGroup();
      if (t<=0)   { //Machinen oder AK
            continue;
      }
      string aname = invobj.getName();
      std::transform(aname.begin(),aname.end(),aname.begin(),(int(*) (int))std::toupper);
      if (aname.find("BIOGAS")!=string::npos) continue; //nicht Tiere

      QLabel *lab = new QLabel();
      lab->setText(invobj.getName().c_str());
      int capacity = invobj.getCapacity();

      if (assigneds.find(t)==assigneds.end()) {
        rests[t] = farm->getUnitsProducedOfGroup2(t);
        assigneds[t] = true;
      }
      int used = rests[t]>=capacity ? capacity: rests[t];
      rests[t] = rests[t]>=capacity ? rests[t]-capacity : 0;

      int num = 1;
      int age = invobj.getInvestAge();
      int life = invobj.getEconomicLife();

      QLabel * lab1 = new QLabel();
      lab1->setAlignment(Qt::AlignRight);
      lab1->setText(QString::number(used));

      QLabel *lab3= new QLabel();
      stringstream is4;
      is4<< used <<" ("<< capacity << ")";

      lab3->setAlignment(Qt::AlignRight);
      //lab3->setText(is4.str().c_str());
      lab3->setText(QString::number(capacity));

        investnameLabels.push_back(lab);
        investLabels.push_back(lab1);
        investCapLabels.push_back(lab3);


        QLabel *lab4 = new QLabel();
        lab4->setAlignment(Qt::AlignRight);
        lab4->setText(QString::number(age,'f', 0)+" J.");
        investAgeLabels.push_back(lab4);
    }

    if(investnameLabels.size()==0){
        investnameLabels.push_back(new QLabel("keine"));
        investLabels.push_back(new QLabel());
        investCapLabels.push_back(new QLabel());
        investAgeLabels.push_back(new QLabel());
    }

    QGridLayout *lout = investLayout;
    investLayout = new QGridLayout;

    for (int i=0;i<investnameLabels.size();++i){
        investLayout->addWidget(investnameLabels[i],i,0);
        investLayout->addWidget(investLabels[i],i,1);
        investLayout->addWidget(investCapLabels[i],i,2);
        investLayout->addWidget(investAgeLabels[i],i,3);
    }

    QGroupBox *gb = investGroupBox;
    investGroupBox = new QGroupBox;
    investGroupBox->setLayout(investLayout);
    investGroupBox->setTitle(tr("Tiere  (Anzahl, Stallkapazit�t, Alter)"));
    if (lout) delete lout;
    if (gb) delete gb;
}

void Landwindow::updateModus(int modus, int farmid){
    if (hasProxy){
        scene->removeItem(proxyWidget);
        hasProxy=false; //inScene
    }
    if (hasProxyRent){
        scene->removeItem(proxyWidgetRent);
        hasProxyRent=false; //inScene
    }
    //*/

    setModus(modus);
    setChosenFarmid(farmid);
    updateGraphic();
}

void Landwindow::setModus(int m){
   modus = m;
}

void Landwindow::setChosenFarmid(int m){
   chosenFarmid = m;
}

static double calculateDistance(int c1, int r1, int c2, int r2) {//const RegPlotInfo* P) {
    double d;
    double dx = min(abs(c1-c2),(gg->NO_COLS - abs(c1 - c2)));
        double dy = min(abs((r1 - r2)),(gg->NO_ROWS - abs(r1 - r2)));
        d = sqrt((dx * dx) + (dy * dy));
        return d;
}

static double calculateDistanceCosts(double d) {
    double dist=sqrt(gg->PLOT_SIZE)/10;
    return dist*gg->TRANSPORT_COSTS*d/gg->PLOT_SIZE;
}

void Landwindow::updateRent(int r, int c, bool own){
    RegPlotInfo* rp = Manager->Region->getPlot(r,c);
    double rent=rp->getRentPaid()/gg->PLOT_SIZE;
    int id = rp->getFarmId();

    QString str;
    if (own) str = QString(" Feld: (%1, %2), Eigenumfl�che von Betrieb %3").arg(QString::number(r)).arg(QString::number(c)).arg(QString::number(id));
    else str = QString(" Feld: (%1, %2), Pachtpreis: %3 ").arg(QString::number(r)).arg(QString::number(c)).arg(QLocale().toString(rent,'f',2))+QChar(0x20ac)+"/ha";
    labRent->setText(str);
    labRent->resize(labRent->sizeHint());


    if(hasProxy){
        scene->removeItem(proxyWidget);
        hasProxy=false;
    }

    if (firstProxyRent){
        proxyWidgetRent=scene->addWidget(labRent);
        hasProxyRent=true;
        firstProxyRent=false;
    }
    else if(!hasProxyRent) {
        scene->addItem(proxyWidgetRent);
        hasProxyRent=true;
    }

    int w=PlotItem::w;
    int h=PlotItem::h;

    //int R = gg->NO_ROWS;
    int C = gg->NO_COLS;

    if (c>C-24) c -= 24;
    if (r<2) r+= 2;

    proxyWidgetRent->setPos(c*w+w, r*h-h);
    scene->update();
}

void Landwindow::updateTC(int r, int c){
    int c0 = Manager->Farm0->farm_plot->PA.col;
    int r0 = Manager->Farm0->farm_plot->PA.row;
    double d = calculateDistance(c0,r0,c,r);
    tc = calculateDistanceCosts(d);
    //tc = r*2 + c;

    QString str = QString(" Feld: (%1, %2), Transportkosten: %3 ").arg(QString::number(r)).arg(QString::number(c)).arg(QLocale().toString(tc,'f',2))+QChar(0x20ac)+"/ha  ";
    labTc->setText(str);
    labTc->resize(labTc->sizeHint());

    if(hasProxyRent){
        scene->removeItem(proxyWidgetRent);
        hasProxyRent=false;
    }

    if (firstProxy){
        proxyWidget=scene->addWidget(labTc);
        hasProxy=true;
        firstProxy=false;
    }
    else if(!hasProxy) {
        scene->addItem(proxyWidget);
        hasProxy=true;
    }

    int w=PlotItem::w;
    int h=PlotItem::h;

    //int R = gg->NO_ROWS;
    int C = gg->NO_COLS;

    if (c>C-26) c -= 26;
    if (r<2) r+= 2;

    proxyWidget->setPos(c*w+w, r*h-h);
    scene->update();
}
