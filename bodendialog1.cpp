#include "bodendialog1.h"
#include "AgriPoliS.h"

#include <QLineEdit>
#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QGroupBox>
#include <QScrollArea>
#include <QComboBox>
#include <QSpinBox>
#include <QSignalMapper>

#include "spreadsheet2.h"
#include "cell2.h"

#include "Mainwindow.h"

#include <qaction.h>
#include "pe.h"

static QFont font0 = QFont("SansSerif",14, QFont::Bold);
static QFont font1 = QFont("Times",11, QFont::Bold);
static QFont font2 = QFont("Times",10, QFont::Normal);//:Bold);

BodenDialog1::BodenDialog1(Mainwindow* par):prozent(0),inited(false), firstGUI(true), parent(par), QDialog(){
    setWindowTitle(tr("Bodenmarkt"));
    cellBColor = Qt::lightGray;

    BidIsValid = true;
    toClose=false;
    hasPEwidget=false;
    setWindowIcon(QPixmap(":/images/grafiken/Kuh.png"));
    //setWindowFlags(Qt::WindowStaysOnTopHint);
}

bool BodenDialog1::isInited(){
    return !firstGUI;
}

void BodenDialog1::setShowBSpiegel(bool b){
    showBSpiegel= b;
}

void BodenDialog1::updateGUI(){
  if (!firstGUI) {
        freeAreas = farm0->calFreeAreas(Manager);
        ergAreas = farm0->calNewlyRented();
        updateGUI2();
        return;
   }

    //farm0 = Manager->Farm0;
    //ITER = gg->tIter;
    //soilNames = gg->NAMES_OF_SOIL_TYPES;
    //nsoils = gg->NO_OF_SOIL_TYPES;
    vorschlags_of_type.resize(nsoils);
    maxRows.resize(nsoils);
    freeAreas = farm0->calFreeAreas(Manager);
    ergAreas = farm0->calNewlyRented();

    QString title("Bodenmarkt");// f�r freie Fl�chen");
    prozentStr = QString("%1\% der freien Fl�chen sind vergeben").arg(prozent);

    Title22 *titleLab = new Title22(Qt::AlignCenter, font0);
    titleLab->setText(title);

    prozentLab = new Title22(Qt::AlignCenter, font1, this);
    prozentLab->setText(prozentStr);

    prozentLab1 = new Title22(Qt::AlignCenter, font1);
    prozentLab1->setText("Ihre Fl�chenausstattung nach dieser Periode");
    prozentLab1->setVisible(false);

    QVBoxLayout *v0 = new QVBoxLayout; //out

    QVBoxLayout *vl = new QVBoxLayout;
    v0->addWidget(titleLab);

    v0->addWidget(prozentLab);
    v0->addWidget(prozentLab1);
    /*if (prozent>0) prozentLab->show();
    else prozentLab->hide();
    //*/

    landinputs.resize(nsoils);
    for (int i=0; i<nsoils; ++i){
        landinputs[i] = farm0->getLandInputOfType(i);
    }

    ergWidget = new QWidget;
    ergLab = new Title22(Qt::AlignCenter, font2, ergWidget);
    QString ergStr("Fl�chenstatus");
    ergLab->setStyleSheet("QLabel {color : blue}");
    ergLab->setText(ergStr);

    int nRowErg = 3;
    int nColErg = nsoils+1;
    vector<QString> tv;
    tv.push_back("");
    for (int i=0; i<nsoils; ++i)  {
            tv.push_back(gg->NAMES_OF_SOIL_TYPES[i].c_str());
            ergAreas.push_back(i*10); //ToDo
    }
    ergSS = new SpreadSheet2(nRowErg, nColErg, false, tv, false, false, false, ergWidget);
    ergSS->setFocusPolicy(Qt::NoFocus);//0

    vector<QString> tvv ;
    tvv.push_back("neu gepachtet (ha)");
    tvv.push_back("insgesamt (ha)");
    for (int i=0; i<nColErg; ++i)  {
        ergSS->cell(0,i)->setAlign(Qt::AlignCenter, Qt::AlignCenter);
        ergSS->cell(0,i)->setFormula(tv[i]);

        if (i!=0) {
            ergSS->cell(1,i)->setAlign(Qt::AlignCenter, Qt::AlignCenter);
            ergSS->cell(1,i)->setFormula(QLocale().toString(ergAreas[i-1]));
            ergSS->cell(2,i)->setAlign(Qt::AlignCenter, Qt::AlignCenter);
            ergSS->cell(2,i)->setFormula(QLocale().toString(landinputs[i-1]));
        }
    }
    for (int i=1; i<3; ++i){
        ergSS->cell(i,0)->setAlign(Qt::AlignLeft, Qt::AlignCenter);
        ergSS->cell(i,0)->setFormula(tvv[i-1]);
    }

    QHBoxLayout *eHL = new QHBoxLayout;
    eHL->addStretch();
    eHL->addWidget(ergSS);
    eHL->addStretch();

    QVBoxLayout *eVL = new QVBoxLayout;
    eVL->addWidget(ergLab);
    eVL->addLayout(eHL);

    ergWidget->setLayout(eVL);

    v0->addWidget(ergWidget);
    /*if (prozent>0) ergWidget->show();
    else ergWidget->hide();
    //*/

    //v0->addStretch(1);

    neuBidsButton = new QPushButton("neue Gebote");
    weiterButton = new QPushButton("mit alten Geboten weiter");

    weiterWidget  = new QWidget;
    QHBoxLayout *hl0 = new QHBoxLayout;
    hl0->addStretch();
    hl0->addWidget(neuBidsButton);
    hl0->addWidget(weiterButton);
    hl0->addStretch();

    weiterWidget->setLayout(hl0);
    v0->addWidget(weiterWidget);


    weiterZbutton = new QPushButton("weiter");

    weiterWidget1  = new QWidget;
    QHBoxLayout *hl01 = new QHBoxLayout;
    hl01->addStretch();
    hl01->addWidget(weiterZbutton);
    hl01->addStretch();

    weiterWidget1->setLayout(hl01);
    weiterWidget1->setVisible(false);

    v0->addWidget(weiterWidget1);
    //v0->addStretch(1);

    Title22 *mlab = new Title22(Qt::AlignCenter, font1);
    mlab->setText("Ihre Gebote");
    vl->addWidget(mlab);

    bidsWidget = new QWidget;

    connect(neuBidsButton, SIGNAL(clicked()), this, SLOT(neueGebote()));//bidsWidget, SLOT(show()));
    connect(weiterButton, SIGNAL(clicked()), this, SLOT(weiter()));
    connect(weiterZbutton,SIGNAL(clicked()),this,SLOT(weiter1()));

    //connect(this, SIGNAL(rejected()), this, SLOT(weiter()));

    vector<QString> hHeader;
    hHeader.push_back(QString("Fl�chen (ha) \n von -> bis "));
    hHeader.push_back(QString("Gebot-\nvorschlag(")+QChar(8364)+")");
    hHeader.push_back(QString("Gebot (")+ QChar(8364)+")");

    nRowBid = 4;//5;
    nColBid = 3;// mit Vorschlag ;

    QHBoxLayout *hl = new QHBoxLayout;
    hl->addStretch();

    vector<int> defaultAreas;
    vector<double> defaultBids;
    defaultAreas.push_back(10);
    defaultAreas.push_back(20);
    defaultAreas.push_back(50);
    //defaultAreas.push_back(100);
    defaultAreas.push_back(0);

    for (int i=0; i<nRowBid; ++i){
        defaultBids.push_back(500);
    }

    QSignalMapper *sigmapBids = new QSignalMapper;

    QSignalMapper *sigmapVButtons = new QSignalMapper;
    vector<int> areas;
    for (int i=0; i<nsoils; ++i){
        areas.clear();
        //double maxA = freeAreas[i];
        QVBoxLayout *vl2 = new QVBoxLayout;

        QLabel *lab = new QLabel(soilNames[i].c_str());
        lab->setAlignment(Qt::AlignCenter);

        QString listr = QLocale().toString(landinputs[i])+QString(" ha");
        QLabel *labli = new QLabel(listr);
        labLandinputs.push_back(labli);
        QLabel *labsq1 = new QLabel("status quo : ");

        QHBoxLayout *hl2 = new QHBoxLayout;
        hl2->addWidget(labsq1);
        hl2->addWidget(labli);
        hl2->setAlignment(Qt::AlignCenter);

        SpreadSheet2 *s = new SpreadSheet2(nRowBid, nColBid, true, hHeader, false);

       for (int k=0; k<4;++k){
           s->cell(k,1)->setAlign(Qt::AlignCenter, Qt::AlignCenter);
       }

        //makeEditable(s);
        s->setArt(0);
        gebotstables.push_back(s);

        vector<QSpinBox*> vb0;
        vector<QDoubleSpinBox*> vb1;
        int last = 0;
        double delta = gg->PLOT_SIZE;
        for (int k=0; k<nRowBid; ++k){
            QSpinBox *dsb0 = new QSpinBox;
            double rlast = last==0 ? 0 : last + delta;
            QString preStr = QLocale().toString(rlast,'f',0)+" -> ";

            int as = defaultAreas[k];
            if (k==nRowBid-1)
                as = 500;//maxA;

            areas.push_back(as);

            dsb0->setMinimum(delta);
            dsb0->setMaximum(500);//maxA);
            dsb0->setValue(as);//defaultAreas[k]);
            last = as;
            s->setCellWidget(k,0, dsb0);
            dsb0->setAlignment(Qt::AlignRight);
            dsb0->setPrefix(preStr);
            dsb0->setSuffix("  ");
            dsb0->setSingleStep(delta);
            if (k==nRowBid-1) dsb0->setEnabled(false);
            connect(dsb0,SIGNAL(valueChanged(int)), sigmapBids, SLOT(map()));
            sigmapBids->setMapping(dsb0, 2*i*nRowBid+k );

            QDoubleSpinBox *dsb1 = new QDoubleSpinBox;

            dsb1->setMinimum(0);
            dsb1->setMaximum(3000);

            dsb1->setValue(defaultBids[k]);
            dsb1->setAlignment(Qt::AlignRight);
            dsb1->setSuffix("  ");
            s->setCellWidget(k,2, dsb1);

            connect(dsb1,SIGNAL(valueChanged(double)), sigmapBids, SLOT(map()));
            sigmapBids->setMapping(dsb1, 2*i*nRowBid+k );


            vb0.push_back(dsb0);
            vb1.push_back(dsb1);
        }
        s->adjSize();
        areaDSBs_of_type.push_back(vb0);
        bidDSBs_of_type.push_back(vb1);

        farm0->setAreasOfType(i,areas);


        //s->updateColumn(0, defaultAreas);
        //s->updateColumn(1, defaultBids);
        bidSheets.push_back(s);

        vl2->addWidget(lab);
        vl2->addLayout(hl2);
        vl2->addSpacing(5);
        vl2->addWidget(s);

        QPushButton *vButton = new QPushButton("Vorschlag �bernehmen");

        QHBoxLayout *vblout = new QHBoxLayout;
        vblout->addStretch();
        vblout->addWidget(vButton);
        vblout->addStretch();
        vl2->addLayout(vblout);

        vButtons.push_back(vButton);
        connect(vButton, SIGNAL(clicked()),sigmapVButtons, SLOT(map()));
        sigmapVButtons->setMapping(vButton, i );
        hl->addLayout(vl2);
    }
    farm0->calGDBs2B();

    connect(sigmapBids,SIGNAL(mapped(int)), this, SLOT(validBidDSB(int)));
    connect(sigmapBids,SIGNAL(mapped(int)), this, SLOT(updateArea2B(int)));
    connect(sigmapVButtons,SIGNAL(mapped(int)), this, SLOT(updateVorschlags(int)));

    hl->addStretch();
    vl->addLayout(hl);

    vl->addStretch();

    QLabel *anmerkLab = new QLabel("Bitte geben Sie Ihren Schattenpreis ein."
                    " Die Transportkosten werden automatisch vom Programm ber�cksichtigt.");
    anmerkLab->setStyleSheet("QLabel{color: blue}");
    vl->addWidget(anmerkLab);

    QHBoxLayout *hl2 = new QHBoxLayout;

    QPushButton *abgebenButton = new QPushButton("Gebot abgeben");
    connect(abgebenButton,SIGNAL(clicked()), this, SLOT(getBids()));
    //QPushButton *vorschlagButton = new QPushButton("Vorschlag �bernehmen");
    //connect(vorschlagButton,SIGNAL(clicked()), this, SLOT(getVorschlag()));

    hl2->addStretch();
    //hl2->addWidget(vorschlagButton);
    //hl2->addSpacing(20);
    hl2->addWidget(abgebenButton);
    hl2->addStretch();

    vl->addLayout(hl2);

    vl->addStretch(1);
    vl->addSpacing(20);

    QWidget *freeAreaWidget = new QWidget;
    Title22 *faTit = new Title22(Qt::AlignCenter, font1);
    faTit->setText("Freie Fl�chen (ha)");

    radSBox = new QSpinBox;
    radSBox->setAlignment(Qt::AlignLeft);//Right);
    radSBox->setPrefix("im Radius von  ");
    radSBox->setSuffix("  km");
    int max =(int) (1.5*gg->NO_COLS*sqrt(gg->PLOT_SIZE)*100 )/ 1000 +1;//1.5>sqrt(2)
    //max *= 1000;
    radSBox->setMaximum(max/2+1); //spieler in der Mitte
    radSBox->setMinimum(0);
    radSBox->setMinimumWidth(150);//90);
    int init_dist  = 2; //000;
    radSBox->setValue(init_dist);
    radSBox->adjustSize();

    connect(radSBox, SIGNAL(valueChanged(int)), this, SLOT(updateFreeArea(int)));

    //vector<QString> tv;
    freeAreaSS = new SpreadSheet2(3,nsoils+1,false,tv,false);
    freeAreaSS->cell(1,0)->setFormula("insgesamt");
    freeAreaSS->setCellWidget(2,0,radSBox);
    //vector<double> freeAreas, radFreeAreas;
    /*freeAreas.push_back(500); // ??
    freeAreas.push_back(350); // ??
    radFreeAreas.push_back(100);
    radFreeAreas.push_back(50);
    //*/
    radFreeAreas = farm0->calRadFreeAreas(init_dist, Manager);

    for (int i=0; i<nsoils; ++i){
        freeAreaSS->cell(0,i+1)->setAlign(Qt::AlignCenter, Qt::AlignCenter);
        freeAreaSS->cell(1,i+1)->setAlign(Qt::AlignCenter, Qt::AlignCenter);
        freeAreaSS->cell(2,i+1)->setAlign(Qt::AlignCenter, Qt::AlignCenter);
        freeAreaSS->cell(0,i+1)->setFormula(gg->NAMES_OF_SOIL_TYPES[i].c_str());
        freeAreaSS->cell(1,i+1)->setFormula(QLocale().toString(freeAreas[i]));
        freeAreaSS->cell(2,i+1)->setFormula(QLocale().toString(radFreeAreas[i]));
        areaDSBs_of_type[i][nRowBid-1]->setValue(freeAreas[i]>=500?freeAreas[i]:500);
    }

    {
        double delta = gg->PLOT_SIZE;
        QSpinBox *dsb0;
        int ma, mi;
    for (int x=areaDSBs_of_type.size()-1; x>=0; --x){
        int s = x / (2*nRowBid);
        int k = x % (2*nRowBid);

        if (k>=nRowBid) continue;
        dsb0 = areaDSBs_of_type[s][k];
        if (k==nRowBid-1){     //areas
            mi = areaDSBs_of_type[s][k-1]->value()+delta;
            dsb0->setMinimum(mi);
        }else if (k==0){
            ma = areaDSBs_of_type[s][k+1]->value()-delta;
            dsb0->setMaximum(ma);
        }else {
            ma = areaDSBs_of_type[s][k+1]->value()-delta;
            mi = areaDSBs_of_type[s][k-1]->value()+delta;
            dsb0->setMaximum(ma);
            dsb0->setMinimum(mi);
        }
        }
    }


    QHBoxLayout *faHL = new QHBoxLayout;
    faHL->addStretch();
    faHL->addWidget(freeAreaSS);
    faHL->addStretch();

    QVBoxLayout *faVL = new QVBoxLayout;
    faVL->addWidget(faTit);
    faVL->addLayout(faHL);

    freeAreaWidget->setLayout(faVL);
    vl->addWidget(freeAreaWidget);

    vl->addSpacing(25);
    QHBoxLayout *hl3 = new QHBoxLayout;
    //hl3->addStretch();
    hilfeButton = new QPushButton("weitere Hilfe zur Entscheidung");
    hl3->addWidget(hilfeButton);
    hl3->addStretch();
    vl->addLayout(hl3);

    vl->addSpacing(20);
    vl->addStretch();
    QLabel *remLab = new QLabel("Sie k�nnen die aktuelle Fl�chen�bersicht in Betriebsspiegel ansehen.");
    remLab->setAlignment(Qt::AlignLeft);
    remLab->setStyleSheet("color: blue");
    vl->addWidget(remLab);

    QHBoxLayout *hl3a = new QHBoxLayout;
    if (showBSpiegel)
        spiegelButton = new QPushButton("Betriebsspiegel einblenden");//akt. Fl�chen�bersicht einblenden");
    else
        spiegelButton = new QPushButton("Betriebsspiegel ausblenden");//akt. Fl�chen�bersicht ausblenden");
    hl3a->addWidget(spiegelButton);
    hl3a->addStretch();
    connect(spiegelButton, SIGNAL(clicked()), this, SLOT(blendenBSpiegel()));
    vl->addLayout(hl3a);


    dialBodenHilfe = new QDialog();
    dialBodenHilfe->setWindowTitle("GDB und Transportkosten f�r angepa�te Fl�chen/Preiserwartung");

    connect(hilfeButton, SIGNAL(clicked()), this, SLOT(showHilfe()));
    connect(dialBodenHilfe, SIGNAL(rejected()), this, SLOT(hilfeClosed()));

    //makePeWidget();
    makeAreaWidget();

    updateVorschlags();
    for (int i=0; i<nsoils;++i){
        updateVorschlags(i);
    }

/*
    QHBoxLayout *hl4 = new QHBoxLayout;
    hl4->addWidget(peWidget);
    hl4->addStretch();
    hl4->addWidget(areaWidget);
    //hilfeGBox->setLayout(hl4);
    dialBodenHilfe->setLayout(hl4);
//*/

    QVBoxLayout *vl4 = new QVBoxLayout;
    vl4->addWidget(areaWidget);
    vl4->setSizeConstraint(QLayout::SetFixedSize);
    //vl4->addStretch();
    //vl4->addWidget(peWidget);
    dialBodenHilfe->setLayout(vl4);
    //vl->addWidget(hilfeGBox);

    bidsWidget->setLayout(vl);

    v0->addWidget(bidsWidget);
    v0->setSizeConstraint(QLayout::SetFixedSize);

    setLayout(v0);
    firstGUI = false;
    //resize(400,500);
}

void BodenDialog1::updateFreeArea(int dist){
    vector<int>  ds = farm0->calRadFreeAreas(dist,Manager);
    for (int i=0; i<nsoils; ++i){
        freeAreaSS->cell(2,i+1)->setFormula(QLocale().toString(ds[i]));
    }
}

void BodenDialog1::validBidDSB(int x){
    if (!inited) return;
    int s = x / (2*nRowBid);
    int k = x % (2*nRowBid);

    QSpinBox *dsb0, *dsb1 ;
    double delta = gg->PLOT_SIZE;
    int wert= areaDSBs_of_type[s][k]->value();
    if (k<nRowBid){     //areas
        if (k==0){
            dsb1 = areaDSBs_of_type[s][k+1];
            dsb1->setMinimum(wert+delta);
        }else if (k==nRowBid-1) {
            dsb0 = areaDSBs_of_type[s][k-1];
            dsb0->setMaximum(wert-delta);
        }else {
            dsb1 = areaDSBs_of_type[s][k+1];
            dsb1->setMinimum(wert+delta);
            dsb0 = areaDSBs_of_type[s][k-1];
            dsb0->setMaximum(wert-delta);
        }
        if (k<nRowBid-1){
            double rlast = wert + gg->PLOT_SIZE;
            QString preStr = QLocale().toString(rlast,'f',0)+" -> ";
            areaDSBs_of_type[s][k+1]->setPrefix(preStr);
        }
   }
}

void BodenDialog1::validBidDSB0(int x){
    int s = x / (2*nRowBid);
    int k = x % (2*nRowBid);
    bidSheets[s]->adjSize();
    bool toWarnen = false;
    if (k<nRowBid){     //areas

        if  (k==0) {
            int v1 = areaDSBs_of_type[s][k]->value();
            int v2 = areaDSBs_of_type[s][k+1]->value();
            if ((v1>=v2 && v2!=0)|| v1==0)
                toWarnen = true;
        }else if(k==nRowBid-1) {
            int v0 = areaDSBs_of_type[s][k-1]->value();
            int v1 = areaDSBs_of_type[s][k]->value();
            if ((v0>=v1 && v1!=0)||(v1!=0 && v0==0))
                 toWarnen = true;
        }else {
            int v0 = areaDSBs_of_type[s][k-1]->value();
            int v1 = areaDSBs_of_type[s][k]->value();
            int v2 = areaDSBs_of_type[s][k+1]->value();
            if ((v0>=v1 && v1!=0) ||(v0==0 && v1!=0) || (v1>=v2 && v2!=0) || (v1==0 && v2!=0))
                  toWarnen = true;
        }

        if (toWarnen) {
            BidIsValid  = false;
            QMessageBox::warning(this, QString("Problem beim Bieten"),
                    QString("Fl�chen f�r Bodentyp %1 m�ssen aufsteigend angegeben werden").arg(soilNames[s].c_str()));
        }
        else if (k<nRowBid-1){
            int last = areaDSBs_of_type[s][k]->value();
            double rlast = last + gg->PLOT_SIZE;
            QString preStr = QLocale().toString(rlast,'f',0)+"->";
            areaDSBs_of_type[s][k+1]->setPrefix(preStr);
        }
    }
}

void BodenDialog1::updateGUI2(){
    toClose=false;
    ITER = gg->tIter;
    prozentStr = QString("%1\% der freien Fl�chen sind vergeben").arg(prozent);
    prozentLab->setText(prozentStr);

    //aktualisiertung status Quo
    for (int i=0; i<nsoils; ++i){
        landinputs[i] = farm0->getLandInputOfType(i);
        QString listr = QLocale().toString(landinputs[i])+QString(" ha");
        labLandinputs[i]->setText(listr);
    }

    /*if (prozent>0) prozentLab->show();
    else prozentLab->hide();
    //*/

    updateErg(); //Fl�chen aktualisieren

    /*if (prozent>0) ergWidget->show();//setVisible(true);//show();!!
    else ergWidget->hide();//setVisible(false);//>hide();
    //*/

    for (int i=0; i<nsoils; ++i)  {
        ergSS->cell(1,i+1)->setFormula(QLocale().toString(ergAreas[i]));
        ergSS->cell(2,i+1)->setFormula(QLocale().toString(landinputs[i]));
    }

    //aktualisieren der max. Fl�chen
    /*for (int i=0; i<nsoils; ++i)  {
        areaDSBs_of_type[i][nRowBid-1]->setValue(freeAreas[i]);
     }
     //*/

    radFreeAreas = farm0->calRadFreeAreas(radSBox->value(),Manager);
    for (int i=0; i<nsoils; ++i){
        freeAreaSS->cell(1,i+1)->setFormula(QLocale().toString(freeAreas[i]));
        freeAreaSS->cell(2,i+1)->setFormula(QLocale().toString(radFreeAreas[i]));
    }

    //makePeWidget2();
    makeAreaWidget2();
    updateVorschlags();
    for (int i=0;i<nsoils;++i){
        updateVorschlags(i);
    }

    if (showBSpiegel)
        spiegelButton->setText("Betriebsspiegel ausblenden");//akt. Fl�chen�bersicht ausblenden");
    else
        spiegelButton->setText("Betriebsspiegel einblenden");//akt. Fl�chen�bersicht einblenden");
}

void BodenDialog1::updateGUIz(){
    prozentLab1->setVisible(true);
    prozentLab->setVisible(false);

    //aktualisiertung status Quo
    for (int i=0; i<nsoils; ++i){
        landinputs[i] = farm0->getLandInputOfType(i);
        QString listr = QLocale().toString(landinputs[i])+QString(" ha");
        labLandinputs[i]->setText(listr);
    }

    for (int i=0; i<nsoils; ++i)  {
        ergSS->cell(1,i+1)->setFormula(QLocale().toString(ergAreas[i]));
        ergSS->cell(2,i+1)->setFormula(QLocale().toString(landinputs[i]));
    }

    weiterWidget->setVisible(false);
    weiterWidget1->setVisible(true);
    ergLab->setVisible(false);
    bidsWidget->setVisible(false);
    adjustSize();
}

void BodenDialog1::updateErg(){
    //ergAreas = farm0->getErgAreas();
}

void BodenDialog1::makeEditable(SpreadSheet2 *s){
    int r = s->rowCount();
    int c = s->columnCount();

    for (int i=0; i<r; ++i){
        for (int j=0; j<c; ++j){
            s->cell(i,j)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable);
        }
    }
}

void BodenDialog1::updatePEs(){
    if (!hasPEwidget){
        farm0 = Manager->Farm0;
        ITER=gg->tIter;
        soilNames = gg->NAMES_OF_SOIL_TYPES;
        nsoils = gg->NO_OF_SOIL_TYPES;
        makePeWidget();
        hasPEwidget=true;
    }else
        makePeWidget2();
}

void BodenDialog1::makePeWidget(){
   /* if(parent->getPeWidget()){
        peWidget=parent->getPeWidget();

        disconnect(peWidget, SIGNAL(peSig(int)), parent->closeDialog, SLOT(getPeSig(int)));
        disconnect(peWidget, SIGNAL(peVarSig(int)), parent->closeDialog, SLOT(getPeVarSig(int)));
        disconnect(parent->closeDialog, SIGNAL(peOK(int,bool)), peWidget, SLOT(getPeOK(int,bool)));

        connect(peWidget, SIGNAL(peSig(int)), this, SLOT(getPeSig(int)));
        connect(peWidget, SIGNAL(peVarSig(int)), this, SLOT(getPeVarSig(int)));
        connect(this, SIGNAL(peOK(int,bool)), peWidget, SLOT(getPeOK(int,bool)));
        return;
    }
    //*/

    vector<QString> vs;
    vs.push_back("Produktname");
    vs.push_back(QString("Einheit "));
    vs.push_back(QString("Preis "));
    vs.push_back(QString("")+QChar(0x0D8)+QString("Preis\nder Ref-Zeit"));
    vs.push_back(QString("PE\nvon Modell"));
    vs.push_back(QString("Meine PE"));
    vs.push_back(QString("Deckungsbeitrag"));
    vs.push_back(QString("Einheit von DB"));
    //vs.push_back(QString("Preisindex"));
    //*/

    int xcount = 0;
    vector<RegProductInfo> pcat = *(farm0->product_cat);
    int sz = pcat.size();
    bool refnameFound = false;
    vector<bool> peSpiels;
    for (int i=0; i<sz; ++i){
        //cout<<pcat[i].getName().c_str()<<"\t"<<pcat[i].getProductGroup()<<"\t"<<pcat[i].getPreisSchwankung()<<endl;
        if (!pcat[i].getPreisSchwankung()) continue;

        //if (pcat[i].getProductGroup()<0) continue;
        if (pcat[i].getProductGroup()== 0  && !refnameFound) {
            refname = QString(pcat[i].getName().c_str());
            refprodID = i;
            refpos = xcount;
            refnameFound = true;
            //cout<<"REFName:\t"<<refname.toStdString()<<endl;

        }else if (pcat[i].getProductGroup()==0){
            //cout<<pcat[i].getName().c_str()<<"\t"<<pcat[i].getProductGroup()<<endl;
            getreideIDs.push_back(i);
            getreidePEs.push_back(pcat[i].getPriceExpectation());
            getreidePrices.push_back(pcat[i].getPrice());
            continue;
        }else if (pcat[i].getProductGroup()==-1 && pcat[i].getClass().compare("GRASSLAND")!=0) // nur f. Altmark ??
             continue;

        ++xcount;

        if (parent->spielPeProds.size()==0) {
        string aName = pcat[i].getName();
        bool aFind=false;
        for (int ti=0;ti<gg->MyPeProducts.size();++ti){
            if (gg->MyPeProducts[ti]==aName) aFind = true;
        }
        peSpiels.push_back(aFind);
        }
        prodNames.push_back(QString(pcat[i].getName().c_str()));
        prodPrices.push_back(pcat[i].getPrice());
        prodCosts.push_back(pcat[i].getVarCost());
        prodIDs.push_back(i);
        prodPEs.push_back(pcat[i].getPriceExpectation());
        prodUnits.push_back(pcat[i].getUnit().c_str());
        prodGMunits.push_back(pcat[i].getGMunit().c_str());
        prodErtrags.push_back(pcat[i].getErtrag());
        prodNertrags.push_back(pcat[i].getNebenErtrag());
        //prodRefPEs.push_back(100);
    }

    if (parent->spielPeProds.size()==0)
        parent->setSpielPeProds(peSpiels);
    else
        peSpiels = parent->getSpielPeProds();

    prodRefPEs = prodPEs;
    prodRefPrices = prodPrices;
    naivPEs = prodPrices;
    adaptPEs = prodPEs;
    if (parent->getPeWidget()){
        manPEs = parent->closeDialog->getEffPEs();
        setPEs();
    }
    else manPEs = prodPEs;
    //cout<<"Boden:  manPEs.size() = "<<manPEs.size()<<endl;
    numPEs = prodIDs.size();

    if (ITER==1) //0-te Iteration kein LA
        allPrices.push_back(prodPrices);
    allPrices.push_back(prodPrices);
    //calRefPrices();

    peVar = 0;
    peWidget = new PEwidget(ITER, &peVar, refname, vs, prodNames, prodUnits, prodErtrags, prodNertrags,prodGMunits);
    peWidget->setPeSpiels(peSpiels);
    peWidget->updateCosts(prodCosts);
    peWidget->updateData(prodPrices,prodRefPEs, manPEs);//prodPEs);
    peWidget->updateData1(getreideIDs,getreidePrices, getreidePEs);
    peWidget->updateData2(refpos, &manPEs, adaptPEs, naivPEs);
    peWidget->updateData3(prodIDs, &allPrices);
    peWidget->updateRefs();
    peWidget->setPeInited(true);

    connect(peWidget, SIGNAL(peSig(int)), this, SLOT(getPeSig(int)));
    connect(peWidget, SIGNAL(peVarSig(int)), this, SLOT(getPeVarSig(int)));
    connect(this, SIGNAL(peOK(int,bool)), peWidget, SLOT(getPeOK(int,bool)));

    if (!parent->getPeWidget())
        parent->setPeWidget(peWidget);
}

void BodenDialog1::setPEs(){
    int n = prodIDs.size();
    for (int r=0;r<n;++r){
        if (r==refpos) {
            double tind;
            tind = prodPrices[refpos]==0? 1:manPEs[r]/prodPrices[refpos];
            getreidePEs.clear();
            int ts = getreideIDs.size();
            for (int i=0; i<ts; ++i){
               getreidePEs.push_back(Manager->product_cat0[getreideIDs[i]].getPriceExpectation());
            }
            for (int i=0; i<ts; ++i){
                Manager->product_cat0[getreideIDs[i]].setPriceExpectation(getreidePrices[i]*tind);
            }
         }
         Manager->product_cat0[prodIDs[r]].setPriceExpectation(manPEs[r]);
    }
}

void BodenDialog1::getPeSig(int row){
    emit peSig(row);
}

void BodenDialog1::getPeVarSig(int var){
    emit peVarSig(var);
}

void BodenDialog1::updateGDBs(int v){
    updateAreas();
}

void BodenDialog1::updateGDBs(){
    updateAreas();
}

vector<double> BodenDialog1::getManPEs() const{
//cout<<"Boden:return get  manPEs.size() = "<<manPEs.size()<<endl;
    return manPEs;
}

vector<double> BodenDialog1::getEffPEs() {
    //cout <<"proz0:\t"<< proz0<<endl;
    vector<double> res = it_myPEs[proz0];
    return res;
}

void BodenDialog1::makePeWidget2(){
    peWidget->updateIter(ITER);
    //refzeitSBox->setMaximum(ITER);

    vector<RegProductInfo> pcat = *(farm0->product_cat);
    int sz = numPEs;//pcat.size();

    for (int i=0; i<sz; ++i){
        prodPrices[i]=pcat[prodIDs[i]].getPrice();
        prodPEs[i]=pcat[prodIDs[i]].getPriceExpectation();
    }
    prodRefPEs = prodPEs;
    //prodRefPrices = prodPrices;
    naivPEs = prodPrices;
    adaptPEs = prodPEs;
    //manPEs = prodPEs;
    if (proz0==0) { //neue Periode
        manPEs = parent->closeDialog->getEffPEs();//>getManPEs();
        setPEs();
    }
    if(proz0==0)
        allPrices.push_back(prodPrices);
    //calRefPrices();

    vector<double> pes ;
    switch(peVar){
    case 0: pes=manPEs;  break;
    case 1: pes=naivPEs; break;
    case 2: pes=adaptPEs; break;
    default:pes=manPEs;
    }

    peWidget->updateData(prodPrices, prodRefPEs, pes, true);//prodPEs,true);
    peWidget->updateData1a(getreidePrices, getreidePEs);
    peWidget->updateRefs();
    peWidget->setPeInited(true);
}

void BodenDialog1::makeAreaWidget(){
    Title22* areaTitle = new Title22(Qt::AlignCenter, font0);
    areaTitle->setText("Fl�chenoptionen");

    /*QLabel *stype = new QLabel("Bodentyp");
    soiltypeCBox = new QComboBox;

    for (int i=0; i<nsoils; ++i){
        soiltypeCBox->addItem(soilNames[i].c_str());
    }


    QHBoxLayout *hl = new QHBoxLayout;
    hl->addStretch();
    hl->addWidget(stype);
    hl->addWidget(soiltypeCBox);
    hl->addStretch();

    connect(soiltypeCBox, SIGNAL(currentIndexChanged(int)), this, SLOT(updateSoilType(int)));
    //*/
    vector<QString> vs;
    for (int i=0; i<nsoils; ++i){
        vs.push_back(QString("Fl�chenzuwachs (ha) ")+ gg->NAMES_OF_SOIL_TYPES[i].c_str()); //zuzupachtende Fl�chen (ha)");
    }
    vs.push_back(QString("Gesamtdeckungsbeitrag(GDB) (")+QChar(0x20ac)+"/ha)");
    vs.push_back(QString("Schattenpreis (")+QChar(0x20ac)+ "/ha) (Modellvorschlag)");
    //: "+QChar(0x0394)+"GDB zu status quo");
    vs.push_back(QString("ca. ")+QChar(0x0394)+"Transportkosten ("+QChar(0x20ac)+"/ha)");
    vs.push_back(QString("resultierendes Maximalgebot (")+QChar(0x20ac)+ "/ha)"); //?? Bemerkung !!!resultierendes Gebot

    nRowArea = 4+nsoils;
    nColArea = 4*nsoils+2;
    areaSS = new SpreadSheet2(nRowArea, nColArea, false, vs, false);
    areaSS->setArt(1);
    areaSS->updateColumn(0, vs);
    areaSS->setTabKeyNavigation(false);

    for (int i=0; i<nsoils; ++i){
        areaSS->cell(i,1)->setFormula("status quo");
    }

    //int type = soiltypeCBox->currentIndex();
    for (int i=0; i<nsoils; ++i){
        varAreas.push_back(farm0->getBidAreasOfType(i));
    }

    QSignalMapper *sigmapArea = new QSignalMapper;
    double delta = gg->PLOT_SIZE;
 for (int j=0; j<nsoils; ++j){
    vector<QSpinBox*> areas;

    for (int i=2; i<nColArea; ++i){
        QSpinBox *dsb = new QSpinBox();
        //dsb->setMinimum(1);

        dsb->setMaximum(500);
        dsb->setSuffix(" ");
        dsb->setAlignment(Qt::AlignRight);
        dsb->setValue(varAreas[j][i-1]);
        dsb->setSingleStep(delta);

        areaSS->setCellWidget(j,i,dsb);
        connect(dsb,SIGNAL(valueChanged(int)), sigmapArea,SLOT(map()));
        sigmapArea->setMapping(dsb,j*nColArea+i);
        areas.push_back(dsb);
        /*areaSS->cell(0,i)->setBackgroundColor(cellBColor);
        areaSS->cell(0,i)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable);;
        areaSS->cell(0,i)->setFormula(QString::number(varAreas[i-1]));
        //*/
    }
    areaDSBs.push_back(areas);
 }
    areaSS->adjSize();
    areaSS->setRowStyleSheet(nsoils+1, QFont("Times",10,QFont::Bold));
    areaSS->setRowFrame(nsoils+1,2);//row, width

    connect(sigmapArea,SIGNAL(mapped(int)),this, SLOT(updateArea2(int)));
    //updateSoilType(type);
    updateAreas();

    areaWidget=new QWidget;
    QVBoxLayout *vl = new QVBoxLayout;

    vl->addWidget(areaTitle);
    vl->addSpacing(5);
    //vl->addLayout(hl);

    QHBoxLayout *hh0 = new QHBoxLayout;
    hh0->addStretch();
    hh0->addWidget(areaSS);
    hh0->addStretch();
    vl->addLayout(hh0);

    vl->addSpacing(25);
    peButton = new QPushButton("Preiserwartung anpassen");
    showPEwidget=false;
    peWidget->setVisible(showPEwidget);//hide();
    connect(peButton, SIGNAL(clicked()), this, SLOT(blendenPE()));

    QHBoxLayout *hl5 = new QHBoxLayout;
    hl5->addWidget(peButton);
    hl5->addStretch();

    vl->addLayout(hl5);

    QHBoxLayout *hh = new QHBoxLayout;
    hh->addStretch();
    hh->addWidget(peWidget);
    hh->addStretch();
    vl->addLayout(hh);

    vl->addStretch(1);
    vl->setSizeConstraint(QLayout::SetFixedSize);
    areaWidget->setLayout(vl);
}

void BodenDialog1::makeAreaWidget2(){
    //int type = soiltypeCBox->currentIndex();
  varAreas.clear();
  for (int i=0; i<nsoils; ++i){
    varAreas.push_back(farm0->getBidAreasOfType(i));
  }
  for (int j=0; j<nsoils; ++j){
    for (int i=2; i<nColArea; ++i){
        QSpinBox *dsb = areaDSBs[j][i-2];
        dsb->setValue(varAreas[j][i-1]);
    }
  }
  //updateSoilType(type);
  updateAreas();
}


void BodenDialog1::blendenPE(){
    if (!showPEwidget) {
        peButton->setText("Ausblenden Preiserwartung");
    }else{
        peButton->setText("Preiserwartung anpassen");
    }
    peWidget->setVisible(!showPEwidget);
    showPEwidget = !showPEwidget;

    peWidget->parentWidget()->layout()->invalidate();
    QWidget *parent = peWidget->parentWidget();
    while (parent){//parent->parentWidget()) {
        parent->adjustSize();
        parent = parent->parentWidget();
    }
    //*/
}

void BodenDialog1::neueGebote(){
   neuBidsButton->setEnabled(false);
   bidsWidget->show();
}

void BodenDialog1::weiter1(){
    prozentLab1->setVisible(false);
    prozentLab->setVisible(true);
    weiterWidget->setVisible(true);
    weiterWidget1->setVisible(false);
    ergLab->setVisible(true);
    adjustSize();
    toClose=true;
    close();
}

void BodenDialog1::weiter(){
    savePEs();
    if (proz0==0) {
        vector<int> maxFreeAreas;
        maxFreeAreas.push_back(1000); // ?ha
        maxFreeAreas.push_back(1000); // ?ha

        vector<vector<pair<int,double> > > offers_of;
        for (int i = 0; i<nsoils; ++i){
            vector<pair<int,double> > offers;
            offers.push_back(pair<int,double>(maxFreeAreas[i],0));
            offers_of.push_back(offers);
        }
        farm0->offers_of_type = offers_of;
    }

    /*if(proz>100){
        inited = false;
        peWidget->setPeInited(false);
        Manager->product_cat0 = Manager->Market->getProductCat();
        updateGUIz();
    }
    else
    //*/
    switch((int)proz0){
    case 50: it_bids[proz0]=it_bids[0];break;
    case 90: it_bids[proz0]=it_bids[50];break;
    default:;
    }
    toClose=true;
    abspeichern(); //emit ok(proz0);
    close();
}

void BodenDialog1::hilfeClosed(){
    hilfeButton->setEnabled(true);
}

void BodenDialog1::showHilfe(){
    dialBodenHilfe->show();
    hilfeButton->setEnabled(false);
}

void BodenDialog1::savePEs(){
    if (proz0==0) it_myPEs.clear();

    switch(peVar) {
    case 0: it_myPEs[proz0] = manPEs; break;
    case 1: it_myPEs[proz0] = naivPEs; break;
    case 2: it_myPEs[proz0] = adaptPEs; break;
    default: ;
    }

    if (proz >=100) all_myPEs.push_back(it_myPEs);
}

void BodenDialog1::getBids(){
    if (!validBids()) return ;
    savePEs();

    weiterButton->setEnabled(false);
    if (proz0==0) it_bids.clear();
    if (proz0==0) emit firstBids();

    vector<vector <pair<int, double> > > offs_of_type;
    for (int i=0; i<nsoils; ++i){
        int maxRow = maxRows[i];
        vector<pair<int, double> > offers;
        for (int k=0; k<=maxRow; ++k){
            int f = areaDSBs_of_type[i][k]->value();
            double p = bidDSBs_of_type[i][k]->value()*gg->PLOT_SIZE; //plotpreis!
            offers.push_back(pair<int, double>(f,p));
        }

        offs_of_type.push_back(offers);
    }

    it_bids[proz0] = offs_of_type;

    farm0->offers_of_type = offs_of_type;
    farm0->initNewOffers();

    if (proz>=100) all_bids.push_back(it_bids);
    accept(); //ohne diese w�re rejected();

    if (proz>=100) {
        inited =  false;
        peWidget->setPeInited(false);
        //PE zur�cksetzen
        Manager->product_cat0 = Manager->Market->getProductCat();
    }
    //*/
    toClose=true;
    abspeichern(); //emit ok(proz0);
    close();
}

void BodenDialog1::abspeichern(){
    vector<bool> spiels=parent->getSpielPeProds();
    int sz=prodIDs.size();
    int t=parent->aTime.elapsed();	
    if (gg->tIter==1&& proz0==0) { // first iter
        parent->tstrBoden<<QString("\t\t\t Fl�chen(ha)\tGebote(")+QChar(0x20ac)+"/ha)"+"\n";
        parent->tstrPe<<QString("\t\t Produkt\t");//
        for (int i=0;i<sz;++i){
            if (spiels[i]) {
                parent->tstrPe<<QString(prodNames[i].toStdString().c_str())+"_model\t"
                              <<QString(prodNames[i].toStdString().c_str())+"_spiel\t";
            }
        }parent->tstrPe<<"\n";

        parent->tstrPe<<QString("\t\t Einheit\t");//
        for (int i=0;i<sz;++i){
            if (spiels[i]) {
                parent->tstrPe<<QString(prodUnits[i].toStdString().c_str())+"    \t"
                              <<QString(prodUnits[i].toStdString().c_str())+"    \t";
            }
        }parent->tstrPe<<"\n\n";
    }
    if (proz0==0){
            parent->tstrBoden<<"\nRunde\t"<<gg->tIter<<"\n";
            parent->tstrPe<<"\nRunde\t"<<gg->tIter<<"\n";
    }
    parent->tstrBoden<<"\t"<<proz0<<"%\t"<<QLocale().toString(t/1000.,'f',2)<<" Sekunden\n";
    parent->tstrPe<<"\t Bodenmarkt("<<proz0<<"%) \t";
        for (int i=0;i<sz;++i){
                if (spiels[i]) {
                parent->tstrPe<<QLocale().toString((prodRefPEs[i]-prodNertrags[i])/prodErtrags[i],'f',2)+"\t"
                              <<QLocale().toString((it_myPEs[proz0][i]-prodNertrags[i])/prodErtrags[i],'f',2)+"\t";
            }
        }parent->tstrPe<<"\n";

    for (int i=0;i<nsoils;++i){
            parent->tstrBoden<<" \t    "<<gg->NAMES_OF_SOIL_TYPES[i].c_str()<<"\n";
            vector<pair<int, double> > offers=(it_bids[proz0])[i];
            sz=offers.size();
            pair<int,double> p;
            for (int j=0;j<sz;++j){
                p=offers[j];
                parent->tstrBoden<<"\t\t\t"<<QLocale().toString(p.first)<<"\t"
                <<QLocale().toString(p.second/gg->PLOT_SIZE, 'f',2)<<"\n";
            }
    }
}

void BodenDialog1::getVorschlag(){
    savePEs();

    if (proz0==0) it_bids.clear();

    vector<vector <pair<int, double> > > offs_of_type;
    for (int i=0; i<nsoils; ++i){
        int maxRow = maxRows[i];
        vector<pair<int, double> > offers;
        for (int k=0; k<=maxRow; ++k){
            int f = areaDSBs_of_type[i][k]->value();
            double p = vorschlags_of_type[i][k];
            offers.push_back(pair<int, double>(f,p));
        }

        offs_of_type.push_back(offers);
    }

    it_bids[proz0] = offs_of_type;

    farm0->offers_of_type = offs_of_type;
    farm0->initNewOffers();

    if (proz>=100) all_bids.push_back(it_bids);
    accept(); //ohne diese w�re rejected();
    if (proz>=100) {
        inited =  false;
        peWidget->setPeInited(false);
        //PE zur�cksetzen
        Manager->product_cat0 = Manager->Market->getProductCat();
    }
    toClose=true;
    close();
}

void BodenDialog1::getBids0(){
    savePEs();
    validBids();
    weiterButton->setEnabled(false);

    if (proz0==0) it_bids.clear();

    vector<vector <pair<int,double> > > offs_of_type;
    for (int i=0; i<nsoils; ++i){
        int maxRow = maxRows[i];
        vector<pair<int, double> > offers;
        for (int k=0; k<=maxRow; ++k){
            double f = bidSheets[i]->cell(k,0)->data(Qt::DisplayRole).toDouble();
            double p = bidSheets[i]->cell(k,1)->data(Qt::DisplayRole).toDouble();
            offers.push_back(pair<int,int>(f,p));
        }

        offs_of_type.push_back(offers);
    }

    it_bids[proz0] = offs_of_type;

    farm0->offers_of_type = offs_of_type;
    farm0->initNewOffers();

    if (proz>=100) all_bids.push_back(it_bids);
    accept(); //ohne diese w�re rejected();
    toClose=true;
    close();
}

bool BodenDialog1::validBids(){
    for (int i=0; i<nsoils; ++i){
        if (!validSoilType(i)) {
            return false;
        }
    }
    return true;
}

void BodenDialog1::validSoilType0(int t){
    SpreadSheet2 *s = bidSheets[t];
    int row = s->rowCount();
    vector<double> ds;
    for (int i=0; i<row; ++i){
        ds.push_back((s->cell(i,0))->data(Qt::DisplayRole).toDouble());
    }
    int maxRow=0;
    int p = row-1;
    while (p >=0 ){
        if (ds[p]>0) {
            maxRow = p;
            break;
        }
        --p;
    }
    if (ds[0] <=0)
        QMessageBox::warning(this, QString("Problem beim Bieten"),
        QString("Anfangsfl�chen f�r Bodentyp %1 m�ssen gr��er 0 sein").arg(soilNames[t].c_str()));

    else {
      for (int i=maxRow; i>0; --i){
        if (ds[i] < ds[i-1]) {
            QMessageBox::warning(this, QString("Problem beim Bieten"),
            QString("Fl�chen f�r Bodentyp %1 m�ssen aufsteigend angegeben werden").arg(soilNames[t].c_str()));
            break;
        }
      }
    }

    for (int i=0; i<row; ++i){
        double d = s->cell(i,1)->data(Qt::DisplayRole).toDouble();
        if (d<0) {
            QMessageBox::warning(this, QString("Problem beim Bieten"),
            QString("Gebote f�r Bodentyp %1 m�ssen gr��er 0 sein").arg(soilNames[t].c_str()));
            break;
        }
    }

    maxRows[t] = maxRow;
}

bool BodenDialog1::validSoilType(int t){
    SpreadSheet2 *s = bidSheets[t];
    int row = s->rowCount();
    vector<int> ds;
    for (int i=0; i<row; ++i){
        ds.push_back(areaDSBs_of_type[t][i]->value());
    }
    int maxRow=0;
    int p = row-1;
    while (p >=0 ){
        if (ds[p]>0) {
            maxRow = p;
            break;
        }
        --p;
    }
      for (int i=maxRow; i>0; --i){
        if (ds[i] <= ds[i-1]) {
            QMessageBox::warning(this, QString("Problem beim Bieten"),
            QString("Fl�chen f�r Bodentyp %1 m�ssen aufsteigend angegeben werden").arg(soilNames[t].c_str()));
            return false;
            //break;
        }
      }
    maxRows[t] = maxRow;
    return true;
}


void BodenDialog1::closeEvent(QCloseEvent * event){
/*
    int n = gg->NO_OF_SOIL_TYPES;
    for (int i=0; i<n; ++i){
        vector<pair<double, double> > tmpOffers;
        tmpOffers.push_back(pair<double,double>(10, 1000));
        tmpOffers.push_back(pair<double,double>(20, 1200));
        tmpOffers.push_back(pair<double,double>(30, 1500));

        offers_of_type.push_back(tmpOffers);
    }

    Manager->Farm0->offers_of_type = offers_of_type;

    for (int i=0; i<n; ++i){
       int ind = Manager->Farm0->offer_index_of_type[i] = 0;
       Manager->Farm0->offer_of_type[i] = offers_of_type[i][ind].second;
    }
//*/
    if (!( parent->faOver()||toClose)){
        event->ignore();
        return;
    }

    gg->SpielerHasBids = true;
    if (prozent>=100){
        /*parent->mydatWindow->hide();
        parent->actMyData->setEnabled(true);
        //*/
        Manager->getGlobals()->SIM_WEITER =true;
        //emit fertig();
    }
    Manager->product_cat0 = Manager->Market->getProductCat();
/*
    file.setFileName(QString(gg->OUTPUTFILEdir.c_str())+parent->decPEdat);
    if (!file.open(QIODevice::WriteOnly)){
        QMessageBox::warning(this,
        tr("FarmAgriPolis"), tr("Fehler! beim �ffnen der Datei %1").arg(file.fileName()));
    }

    tstr.setDevice(&file);
    tstr<<"=======FarmAgriPolis========\n"
        <<"=======IAMO 2011-2013=======\n\n"
        << "Spielanfang:\t";
    tstr<<dateTime.toString()<<"\n\n";
    file.close();
    file.open(QIODevice::Append);
    tstr.setDevice(&file);
//*/
    dialBodenHilfe->close();
    QDialog::closeEvent(event);
}

static bool compTransportCosts(RegPlotInfo* p1, RegPlotInfo* p2){
    RegPlotInfo *p = Manager->Farm0->farm_plot;
    double d1 = p->calculateDistanceCosts(p1);
    double d2 = p->calculateDistanceCosts(p2);
    return (d1<=d2);
}

void BodenDialog1::updateFreePlots(){
  /*  vector<RegPlotInfo*> fplots = Manager->Region->free_plots;
    int sz = fplots.size();

    for (int i=0; i<sz; ++i){
        int t = fplots[i]->getSoilType();
        free_plots_of_type[t].push_back(fplots[i]);
    }

    int n = gg->NO_OF_SOIL_TYPES;
    for (int i=0; i<n; ++i){
        sort(free_plots_of_type[i].begin(), free_plots_of_type[i].end(), compTransportCosts);
    }
//*/
}

void BodenDialog1::updateOffers(){
    Manager->Farm0->offers_of_type = offers_of_type;
}

void BodenDialog1::showBoden(int p0, int p){
   /*parent->mydatWindow->layerComboBox->setCurrentIndex(3); //pachtbilanz
    parent->mydatWindow->show();
    parent->actMyData->setEnabled(false);
    //*/
    /*proz0= p0;
    proz = p;

    if(proz0==0)  //aktualisieren
        Manager->product_cat0 = Manager->Market->getProductCat();
   //*/

    inited=false;
    updateGUI();
    inited=true;

    peWidget->setInited(true);
    areaSS->setInited(true);
    for (int i=0; i<nsoils; ++i){
        bidSheets[i]->setInited(true);
    }

    prozent = proz;
    prozentStr = QString("%1\% der freien Fl�chen sind vergeben").arg(proz0);
    prozentLab->setText(prozentStr);
    prozentLab1->hide();
    weiterWidget1->hide();
    if (proz0>0){
        prozentLab->show();
        bidsWidget->hide();
        weiterWidget->show();
        ergWidget->show();
        adjustSize();
    }else{
        prozentLab->hide();
        bidsWidget->show();
        weiterWidget->hide();
        ergWidget->hide();
    }
    weiterButton->setEnabled(true);
    neuBidsButton->setEnabled(true);

    //aktualisiertung status Quo
    for (int i=0; i<nsoils; ++i){
        landinputs[i] = farm0->getLandInputOfType(i);
        QString listr = QLocale().toString(landinputs[i])+" ha";
        labLandinputs[i]->setText(listr);
    }

    //if (proz>=100) inited = false;
    show();
    //exec();//show();
    parent->aTime.restart();
}

void BodenDialog1::getLAsig(int p0, int p){
    proz0=p0;
    proz=p;
    if(proz0==0)  //aktualisieren
        Manager->product_cat0 = Manager->Market->getProductCat();
    updatePEs();
    emit LAsig(p0,p);
}

void BodenDialog1::showBoden(int p){
    //proz0= p;
    proz = 200;
    prozent = proz;
    ergWidget->show();
    updateGUIz();
    exec();
}

void BodenDialog1::updateSoilType(int type){
        soiltype = type;
/*
        defaultAreas = farm0->getBidAreasOfType(type);
        int num=defaultAreas.size();
        int r=0;
        for(int x=1; x<num; ++x){
            areaDSBs[x-1]->setValue(farm0->getBidAreasOfType(type)[x]);
            //areaSS->cell(r,x+1)->setFormula(QLocale().toString(farm0->getGDB(type,x),'f',2));
        }

        r=1;
        for(int x=0; x<num; ++x){
            areaSS->cell(r,x+1)->setFormula(QLocale().toString(farm0->getGDB(type,x),'f',2));
        }

        r=2;
        for(int x=0; x<num; ++x){
            double tt=farm0->getGDB(type,x)-farm0->getGDB(type,0);
            if (x==0)areaSS->cell(r,x+1)->setFormula(QLocale().toString(tt,'f',2));
            else areaSS->cell(r,x+1)->setFormula(QLocale().toString(tt/defaultAreas[x],'f',2));
        }

        r=3;
        for(int x=0; x<num; ++x){
            areaSS->cell(r,x+1)->setFormula(QLocale().toString(farm0->getTrK(type,x),'f',2));
        }

        r=4;
        for(int x=0; x<num; ++x){
            double tt = areaSS->cell(r-2,x+1)->data(Qt::DisplayRole).toDouble()-areaSS->cell(r-1,x+1)->data(Qt::DisplayRole).toDouble();
            areaSS->cell(r,x+1)->setFormula(QLocale().toString(tt));
        }
        //*/
}

void BodenDialog1::updateAreas(){
    int r=0;
    int num=0;
    vector<double> x1s, x2s;

    for (int type=0; type<nsoils; ++type){
        varAreas[type] = farm0->getBidAreasOfType(type);
        num=varAreas[type].size();
        for(int x=1; x<num; ++x){
            areaDSBs[type][x-1]->setValue(farm0->getBidAreasOfType(type)[x]);
            //areaSS->cell(r,x+1)->setFormula(QLocale().toString(farm0->getGDB(type,x),'f',2));
        }
    }

        r=nsoils;//2;
        for(int x=0; x<num; ++x){
            areaSS->cell(r,x+1)->setFormula(QLocale().toString(farm0->getGDB(x),'f',2));
        }


        ++r;//=3;
        for(int x=0; x<num; ++x){
            double tt=farm0->getGDB(x)-farm0->getGDB(0);
            double asum=0;
            for (int i=0; i<nsoils; ++i){
                asum += varAreas[i][x];
            }
            if (x==0)areaSS->cell(r,x+1)->setFormula(QLocale().toString(tt));
            else {
                double xx = asum==0?0:tt/asum;
                x1s.push_back(xx);
                areaSS->cell(r,x+1)->setFormula(QLocale().toString(xx,'f',2));
            }
        }

        ++r;
        for(int x=0; x<num; ++x){
            if (x==0) areaSS->cell(r,x+1)->setFormula(QLocale().toString(farm0->getTrK(x)));
            else {
                double xx = farm0->getTrK(x);
                areaSS->cell(r,x+1)->setFormula(QLocale().toString(xx,'f',2));
                x2s.push_back(xx);
            }
        }

        ++r; //=5
        for(int x=0; x<num; ++x){
            double tt ;
            if (x==0) tt = 0;
            else tt= x1s[x-1]-x2s[x-1];
                //areaSS->cell(r-2,x+1)->data(Qt::DisplayRole).toDouble()-areaSS->cell(r-1,x+1)->data(Qt::DisplayRole).toDouble();
            if (x==0) areaSS->cell(r,x+1)->setFormula(QLocale().toString(tt));
            else {
                areaSS->cell(r,x+1)->setFormula(QLocale().toString(tt, 'f',2));
            }
        }
        //*/
}

void BodenDialog1::updateVorschlags(){
    int num=4; //TODO

    for (int type=0; type<nsoils; ++type){
        vector<double> vs;
        for(int x=0; x<num; ++x){
            double tt=farm0->getGDB2B(type,x)-farm0->getGDB(0);
            double a = areaDSBs_of_type[type][x]->value();
            vs.push_back(farm0->getRentAdjustCoefficient()*tt/a);
                //gg->RENT_ADJUST_COEFFICIENT*tt/a);
        }
        vorschlags_of_type[type]=vs;
        gebotstables[type]->updateColumn(1,vorschlags_of_type[type]);
    }
    //*/
}

void BodenDialog1::updateVorschlags(int typ){
    int num=4; //TODO

    for(int x=0; x<num; ++x){
        bidDSBs_of_type[typ][x]->setValue(vorschlags_of_type[typ][x]);
    }
}

void BodenDialog1::updateArea2(int pos){
        if (!inited) return;

        //cout << "a: pos: " << pos <<endl;
        int zeile = pos/nColArea;
        int spalte= pos%nColArea;

        vector<int> ds;
        for (int i=0; i<nsoils; ++i){
            areaDSBs[i][spalte-2]->setEnabled(false);
            ds.push_back(areaDSBs[i][spalte-2]->value());
        }

        farm0->setBidAreas(spalte-1,ds);
        emit areaSig2(spalte-1);
        //*/
}

void BodenDialog1::updateArea2B(int pos){
        //ACHTUNG!! pos 2x
        if (!inited) return;

        int zeile = pos%(2*nRowBid);
        int spalte= pos/(2*nRowBid);

        int ar = areaDSBs_of_type[spalte][zeile]->value();
        farm0->setBidArea(spalte,zeile,ar);
        emit areaSig2B(spalte,zeile);
        //*/
}

void BodenDialog1::updateArea(int pos){
        if (!inited) return;

        //cout << "a: pos: " << pos <<endl;
        int zeile = pos/nColArea;
        int spalte= pos%nColArea;
        areaDSBs[zeile][spalte-2]->setEnabled(false);
        int d = areaDSBs[zeile][spalte-2]->value();
        /*int type = soiltype;
        farm0->setBidAreaOfType(type,pos-1,d);
        emit areaSig(type, pos-1);
        //*/
}

void BodenDialog1::getPeOK(int r, bool b){
    emit peOK(r, b);
       if (!b) {
           QMessageBox::warning(this, tr("Warnung"),//Solver Infomation,
                             tr("Erwartete Preise zu extrem"));//Bedingungen verletzt"));
        }

        if (!b) return;

        updateGDBs();
        updateVorschlags();
}

void BodenDialog1::getPeVarOK(int r, bool b){
    updateGDBs();
    updateVorschlags();
}

void BodenDialog1::getAreaOK(int type, int pos){
        /*if (type != soiltype) return;
        //cout << "b: pos: " << pos <<endl;
        areaDSBs[pos-1]->setEnabled(true);

        int r=1;
        areaSS->cell(r,pos+1)->setFormula(QLocale().toString(farm0->getGDB(type,pos),'f',2));

        r=2;
        double tt=farm0->getGDB(type,pos)-farm0->getGDB(type,0);
        areaSS->cell(r,pos+1)->setFormula(QLocale().toString(tt/areaDSBs[pos-1]->value(),'f',2));

        r=3;
        areaSS->cell(r,pos+1)->setFormula(QLocale().toString(farm0->getTrK(type,pos),'f',2));

        r=4;
        tt = areaSS->cell(r-2,pos+1)->data(Qt::DisplayRole).toDouble()
            -areaSS->cell(r-1,pos+1)->data(Qt::DisplayRole).toDouble();
        areaSS->cell(r,pos+1)->setFormula(QLocale().toString(tt,'f',2));
        //*/
}

void BodenDialog1::getAreaOK2B(int typ, int pos){
    double gdb0 = farm0->getGDB(0);
    double gdb = farm0->getGDB2B(typ,pos);
    double area = areaDSBs_of_type[typ][pos]->value();
    double x = farm0->getRentAdjustCoefficient()*(gdb-gdb0)/area; //gg->RENT_ADJUST_COEFFICIENT*(gdb-gdb0)/area;
    gebotstables[typ]->cell(pos,1)->setFormula(QLocale().toString(x,'f',2));
    vorschlags_of_type[typ][pos]=x;
}

void BodenDialog1::getAreaOK2(int pos){
        //cout << "b: pos: " << pos <<endl;
    for (int i=0; i<nsoils; ++i){
        areaDSBs[i][pos-1]->setEnabled(true);
    }

    int r=nsoils;//2;
    areaSS->cell(r,pos+1)->setFormula(QLocale().toString(farm0->getGDB(pos),'f',2));

    ++r; // =3
    double tt=farm0->getGDB(pos)-farm0->getGDB(0);
    double asum=0;
    for (int i=0; i<nsoils; ++i){
        asum+= areaDSBs[i][pos-1]->value();
    }
    double x1 = asum==0? 0: tt/asum;
    areaSS->cell(r,pos+1)->setFormula(QLocale().toString(x1,'f',2));

    ++r; //=4;
    double x2 = farm0->getTrK(pos);
    areaSS->cell(r,pos+1)->setFormula(QLocale().toString(x2,'f',2));

    ++r;// 5;
    tt = x1-x2; //areaSS->cell(r-2,pos+1)->data(Qt::DisplayRole).toDouble()
            //-areaSS->cell(r-1,pos+1)->data(Qt::DisplayRole).toDouble();
    areaSS->cell(r,pos+1)->setFormula(QLocale().toString(tt,'f',2));
    //*/
}

void BodenDialog1::blendenBSpiegel(){
    if (!showBSpiegel) {
        spiegelButton->setText("Betriebsspiegel ausblenden");//akt. Fl�chen�bersicht ausblenden");
        parent->actMyData->setDisabled(true);
    }else{
        spiegelButton->setText("Betriebsspiegel einblenden");//akt. Fl�chen�bersicht einblenden");
        parent->actMyData->setEnabled(true);
    }
    parent->mydatWindow->updateGraphic(6);
    //layerComboBox->setCurrentIndex(5); //betriebsspiegel
    parent->mydatWindow->setVisible(!showBSpiegel);
    showBSpiegel = !showBSpiegel;
}

void BodenDialog1::enableBSbutton(bool b){
    spiegelButton->setEnabled(b);
}

void BodenDialog1::getMydatwindowHi(bool t){
    if(!gg->ToPlay) return;
    if (!t) return;
    if (!firstGUI)
        spiegelButton->setText("Betriebsspiegel einblenden");//akt. Fl�chen�bersicht einblenden");
    else {
        if (isInited())
            spiegelButton->setText("Betriebsspiegel ausblenden");
    }
    showBSpiegel = false;
}


Title22::Title22(Qt::AlignmentFlag align, QFont f, QWidget*w):QLabel(w){
    this->setAlignment(align);
    this->setFont(f);
}
