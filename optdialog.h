#ifndef OPTDIALOG_H
#define OPTDIALOG_H

#include <QString.h>
#include <QDialog>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>

using namespace std;

class OptDialog: public QDialog {
    Q_OBJECT
public:
    explicit OptDialog(QWidget *parent=0);
    QString optdir;
    QString policy;
public slots:
    void getInDir();
    void getPol();
    void getDir(QString);
    void getFile(QString);

protected:
    void accept();

private:
    QLabel *optLabel;
    QLineEdit *optEdit;

    QLabel *polLabel;
    QLineEdit *polEdit;

    QPushButton *okButton;
    QPushButton *cancelButton;
};


#endif // OPTDIALOG_H
