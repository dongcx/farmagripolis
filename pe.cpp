#include "pe.h"
#include "AgriPoliS.h"

#include <QLineEdit>
#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QGroupBox>
#include <QScrollArea>
#include <QComboBox>
#include <QSpinBox>
#include <QSignalMapper>

#include "spreadsheet2.h"
#include "cell2.h"

static QFont font0 = QFont("SansSerif",14, QFont::Bold);
static QFont font1 = QFont("Times",11, QFont::Bold);
static QFont font2 = QFont("Times",10, QFont::Normal);

static QString textIndex = "Die Preisentwicklung der �brigen Marktfr�chte folgt der der ";

PEwidget::PEwidget(int iter,int *var, QString aname, vector<QString> hheader, vector<QString> nams,
                    vector<QString> unts, vector<double> erts, vector<double> nerts,vector<QString>gmunts, QDialog *par)
    :ITER(iter), peVar(var), refname(aname),names(nams), units(unts), ertrags(erts), nebenErtrags(nerts), gmunits(gmunts), QWidget(par){
    setWindowTitle(tr("PE"));
    vorhanden = false;
    reakt = true;
    prettyUnits();
        Title02* peTitle = new Title02(Qt::AlignCenter, font0);
        peTitle->setText("Preiserwartung");

        QLabel *vartype = new QLabel("Variante");
        peVarCBox = new QComboBox;
        peVarCBox->addItem(QString("Manuell"));
        peVarCBox->addItem(QString("Naiv"));
        peVarCBox->addItem(QString("Adaptiv"));
        peVarCBox->setCurrentIndex(*peVar);

        connect(peVarCBox, SIGNAL(currentIndexChanged(int)), this, SLOT(updateVar(int)));

        QHBoxLayout *hl = new QHBoxLayout;
        hl->addStretch();
        hl->addWidget(vartype);
        hl->addWidget(peVarCBox);
        hl->addStretch();

        QLabel *refLab = new QLabel("Referenz-Zeit");
        refzeitSBox = new QSpinBox;
        refzeitSBox->setMaximum(ITER);
        refzeitSBox->setValue(1);
        refzeitSBox->setMinimum(1);
        QLabel *jahrLab = new QLabel("Jahre");

        connect(refzeitSBox, SIGNAL(valueChanged(int)), this, SLOT(updateRef(int)));

        QHBoxLayout *hl1 = new QHBoxLayout;
        hl1->addStretch();
        hl1->addWidget(refLab);
        hl1->addWidget(refzeitSBox);
        hl1->addWidget(jahrLab);
        hl1->addStretch();

        nRowPE = names.size();
        nColPE = hheader.size();

        peSS = new SpreadSheet2(nRowPE, nColPE, true, hheader, false);
        peSS->setArt(2);
        peSS->setTabKeyNavigation(false);

        peSS->updateColumn(0,names);
        peSS->updateColumn(1,units);
        peSS->updateColumn(nColPE-1,gmunits);

        QVBoxLayout *vl = new QVBoxLayout;
        vl->addWidget(peTitle);
        vl->addSpacing(5);

        preisIndex = 1;//prodPEs[refpos]/prodPrices[refpos];//1.101;
        //pindexLab =  new QLabel(QString("Preisindex f�r alle Marktfr�chte (wie der von %1) : %2")
        //                        .arg(refname, QLocale().toString(preisIndex)));
        pindexLab =  new QLabel(textIndex + QString("%1").arg(refname));
        pindexLab->setVisible(gg->hasMarktfruechte);

        QGridLayout *gl = new QGridLayout;
        gl->addLayout(hl, 0, 0);
        gl->addLayout(hl1, 0, 1);
        //vl->addStretch();
        vl->addLayout(gl);
        vl->addWidget(peSS);
        vl->addWidget(pindexLab);

        vl->setSizeConstraint(QLayout::SetFixedSize);
        // vl->addStretch(1);
        setLayout(vl);

        setWindowFlags(Qt::WindowStaysOnTopHint);
}

void PEwidget::prettyUnits(){ //euro symbol
    int sz = units.size();
    for (int i=0; i<sz; ++i){
        QString str = units[i];
        str.replace("EuRo",QString("")+QChar(0x20ac), Qt::CaseInsensitive);
        units[i]=str+"  ";
        str = gmunits[i];
        str.replace("EuRo",QString("")+QChar(0x20ac), Qt::CaseInsensitive);
        gmunits[i]=str+"  ";
    }
}

void PEwidget::closeEvent(QCloseEvent *event){
    emit ausBlenden();
    close();
}

//muss direkt nach Konstruktion aufgerufen werden
void PEwidget::setPeSpiels(vector<bool> si){
    peSpiels=si;
}

void PEwidget::updateDSBs(vector<double> pes){
    prodPEs = pes;
    for (int r=0; r<nRowPE; ++r){
        QDoubleSpinBox *dsb=peDSBs[r];
        dsb->setValue((prodPEs[r]-nebenErtrags[r])/ertrags[r]);
     }
}

void PEwidget::updatePrices(vector<double> ps){
    prices=ps;
    peSS->updateColumn(2, ps, ertrags, nebenErtrags);
    //updateIndexLab();
}

void PEwidget::updateCosts(vector<double> cs){
    costs=cs;
}

void PEwidget::updateAvPrices(vector<double> avps){
    avprices=avps;
    peSS->updateColumn(3, avps, ertrags, nebenErtrags);
}

void PEwidget::updateRefPEs(vector<double> pes){
    refPEs=pes;
    peSS->updateColumn(4, pes, ertrags, nebenErtrags, peSpiels);
}

void PEwidget::enableDSBs(bool t){
    for (int r=0; r<nRowPE; ++r){
        if (!peSpiels[r]) continue;
        QDoubleSpinBox *dsb=peDSBs[r];
        dsb->setEnabled(t);
     }
}

void PEwidget::updateMyPEs(vector<double> pes, bool exist){
    prodPEs=pes;
    if (exist) {
        reakt = false;
        //cout << "exist: ";
        for (int r=0; r<nRowPE; ++r){
            if (!peSpiels[r]) continue;
            double max, min;
            max = (prices[r]-nebenErtrags[r])*5/ertrags[r];
            min = (prices[r]-nebenErtrags[r])*0.2/ertrags[r];
            if (max<0) {
                double t = max;
                max = min;
                min = t;
            }
            QDoubleSpinBox *dsb=peDSBs[r];
            dsb->setSingleStep(max/100);
            dsb->setRange(min,max);

            dsb->setValue((prodPEs[r]-nebenErtrags[r])/ertrags[r]);//setText(QLocale().toString(prodPrices[r],'f',2));
         }
         //cout << endl;
        reakt = true;
        preisIndex = prices[refpos]==0?1: prodPEs[refpos]/prices[refpos];
        //pindexLab->setText(QString("Preisindex f�r alle Marktfr�chte (wie der von %1) : %2").arg(refname, QLocale().toString(preisIndex)));
        pindexLab =  new QLabel(textIndex + QString("%1").arg(refname));
        return;
    }
    QSignalMapper *sigmapPE = new QSignalMapper;

        *peVar = peVarCBox->currentIndex();
            reakt = false;
            //cout << "\n";
            for (int r=0; r<nRowPE; ++r){
                QDoubleSpinBox *dsb = new QDoubleSpinBox();

                double max, min;
                max = (prices[r]-nebenErtrags[r])*5/ertrags[r];
                min = (prices[r]-nebenErtrags[r])*0.2/ertrags[r];
                if (max<0) {
                    double t = max;
                    max = min;
                    min = t;
                }

                dsb->setRange(min,max);
                dsb->setSingleStep(max*0.01);
                dsb->setAlignment(Qt::AlignRight);
                dsb->setSuffix("  ");
                //cout << (prices[r]-nebenErtrags[r])/ertrags[r] << " - ";
                dsb->setLocale(QLocale::German);
                dsb->setValue((prices[r]-nebenErtrags[r])/ertrags[r]);
                if (peSpiels[r])
                    peSS->setCellWidget(r,nColPE-3,dsb);

                peDSBs.push_back(dsb);
                connect(dsb, SIGNAL(valueChanged(double)), sigmapPE, SLOT(map()));
                sigmapPE->setMapping(dsb, r);
                //peSS->cell(r,nColPE-1)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable);
                //peSS->cell(r,nColPE-1)->setBackgroundColor(cellBColor);
            }
            //cout << endl;
            reakt = true;

        connect(sigmapPE, SIGNAL(mapped(int)), this, SLOT(updatePE(int)));
        vorhanden = true;
        enableDSBs(true);
        /*if (gg->PreisSchwankung)
            enableDSBs(true);
        else enableDSBs(false);
        //updateIndexLab();
        //*/
}

void PEwidget::updateRef(int x){
    calRefPrices();
    peSS->updateColumn(3, avprices,ertrags, nebenErtrags);
}

void PEwidget::updateRefs(){
    calRefPrices();
    peSS->updateColumn(3, avprices, ertrags, nebenErtrags);
}

void PEwidget::updateGMs(vector<double> gms){
    peSS->updateColumn(6, gms);
}

void PEwidget::updateData1(vector<int> gids, vector<double> gps, vector<double> gpes){
    getreideIDs = gids;
    getreidePrices = gps;
    getreidePEs = gpes;
}

void PEwidget::updateData1a(vector<double> gps, vector<double> gpes){
    getreidePrices = gps;
    getreidePEs = gpes;
}

void PEwidget::updateData2(int rpos, vector<double> *mPEs, vector<double> adPEs, vector<double> naPEs){
    refpos = rpos;
    manPEs = mPEs;
    adaptPEs = adPEs;
    naivPEs = naPEs;
}

void PEwidget::updateData3(vector<int> ids, vector<vector<double>  > *allPs){
    prodIDs = ids;
    allPrices = allPs;
}

void PEwidget::updateData(vector<double> ps,
                          vector<double> pes, vector<double> mypes, bool exist){
    vector<double> gms;
    int sz=ps.size();
    for (int i=0; i<sz;++i){
        gms.push_back(ps[i]-costs[i]);
    }
    updateGMs(gms);
    updatePrices(ps);
    //updateAvPrices(avps);
    updateRefPEs(pes);
    updateMyPEs(mypes,exist);
    /*
    peSS->updateColumn(2, ps);
    peSS->updateColumn(3, avps);
    peSS->updateColumn(4, pes);
    this->updateMyPEs(mypes, vorhanden);
    //*/
    //updateIndexLab();
}

void PEwidget::updateIter(int it){
    ITER = it;
    if (it>0) refzeitSBox->setMaximum(it);
}

void PEwidget::calRefPrices(){
    int sz = names.size();
    int x = refzeitSBox->value();

    if (x==0 || ITER == 0) { //0-te periode
        avprices = prices;
        return;
    }
    avprices.clear();
    avprices.resize(sz);
    for (int i=0; i<sz; ++i){
       avprices[i] = 0;
       for (int k=0; k<x; ++k)  {
            avprices[i] += (*allPrices)[ITER-k-1][i];
       }
       avprices[i] /= x;
    }
}

void PEwidget::setVarIndex(int ind){
    peVarCBox->setCurrentIndex(ind);
}

void PEwidget::updateVar(int pi){
   *peVar = pi;
//peSS->updateColumn(2, prodPEs);
    bool en = false;
    if (pi==0) {
        en = true;
    }

    vector<double> tvalues ;
    switch (pi) {
    case 0: tvalues = *manPEs;break;
    case 1: tvalues = naivPEs; break;
    case 2: tvalues = adaptPEs; break;
    default: ;
    }

    reakt = false;
    int nPEs = tvalues.size();
    for (int i=0; i<nPEs; ++i){
        if(!peSpiels[i]) continue;  //nicht alle Produkte
        double d = tvalues[i];
        //cout << d << " -- ";
        peDSBs[i]->setValue((d-nebenErtrags[i])/ertrags[i]);
        Manager->product_cat0[prodIDs[i]].setPriceExpectation(d);

        if (i==refpos) {
            double tind;
            tind= prices[refpos]==0?1:d/prices[refpos];

            //getreidePEs.clear();
            int ts = getreideIDs.size();
            /*
            for (int i=0; i<ts; ++i){ //backup
                getreidePEs.push_back(Manager->product_cat0[getreideIDs[i]].getPriceExpectation());
            }
            //*/
            for (int i=0; i<ts; ++i){
                Manager->product_cat0[getreideIDs[i]].setPriceExpectation(getreidePrices[i]*tind);
            }
        }
    }
    //cout << endl;

    reakt = true;

    updateIndexLab();
    /*if (i==0) {
        for (int r=0; r<nRowPE; ++r){
            peSS->cell(r,nColPE-1)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable);
            peSS->cell(r,nColPE-1)->setBackgroundColor(cellBColor);
        }
    }else{
        for (int r=0; r<nRowPE; ++r){
            peSS->cell(r,nColPE-1)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
            peSS->cell(r,nColPE-1)->setBackgroundColor(Qt::white);
        }
    }
 //*/

    for (int r=0; r<nRowPE; ++r){
        if(!peSpiels[r]) continue;  //nicht alle Produkte mit PE
        static_cast<QDoubleSpinBox*>(peSS->cellWidget(r,nColPE-3))->setEnabled(en);
    }

    emit peVarSig(*peVar);
}

void PEwidget::setInited(bool b){
    peSS->setInited(b);
}

void PEwidget::setPeInited(bool b){
    inited = b ;
}

void PEwidget::savePEs(){
 ;
}

void PEwidget::updatePE(int row){
        if (!inited || !reakt) return;
        int sz = prodIDs.size();
        for (int i=0; i<sz; ++i){
            if(peSpiels[i])
                peDSBs[i]->setEnabled(false);
        }

        double d = peDSBs[row]->value()*ertrags[row]+nebenErtrags[row];
        oldvalue = Manager->product_cat0[prodIDs[row]].getPriceExpectation();
        Manager->product_cat0[prodIDs[row]].setPriceExpectation(d);

        if (row == refpos) {
            double tind;
            tind = prices[refpos]==0?1:d/prices[refpos] ;
            getreidePEs.clear();
            int ts = getreideIDs.size();
            for (int i=0; i<ts; ++i){
                getreidePEs.push_back(Manager->product_cat0[getreideIDs[i]].getPriceExpectation());
            }
            for (int i=0; i<ts; ++i){
                Manager->product_cat0[getreideIDs[i]].setPriceExpectation(getreidePrices[i]*tind);
            }
        }

        //cout << "a: row: " << row <<endl;
        emit peSig(row);
}

void PEwidget::updateIndexLab(){
    double refpe = peDSBs[refpos]->value()*ertrags[refpos]+nebenErtrags[refpos];
    double refp = prices[refpos];
    preisIndex = refp==0?1:refpe/refp;
    //pindexLab->setText(QString("Preisindex f�r alle Marktfr�chte (wie der von %1) : %2").arg(refname, QLocale().toString(preisIndex,'f',3)));
    pindexLab =  new QLabel(textIndex + QString("%1").arg(refname));
}

void PEwidget::getPeOK(int r, bool b){
        if (!b) {
            inited = false;
            peDSBs[r]->setValue((oldvalue-nebenErtrags[r])/ertrags[r]);
            inited = true;

            if (r==refpos) {
                int ts = getreideIDs.size();
                for (int i=0; i<ts; ++i){
                    Manager->product_cat0[getreideIDs[i]].setPriceExpectation(getreidePEs[i]);
                }
            }

            Manager->product_cat0[prodIDs[r]].setPriceExpectation(oldvalue);
            //QMessageBox::warning(this, tr("Solver Information"),
              //               tr("Bedingungen verletzt"));

        }
        int sz = prodIDs.size();
        for (int i=0; i<sz; ++i){
            if(peSpiels[i])
                peDSBs[i]->setEnabled(true);
        }

        if (!b) return;

        if (*peVar==0) {
                double xxx = (*manPEs)[r] = peDSBs[r]->value()*ertrags[r]+nebenErtrags[r]-costs[r];
                peSS->cell(r,6)->setFormula(QLocale().toString(xxx,'f',2));
        }

        if (r==refpos) {
            updateIndexLab();
            /*  schon aktualisiert
            int ts = getreideIDs.size();
            for (int i=0; i<ts; ++i){
                ;//Manager->product_cat0[getreideIDs[i]].setPriceExpectation(getreidePrices[getreideIDs[i]]*preisIndex);
            }
            //*/
        }
}

Title02::Title02(Qt::AlignmentFlag align, QFont f, QWidget*w):QLabel(w){
    this->setAlignment(align);
    this->setFont(f);
}
