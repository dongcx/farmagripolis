#ifndef PLOTDRAWITEM_H
#define PLOTDRAWITEM_H

#include <QGraphicsItem>
#include "Landwindow.h"

class Landwindow;
class PlotItem:public QGraphicsItem {
public:
    enum PlotType {OWNER, SOILTYPEFREE, FARMTYPE};
    enum Modus {REGION, FARM};
    static const int w = 8;
    static const int h= 8;
    static const int x0= -4;
    static const int y0= -4;

    explicit PlotItem(int,QColor,bool,bool,Landwindow *);

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void setColor(QColor);
    void setIsFarmstead(bool);
    void setIsFirstFarm(bool);

    void setIsOwnland(bool);
    void setModus(int);
    void setChosenFarmid(int);

    void setType(int);
    void setFree(bool);
    void setBorder(bool);

protected:
    void setImage(QImage *);
    void mousePressEvent(QGraphicsSceneMouseEvent *);
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    //void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    int ptype;
    bool isFree;
    bool withBorder;
    QColor color;
    bool isFarmstead;
    bool isFirstFarm;
    QImage *img;
    PlotItem *lastItem;
    Landwindow *landWin;

    Modus modus;
    int chosenFarmid;
    bool isOwnland;
};

#endif // PLOTDRAWITEM_H
