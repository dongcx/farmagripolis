#include "Mydatwindow.h"
#include <QDockWidget>
#include <QAction>
#include <QToolBar>
#include <QGraphicsView>
#include <QGraphicsScene>
#include "RegMessages.h"
#include "RegPlot.h"

#include <QVBoxLayout>
#include <QLineEdit>
#include <QPicture>
#include <QCoreApplication>

#include <QColorDialog>
#include "legendIcon.h"

#include <QPushButton>
#include <QSignalMapper>
#include <QGroupBox>
#include <QDialogButtonBox>

#include <QSettings>
#include <QPrintDialog>
#include <QList>
#include <QStatusBar>

#include <qwt_plot_zoomer.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_picker.h>

#include <qcheckbox.h>
#include <qradiobutton.h>
#include <QDir>

#include <qspinbox.h>
#include "mmspinbox.h"

static void SetBackgroundImage ( QWidget *fp_widget, const QPixmap *fp_image )
    {
       QPalette newPalette = fp_widget->palette ( );
       newPalette.setBrush
       (
          QPalette::Base,
          QBrush ( *fp_image )
       );

       newPalette.setBrush
       (
          QPalette::Background,
          QBrush ( *fp_image )
       );

       fp_widget->setPalette ( newPalette );
    }

static QBrush BrushSetAlpha ( QBrush f_brush, const int f_alpha )
    {
       QColor color = f_brush.color ( );
       color.setAlpha ( f_alpha );
       f_brush.setColor ( color );

       return f_brush;
    }

static void SetTransparency ( QWidget *fp_widget, bool f_transparent )
    {
       int alpha ( 255 );

       if ( f_transparent )
          alpha = 0;

       QPalette newPalette = fp_widget->palette ( );

       newPalette.setBrush ( QPalette::Base, BrushSetAlpha ( newPalette.base ( ), alpha ) ); // sollte rausgeschmissen werden, wenn Textfelder sonst zu unleserlich sind.
       newPalette.setBrush ( QPalette::Background, BrushSetAlpha ( newPalette.background ( ), alpha ) );

       fp_widget->setPalette ( newPalette );
    }

class Zoomer: public QwtPlotZoomer
{
public:
    Zoomer(int xAxis, int yAxis, QwtPlotCanvas *canvas):
        QwtPlotZoomer(xAxis, yAxis, canvas)
    {
        setTrackerMode(QwtPicker::AlwaysOff);
        setRubberBand(QwtPicker::NoRubberBand);

        // RightButton: zoom out by 1
        // Ctrl+RightButton: zoom out to full size

        setMousePattern(QwtEventPattern::MouseSelect2,
            Qt::RightButton, Qt::ControlModifier);
        setMousePattern(QwtEventPattern::MouseSelect3,
            Qt::RightButton);
    }
};

void Mydatwindow::initXs(){
    maxX=maxIter=gg->RUNS;
    minX=minIter=-1;
    maxXEK=maxIter=gg->RUNS;
    minXEK=minIter=-1;
}

void Mydatwindow::initHasDialogs(){
    for (int i=0; i<numCWidgets; ++i){
       hasDialogs.push_back(false);;
    }
}

Mydatwindow::Mydatwindow(QWidget *parent, Mainwindow* p): gridGroup(0),hasCWidgets(false),
     par(p), QMainWindow(parent) {
    setWindowTitle(tr("Daten--Mein Betrieb"));

    printer = new QPrinter(QPrinter::HighResolution);
    //printer->setOutputFileName("tmyd.pdf");

    classnames[1]="Veredlung";//Pig/Poultry";
    classnames[2]="Futterbau";//Greenland";
    classnames[3]="Marktfruchtbau";//Arable";
    classnames[4]="Gemischt";//Mixed";
    classnames[5]="Geschlo�en";//Mixed";

    legnames[0]="Einzelbetrieb im Nebenerwerb";
    legnames[1]="Einzelbetrieb im Haupterwerb";
    legnames[2]="Juristische Personen";
    legnames[3]="Personengesellschaften";

    createActions();
    createToolBars();

    //createStatusBar();

    nFarmtypes=4;

    minIter=-1;
    maxIter=25;

    initHasDialogs();
    initPlotColors();

    cWidget=new QWidget(this);

    clout = new QVBoxLayout;//(cWidget);

    /*QHBoxLayout *hlout = new QHBoxLayout;
    hlout->addStretch();
    hlout->addLayout(clout);
    hlout->addStretch();
//*/
    oldlayer= layerComboBox->currentIndex();

    inited=false;

    cWidget->setLayout(clout);//hlout);
    //SetTransparency(cWidget, true);
    setCentralWidget(cWidget);

    setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
    //resize(800,500);

    maxRunde=0;
    nSoils=0;

    setWindowIcon(QPixmap(":/images/house-md.png"));
    fwReady=false;
}

/*
void Mydatwindow::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    QPen p = painter.pen();
    p.setWidth(5);
    p.setColor(QColor("blue"));

    painter.setPen(p);
    painter.drawRect(0,0,width(),height());
}
//*/

void Mydatwindow::initPlotColors(){
    QSettings dataSettings("IAMO, BSE", "Farm AgriPoliS");
        gridColor=dataSettings.value(QString("myGridColor"), Qt::gray).value<QColor>();
        canvasColor=dataSettings.value(QString("myCanvasColor"), Qt::darkCyan).value<QColor>();

        gridColorEK=dataSettings.value(QString("myGridColorEK"), Qt::gray).value<QColor>();
        canvasColorEK=dataSettings.value(QString("myCanvasColorEK"), Qt::darkCyan).value<QColor>();
}

void Mydatwindow::initStyles(){
    int k= QwtSymbol::NoSymbol;
    k++;

    vector<QwtSymbol::Style> vs;
    for (int i=0;i<MAXCURVES;++i){
        vs.push_back(static_cast<QwtSymbol::Style>(k++));
    }

    QSettings dataSettings("IAMO, BSE", "Farm AgriPoliS");

    esuStyles.resize(MAXCURVES);
    ekStyles.resize(MAXCURVES);
    for (int i=0;i<MAXCURVES;++i){
        esuStyles[i]=static_cast<QwtSymbol::Style>(dataSettings.value(QString("esuStyle_")+i, i+1).value<int>());
        ekStyles[i]=static_cast<QwtSymbol::Style>(dataSettings.value(QString("ekStyle_")+i, i+1).value<int>());
    }
}

void Mydatwindow::initCurveColors(){
    vector<QColor> colors;
    for (int k=0;k<MAXCURVES;++k){
        colors.push_back(QColor((k%2)*255, ((k>>1)%2)*255, ((k>>2)%2)*255));
    }
    QSettings dataSettings("IAMO, BSE", "Farm AgriPoliS");
    int t,sz;
    sz=numCurves+1;
    esuColors.resize(sz);
    for(t=0;t<sz;++t){
        esuColors[t]=dataSettings.value(QString("esuColor_")+t,colors[t]).value<QColor>();
    }
    sz=numCurves+1; //ek + ranking
    ekColors.resize(sz);
    for(t=0;t<sz;++t){
        ekColors[t]=dataSettings.value(QString("ekColor_")+t,colors[t]).value<QColor>();
    }
}

static bool esuCompare(DataReg1 x1, DataReg1 x2){
    return x1.esusize>x2.esusize;
}

static bool ekCompare(DataReg2 x1, DataReg2 x2){
    return x1.ek>x2.ek;
}

void Mydatwindow::initMydata(){
    //maxRunde=gg->RUNS-1;
    //iterSBox->setMaximum(maxRunde);
    iter=0;

    esuYtitles.push_back("Alle Betriebe");
    esuYtitles.push_back("Gr��te 25\% Betriebe");
    esuYtitles.push_back("Mein Betrieb");
    esuYtitles.push_back("Kleinste 25\% Betriebe");
    esuYtitles.push_back("Mein Rang");

    esuYaxes.push_back(QwtPlot::yLeft);
    esuYaxes.push_back(QwtPlot::yLeft);
    esuYaxes.push_back(QwtPlot::yLeft);
    esuYaxes.push_back(QwtPlot::yLeft);
    esuYaxes.push_back(QwtPlot::yRight);

    ekYtitles.push_back("Alle Betriebe");
    ekYtitles.push_back("Beste 25\% Betriebe");
    ekYtitles.push_back("Mein Betrieb");
    ekYtitles.push_back("Schlechteste 25\% Betriebe");
    ekYtitles.push_back("Mein Rang");

    ekYaxes.push_back(QwtPlot::yLeft);
    ekYaxes.push_back(QwtPlot::yLeft);
    ekYaxes.push_back(QwtPlot::yLeft);
    ekYaxes.push_back(QwtPlot::yLeft);
    ekYaxes.push_back(QwtPlot::yRight);

    initCurveColors();
    initStyles();
    initXs();

    QStringList sl;
    sl<<"Bodenverm�gen" << "Anlageverm�gen" << "Geldverm�gen" <<"" <<"Summe";

    for (QStringList::ConstIterator it=sl.begin(); it!=sl.end(); ++it){
        sAktivas.push_back(*it);
    }

    sl.clear();
    sl<<"Eigenkapital" << "Fremdkapital" << "" <<"" <<"Summe";

    for (QStringList::ConstIterator it=sl.begin(); it!=sl.end(); ++it){
        sPassivas.push_back(*it);
    }

    QStringList slG;
    slG<<"Ackerbau" <<"Milch" <<"Rinder" << "Schweine" << "Au�erbetriebliche Arbeit"
            <<"Direktzahlungen" << "Sonstige Ertr�ge";
    lenG = slG.size();

    QStringList slV;
    slV<<"Ackerbau" <<"Milch" <<"Rinder" << "Schweine" << "L�hne" <<"Unterhaltung"
            << "Abschreibungen" << "Gemeinkosten" << "Zupachtung"
            << "Sonstige Aufwendungen";
    lenV = slV.size();

    QStringList slF;
    slF<<"Zinsertr�ge" << "Zinsaufwendungen" ;
    lenF = slF.size();

    for (int i=0; i<lenV+2;++i){
        if (i<lenV+1) {
            if (i>=lenG) slG <<"";
            if (i>=lenV) slV << "";
            if (i>=lenF) slF << "";
        }else {
            slG << "Summe";
            slV << "Summe";
            slF << "Summe";
        }
    }

    for (QStringList::ConstIterator it=slG.begin(); it!=slG.end(); ++it){
        sErtraege.push_back(*it);
    }
    for (QStringList::ConstIterator it=slV.begin(); it!=slV.end(); ++it){
        sAufwendungen.push_back(*it);
    }
    for (QStringList::ConstIterator it=slF.begin(); it!=slF.end(); ++it){
        sFinanz.push_back(*it);
    }

    //Kontoauszug
    slG.clear();
    slG<<"Ackerbau" <<"Milch" <<"Rinder" << "Schweine" << "Au�erbetriebliche Arbeit"
            <<"Direktzahlungen" << "Zinsertr�ge" << "Sonstige Einnahmen";// << "totalincome(nicht in summe)";
        //<<"totalIncome" <<"withdrawal" <<"(Dis-)Invest"<< " "<< " ";
    slV.clear();
    slV<<"Ackerbau" <<"Milch" <<"Rinder" << "Schweine" << "L�hne" <<"Unterhaltung"
            << "Annuit�t" << "Gemeinkosten" << "Zupachtung" << "Zinsaufwendung (kurzfristiges FK)"
            << "Entnahmen" << "Sonstige Ausgaben" ;// <<"(dis-)Invest (nicht in summe)";
    lenKG=slG.size();
    lenKV=slV.size();
    for (int i=0; i<lenKV+2;++i){
        if (i<lenKV+1) {
            if (i>=lenKG) slG <<"";
            if (i>=lenKV) slV << "";
         }else {
            slG << "Summe";
            slV << "Summe";
         }
    }

    for (QStringList::ConstIterator it=slG.begin(); it!=slG.end(); ++it){
        sHabens.push_back(*it);
    }
    for (QStringList::ConstIterator it=slV.begin(); it!=slV.end(); ++it){
        sSolls.push_back(*it);
    }
    #define KONTOAUSZUG0
    #undef KONTOAUSZUG0
    #ifdef KONTOAUSZUG0
    for (QStringList::ConstIterator it=sl.begin(); it!=sl.end(); ++it){
        sKAs.push_back(*it);
    }

    vector<int> kaCols; // welche Spalte
    kaCols.push_back(1);
    kaCols.push_back(2);
    kaCols.push_back(2);
    kaCols.push_back(1);
    kaCols.push_back(1);

    kaColsI.push_back(kaCols);
    #endif

    altKontos.push_back(liquidity0); //liquidity by o-te periode
    //Betriebsspiegel
    inited=true;
}

/*
struct DataMy {
    double landAssets;
    double totalFixedAssets;
    double liquidity;
    double equityCapital;
    double borrowedCapital;

    double totalLand;
    double zugepachtLand;

    //Gewinn-/Verlustrechnung
    double ackerbauG;
    double milchG;
    double rinderG;
    double schweineG;
    double offfarmlabG;
    double dirZahlungenG;

    double ackerbauV;
    double milchV;
    double rinderV;
    double schweineV;
    double loehneV ;
    double unterhaltungV;
    double abschreibungV;
    double gemeinkostenV;
    double zupachtungV;

    double zinsG;
    double zinsV;

    double totalIncome;

    //KA
    double resEcShare;
    double withdraw;
};
//*/

void Mydatwindow::getLiquidity0(){
   list<RegFarmInfo*>::iterator it = Manager->FarmList.begin();
   while (it != Manager->FarmList.end()){
        RegFarmInfo * fp = (*it);
        if (fp->getFarmId()==0) { // der Spieler
            liquidity0 = fp->getLiquidity();
            resEcShare0 = fp->FarmInvestList->getTotalResEcShareWithoutLabour();
            break;
        }
        ++it;
    }
}
static bool pdscompare(PlotData d1, PlotData d2){
    if (d1.row==d2.row)
        return d1.col<d2.col;
    else
        return d1.row<d2.row;
}

void Mydatwindow::collectData(list<RegFarmInfo*> li,
    vector<DataReg1> &v, vector<DataReg2> &v2, DataMy &md){
    list<RegFarmInfo*>::iterator it = li.begin();
    DataReg1 rd;
    DataReg2 rd2;
    while (it != li.end()){
        //ESU
        rd.farmid=(*it)->getFarmId();
        rd.esusize = (double)(*it)->getStandardGrossMargin()/gg->ESU;
        v.push_back(rd);

        rd2.farmid = rd.farmid;
        rd2.ek = (*it)->getEquityCapital();
        v2.push_back(rd2);

        RegFarmInfo * fp = (*it);
        RegProductList *pList = fp->getProductList();

        if (fp->getFarmId()==0) { // der Spieler
              md.isClosed = fp->isClosed();

              //Schlussbilanz
              md.landAssets = fp->getLandAssets();
              md.totalFixedAssets = fp->getAssets()-md.landAssets;//->getAssetsProdWoLand();
              md.liquidity = fp->getLiquidity();

              md.equityCapital = fp->getEquityCapital();
              md.borrowedCapital = fp->getLtBorrowedCapital();

              //Gewinn-/Verlustrechnung
              double s=0;
              #define HiCLOSED md.isClosed ? 0 :
              md.ackerbauG = HiCLOSED pList->getReturnOfGroup(0); //Getreide
              md.milchG =HiCLOSED pList->getReturnOfName(gg->sMILK);//"DAIRY");

              int nsz=gg->CattleNames.size();
              double rinderGw=0 ;
              for (int i=0; i<nsz; ++i){
                  rinderGw+=pList->getReturnOfName(gg->CattleNames[i]);
              }
              md.rinderG = HiCLOSED rinderGw;
              //md.rinderG =HiCLOSED pList->getReturnOfName(gg->sIntCattle)//"INT_CATTLE")
                //            + pList->getReturnOfName(gg->sExtCattle);//"EXT_CATTLE");
              md.schweineG =HiCLOSED pList->getReturnOfName(gg->sSOW)    //"SOWS")
                            + pList->getReturnOfName(gg->sFATTENINGPIGS);//"FATTENING_PIGS");

              //md.dirZahlungenG = (gg->FULLY_DECOUPLING)? ((gg->LP_MOD) ? fp->getUnitsOfProduct(gg->TOTAL_PREM_MODULATED) : 0):0;
              md.dirZahlungenG =HiCLOSED (gg->LP_MOD) ? fp->getUnitsOfProduct(gg->TOTAL_PREM_MODULATED) : 0;
              md.sonstG =HiCLOSED pList->getReturnOfType(gg->PRODTYPE)  //????
                          -md.ackerbauG -md.milchG -md.schweineG -md.rinderG
                          -md.dirZahlungenG ;

              /*md.offfarmlabG = pList->getReturnOfType(gg->VAROFFARMLABTYPE)
                               + fp->FarmInvestList->getInvestmentsOfCatalogNumber(gg->FIXED_OFFFARM_LAB)
                                    *(-fp->FarmInvestList->getAcquisitionCostsOfNumber(gg->FIXED_OFFFARM_LAB));
                                    //*/
              md.offfarmlabG =  //md.isClosed ? fp->getOppcost() :
                                fp->getFarmFactorRemunerationFix()
                                +fp->getFarmFactorRemunerationVar();

              md.ackerbauV = HiCLOSED md.ackerbauG - pList->getGrossMarginOfGroup(0);
              md.milchV =HiCLOSED md.milchG - pList->getGMofName(gg->sMILK);//"DAIRY");

              nsz=gg->CattleNames.size();
              double rinderVl=0 ;
              for (int i=0; i<nsz; ++i){
                  rinderVl+=pList->getGMofName(gg->CattleNames[i]);
              }
              md.rinderV = HiCLOSED rinderVl;

              //md.rinderV =HiCLOSED md.rinderG - pList->getGMofName(gg->sIntCattle) //"INT_CATTLE")
              //                        - pList->getGMofName(gg->sExtCattle);//"EXT_CATTLE");
              md.schweineV =HiCLOSED md.schweineG - pList->getGMofName(gg->sSOW)//"SOWS")
                                          - pList->getGMofName(gg->sFATTENINGPIGS);//"FATTENING_PIGS");
              md.sonstV =HiCLOSED pList->getReturnOfType(gg->PRODTYPE)
                         - pList->getGrossMarginOfType(gg->PRODTYPE)
                         -md.ackerbauV -md.milchV -md.rinderV -md.schweineV;

              md.loehneV =  HiCLOSED  fp->getFarmHiredLabourFixPay() +
                            fp->getFarmHiredLabourVarPay();

              md.unterhaltungV =HiCLOSED fp->getTotalMaintenance();
              md.abschreibungV =HiCLOSED fp->getDepreciation();
              md.gemeinkostenV =HiCLOSED fp->getOverheads();
              md.zupachtungV   =HiCLOSED fp->getFarmRentExp();


              md.tacV =HiCLOSED fp->getFarmTac();
              md.distCostsV =HiCLOSED fp->getFarmDistanceCosts();

              md.zinsG = pList->getReturnOfType(gg->ST_EC_INTERESTTYPE); //????????
              md.zinsV = fp->getLtInterestCosts() + fp->getStInterestCosts();

              md.totalIncome = fp->getTotalIncome();


              //Pachtbilanz
              md.totalLand = fp->getLandInput();
              s=0;
              for (int tt=0; tt<nSoils; ++tt){
                 s += fp->getNewRentedLandOfType(tt);
              }
              md.zugepachtLand = s;

              //Kontoauszug
              md.resEcShare = fp->FarmInvestList->getTotalResEcShareWithoutLabour();
              md.withdraw = fp->getWithdrawal();
              md.annuity = fp->getAnnuity();
              md.st_zins = HiCLOSED fp->getStInterestCosts();
                #undef HiCLOSED

              //Betriebsspiegel
              md.farmage = fp->getFarmAge();
              md.farmtype = fp->getFarmClass();
              md.legaltype = fp->getLegalType();

              double labsub2 = ((Manager->InvestCatalog)[gg->FIXED_HIRED_LAB].getLabourSubstitution() * 2);
              md.fam_AK = fp->labour->getFamilyLabour()/labsub2;
              //cout << gg->tIter << " " << md.fam_AK << "\n";
              double p = Manager->Market->getProductCat()[3].getRealPrice();//ind["V_OFF_FARM_LAB"]].
              double offfinc = fp->getFarmFactorRemunerationFix()+
                   fp->getFarmFactorRemunerationVar();
              double lab_inp = fp->labour->getLabourInputHours();
              md.lohn_AK = (lab_inp + offfinc/p)/labsub2-md.fam_AK;
              //cout << gg->tIter << ": " << md.lohn_AK << "\n";
              for (int i=0; i<nSoils; ++i){
                    fp->isClosed() ? md.pachtPreisOfSoils.push_back(0) : md.pachtPreisOfSoils.push_back(fp->getAvRentOfType(i));
              }

              vector<vector<double> > rtAreasSoils;
              rtAreasSoils.resize(nSoils);
              vector<double> sums, owns, rents;
              sums.resize(nSoils);
              owns.resize(nSoils);
              rents.resize(nSoils);
              for (int i=0; i<nSoils; ++i){
                    rtAreasSoils[i].resize(gg->MAX_CONTRACT_LENGTH);
              }

              list<RegPlotInfo* >  plist = fp->getPlotList();
              list<RegPlotInfo*>::iterator pit = plist.begin();
              vector<int> ntypes;ntypes.resize(nSoils);
              while (pit != plist.end()){
                    if ((*pit)->getFarmId()!=0) {++pit;continue;}
                    int t = (*pit)->getSoilType();
                    int rt = (*pit)->getContractLength();
                    /*if (rt <0 || rt > gg->MAX_CONTRACT_LENGTH)
                        cout << "??  " <<  rt << endl;
                    //*/
                    if (rt==-1) { //rt = gg->MAX_CONTRACT_LENGTH; //?? ownland immer max-contract-length
                        owns[t]+=gg->PLOT_SIZE;
                    }else{
                        rtAreasSoils[t][rt-1]+= gg->PLOT_SIZE;
                        rents[t]+=gg->PLOT_SIZE;
                    }
                    sums[t]+=gg->PLOT_SIZE;
                    ntypes[t]++;
                    ++pit;
              }

              pit = plist.begin();
              PlotData pd;
              vector<PlotData> pds;
              while (pit != plist.end()){
                    if ((*pit)->getFarmId()!=0) {++pit;continue;}
                    RegPlotInfo *plot = *pit;
                    int rt = plot->getContractLength();
                    if (rt==-1) {++pit; continue; } //only rented plots

                    pd.col = plot->getCol();
                    pd.row = plot->getRow();
                    int t = plot->getSoilType();
                    pd.price = plot->getRentPaid()/gg->PLOT_SIZE;

                    pd.soiltype = t;
                    pd.resttime = rt;

                    pds.push_back(pd);
                    ++pit;
              }
              sort(pds.begin(),pds.end(), pdscompare);
              md.plotDatas = pds; // contracts of plots

              md.landSums = sums;
              md.ownLands = owns;
              md.rentLands = rents;
              md.restdauerAreasOfSoils = rtAreasSoils;

              invList = *(fp->FarmInvestList);
              nInvests = Manager->InvestCatalog.size();
              getStalls(fp, &md);
              getMachines(fp,&md);

        }
        ++it;
   }
}

void Mydatwindow::updateMydata(){
   if (!inited) initMydata();

   nSoils = gg->NO_OF_SOIL_TYPES;

   vector <DataReg1> v;
   vector<DataReg2> v2;
   DataMy md;

   collectData(Manager->FarmList, v, v2,md);
   collectData(Manager->RemovedFarmList, v, v2, md);

   /*
   list<RegFarmInfo*>::iterator it = Manager->FarmList.begin();
   vector <DataReg1> v;
   vector<DataReg2> v2;
   DataReg1 rd;
   DataReg2 rd2;
   DataMy md;
   while (it != Manager->FarmList.end()){
        }
        ++it;
   }
   //*/
   sort(v.begin(),v.end(),esuCompare);
   regDatavec2.push_back(v);

   sort(v2.begin(),v2.end(),ekCompare);
   regDatavec3.push_back(v2);

   md.EKtotal = v2.size();
   DataReg2 r2 = v2[0];
   {int rank=0 ;
   while (r2.farmid != 0){
       rank++;
       r2 = v2[rank];
    }
   md.EKrank = rank;
   }

   myDatas.push_back(md);
   iterSBox->setMaximum(iter);

   updateESU();
   updateEK();
   updateSB();
   updatePB();
   updateGVR();
   updateKA();
   updateBSpiegel();
   updatePV();
   updateLIQ();

   updateGraphics();
   iter++;
}

void Mydatwindow::getStalls0(RegFarmInfo *farm, DataMy *pmd){
    pmd->stalls.clear();

    map <int, int> invtypCap;
    map <int, int> invtypProdtyp;
    map <int, int> invtypLife;
    map <int, int> invtypNum;

    for(int i=0;i<nInvests;++i){
      invtypNum[i] = 0;
      if (int x =farm->getInvestmentsOfCatalogNumber(i)!=0){
        RegInvestObjectInfo invobj = Manager->InvestCatalog[i];

        invtypNum[invobj.getInvestType()]=x;
        invtypCap[invobj.getInvestType()]=1;//
        invtypLife[invobj.getInvestType()]= invobj.getEconomicLife();
        //farm->getCapacityOfInvestType(i);//x*invobj.getCapacity();
        invtypProdtyp[invobj.getInvestType()]=invobj.getAffectsProductGroup();
      }
    }

     map<int,int>::iterator it= invtypCap.begin();


     while (it!=invtypCap.end()){
        stringstream is2;

        invtypCap[(*it).first]=farm->getCapacityOfType((*it).first);
        int t = invtypProdtyp[(*it).first];

        if (t<=0)   { //Machinen oder AK
            it++;
            continue;
        }

        stalldata sdat;

        vector<string> ss;
        Manager->Market->getNamesOfGroup(t,ss);

        is2 << ss[0];
        sdat.name=is2.str();

        sdat.used =farm->getUnitsProducedOfGroup1(invtypProdtyp[(*it).first]);
        sdat.capacity = static_cast<int>((*it).second);
        sdat.num = invtypNum[(*it).first];
        sdat.age = farm->getAverageAgeOfInvestmentsOfProdtype(t);
        sdat.life = invtypLife[(*it).first];

        pmd->stalls.push_back(sdat);
        it++;
      }
}

static bool invcompare(RegInvestObjectInfo inv1, RegInvestObjectInfo inv2){
    int t1, t2, c1,c2;
    t1=inv1.getAffectsProductGroup();
    t2=inv2.getAffectsProductGroup();
    c1= inv1.getCapacity();
    c2=inv2.getCapacity();
    if (t1<t2) return true;
    else if (t1>t2) return false;
    else return (c1>c2);
}

void Mydatwindow::getStalls(RegFarmInfo *farm, DataMy *pmd){
    pmd->stalls.clear();

    vector<RegInvestObjectInfo> invs = invList.getInvests();
    sort(invs.begin(),invs.end(), invcompare);

    map<int,int> rests;
    map<int,bool> assigneds;

    int sz = invs.size();
    for(int i=0;i<sz;++i){
      RegInvestObjectInfo invobj = invs[i];

      int t = invobj.getAffectsProductGroup();
      if (t<=0)   { //Machinen oder AK
            continue;
      }

      stalldata sdat;
      sdat.name=invobj.getName();
      //sdat.used = farm->getUnitsProducedOfGroup1(t);
      sdat.capacity = invobj.getCapacity();

      if (assigneds.find(t)==assigneds.end()) {
        rests[t] = farm->getUnitsProducedOfGroup2(t);
        assigneds[t] = true;
      }
      sdat.used = farm->isClosed()? 0: rests[t]>=sdat.capacity ? sdat.capacity: rests[t];
      rests[t] = rests[t]>=sdat.capacity ? rests[t]-sdat.capacity : 0;

      sdat.num = 1;
      sdat.age = invobj.getInvestAge();
      sdat.life = invobj.getEconomicLife();

      pmd->stalls.push_back(sdat);
    }
}

void Mydatwindow::getMachines0(RegFarmInfo *farm, DataMy *pmd){
    pmd->machines.clear();
    int life;

    for(int i=0;i<nInvests;++i){
      int x =farm->getInvestmentsOfCatalogNumber(i);
      if (x<=0) continue;
      RegInvestObjectInfo invobj = Manager->InvestCatalog[i];

      int t = invobj.getAffectsProductGroup();
      if (t!=0) continue; //nur Maschinen

      machinedata mdat;
      mdat.name = invobj.getName();
      mdat.capacity = invobj.getCapacity();
      mdat.num = x;
      mdat.age = invList.getAverageAgeOfInvestmentsOfCatalogNumber(i);
      mdat.life = invobj.getEconomicLife();

      pmd->machines.push_back(mdat);
   }
}

void Mydatwindow::getMachines(RegFarmInfo *farm, DataMy *pmd){
    pmd->machines.clear();

    vector<RegInvestObjectInfo> invs = invList.getInvests();

    int sz = invs.size();
    for(int i=0;i<sz;++i){
      int x =1;
      RegInvestObjectInfo invobj = invs[i];

      int t = invobj.getAffectsProductGroup();
      if (t!=0) continue; //nur Maschinen

      machinedata mdat;
      mdat.name = invobj.getName();
      mdat.capacity = invobj.getCapacity();
      mdat.num = x;
      mdat.age = invs[i].getInvestAge();
      mdat.life = invobj.getEconomicLife();

      pmd->machines.push_back(mdat);
   }
}

void Mydatwindow::updateSB(){
   vector<double> dsA, dsP;
   dsA.push_back(myDatas[iter].landAssets);
   dsA.push_back(myDatas[iter].totalFixedAssets);
   dsA.push_back(myDatas[iter].liquidity);
   dsA.push_back(0);
   dsA.push_back(myDatas[iter].landAssets + myDatas[iter].totalFixedAssets + myDatas[iter].liquidity);

   dsP.push_back(myDatas[iter].equityCapital);
   dsP.push_back(myDatas[iter].borrowedCapital);
   dsP.push_back(0);dsP.push_back(0);
   dsP.push_back(myDatas[iter].equityCapital + myDatas[iter].borrowedCapital);

   aktivasI.push_back(dsA);
   passivasI.push_back(dsP);
}

void Mydatwindow::updatePB(){
   vector<double> ds;
   double auslaufLand=0;
   if (iter==0) auslaufLand=0;
   else auslaufLand = myDatas[iter-1].totalLand + myDatas[iter].zugepachtLand
                            -myDatas[iter].totalLand;

   ds.push_back(iter);
   ds.push_back(auslaufLand);
   ds.push_back(myDatas[iter].zugepachtLand);
   ds.push_back(myDatas[iter].totalLand);

   pbDatasI.push_back(ds);
}

vector<int> Mydatwindow::getGVRsizes(){
    vector<int> v;
    v.push_back(lenG);
    v.push_back(lenV);
    v.push_back(lenF);

    return v;
}

void Mydatwindow::updateGVR(){
   vector<double> ds1;
   double s1=0, s=0;
   DataMy md = myDatas[iter];

   ds1.push_back(md.ackerbauG);
   s1 += md.ackerbauG;
   ds1.push_back(md.milchG);
   s1 += md.milchG;
   ds1.push_back(md.rinderG);
   s1 += md.rinderG;
   ds1.push_back(md.schweineG);
   s1 += md.schweineG;
   ds1.push_back(md.offfarmlabG);
   s1 += md.offfarmlabG;
   ds1.push_back(md.dirZahlungenG);
   s1 += md.dirZahlungenG;
   ds1.push_back(md.sonstG);
   s1 += md.sonstG;

   int len = ds1.size();
   while (len < lenV+1){
        ds1.push_back(0);  //ausf�llung
        ++len;
   }
   ds1.push_back(s1);
   ertraeges.push_back(ds1);

   s += s1;

   ds1.clear(); s1=0;
   ds1.push_back(-md.ackerbauV);
   s1 += -md.ackerbauV;
   ds1.push_back(-md.milchV);
   s1 += -md.milchV;
   ds1.push_back(-md.rinderV);
   s1 += -md.rinderV;
   ds1.push_back(-md.schweineV);
   s1 += -md.schweineV;
   ds1.push_back(-md.loehneV);
   s1 += -md.loehneV;
   ds1.push_back(-md.unterhaltungV);
   s1 += -md.unterhaltungV;
   ds1.push_back(-md.abschreibungV);
   s1 += -md.abschreibungV;
   double td = md.gemeinkostenV + md.tacV + md.distCostsV;
   ds1.push_back(-td);//md.gemeinkostenV);
   s1 += -td;//md.gemeinkostenV;
   ds1.push_back(-md.zupachtungV);
   s1 += -md.zupachtungV;
   //ds1.push_back(-md.tacV);
   //s1 += -md.tacV;
   //ds1.push_back(-md.distCostsV);
   //s1 += -md.distCostsV;
   ds1.push_back(-md.sonstV);
   s1 += -md.sonstV;

   len = ds1.size();
   while (len < lenV+1){
        ds1.push_back(0);  //ausf�llung
        ++len;
   }
   ds1.push_back(s1);
   aufwendungens.push_back(ds1);

   s += s1;

   ds1.clear(); s1=0;
   ds1.push_back(md.zinsG);
   s1 += md.zinsG;
   ds1.push_back(-md.zinsV);
   s1 += -md.zinsV;

   len = ds1.size();
   while (len < lenV+1){
        ds1.push_back(0);  //ausf�llung
        ++len;
   }
   ds1.push_back(s1);
   finanzs.push_back(ds1);

   s += s1;

   /*
   ds1.push_back(s1);
   ds2.push_back(s2);
   ds3.push_back(s3);

   ertraeges.push_back(ds1);
   aufwendungens.push_back(ds2);
   finanzs.push_back(ds3);
//*/
   gewinns.push_back(s);//-md.totalIncome);
}

vector<int> Mydatwindow::getKAsizes(){
    vector<int> v;
    v.push_back(lenKG);
    v.push_back(lenKV);

    return v;
}

void Mydatwindow::updateKA(){
   if (iter!=0)
        altKontos.push_back(neuKontos[iter-1]);
   double neukt=altKontos[iter];

   vector<double> ds1;
   double s1=0, s=0;
   DataMy md = myDatas[iter];

   ds1.push_back(-md.ackerbauV);
   s1 += -md.ackerbauV;
   ds1.push_back(-md.milchV);
   s1 += -md.milchV;
   ds1.push_back(-md.rinderV);
   s1 += -md.rinderV;
   ds1.push_back(-md.schweineV);
   s1 += -md.schweineV;
   ds1.push_back(-md.loehneV);
   s1 += -md.loehneV;
   ds1.push_back(-md.unterhaltungV);
   s1 += -md.unterhaltungV;
   ds1.push_back(-md.annuity);
   s1 += -md.annuity;
   double td = md.gemeinkostenV + md.tacV + md.distCostsV;
   ds1.push_back(-td);//md.gemeinkostenV);
   s1 += -td;//md.gemeinkostenV;
   ds1.push_back(-md.zupachtungV);
   s1 += -md.zupachtungV;
   ds1.push_back(-md.st_zins);
   s1 += -md.st_zins;

   ds1.push_back(-md.withdraw);
   s1 += -md.withdraw;

   double altRES = (iter==0)? resEcShare0 : myDatas[iter-1].resEcShare;
   double delta = altRES- md.resEcShare;
   double sonstKV = md.sonstV - delta -md.annuity +md.abschreibungV + md.zinsV - md.st_zins;

   double transKV = 0;
   if (sonstKV<0) {
        transKV = sonstKV;
        sonstKV = 0;
   }
   ds1.push_back(-sonstKV);
   s1 += -sonstKV;
   //ds1.push_back(delta);

   int len = ds1.size();
   while (len < lenKV+1){
        ds1.push_back(0);  //ausf�llung
        ++len;
   }
   ds1.push_back(s1);
   solls.push_back(ds1);

   s += s1;

   ds1.clear(); s1=0;

   ds1.push_back(md.ackerbauG);
   s1 += md.ackerbauG;
   ds1.push_back(md.milchG);
   s1 += md.milchG;
   ds1.push_back(md.rinderG);
   s1 += md.rinderG;
   ds1.push_back(md.schweineG);
   s1 += md.schweineG;
   ds1.push_back(md.offfarmlabG);
   s1 += md.offfarmlabG;
   ds1.push_back(md.dirZahlungenG);
   s1 += md.dirZahlungenG;
   ds1.push_back(md.zinsG);
   s1 += md.zinsG;
   double sonstKG = md.sonstG + (transKV<0? -transKV:0);
   ds1.push_back(sonstKG);
   s1 += sonstKG;
   //ds1.push_back(md.totalIncome);

   len = ds1.size();
   while (len < lenKV+1){
        ds1.push_back(0);  //ausf�llung
        ++len;
   }
   ds1.push_back(s1);
   habens.push_back(ds1);

   s += s1;
  /*
   vector<double> ds;
   DataMy mdi = myDatas[iter];
   ds.push_back(mdi.totalIncome);
   ds.push_back(-mdi.withdraw);
   double altRES = (iter==0)? resEcShare0 : myDatas[iter-1].resEcShare;
   double delta = altRES- mdi.resEcShare;
   ds.push_back(delta);


   if (iter != 0) {
        vector<int> cols = kaColsI[iter-1];
        kaColsI.push_back(cols);
   }
   if (delta<0) kaColsI[iter][2]=2;
   else kaColsI[iter][2]=1;

   neukt += mdi.totalIncome - mdi.withdraw + altRES-mdi.resEcShare;

   kaDatasI.push_back(ds);
   //*/
   neukt += s;
   neuKontos.push_back(neukt);
}

void Mydatwindow::updateESU(){
   esuYdatas.clear();
   ranks.clear();
   tots.clear();

   vector<double> v[numCurves+1];
   vector<double> vx;

   for (int i=0;i<regDatavec2.size();++i){
        vector<DataReg1> tv= regDatavec2[i];
        int ts = tv.size();

        double totaln=0;
        double totalEsu=0;
        double total25=0;
        double total25Esu=0;
        double klein25=0;
        double klein25Esu=0;

        int n25=ts*25/100;

        double myEsu=0;
        double myRank100=0;
        int myRank = 0;
        for (int j=0;j<ts;++j){
           if (tv[j].farmid==0){
                myEsu= tv[j].esusize;
                myRank = j;
                myRank100 = j*100.0/ts;
           }

           totaln++;
           totalEsu+=tv[j].esusize;

           if (j<n25){
                total25++;
                total25Esu+=tv[j].esusize;
           }

           if (j>=ts-n25){
                klein25++;
                klein25Esu+=tv[j].esusize;
           }
        }

        for (int k=0; k<numCurves+1; ++k){ //??????
            switch(k){
            case 0: v[k].push_back(totalEsu/totaln); break;
            case 1: v[k].push_back(total25Esu/total25);break;
            case 2: v[k].push_back(myEsu);  break;
            case 3: v[k].push_back(klein25Esu/klein25); break;
            case 4: v[k].push_back(myRank100); break;
            default: ;
            }
        }

        vx.push_back(i);
        ranks.push_back(myRank100);
        tots.push_back(ts);

    }
    esuXdata=vx;

    for (int i=0;i<numCurves+1;++i){
        esuYdatas.push_back(v[i]);
    }
}

void Mydatwindow::updateEK(){
   ekYdatas.clear();
   rankEKs.clear();
   totEKs.clear();

   vector<double> v[numCurves+1]; //mit Ranking
   vector<double> vx;

   for (int i=0;i<regDatavec3.size();++i){
        vector<DataReg2> tv= regDatavec3[i];
        int ts = tv.size();

        double totaln=0;
        double totalek=0;
        double total25=0;
        double total25ek=0;
        double klein25=0;
        double klein25ek=0;

        int n25=ts*25/100;

        double myek=0;
        double myrank100=0;
        int myrank=0;
        for (int j=0;j<ts;++j){
           if (tv[j].farmid==0){
                myek= tv[j].ek;
                myrank = j;
                myrank100 = j*100./ts; //prozent
           }

           totaln++;
           totalek+=tv[j].ek;

           if (j<n25){
                total25++;
                total25ek+=tv[j].ek;
           }

           if (j>=ts-n25){
                klein25++;
                klein25ek+=tv[j].ek;
           }
        }

        for (int k=0; k<numCurves+1; ++k){ //??????
            switch(k){
            case 0: v[k].push_back(totalek/totaln); break;
            case 1: v[k].push_back(total25ek/total25);break;
            case 2: v[k].push_back(myek);  break;
            case 3: v[k].push_back(klein25ek/klein25); break;
            case 4: v[k].push_back(myrank100); break;
            default: ;
            }
        }
        totEKs.push_back(ts);
        rankEKs.push_back(myrank);
        vx.push_back(i);

    }
    ekXdata=vx;

    for (int i=0;i<numCurves+1;++i){
        ekYdatas.push_back(v[i]);
    }
}

void Mydatwindow::updateBSpiegel(){
    BSdat bd;
    DataMy dm = myDatas[iter];
    bd.isClosed = dm.isClosed;
    bd.ek = dm.equityCapital;
    bd.EKrank = dm.EKrank;
    bd.EKtotal = dm.EKtotal;
    bd.farmage = dm.farmage;
    bd.farmtype = dm.farmtype;
    bd.legaltype = dm.legaltype;
    bd.farmnr = 0 ;  //immer 0!?
    bd.liq = dm.liquidity;
    bd.fam_AK = dm.fam_AK;
    bd.lohn_AK = dm.lohn_AK;
    bd.machines = dm.machines;
    bd.stalls = dm.stalls;
    bd.pachtPreisOfSoils = dm.pachtPreisOfSoils;
    bd.restdauerAreasOfSoils = dm.restdauerAreasOfSoils;
    bd.landSums = dm.landSums;
    bd.ownLands = dm.ownLands;
    bd.rentLands = dm.rentLands;
    bsdats.push_back(bd);

}

void Mydatwindow::updateGraphics(){
   fwReady=false;
   iterSBox->setValue(iter);
   if (!hasCWidgets) {
       createCWidgets();
       hasCWidgets=true;
   }
  //else
 {
   for (int i=0; i<numCWidgets; ++i){
     if (i==0) {
        Plot* tp;
        tp=static_cast<Plot*>(clout->itemAt(i)->widget());
        tp->updatePlot();
        //continue;
     }else if (i==1) {
        Plot* tp;
        tp=static_cast<Plot*>(clout->itemAt(i)->widget());
        tp->updatePlot();
        //continue;
     }

     Farmwidget *fw = static_cast<Farmwidget*>(tabs[i]);
     fw->updateData();
   }
  }
  fwReady=true;
}

void Mydatwindow::updatePV(){
    DataMy dm = myDatas[iter];

    pvdats.push_back(dm.plotDatas);
}

void Mydatwindow::updateLIQ(){
    DataMy dm = myDatas[iter];

    liqdats.push_back(dm.liquidity);
}

void Mydatwindow::closeEvent(QCloseEvent* event){
    emit hi(true);
    QMainWindow::closeEvent(event);
}

void Mydatwindow::updatePlot(){
    //updateData();
    updateGraphics();

}

void Mydatwindow::createCWidgets(){
    tabs.clear();
    //clout->addStretch();

      Plot *p= new Plot(3, this,0); //esu 3
      tabs.push_back(p);
      clout->addWidget(p);

      Plot *pp= new Plot(6, this,0); //ek 6
      tabs.push_back(pp);
      clout->addWidget(pp);

      //*/
    Farmwidget *fw;
    for (int i=0;i<7;++i){ //au�er graphiken (SB, GVR, PB, KA, BS, PV, LIQ)
        fw= new Farmwidget(i,this);
        tabs.push_back(fw);
        clout->addWidget(fw);
        //clout->setAlignment(fw,Qt::AlignVCenter);
    }
    //clout->addStretch();

    for (int i=0;i<numCWidgets;++i){
        if (i!=oldlayer) tabs[i]->hide();
    }
}

 
void Mydatwindow::enableZoomMode(bool on)
{
    //d_panner->setEnabled(on);
    /*
    for (int i=0; i<numCWidgets ; ++i){
        d_zoomer[i]->setEnabled(on);
        d_zoomer[i]->zoom(0);
    }
    //*/
    //d_picker->setEnabled(!on);
}

void Mydatwindow::createActions(){

    layerLabel = new QLabel(tr("Zur Wahl "));
    layerComboBox=new QComboBox();
    layerComboBox->addItem(tr("Einteilung nach EGE"));
    layerComboBox->addItem(tr("Einteilung nach Eigenkapital"));
    layerComboBox->addItem(tr("Schlussbilanz"));
    layerComboBox->addItem(tr("Gewinn-/Verlustrechnung"));
    layerComboBox->addItem(tr("Pachtbilanz"));
    layerComboBox->addItem(tr("Kontoauszug"));
    layerComboBox->addItem(tr("Betriebsspiegel"));
    layerComboBox->addItem(tr("�bersicht Pachtvertr�ge"));
    layerComboBox->addItem(tr("Liquidit�t"));

    layerComboBox->setCurrentIndex(2);
    layerLabel->setBuddy(layerComboBox);

    connect(layerComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(updateGraphic(int)));

    actConfig = new QAction("Anpassen",this);
    actConfig->setIcon(QIcon(":/images/config.png"));

    layerComboBox->currentIndex()==0 ? actConfig->setVisible(true):actConfig->setVisible(false);
    connect(actConfig, SIGNAL(triggered(bool)), this, SLOT(showAdjDialog(bool)));

    //actWithBorder = new QAction("Gitter",this);
    //actWithBorder->setIcon(QIcon(":/images/grid.png"));

    //iterLab = new QLabel("   Runde: ");
    iterSBox = new QSpinBox();
    iterSBox->setPrefix("Runde:  ");
    //iterLab->setBuddy(iterSBox);
    connect(iterSBox, SIGNAL(valueChanged(int)), this, SLOT(showRunde(int)));

    actPrint = new QAction("Drucken",this);
    actPrint->setIcon(QIcon(":/images/drucken.png"));

    //connect(actWithBorder, SIGNAL(triggered()), this, SLOT(updateWithBorder()));
    connect(actPrint,SIGNAL(triggered()), this, SLOT(drucken()));
}

void Mydatwindow::showRunde(int it){
  if (!fwReady) return;
  for (int i=1; i<numCWidgets; ++i){
    if (i==4) continue; // PB
    Farmwidget *fw= static_cast<Farmwidget*>(tabs[i]);
    fw->updateData(it);
  }
}

void Mydatwindow::showAdjDialog(bool b){
    int l = layerComboBox->currentIndex();
    if (l!=0 && l!=1) return;

    if (hasDialogs[l]) {
        l==0?anpassenDialog->show()
            :anpassenDialogEK->show();
        actConfig->setEnabled(false);
        layerComboBox->setEnabled(false);
        return;
    }

    vector<QwtSymbol> vs;
    vector<string> titles;
    vector<QColor> colors;
        vector<QwtSymbol::Style> styles;
        switch (l) {
        case 0: styles=esuStyles;titles=esuYtitles;colors=esuColors;break;
        case 1: styles=ekStyles;titles=ekYtitles;colors=ekColors;break;
        default:;
        }

        for (int i=0;i<styles.size();++i){
            QwtSymbol *ps= new QwtSymbol(styles[i]);
            vs.push_back(*ps);
        }

    adjDialog *dialog;

    l==0? dialog= new adjDialog(titles,colors,vs,gridColor, canvasColor, maxX,minX,l,this)
        : dialog= new adjDialog(titles,colors,vs,gridColorEK, canvasColorEK, maxXEK,minXEK,l,this);
    hasDialogs[l]=true;
    if (l==0) anpassenDialog=dialog;
            else anpassenDialogEK = dialog;

    connect(dialog,SIGNAL(accepted()),this, SLOT(updateConfig()));
    connect(dialog,SIGNAL(rejected()),this, SLOT(updateConfig()));

    connect(dialog,SIGNAL(maxValue(int)),this,SLOT(updateXmax(int)));
    connect(dialog,SIGNAL(minValue(int)),this,SLOT(updateXmin(int)));

    connect(dialog,SIGNAL(plotFarben(QColor,QColor)),this,SLOT(updatePlotFarben(QColor,QColor)));
    connect(dialog,SIGNAL(curveFarben(int,QColor)),this,SLOT(updateCurveFarben(int,QColor)));
    connect(dialog,SIGNAL(curveStyle(int,QwtSymbol)),this,SLOT(updateCurveStyle(int,QwtSymbol)));

    dialog->show();

    actConfig->setEnabled(false);
    layerComboBox->setEnabled(false);
}

void Mydatwindow::updateCurveStyle(int i, QwtSymbol s){
    int l = layerComboBox->currentIndex();
    if (l!=0 &&l!=1) return;

    l==0? esuStyles[i]=s.style(): ekStyles[i]=s.style();
    static_cast<Plot*>(tabs[l])->updatePlot();
}

void Mydatwindow::updatePlotFarben(QColor gridC, QColor canvasC){
    int i=layerComboBox->currentIndex();
    if (i!=0 && i!=1) return;
    if (i==0){
        canvasColor=canvasC;
        gridColor=gridC;
    } else {
        canvasColorEK=canvasC;
        gridColorEK=gridC;
    }
        static_cast<Plot*> (tabs[i])->setCanvasBackground(canvasC);
        static_cast<Plot*> (tabs[i])->getGrid()->setPen(QPen(gridC,0,Qt::DotLine));
        static_cast<Plot*> (tabs[i])->updatePlot();
}

void Mydatwindow::updateCurveFarben(int i, QColor c){
    int l = layerComboBox->currentIndex();
    if (l!=0 && l!=1) return;

    l==0?esuColors[i]=c: ekColors[i]=c;
    static_cast<Plot*>(tabs[l])->updatePlot();
}

void Mydatwindow::updateXmax(int m){
    int l = layerComboBox->currentIndex();
    if (l==0) {
        maxX=m;
        static_cast<Plot*>(tabs[l])->setAxisScale(QwtPlot::xBottom, minX, maxX, maxX-minX<12?1:2);
    }else{
        maxXEK=m;
        static_cast<Plot*>(tabs[l])->setAxisScale(QwtPlot::xBottom, minXEK, maxXEK, maxXEK-minXEK<12?1:2);
    }
}

void Mydatwindow::updateXmin(int m){
    int l = layerComboBox->currentIndex();
    if (l==0){
        minX= m;
        static_cast<Plot*>(tabs[l])->setAxisScale(QwtPlot::xBottom, minX, maxX, maxX-minX<12?1:2);
    }else {
        minXEK= m;
        static_cast<Plot*>(tabs[l])->setAxisScale(QwtPlot::xBottom, minXEK, maxXEK, maxXEK-minXEK<12?1:2);
    }
}

void Mydatwindow::updateConfig(){
    actConfig->setEnabled(true);
    layerComboBox->setEnabled(true);
}

void Mydatwindow::createStatusBar(){
    statusLabel = new QLabel();
    statusLabel->setIndent(10);
    statusLabel->setTextFormat(Qt::RichText);
    statusLabel->setText("HI");
    statusBar()->addWidget(statusLabel);
}

void Mydatwindow::updateStatusBar(){
    statusLabel->setText(tr("HALLO"));
}

void Mydatwindow::createToolBars(){
    chooseLayerToolBar = addToolBar("Auswahl");

    chooseLayerToolBar->addWidget(layerLabel);
    chooseLayerToolBar->addWidget(layerComboBox);

    //chooseLayerToolBar->addWidget(iterLab);
    actIter=chooseLayerToolBar->addWidget(iterSBox);

    bool b;
    int l= layerComboBox->currentIndex();
    b= (l!=0) && (l!=1) && (l!=4);
    actIter->setVisible(b);

    chooseLayerToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    chooseLayerToolBar->addAction(actConfig);

    zoomToolBar = addToolBar("Zoom");
    zoomToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

    //zoomToolBar->addAction(actWithBorder);
    zoomToolBar->addAction(actPrint);

    optToolBar = addToolBar("showhide");
    optToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
}


void Mydatwindow::updateGraphic(int id){
layerComboBox->setCurrentIndex(id);
bool b=false;
if ( id!=0 && id!=1 &&(id!=4) && (id!=8))
    b=true;
actIter->setVisible(b);//>setEnabled(b);
//iterLab->setVisible(b);//Enabled(b);

if (id!=0 && id!=1) actConfig->setVisible(false);//setEnabled(false);
else actConfig->setVisible(true);//Enabled(true);

QVBoxLayout *lout = static_cast<QVBoxLayout*>(centralWidget()->layout());
//clout->itemAt(0)->widget()->layout()->itemAt(0)->widget())
 lout->itemAt(oldlayer)->widget()->hide();
 lout->itemAt(id)->widget()->show();

 /*if (id==0 || id==1) {
    setMinimumSize(700,500);
    }
 else {
    QSize s=sizeHint()+QSize(10,2);
    setFixedSize(s);
   }
//*/
 oldlayer=id;

}

void Mydatwindow::updateWithBorder(){
   int id = layerComboBox->currentIndex();
 //   withBorder = static_cast<Plot*>(tabs[id]->layout()->itemAt(0)->widget())->withGrid
   //         =!static_cast<Plot*>(tabs[id]->layout()->itemAt(0)->widget())->withGrid;

}

void Mydatwindow::updateGraphic(bool b) {
 int id = layerComboBox->currentIndex();

 QwtPlotGrid* g;
// g=static_cast<Plot*>(tabs[id]->layout()->itemAt(0)->widget())->getGrid();

//b?g->show():g->hide();

}

QString Mydatwindow::getFileName(){
    QString name;
    int i = layerComboBox->currentIndex();
    switch(i){
    case 0: name="ege";break;
    case 1: name="ek";break;
    case 2: name="sb";break;
    case 3: name="gvr";break;
    case 4: name="pb";break;
    case 5: name="ka";break;
    case 6: name="bs";break;
    case 7: name="pv";break;
    case 8: name="liq";break;
    default:;
    }
    return name;
}

void Mydatwindow::drucken(){
   QString opstr = QString(gg->OUTPUTFILEdir.c_str());
   printer->setOutputFileName(opstr+"DataBetrieb/"+getFileName()+".pdf");//"tmyd.pdf");
    /*QPrintDialog dialog(printer);
    if (dialog.exec()){
    {
        //*/
            QPainter painter(printer);
        /*QPainter painter;
        QPrinter printer;
        painter.begin(&printer);

        //*/

        /* QFont textFont("Times", 20, QFont::Bold);

        painter.setFont(textFont);
        //QRect textRect = painter.boundingRect(QRectF(0,0,9600,500),Qt::AlignCenter,
           // "Das ist eine Graphik");

        painter.drawText(QRect(0,0,painter.window().width(),500),Qt::AlignCenter,
            QString("Graphik nach %1").arg(layerComboBox->currentText()));
            //Das ist eine Graphik");
        painter.translate(0,700);
        scene->render(&painter);

        painter.translate(0,scene->sceneRect().height()*11+50);
        painter.scale(10,10);
        legendWidgets[layerComboBox->currentIndex()]->render(&painter);
       //*/

        painter.scale(8,8);
        centralWidget()->render(&painter);
    //}
}

void Mydatwindow::saveConfig(){
    QSettings dataSettings("IAMO, BSE", "Farm AgriPoliS");

    dataSettings.setValue(QString("myGridColor"), gridColor);
    dataSettings.setValue(QString("myCanvasColor"), canvasColor);
    dataSettings.setValue(QString("myGridColorEK"), gridColorEK);
    dataSettings.setValue(QString("myCanvasColorEK"), canvasColorEK);

    for (int i=0; i<esuYtitles.size();++i){
        dataSettings.setValue(QString("esuStyle_")+i, esuStyles[i]);
    }

    for (int i=0; i<esuYtitles.size();++i){
        dataSettings.setValue(QString("esuColor_")+i, esuColors[i]);
    }

    for (int i=0; i<ekYtitles.size();++i){
        dataSettings.setValue(QString("ekStyle_")+i, ekStyles[i]);
    }
    for (int i=0; i<ekYtitles.size();++i){
        dataSettings.setValue(QString("ekColor_")+i, ekColors[i]);
    }
    //*/
}

bool Mydatwindow::plotsCreated(){
    return hasCWidgets;
}

void Mydatwindow::faOutput(){
    kurvenOutput();     QCoreApplication::processEvents();
    sbilanzOutput();    QCoreApplication::processEvents();
    pbilanzOutput();    QCoreApplication::processEvents();
    kontoOutput();      QCoreApplication::processEvents();
    gvRechnungOutput(); QCoreApplication::processEvents();
    bspiegelOutput();   QCoreApplication::processEvents();
}

void Mydatwindow::kurvenOutput(){
    updateGraphic(0);
    //printer->setOutputFileName("ege.pdf");
    drucken();
    updateGraphic(1);
    //printer->setOutputFileName("ek.pdf");
    drucken();
    //*/
}

void Mydatwindow::pbilanzOutput(){
   ofstream of;
   of.open((gg->OUTPUTFILEdir+"DataBetrieb/"+"pb.dat").c_str());
   of<<"Runde\t"<< "ausgelaufeneFl\t"<< "zugepachteFl\t"<< "Flaechen\n";
   int sz=pbDatasI.size();
   for (int i=0;i<sz;++i){
        vector<double> ds = pbDatasI[i];
        int dsz=ds.size();
        for (int j=0;j<dsz;++j){
            of<<ds[j]<<"\t";
        }
        of<<"\n";
    }
    of.close();
    //*/
 }

void Mydatwindow::bspiegelOutput(){
   ofstream of;

   of.open((gg->OUTPUTFILEdir+"DataBetrieb/"+"bspiegel.dat").c_str());
   of<<"Runde\t"<<"Alter\t"<< "Eigenkapital\t"<< "RangEK\t"
        <<"Liquiditaet\t"<<"Betriebstyp\t"<<"Rechtsform\t";
   of<<"Gesamt_AK\t"<< "Familie_AK\t"<< "Lohn_AK\t";
   int bsz=gg->NAMES_OF_SOIL_TYPES.size();
   for (int i=0;i<bsz;++i){
     of<<"Preis_"<<gg->NAMES_OF_SOIL_TYPES[i]<<"\t";
    }
   for (int i=0;i<bsz;++i){
     of<<"Flaechen_"<<gg->NAMES_OF_SOIL_TYPES[i]<<"\t";
    }
   for (int i=0;i<bsz;++i){
     of<<"EigenFlaechen_"<<gg->NAMES_OF_SOIL_TYPES[i]<<"\t";
    }
    of<<"\n";

   int sz=bsdats.size();
   for (int ii=0;ii<sz;++ii){
        BSdat bd = bsdats[ii];
        of<<ii<<"\t"<<bd.farmage<<"\t"<<bd.ek<<"\t"<<bd.EKrank<<"\t"<<bd.liq<<"\t"
           << (bd.isClosed? "geschlossen": classnames[bd.farmtype])<<"\t"
           << (bd.isClosed? "geschlossen": legnames[bd.legaltype])<<"\t"
           <<bd.fam_AK+bd.lohn_AK<<"\t"
           <<bd.fam_AK<<"\t"<<bd.lohn_AK<<"\t";
       for (int i=0;i<bsz;++i){
         of<<bd.pachtPreisOfSoils[i]<<"\t";
        }
       for (int i=0;i<bsz;++i){
         of<<bd.landSums[i]<<"\t";
        }
       for (int i=0;i<bsz;++i){
         of<<bd.ownLands[i]<<"\t";
        }

        of<<"\n";
   }

   of<<"\nGebauede, \tMaschinen und \tRestdauerverteilung \tder Pachtflaechen\n";
   for (int ii=0;ii<sz;++ii){
        of<<"Runde\t"<< ii<<"\n";
        BSdat bd = bsdats[ii];

        of<<"\nGebauede: Name \tTiere \tKap \tAlter \tmax_Alter"<<"\n";
        int gsz=bd.stalls.size();
        for (int i=0;i<gsz;++i){
            stalldata sd = bd.stalls[i];
            of<<sd.name<<"\t"<<sd.used << "\t";
            of<<sd.capacity<<"\t"<<sd.age<<"\t"<<sd.life<<"\n";
        }

        of<<"\nMaschinen: Name \tKap \tAlter \tmax_Alter"<<"\n";
        int msz=bd.machines.size();
        for (int i=0;i<msz;++i){
            machinedata ms = bd.machines[i];
            of<<ms.name<<"\t"<<ms.capacity<<"\t"<<ms.age<<"\t"<<ms.life<<"\n";
        }

        of<<"\nRestdauerverteilung \tder Pachtflaechen"<<"\n";
        of<<"Restdauer"<<"\t";
        for(int j=0;j<bsz;++j){
            of<<"Umfang_"<<gg->NAMES_OF_SOIL_TYPES[j]<<"\t";
        }
        of<<"\n";
        for (int i=0;i<gg->MAX_CONTRACT_LENGTH;++i){
            of<<i+1<<"\t";
            for(int j=0;j<bsz;++j){
                of<< bd.restdauerAreasOfSoils[j][i]<<"\t";
            }
            of<<"\n";
        }
        of<<"\n";
    }
    of.close();
    //*/
}

void Mydatwindow::sbilanzOutput(){
   ofstream of;
   of.open((gg->OUTPUTFILEdir+"DataBetrieb/"+"sBilanz.dat").c_str());
    of<<"Runde\t"<<"Bodenvermoegen\t"<<"Anlagevermoegen\t"<<"Geldvermoegen\t"
        <<"AktivaSumme\t"<<"Eigenkapital\t"<<"Fremdkapital\t"<<"PassivaSumme\n";
    int ssz = aktivasI.size();
    for (int i=0;i<ssz;++i){
        vector<double>dsa,dsp;
        dsa=aktivasI[i];
        dsp=passivasI[i];
        of.precision(9);
        of<<i<<"\t"<<dsa[0]<<"\t"<<dsa[1]<<"\t"<<dsa[2]<<"\t"<<dsa[4]<<"\t"
            <<dsp[0]<<"\t"<<dsp[1]<<"\t"<<dsp[4]<<"\n";

    }
    of.close();
    //*/
 }

void Mydatwindow::gvRechnungOutput(){
   ofstream of;

    of.open((gg->OUTPUTFILEdir+"DataBetrieb/"+"gvRechnung.dat").c_str());
    of<<"Runde\t"<<"AckerbauErt\t"<<"MilchErt\t"<<"RinderErt\t"<<"SchweineErt\t"
        <<"Ausserbetr.Arbeit\t"<<"Direktzahlungen\t"<<"SonstigeErt\t"<<"SummeErt\t"
        <<"AckerbauAufw\t"<<"MilchAufw\t"<<"RinderAufw\t"<<"SchweineAufw\t"
        <<"Loehne\t"<<"Unterhaltung\t"<<"Abschreibungen\t"<<"Gemeinkosten\t"<<"Zupachtung\t"
        <<"SonstigeAufw\t"<<"SummeAufw\t"<<"ZinsErt\t"<<"ZinsAufw\t"<<"SummeZins\t"<<"Gewinn/Verlust\n";

    of.precision(9);
   int gsz=ertraeges.size();
   for (int i=0;i<gsz;++i){
        vector<double> ds = ertraeges[i];
        of<<i<<"\t";
        int dz = ds.size();
        for (int di = 0;di<dz;++di){
            if (di>6 && di<dz-1) continue;
            of<<ds[di]<<"\t";
        }

        ds = aufwendungens[i];
        dz = ds.size();
        for (int di = 0;di<dz;++di){
            if (di==dz-2) continue;
            of<<ds[di]<<"\t";
        }

        ds = finanzs[i];
        dz = ds.size();
        for (int di = 0;di<dz;++di){
            if (di>1 && di<dz-1) continue;
            of<<ds[di]<<"\t";
        }
        of<<gewinns[i]<<"\n";
    }
    of.close();
    //*/
}

void Mydatwindow::kontoOutput(){
    ofstream of;
    of.open((gg->OUTPUTFILEdir+"DataBetrieb/"+"Kontoauszug.dat").c_str());
    of<<"Runde\t"<<"AltKonto\t"
        <<"AckerbauH\t"<<"MilchH\t"<<"RinderH\t"<<"SchweineH\t"
        <<"Ausserbetr.Arbeit\t"<<"Direktzahlungen\t"<<"Zinsertraege\t"<<"SonstigeEinnahmen\t"<<"SummeHaben\t"
        <<"AckerbauS\t"<<"MilchS\t"<<"RinderS\t"<<"SchweineS\t"
        <<"Loehne\t"<<"Unterhaltung\t"<<"Annuitaet\t"<<"Gemeinkosten\t"<<"Zupachtung\t"
        <<"Zinsaufw(kurzfristig.FK)\t"<<"Entnahmen\t"<<"SonstigeAusgaben\t"<<"SummeSoll\t"<<"neuKonto\n";

    of.precision(9);
    int ksz=habens.size();
    for (int i=0;i<ksz;++i){
        vector<double> ds = habens[i];
        of<<i<<"\t"<<altKontos[i]<<"\t";
        int dz = ds.size();
        for (int di = 0;di<dz;++di){
            if (di>7 && di<dz-1) continue;
            of<<ds[di]<<"\t";
        }

        ds = solls[i];
        dz = ds.size();
        for (int di = 0;di<dz;++di){
            if (di==dz-2) continue;
            of<<ds[di]<<"\t";
        }
        of<<neuKontos[i]<<"\n";
    }
    of.close();
    //*/
}
