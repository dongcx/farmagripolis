#include "plotDrawItem.h"
#include <QStyleOptionGraphicsItem>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>

Q_DECLARE_METATYPE(PlA*);
PlotItem::PlotItem(int pt, QColor c, bool isSitz, bool isFirst, Landwindow *lwin){
    ptype = pt;
    color = c;
    isFarmstead = isSitz;
    isFirstFarm = isFirst;
    img=0;
    lastItem= lwin->lastPlot;
    landWin=lwin;
    setFlags(ItemIsSelectable |ItemSendsGeometryChanges);
    modus = REGION;
    isOwnland=false;
    //setAcceptHoverEvents(true);
}


void PlotItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                     QWidget *){
    QPen pen;
    if ((ptype == SOILTYPEFREE && isFree)||(modus==FARM && isFree)){
       PlA* p=data(100).value<PlA*>();
       if (!(p->soil_type == gg->NO_OF_SOIL_TYPES)){
         painter->fillRect(boundingRect().adjusted(1,1,-1,-1),Qt::white);
         pen.setColor(color);
         pen.setWidth(2);
         pen.setJoinStyle(Qt::MiterJoin);
         painter->setPen(pen);
         painter->drawRect(boundingRect().adjusted(1,1,-1,-1));
       }
     }else
        painter->fillRect(QRect(x0,y0,w, h), color);


     /*if (withBorder) {
        pen.setColor(Qt::black);
        pen.setWidthF(0.5);
        painter->setPen(pen);
        painter->drawRect(boundingRect());
     }
     //*/

     if (isOwnland && modus==FARM){
         int id=data(100).value<PlA*>()->farm_id;
      if (id==0 || chosenFarmid==id){
         pen.setColor(Qt::black);
         pen.setWidth(1);
         painter->setPen(pen);
         painter->setRenderHint(QPainter::Antialiasing);
         painter->drawEllipse(boundingRect().adjusted(1,1,-1,-1));
      }
     }


    if (isFarmstead){
        //setFlags(ItemClipsToShape);
        //painter->drawImage(-w/2,-h/2,(QImage(":/images/pause.png")));//farmhaus.png")));

       QPen pen;
       pen.setWidth(1.5);
       painter->setPen(pen);

       painter->drawLine(QPointF(-w/2+1,-h/2+1), QPointF(w/2-1,h/2-1));
       painter->drawLine(QPointF(w/2-1,-h/2+1), QPointF(-w/2+1, h/2-1));

       if (isFirstFarm) {
          QPen pen;
          pen.setColor(QColor(255-qRed(color.rgb()), 255-qGreen(color.rgb()),
                        255-qBlue(color.rgb())));
          pen.setWidth(2);
          painter->setPen(pen);
          painter->setRenderHint(QPainter::Antialiasing);
          painter->drawEllipse(-w/4,-h/4, w/2-1, h/2-1);//->drawText(boundingRect(),"O");
       }

       setCursor(Qt::PointingHandCursor);

     /* if (img) { //img

       painter->drawImage(boundingRect(),*img, img->rect());//QImage(":/images/pin-icon.png"));
      }
      //*/
    }
}

QRectF PlotItem::boundingRect() const{

    return QRectF(x0,y0,w,h);
}

void PlotItem::setImage(QImage *image){
    prepareGeometryChange();
    img = image;
    update();

}

void PlotItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event){
  int s = data(100).value<PlA*>()->state;
  if (s==1){
     int ct = data(100).value<PlA*>()->col;
     int rt = data(100).value<PlA*>()->row;
     landWin->updateRent(rt,ct);
  }
}

void PlotItem::mousePressEvent(QGraphicsSceneMouseEvent *event){
  if (event->buttons()& Qt::RightButton) {
    modus=REGION;
    landWin->updateModus(modus,chosenFarmid);
    return;
  }
  /*if (isFarmstead){
    lastItem=landWin->lastPlot;
    if (lastItem){
        delete lastItem->img;
        lastItem->img=0;
        lastItem->update();
    }
    landWin->lastPlot = this;
    landWin->updateFarminfo(data(100).value<PlA*>()->farm_id);

    //cout <<  data(100).toInt() << " is the farm_id" << endl;

    QImage *ig= new QImage(":/images/haus.jpg");//pin-icon.png");//push_pin.png");//haus.jpg");//pin-icon.png");
    setImage(ig);

    //scene()->addPixmap(QPixmap(":/images/pin-icon.png"));
  }//*/

  int ct = data(100).value<PlA*>()->col;
  int rt = data(100).value<PlA*>()->row;
  if (isFree) {
     landWin->updateTC(rt, ct);
     return;
  }

  int s = data(100).value<PlA*>()->state;
  if (s==1){
     landWin->updateRent(rt,ct);
     return;
  }
  //*/

  if (s==3){
     landWin->updateRent(rt,ct,true);
     return;
  }

  if (isFarmstead){
    modus = FARM;
    chosenFarmid = data(100).value<PlA*>()->farm_id;
    landWin->updateFarminfo(chosenFarmid);
    landWin->chosenFarm=chosenFarmid;
  }
/*else {
    modus=REGION;
  }
  //*/
   landWin->updateModus(modus,chosenFarmid);
}

/*
void  PlotItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event){
    if (img) delete img;
    img = 0;
    update();
}
//*/
void PlotItem::setColor(QColor c){
    color = c;
}

void PlotItem::setIsFarmstead(bool b){
  isFarmstead=b;

}

void PlotItem::setIsFirstFarm(bool b) {
  isFirstFarm = b;
}

void PlotItem::setType(int t) {
    ptype= t;
}

void PlotItem::setFree(bool b){
  isFree = b;
}

void PlotItem::setBorder(bool b){
  withBorder =b;
}

void PlotItem::setIsOwnland(bool t){
    isOwnland = t;
}

void PlotItem::setModus(int t){
    modus = static_cast<Modus>(t);
}

void PlotItem::setChosenFarmid(int t){
    chosenFarmid = t;
}
