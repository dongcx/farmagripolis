#ifndef LANDWINDOW_H
#define LANDWINDOW_H

#include <QMainWindow>
#include <QComboBox>
#include <QLabel>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsProxyWidget>

#include "AgriPoliS.h"
#include "plotDrawItem.h"
#include "RegFarm.h"
#include <QGridLayout>
#include <QGroupBox>

#include <QPrinter>
#include "myTypes.h"


class PlotItem;
struct RegionInfo;
class Landwindow:public QMainWindow {
    Q_OBJECT
public:
    static const int numPlotType= 3;
    enum PlotType {OWNER,SOILTYPEFREE, FARMTYPE};
    PlotItem *lastPlot;
    QGraphicsPixmapItem *pinItem;
    map<int,RegFarmInfo*> mapIDpFarm;
    int chosenFarm;

    explicit Landwindow(QWidget *parent);
    bool sceneCreated();
    void initScene();

    void updateFarminfo(int);
    void updateFarminfo();
    void updateRegionData();
    void initRegionData();
    void updateFarms();

    void initColors();
    void initLegends();
    void initColorDialogs();
    void initColorDialogs2();
    void initInfos();

    void initInfoWidget();
    void initInfoWidgetRegion();

    void saveColors();

    QGraphicsView *getView();
    void updateModus(int t,int);


public slots:
    void newplot(PlA *);
    void delplot(PlA *);
    //void createScene();

    void zoomout();
    void zoomin();

    void closeEvent(QCloseEvent*);

    void enableLegend(bool);
    void enableFarminfo(bool);

    void showLegend();
    void showFarminfo();

    void updateGraphic(int);
    void updateGraphic(bool);
    void updateGraphic();
    void updateWithBorder();

    void drucken();
    void showColorDialog();
    void showColorDialog2();

    void changeColor(QString);
    void colorDialogClosed(int);

    //void drucken();
    void updateTC(int,int);
    void updateRent(int,int,bool b = false);


signals:
    void hi(bool);
    void lwMouseMove(QMouseEvent* );

protected:
    //void mouseMoveEvent(QMouseEvent *);
    void createActions();
    void createToolBars();
    void makeDocks();

    QColor getColor(int,PlA*);
    QColor getColor(PlA*);

    bool getIsFarmstead(PlA*);
    bool getIsFirstFarm(PlA*);

    void updateLegendWidget(int );
    void updateLegendWidget();
    void updateLegends();
    //void updateGrid();

    void setModus(int);
    void setChosenFarmid(int);

    void createStatusBar();
    void updateStatusBar();

    void updatePlot(PlA* );

    QPixmap viewIcon();
    void removeProxies();

private:
    int modus;
    int chosenFarmid;

    int nSoils;
    int nFarmtypes;
    vector<string> sSoilnames;
    int nInvests;

    QPrinter *printer;

    map<int,string> classnames;
    void  initLegendWidgets();

    void getStallMachine0(RegFarmInfo*);
    void getMachines0(RegFarmInfo*);
    void getStallMachine(RegFarmInfo*);
    void getMachines(RegFarmInfo*);
    void getBuildings(RegFarmInfo*);

    bool zunah(QColor, int, int, int);
    int picType;
    bool hasScene;
    bool withBorder;
    PlA *pInfo;

    QGraphicsView *view;
public:    QGraphicsScene *scene;

private:    QToolBar *chooseLayerToolBar;
    QToolBar *zoomToolBar;

    QToolBar *colorToolBar;
    QToolBar *optToolBar;

    QToolBar *moveToolBar;
    QToolBar *spaceToolBar;

    QAction* actZoomIn;
    QAction* actZoomOut;

    QAction *actColor;
    QAction *actWithBorder;
    QAction *actPrint;

    QAction *actLegende;
    QAction *actFarminfo;

    QLabel *layerLabel;
public:   QComboBox *layerComboBox;

private:
    QDockWidget *legend;
    QDockWidget *info;

    //vector<QWidget*> infoWidgets;
    QWidget *infoWidgetRegion;
    QScrollArea *infoWidget;//QWidget *infoWidget;
    //QLineEdit *idEdit;
    QLabel *idLabel;

    QLineEdit *rentLandEdit;

    QComboBox *capComboBox;
    //QLineEdit *eigenKapitalEdit;
    QLabel *kapLabel;

    //QLineEdit *liquidityEdit;
    QLabel *liqLabel;
    QLabel *farmtypeLabel;

    QLabel *managekoeffLabel;
    QLabel *betriebsalterLabel;

    QGridLayout *glout;
    QLabel *machineLabel;

    //vector<QLineEdit*> landEdits;
    vector<QLabel*> landLabels;
    vector<QLabel*> landpriceLabels;

    vector<QLabel*>investnameLabels,investLabels;
    vector<QLabel*> investCapLabels, investAgeLabels;

    QGridLayout *investLayout;
    QGroupBox *investGroupBox;
    QVBoxLayout *vlayout;

    vector<QLabel*>machinenameLabels,machineLabels;
    vector<QLabel*> machineAgeLabels, machineCapLabels;

    QGridLayout *machineLayout;
    QGroupBox *machineGroupBox;
    QVBoxLayout *mvlayout;

    vector<QLabel*>buildingnameLabels,buildingLabels;
    vector<QLabel*> buildingAgeLabels, buildingCapLabels;

    QGridLayout *buildingLayout;
    QGroupBox *buildingGroupBox;
    QVBoxLayout *bvlayout;

    QColor nonAgColor, agColor;
    QColor freeColor, nonfreeColor;
    vector<QColor> soiltypeColors;
    QColor farm0Color;
    vector<QColor> farmtypeColors;

    QColor otherfarmColor;
    QColor myColor;

    vector<QWidget*> legendWidgets;
    vector<QDialog*> colorDialogs;
    QDialog *colorDialog;

    vector <QGridLayout*> layouts;

    QWidget *legendWidget;
    QGridLayout *layout;

    QGraphicsItemGroup *gridGroup;
    QColor gridColor;

    QLabel *statusLabel;

    struct RegionInfo *reginfo;

    QLabel *anzLabel;
    QLabel *sizeLabel;
    QLabel *pachtLabel;
    QLabel *gesamtLabel;
    vector<QLabel*> landLabels1, freeLandLabels;
    vector<QLabel*> farmtypeLabels,ftAreaLabels;

    //QWidget *tcWidget;
    double tc; //transport cost
    QLabel *labTc;
    QGraphicsProxyWidget* proxyWidget;
    bool hasProxy;
    bool firstProxy;

    QLabel *labRent;
    QGraphicsProxyWidget* proxyWidgetRent;
    bool hasProxyRent;
    bool firstProxyRent;

    vector<QLabel *> myFarmSitzIcons, myFarmSitzTexts;

};

#endif // LANDWINDOW_H
