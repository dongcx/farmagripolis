#ifndef FARMWIDGET_H
#define FARMWIDGET_H

#include <QTableWidget>
#include <qlabel.h>
#include <qgroupbox.h>
#include <QVBoxLayout>
#include <QComboBox>
#include "Mydatwindow.h"
#include "Datawindow.h"

#include "spreadsheet.h"

class Mydatwindow;
class Title;
class PlotData;

class Farmwidget : public QWidget
{
    Q_OBJECT

public:
    enum SORTINGFELD {FELD, BODEN, PREIS, RESTDAUER};
    Farmwidget(int,QWidget *par=0);
    void updateData();
    void updateData(int);
    void paintEvent(QPaintEvent *);

signals:


private slots:
    void updateStalls(int);
    void updateMachines(int);
    void updatePV(QString);
    void updatePV(SORTINGFELD);
    void updatePV0(SORTINGFELD);

private:
    bool toSort;
    SpreadSheet *mkpvSS();
    int iterAlt;
    bool inited;
    int plotn;
    vector<SpreadSheet*> spreadSheets;
    Mydatwindow *mparent;
    QMainWindow *parent;
    Datawindow *dparent;

    Title *gewinnLab;
    Title *altStand, *neuStand;

    QLabel *ageLab, *bnrLab;
    QLabel *ekLab, *liqLab, *farmtypeLab, *ekrankLab;
    QLabel *rechtsformLab;
    int ekplatz, ektotal;
    QLabel *sumAKLab, *famAKLab, *lohnAKLab;

    int nsoils;
    int maxtime;
    vector<string> soilnames;
    SpreadSheet *rdvSS, *pachtPreisSS;
    QVBoxLayout *fwLout;
    SpreadSheet *pvSS; //pachtverträge
    vector<pair<SpreadSheet*, int> > pvSSs;
    int curIter;
    vector<QLabel*> sumSoilLabs, ownSoilLabs, rentSoilLabs;

    QGroupBox *stallGBox;
    QGroupBox *machineGBox;
    QVBoxLayout *vlout;
    int indexStall, indexMachine;
    vector<QLabel*> landLabs;

    vector<PlotData> pds ;
    vector<vector<PlotData> > pdss ;
    vector<PlotData*> ppds ;
    vector<vector<PlotData*> > ppdss ;
    QComboBox* sortingCBox;
    SORTINGFELD sortfeld;
};

class Title : public QLabel{
    Q_OBJECT
public: explicit Title(Qt::AlignmentFlag align, QFont font, QWidget* w);

};

#endif
