#ifndef ADJDIALOG_H
#define ADJDIALOG_H

#include <qdialog.h>
#include <qwt_symbol.h>
#include <qlabel.h>
#include <qcombobox.h>
#include <qlineedit.h>

using namespace std;


class mmLineEdit:public QLineEdit {
    Q_OBJECT
public:
    mmLineEdit(QString s):QLineEdit(s){}
    QSize sizeHint();

public slots:
    void updateMin(QString);
    void updateMax(QString);
};


class adjDialog:public QDialog{
    Q_OBJECT
public:
    static const int NSYMBOLS=16;
    explicit adjDialog(vector<string> ns,vector<QColor> cs, vector<QwtSymbol> ss,
                       QColor gc, QColor cvs, int maxx, int minx, int type, QWidget* parent=0);

public slots:
    void updateIcon(int );
    void updateColor(int);
    void updatePlot(QString);
    void xMinChanged(int);
    void xMaxChanged(int);
    void updateFT(int);

    void dichteXMinChanged(QString);
    void dichteXMaxChanged(QString);

    void emitPlotFarben(QString);
    void emitCurveFarben(int);

signals:
    void minValue(int);
    void maxValue(int);

    void minDichte(double);
    void maxDichte(double);

    void plotFarben(QColor, QColor);
    void curveFarben(int, QColor);
    void curveStyle(int,QwtSymbol);

private:
    vector<string> names;
    vector<QColor> colors;
    vector<QwtSymbol> symbols;
    QColor gridColor;
    QColor canvasColor;
    int maxX, minX;

    vector<QLabel*> iconLabs;
    vector<QComboBox*> styleCBs;
    vector<QPushButton*> colorPBs;
    vector<vector<QwtSymbol> > syms;

    vector<QLabel*> plotIconLabs, plotLabs;
    vector<QLabel*> ftIconLabs, ftLabs;
    int type;

    mmLineEdit *minDichteEdit, *maxDichteEdit;
};

#endif // ADJDIALOG_H
