#ifndef SIMTHREAD_H
#define SIMTHREAD_H
#include "RegMessages.h"

#include <QThread>

enum SimZustand {NOTREADY, READY, RUNNING, PAUSED, STOPPED};

class SimThread : public QThread
{
    Q_OBJECT

public:
    SimThread(QString, QString);
    volatile SimZustand zustand;

    void stop();
signals:
    void busy();
    void initover();
    void simover();

    void newplot(PlA*);
    void delplot(PlA*);

    void decboden();
    void decinvest();
    void decclose();

    void itOver(int);
    void itBeginn();
    void produced();

    void LAOver();
    void hasPeriodResults();

    void LAsig(int, int);
    void LAsig(int);
    void bidValidate();

    void FoFsig(int);
    void INVsig();
    void simOver();
    void PRODsig();

public slots:
    void takedecboden();
    void takedecinvest();
    void takedecclose();

    void emitSimOver();
    void emitBusy();
    void emitInitover();
    void emitdecinvest();
    void emitdecclose();
    void emitdecboden();

    void emitNewplot(PlA*);
    void emitDelplot(PlA*);

    void emitItOver(int);
    void emitProduced();

    void emitLAOver();
    void emitPeriodResults();

    void toZustand(SimZustand);

    void emitLAsig(int, int);
    void emitLAsig(int);
    void emitBidValid();

    void emitFoFsig(int);
    void emitINVsig();
    void emitPRODsig();

    void emitItBeginn();

protected:
    void run();

private:
    QString inputDir;
    QString policyFile;

    volatile  bool stopped;
};

#endif // SIMTHREAD_H
