#ifndef CLOSEDIALOG_H
#define CLOSEDIALOG_H

#include <stdio.h>
#include <stdlib.h>

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>

#include "pe.h"


class RegFarmInfo;
class QSpinBox;
class SpreadSheet2;
class QDoubleSpinBox;
class Mainwindow;

using namespace std;
class CloseDialog : public QDialog {
    Q_OBJECT
public:
    CloseDialog(Mainwindow* par=0);
    void closeEvent(QCloseEvent*);
    void enableBSbutton(bool);
    bool isInited();
    void setShowBSpiegel(bool);
    vector<double> getManPEs()const;
    vector<double> getEffPEs() ;

public slots:
    void showClose(int);
    void updateGUI();
    void updateGUI2();

    void makePeWidget();
    void makePeWidget2();
    void makeFinWidget();
    void makeOppWidget();

    void showHilfe();
    void blendenBSpiegel();
    void hilfeClosed();

    void getPeOK(int,bool);
    void savePEs();
    void updateFinOpp();

    void aussteigen();
    void bleiben();
    void weiter();

    void blendenPE();

    void getPeVarOK(int,bool);
    void getPeVarSig(int);
    void getPeSig(int);

    void getMydatwindowHi(bool);

signals:
    void closeSig(bool);
    void ClosePeSig(int);

    void ClosePeVarSig(int);
    void peOK(int, bool);
    void fertig();

private:
    void abspeichern();
    int nsoils;
    vector<string> soilNames;
    RegFarmInfo *farm0;
    int ITER;

    int closeState;
    bool firstGUI;
    QPushButton *weiterButton;
    QWidget* weiterWidget;

    QWidget* decWidget;
    QPushButton *aussteigButton, *bleibButton;
    QPushButton *hilfeButton, *spiegelButton;

    QDialog *hilfeDialog;

    PEwidget *peWidget;

    vector<QString> prodNames;
    vector<int> prodIDs;
    vector<double> prodPrices;
    vector<double> prodCosts;
    vector<double> prodRefPEs;
    vector<double> prodPEs;
    int numPEs;

    vector<int> getreideIDs;
    vector<double> getreidePrices, getreidePEs;

    vector<double> adaptPEs, naivPEs, manPEs;
    vector<vector<double> > allPrices;  //Prices for all iterations

    double preisIndex;

    int peVar;
    QString refname;
    int refprodID;
    int refpos;

    vector<vector<double> > all_myPEs;
    vector<double>  it_myPEs;

    bool inited;
    double oldvalue;

    QWidget *oppCostWidget;
    QWidget *finWidget;
    SpreadSheet2 *finSS;
    SpreadSheet2 *oppSS;

    QPushButton *peButton;
    double langFK, dispo, GDB, liquidity;
    double profit, income;

    double expIncome;
    QLabel *expIncomeLab;
    double oppcost;
    QLabel *oppcostLab;
    vector<bool> decisions;

    QLabel *langFKLab, *dispoLab , *GDBLab , *liqLab;
    bool showPEwidget;
    bool showBSpiegel;

    vector<QString> prodUnits, prodGMunits;
    vector<double> prodRefPrices;
    vector<double> prodErtrags, prodNertrags;

    Mainwindow *parent;
    bool toClose;
};

class Title3 : public QLabel{
    Q_OBJECT
public: explicit Title3(Qt::AlignmentFlag align, QFont font, QWidget* w=0);

};

#endif // CLOSEDIALOG_H
