#ifndef INVESTDIALOG_H
#define INVESTDIALOG_H

#include <QDialog>
#include <QCloseEvent>
#include <QLabel>
#include <QLineEdit>

#include "spreadsheet2.h"
#include "pe.h"

class RegFarmInfo;
class Title4;
class QDoubleSpinBox;
class QSpinBox;
class QComboBox;
struct Komb;
class QScrollArea;
class QHBoxLayout;
class Mainwindow;

class InvestDialog : public QDialog {
    Q_OBJECT
public:
    InvestDialog(Mainwindow* parent=0);
    void closeEvent(QCloseEvent*);
public slots:
    void showInvest();
    void updateGUI();
    void updateGUI2();

    void getPeOK(int,bool);
    void savePEs();
    void blendenPE();

    void makePeWidget();
    void makePeWidget2();
    void updatePeWidget(int);

    void makeFinWidget();
    void makeVorschlagWidget();
    void updateVorWidget();
    void makeInvWidget();
    void updateFinWidget(int);
    void updateFinWidget();
    void updateFins(int);
    void updateFins();
    void updateInvWidget();
    void updateInvWidget(int);
    void updateHead();

    QWidget* createFinCopy(struct Komb,int);
    QWidget* makeFinNamen();

    void weiter();

    void updateInv(int);
    void getInvestOK(int,bool);

    void updateComb(int);
    void updateComb(QString);
    int combInd(int);
    int indComb(int);

    void neuKomb();
    void delKomb();
    void updateCombs();
    void saveComb(int);

    void saveCombInv(int);
    void saveCombPe(int);
    void saveCombFin(int);

    void getPeSig(int);
    void getPeVarSig(int);
    void getPeVarOK(int,bool);

    void getAusblenden();
    void updateRefInvs(int);

    void updateInvtyp(int);
    void updateSubtable(int,int);
    void abspeichern();

    void updatePEs();
    void getINVsig();

signals:
    void investPeSig(int);
    void investSig(int,int);

    void investPeVarSig(int);
    void peOK(int,bool);
    void peVarOK(int,bool);
    void fertig();
    void INVsig();

private:
    vector<QColor> iColors;
    void setPEs();
    bool hasPEwidget;

    QString makeKapName(QString,int);
    void updateWeiterButton();
    void getInvtypNames();
    QString makeInvtypName(QString);
    map<int, QString> invtypNames;
    void makeInvtables();

    vector<SpreadSheet2*> invtables;

    vector<vector<QString> >invNamess;
    vector<vector<double> > invKostens, invKosten1ss, invLabsubss;
    vector<vector<int> >invTypess;
    //vector<QColor> invColors;
    vector<vector<int> >invCapss;
    vector<vector<int> >invRefss;
    vector<vector<int> >myInvss;
    vector<vector<int> >invLifess;
    vector<vector<double> >invKostenJahrss;

    vector<vector<int> > posss; //positionen in ganztabelle
    vector<QString> invHeader;
    int typIndex0, typIndex;
    vector<vector<QSpinBox*> > invDSBss;
    vector<pair<int,int> > subtablerows;
    vector<QSpinBox*> invSBs;


    RegFarmInfo *farm0;
    int aktComb, numComb;
    bool firstGUI, inited, showPEwidget;

    PEwidget *peWidget;

    vector<QString> prodNames;
    vector<int> prodIDs;
    vector<double> prodPrices;
    vector<double> prodCosts;
    vector<double> prodRefPEs;
    vector<double> prodPEs;
    vector<QString> prodUnits, prodGMunits;
    vector<double> prodErtrags, prodNertrags;
    vector<double> prodRefPrices;
    int numPEs;

    vector<int> getreideIDs;
    vector<double> getreidePrices, getreidePEs;

    vector<double> adaptPEs, naivPEs, manPEs;
    vector<vector<double> > allPrices;  //Prices for all iterations

    double preisIndex;
    int peVar;
    QString refname;
    int refprodID;
    int refpos;

    SpreadSheet2 *finSS, *invSS;
    SpreadSheet2 *vorSS;

    vector<vector<double> > all_myPEs;
    vector<double>  it_myPEs;

    double oldvalue;

    vector<QString> invNames;
    vector<double> invKosten, invKosten1, invLabsub;
    vector<int> invTypes;
    vector<QColor> invColors;
    vector<int> invCaps;
    vector<int> invRefs;
    vector<int> myInvs;
    vector<int> invLifes;
    vector<double> invKostenJahrs;

    int nRowInv, nColInv;
    vector<QSpinBox*> invDSBs;

    QWidget *finWidget, *invWidget;
    QWidget *vorWidget;
    QLabel *vorLab;
    double langFK, dispo, GDB, liquidity;
    double saving;
    double profit, income;

    QPushButton *peButton, *newKombButton, *weiterButton;
    QPushButton *delButton;
    QWidget* weiterWidget;
    QLineEdit *anzKomb;
    QSpinBox *nrSBox;
    QLineEdit *aktLE;

    QScrollArea *sa;
    //QWidget *saWidget;
    QHBoxLayout *saHLout;
    vector <Komb> combs;
    vector<SpreadSheet2*> fins;
    vector<QWidget*> wins;
    int finwidth;
    QWidget *finW;

    int ITER;
    bool nurHead;
    bool inComb;

    vector<int> plans;
    int maxPlan;
    vector<Title4*> titles;

    Mainwindow *parent;
    //QWidget *widgetPE;
    bool toClose;
};

struct Komb {
    vector<double> fins;
    vector<double> pes;
    vector<int> invs;
    vector<int> invRefs;
};

class Title4 : public QLabel{
    Q_OBJECT
public: explicit Title4(Qt::AlignmentFlag align, QFont font, QWidget* w=0);

};
#endif // INVESTDIALOG_H
