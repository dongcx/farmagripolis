#include <QtGui>
#include <QTime>

#include "dclock.h"

DigitalClock::DigitalClock(QWidget *parent)
    : angehalten(false), QLCDNumber(8,parent)
{
    setSegmentStyle(Filled);
    time0.start();
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(showTime()));
    timer->start(1000);

    showTime();

    //setWindowTitle(tr("Digital Clock"));
    resize(200, 100);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}


void DigitalClock::showTime()
{
    QString text;
    int sec;
    int h, m, s;
    if (angehalten){
        sec = timesecs;
    }
    else  sec = time0.elapsed()/1000;
    s = sec%60;
    m = (sec/60)%60;
    h = (sec/3600)%24;

    QTime time(h,m,s);// = QTime::currentTime();

    text = time.toString("hh:mm:ss");

    if (!angehalten){
        if ((time.second() % 2) == 0)
            text[2] = ' ';
        else text[5] = ' ';
    }
    display(text);
}

void DigitalClock::resetTime(){
        time0.restart();
        angehalten=false;
}

void DigitalClock::anhalten(){
        timesecs =  time0.elapsed()/1000;
        angehalten = true;
}
