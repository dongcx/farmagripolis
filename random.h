#ifndef MT_RANDH
#define MT_RANDH

void randInit(unsigned long int x );

long int randlong(void);

extern long int mtRandMin ;
extern long int mtRandMax ;

double rand_normal(double mean, double stddev);
#endif
