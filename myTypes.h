#ifndef MYTYPES_H
#define MYTYPES_H

#include <vector>

struct RegionInfo {
    int anzFarms;
    int agland;//, nonagland;
    int pachtland;
    vector<int> landSoils, landSoilsFree;
    vector<int> landFarmtypes;
    vector<int> anzFarmtypes;
    //*/
};

struct DataReg {
    int farmid;
    int rechtsform;
    int farmtype;
    double esusize;
    double einkommen;
    double totalarea;
    double landused;
    double lu;
    vector <double> areasoils;
    vector <double> pricesoils;
    vector <double> animals;
    double prof;
};


#endif // MYTYPES_H

