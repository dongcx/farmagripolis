#include <QApplication>
#include <QMessageBox>
#include <QSplashScreen>

//#include "optdialog.h"
#include "AgriPoliS.h"
#include "Mainwindow.h"
//#include "curvdemo2.h"
#include <QStatusBar>
#include <QLibraryInfo>
#include <QTranslator>
#include <QDir>

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    app.setApplicationName(app.translate("main", "FarmAgriPoliS v1.0"));
    QLocale loc(QLocale::German, QLocale::Germany);
    QLocale::setDefault(loc);

    QTranslator qtTranslator;
    qtTranslator.load("qt_de",QDir::currentPath());
    // QLocale::system().name(),QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

     /*  OptDialog dialog;
    if (dialog.exec()) {
        agpmain(dialog.optdir, dialog.policy);
    } else {
        QMessageBox msgBox;
        msgBox.setText("Option folder is necessary; Policy file is optional.");
        msgBox.exec();
    }
*/
    //MainWin *w = new MainWin();
    //QPixmap pixmap(":/images/grafiken/schweinstall.jpg");
    //QSplashScreen splash(pixmap);
    //splash.show();
    //splash.showMessage("Wait...");

    Mainwindow window;
    //window.setAttribute(Qt::WA_DeleteOnClose,true);

    /*const QColor bgColor(30,30,50);
    QPalette p = QPalette();
    p.setColor(QPalette::Background,bgColor);
    window->setPalette(p);
//*/
    window.show();
    //splash.finish(window);
    //w->show();
    int x = app.exec();
    return x;
}
