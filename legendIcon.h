#ifndef LEGENDICON_H
#define LEGENDICON_H

#include <QPixmap>
#include <QSize>

class QColor;

static const QSize DefaultSize(24, 24);

QPixmap normalIcon(const QColor &color,
                 const QSize &size=DefaultSize);
QPixmap freeIcon(const QColor &color,
                 const QSize &size=DefaultSize);
QPixmap farmSitzIcon(const QColor &color,
                 const QSize &size=DefaultSize);
QPixmap mySitzIcon(const QColor &color,
                 const QSize &size=DefaultSize);
QPixmap ownlandIcon(const QColor &color,
                 const QSize &size=DefaultSize);
QPixmap gridIcon(const QColor &color,
                 const QSize &size=DefaultSize);

#endif // LEGENDICON_H
