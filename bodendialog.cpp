#include "bodendialog.h"
#include "AgriPoliS.h"

#include <QLineEdit>
#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QGroupBox>
#include <QScrollArea>
#include <QComboBox>
#include <QSpinBox>
#include <QSignalMapper>

#include "spreadsheet2.h"
#include "cell2.h"

#include "Mainwindow.h"

#include <qaction.h>

static QFont font0 = QFont("SansSerif",14, QFont::Bold);
static QFont font1 = QFont("Times",11, QFont::Bold);
static QFont font2 = QFont("Times",10, QFont::Normal);//:Bold);

BodenDialog::BodenDialog(Mainwindow* par):prozent(0),inited(false), firstGUI(true), parent(par), QDialog(){
    setWindowTitle(tr("Bodenmarkt"));
    cellBColor = Qt::lightGray;

    BidIsValid = true;

  /*  QLabel *lab1 = new QLabel("Bodentyp");
    le1 = new QLineEdit("0");
    QLabel *lab2 = new QLabel("Gebot");
    le2 = new QLineEdit("100.5");
    QGridLayout *gl = new QGridLayout;
    QPushButton *pb = new QPushButton("Ok");
    connect(pb,SIGNAL(clicked()),this, SLOT(okPushed()));

    l1=new QLabel("");
    l2=new QLabel("");
    gl->addWidget(lab1,0,0);
    gl->addWidget(le1,0,1);
    gl->addWidget(lab2,1,0);
    gl->addWidget(le2,1,1);
    gl->addWidget(pb,2,0,1,2);
    gl->addWidget(l1, 3,0);
    gl->addWidget(l2,3,1);
    //setLayout(gl);
    //resize(200,500);
    //*/
}

void BodenDialog::updateGUI(){
  if (!firstGUI) {
        freeAreas = farm0->calFreeAreas(Manager);
        ergAreas = farm0->calNewlyRented();
        updateGUI2();
        return;
   }

    farm0 = Manager->Farm0;
    ITER = gg->tIter;
    soilNames = gg->NAMES_OF_SOIL_TYPES;
    nsoils = gg->NO_OF_SOIL_TYPES;
    maxRows.resize(nsoils);
    freeAreas = farm0->calFreeAreas(Manager);
    ergAreas = farm0->calNewlyRented();

    //QWidget *sWidget = new QWidget();
    //QScrollArea *sPE = new QScrollArea;
    //*/

    QString title("Bodenmarkt");// f�r freie Fl�chen");
    prozentStr = QString("%1\% der freien Fl�chen sind vergeben").arg(prozent);

    Title2 *titleLab = new Title2(Qt::AlignCenter, font0);
    titleLab->setText(title);

    prozentLab = new Title2(Qt::AlignCenter, font1, this);
    prozentLab->setText(prozentStr);

    QVBoxLayout *v0 = new QVBoxLayout; //out

    QVBoxLayout *vl = new QVBoxLayout;
    v0->addWidget(titleLab);

    v0->addWidget(prozentLab);
    /*if (prozent>0) prozentLab->show();
    else prozentLab->hide();
    //*/

    landinputs.resize(nsoils);
    for (int i=0; i<nsoils; ++i){
        landinputs[i] = farm0->getLandInputOfType(i);
    }

    ergWidget = new QWidget;
    Title2 *ergLab = new Title2(Qt::AlignCenter, font2, ergWidget);
    QString ergStr("Fl�chenstatus");
    ergLab->setStyleSheet("QLabel {color : blue}");
    ergLab->setText(ergStr);

    int nRowErg = 3;
    int nColErg = nsoils+1;
    vector<QString> tv;
    tv.push_back("");
    for (int i=0; i<nsoils; ++i)  {
            tv.push_back(gg->NAMES_OF_SOIL_TYPES[i].c_str());
            ergAreas.push_back(i*10); //ToDo
    }
    ergSS = new SpreadSheet2(nRowErg, nColErg, false, tv, false, false, false, ergWidget);
    ergSS->setFocusPolicy(Qt::NoFocus);//0

    vector<QString> tvv ;
    tvv.push_back("neu gepachtet (ha)");
    tvv.push_back("insgesamt (ha)");
    for (int i=0; i<nColErg; ++i)  {
        ergSS->cell(0,i)->setAlign(Qt::AlignCenter, Qt::AlignCenter);
        ergSS->cell(0,i)->setFormula(tv[i]);

        if (i!=0) {
            ergSS->cell(1,i)->setAlign(Qt::AlignCenter, Qt::AlignCenter);
            ergSS->cell(1,i)->setFormula(QLocale().toString(ergAreas[i-1]));
            ergSS->cell(2,i)->setAlign(Qt::AlignCenter, Qt::AlignCenter);
            ergSS->cell(2,i)->setFormula(QLocale().toString(landinputs[i-1]));
        }
    }
    for (int i=1; i<3; ++i){
        ergSS->cell(i,0)->setAlign(Qt::AlignLeft, Qt::AlignCenter);
        ergSS->cell(i,0)->setFormula(tvv[i-1]);
    }

    QHBoxLayout *eHL = new QHBoxLayout;
    eHL->addStretch();
    eHL->addWidget(ergSS);
    eHL->addStretch();

    QVBoxLayout *eVL = new QVBoxLayout;
    eVL->addWidget(ergLab);
    eVL->addLayout(eHL);

    ergWidget->setLayout(eVL);

    v0->addWidget(ergWidget);
    /*if (prozent>0) ergWidget->show();
    else ergWidget->hide();
    //*/

    //v0->addStretch(1);

    neuBidsButton = new QPushButton("neue Gebote");
    weiterButton = new QPushButton("mit alten Geboten weiter");

    weiterWidget  = new QWidget;
    QHBoxLayout *hl0 = new QHBoxLayout;
    hl0->addStretch();
    hl0->addWidget(neuBidsButton);
    hl0->addWidget(weiterButton);
    hl0->addStretch();

    weiterWidget->setLayout(hl0);

    v0->addWidget(weiterWidget);
    //v0->addStretch(1);

    Title2 *mlab = new Title2(Qt::AlignCenter, font1);
    mlab->setText("Meine Gebote");
    vl->addWidget(mlab);

    bidsWidget = new QWidget;

    connect(neuBidsButton, SIGNAL(clicked()), this, SLOT(neueGebote()));//bidsWidget, SLOT(show()));
    connect(weiterButton, SIGNAL(clicked()), this, SLOT(weiter()));

    connect(this, SIGNAL(rejected()), this, SLOT(weiter()));

    vector<QString> hHeader;
    hHeader.push_back(QString("Fl�chen bis (ha)"));
    hHeader.push_back(QString("Preis (")+ QChar(8364)+")");

    nRowBid = 5;
    nColBid = 2;

    QHBoxLayout *hl = new QHBoxLayout;
    hl->addStretch();

    vector<int> defaultAreas, defaultBids;
    defaultAreas.push_back(10);
    defaultAreas.push_back(30);
    defaultAreas.push_back(100);
    defaultAreas.push_back(500);
    defaultAreas.push_back(0);

    for (int i=0; i<nRowBid; ++i){
        defaultBids.push_back(1200);
    }

    QSignalMapper *sigmapBids = new QSignalMapper;

    for (int i=0; i<nsoils; ++i){
        QVBoxLayout *vl2 = new QVBoxLayout;

        QLabel *lab = new QLabel(soilNames[i].c_str());
        lab->setAlignment(Qt::AlignCenter);

        QString listr = QLocale().toString(landinputs[i])+QString(" ha");
        QLabel *labli = new QLabel(listr);
        labLandinputs.push_back(labli);
        QLabel *labsq1 = new QLabel("status quo : ");

        QHBoxLayout *hl2 = new QHBoxLayout;
        hl2->addWidget(labsq1);
        hl2->addWidget(labli);
        hl2->setAlignment(Qt::AlignCenter);

        SpreadSheet2 *s = new SpreadSheet2(nRowBid, nColBid, true, hHeader, false);
        makeEditable(s);
        s->setArt(0);

        vector<QSpinBox*> vb0, vb1;
        for (int k=0; k<nRowBid; ++k){
            QSpinBox *dsb0 = new QSpinBox;

            dsb0->setMinimum(0);
            dsb0->setMaximum(1000);
            dsb0->setValue(defaultAreas[k]);
            s->setCellWidget(k,0, dsb0);
            dsb0->setAlignment(Qt::AlignRight);
            dsb0->setSuffix("  ");
            connect(dsb0,SIGNAL(valueChanged(int)), sigmapBids, SLOT(map()));
            sigmapBids->setMapping(dsb0, 2*i*nRowBid+k );

            QSpinBox *dsb1 = new QSpinBox;

            dsb1->setMinimum(0);
            dsb1->setMaximum(3000);
            dsb1->setValue(defaultBids[k]);
            s->setCellWidget(k,1, dsb1);
            dsb1->setAlignment(Qt::AlignRight);
            dsb1->setSuffix("  ");

            connect(dsb1,SIGNAL(valueChanged(int)), sigmapBids, SLOT(map()));
            sigmapBids->setMapping(dsb1, 2*i*nRowBid+k );


            vb0.push_back(dsb0);
            vb1.push_back(dsb1);
        }
        s->adjSize();
        areaDSBs_of_type.push_back(vb0);
        bidDSBs_of_type.push_back(vb1);

        //s->updateColumn(0, defaultAreas);
        //s->updateColumn(1, defaultBids);
        bidSheets.push_back(s);

        vl2->addWidget(lab);
        vl2->addLayout(hl2);
        vl2->addSpacing(5);
        vl2->addWidget(s);

        hl->addLayout(vl2);
    }

    connect(sigmapBids,SIGNAL(mapped(int)), this, SLOT(validBidDSB(int)));

    hl->addStretch();
    vl->addLayout(hl);

    vl->addStretch();

    QHBoxLayout *hl2 = new QHBoxLayout;

    QPushButton *abgebenButton = new QPushButton("abgeben");
    connect(abgebenButton,SIGNAL(clicked()), this, SLOT(getBids()));

    hl2->addStretch();
    hl2->addWidget(abgebenButton);
    hl2->addStretch();

    vl->addLayout(hl2);

    vl->addStretch(1);
    vl->addSpacing(20);

    QWidget *freeAreaWidget = new QWidget;
    Title2 *faTit = new Title2(Qt::AlignCenter, font1);
    faTit->setText("Freie Fl�chen (ha)");

    radSBox = new QSpinBox;
    radSBox->setAlignment(Qt::AlignLeft);//Right);
    radSBox->setPrefix("im Radius von  ");
    radSBox->setSuffix("  m");
    int max =(int) (1.5*gg->NO_COLS*sqrt(gg->PLOT_SIZE)*100 )/ 1000 +1;//1.5>sqrt(2)
    max *= 1000;
    radSBox->setMaximum(max/2); //spieler in der Mitte
    radSBox->setMinimum(0);
    radSBox->setMinimumWidth(190);
    int init_dist  = 2000;
    radSBox->setValue(init_dist);
    radSBox->adjustSize();

    connect(radSBox, SIGNAL(valueChanged(int)), this, SLOT(updateFreeArea(int)));

    //vector<QString> tv;
    freeAreaSS = new SpreadSheet2(3,nsoils+1,false,tv,false);
    freeAreaSS->cell(1,0)->setFormula("insgesamt");
    freeAreaSS->setCellWidget(2,0,radSBox);
    //vector<double> freeAreas, radFreeAreas;
    /*freeAreas.push_back(500); // ??
    freeAreas.push_back(350); // ??
    radFreeAreas.push_back(100);
    radFreeAreas.push_back(50);
    //*/
    radFreeAreas = farm0->calRadFreeAreas(init_dist, Manager);

    for (int i=0; i<nsoils; ++i){
        freeAreaSS->cell(0,i+1)->setFormula(gg->NAMES_OF_SOIL_TYPES[i].c_str());
        freeAreaSS->cell(1,i+1)->setFormula(QLocale().toString(freeAreas[i]));
        freeAreaSS->cell(2,i+1)->setFormula(QLocale().toString(radFreeAreas[i]));
    }

    QHBoxLayout *faHL = new QHBoxLayout;
    faHL->addStretch();
    faHL->addWidget(freeAreaSS);
    faHL->addStretch();

    QVBoxLayout *faVL = new QVBoxLayout;
    faVL->addWidget(faTit);
    faVL->addLayout(faHL);

    freeAreaWidget->setLayout(faVL);
    vl->addWidget(freeAreaWidget);

    vl->addSpacing(25);
    QHBoxLayout *hl3 = new QHBoxLayout;
    //hl3->addStretch();
    hilfeButton = new QPushButton("weitere Hilfe zur Entscheidung");
    hl3->addWidget(hilfeButton);
    hl3->addStretch();

    vl->addLayout(hl3);


    dialBodenHilfe = new QDialog();
    dialBodenHilfe->setWindowTitle("GDB und Transportkosten f�r angepa�te Fl�chen/Preiserwartung");

    connect(hilfeButton, SIGNAL(clicked()), this, SLOT(showHilfe()));
    connect(dialBodenHilfe, SIGNAL(rejected()), this, SLOT(hilfeClosed()));

    makePeWidget();
    makeAreaWidget();

/*
    QHBoxLayout *hl4 = new QHBoxLayout;
    hl4->addWidget(peWidget);
    hl4->addStretch();
    hl4->addWidget(areaWidget);
    //hilfeGBox->setLayout(hl4);
    dialBodenHilfe->setLayout(hl4);
//*/

    QVBoxLayout *vl4 = new QVBoxLayout;
    vl4->addWidget(areaWidget);

    //vl4->addStretch();
    //vl4->addWidget(peWidget);
    dialBodenHilfe->setLayout(vl4);
    //vl->addWidget(hilfeGBox);

    bidsWidget->setLayout(vl);

    v0->addWidget(bidsWidget);

    setLayout(v0);
    firstGUI = false;
    //resize(400,500);
}

void BodenDialog::updateFreeArea(int dist){
    //TODO
    vector<int>  ds = farm0->calRadFreeAreas(dist,Manager);
    for (int i=0; i<nsoils; ++i){
        freeAreaSS->cell(2,i+1)->setFormula(QLocale().toString(ds[i]));
    }
}

void BodenDialog::validBidDSB(int x){
    int s = x / (2*nRowBid);
    int k = x % (2*nRowBid);
    bidSheets[s]->adjSize();
    bool toWarnen = false;
    if (k<nRowBid){     //areas
        if  (k==0) {
            int v1 = areaDSBs_of_type[s][k]->value();
            int v2 = areaDSBs_of_type[s][k+1]->value();
            if ((v1>=v2 && v2!=0)|| v1==0)
                toWarnen = true;
        }else if(k==nRowBid-1) {
            int v0 = areaDSBs_of_type[s][k-1]->value();
            int v1 = areaDSBs_of_type[s][k]->value();
            if ((v0>=v1 && v1!=0)||(v1!=0 && v0==0))
                 toWarnen = true;
        }else {
            int v0 = areaDSBs_of_type[s][k-1]->value();
            int v1 = areaDSBs_of_type[s][k]->value();
            int v2 = areaDSBs_of_type[s][k+1]->value();
            if ((v0>=v1 && v1!=0) ||(v0==0 && v1!=0) || (v1>=v2 && v2!=0) || (v1==0 && v2!=0))
                  toWarnen = true;
        }

        if (toWarnen) {
            BidIsValid  = false;
            QMessageBox::warning(this, QString("Problem beim Bieten"),
                    QString("Fl�chen f�r Bodentyp %1 m�ssen aufsteigend angegeben werden").arg(soilNames[s].c_str()));
        }
    }
}

void BodenDialog::updateGUI2(){
    ITER = gg->tIter;
    prozentStr = QString("%1\% der freien Fl�chen sind vergeben").arg(prozent);
    prozentLab->setText(prozentStr);

    //aktualisiertung status Quo
    for (int i=0; i<nsoils; ++i){
        landinputs[i] = farm0->getLandInputOfType(i);
        QString listr = QLocale().toString(landinputs[i])+QString(" ha");
        labLandinputs[i]->setText(listr);
    }

    /*if (prozent>0) prozentLab->show();
    else prozentLab->hide();
    //*/

    updateErg(); //Fl�chen aktualisieren

    /*if (prozent>0) ergWidget->show();//setVisible(true);//show();!!
    else ergWidget->hide();//setVisible(false);//>hide();
    //*/

    for (int i=0; i<nsoils; ++i)  {
        ergSS->cell(1,i+1)->setFormula(QLocale().toString(ergAreas[i]));
        ergSS->cell(2,i+1)->setFormula(QLocale().toString(landinputs[i]));
    }

    radFreeAreas = farm0->calRadFreeAreas(radSBox->value(),Manager);
    for (int i=0; i<nsoils; ++i){
        freeAreaSS->cell(1,i+1)->setFormula(QLocale().toString(freeAreas[i]));
        freeAreaSS->cell(2,i+1)->setFormula(QLocale().toString(radFreeAreas[i]));
    }

    makePeWidget2();
    makeAreaWidget2();
}

void BodenDialog::updateErg(){
    //ergAreas = farm0->getErgAreas();
}

void BodenDialog::makeEditable(SpreadSheet2 *s){
    int r = s->rowCount();
    int c = s->columnCount();

    for (int i=0; i<r; ++i){
        for (int j=0; j<c; ++j){
            s->cell(i,j)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable);
        }
    }
}

void BodenDialog::updateRef(int x){
    /*for (int i=0; i<numPEs; ++i){
       prodRefPEs[i]=0;
       for (int k=0; k<x; ++k) {
            prodRefPEs[i] += allPEs[ITER-k-1][i];
       }
       prodRefPEs[i] /= x;
    }
    //*/
    calRefPrices();
    peSS->updateColumn(3, prodRefPrices);
}

void BodenDialog::makePeWidget(){
    Title2* peTitle = new Title2(Qt::AlignCenter, font0);
    peTitle->setText("Preiserwartung");

    QLabel *vartype = new QLabel("Variante");
    peVarCBox = new QComboBox;
    peVarCBox->addItem(QString("Manuell"));
    peVarCBox->addItem(QString("Naiv"));
    peVarCBox->addItem(QString("Adaptiv"));

    connect(peVarCBox, SIGNAL(currentIndexChanged(int)), this, SLOT(updateVar(int)));

    QHBoxLayout *hl = new QHBoxLayout;
    hl->addStretch();
    hl->addWidget(vartype);
    hl->addWidget(peVarCBox);
    hl->addStretch();

    QLabel *refLab = new QLabel("Referenz-Zeit");
    refzeitSBox = new QSpinBox;
    refzeitSBox->setMaximum(ITER);
    refzeitSBox->setValue(1);
    refzeitSBox->setMinimum(1);
    QLabel *jahrLab = new QLabel("Jahre");

    connect(refzeitSBox, SIGNAL(valueChanged(int)), this, SLOT(updateRef(int)));

    QHBoxLayout *hl1 = new QHBoxLayout;
    hl1->addStretch();
    hl1->addWidget(refLab);
    hl1->addWidget(refzeitSBox);
    hl1->addWidget(jahrLab);
    hl1->addStretch();

    //*/

    vector<QString> vs;
    vs.push_back("Produktname");
    vs.push_back(QString("Einheit "));
   /* vs.push_back(QString("Preis "));
    vs.push_back(QString("")+QChar(0x0D8)+QString("Preis der Ref-Zeit"));
    vs.push_back(QString("PE von Modell"));
    vs.push_back(QString("Meine PE"));
    vs.push_back(QString("Preisindex"));
    //*/

    vs.push_back(QString("Preis (")+QChar(0x20ac)+")");
    vs.push_back(QString("")+QChar(0x0D8)+QString("Preis der Ref-Zeit(")+QChar(0x20ac)+")");
    vs.push_back(QString("PE von Modell(")+QChar(0x20ac)+")");
    vs.push_back(QString("Meine PE (")+QChar(0x20ac)+")");
    vs.push_back(QString("Preisindex"));
//*/

    int xcount = 0;
    vector<RegProductInfo> pcat = *(farm0->product_cat);
    int sz = pcat.size();
    bool refnameFound = false;

    for (int i=0; i<sz; ++i){
        //if (pcat[i].getProductGroup()<0) continue;
        if (pcat[i].getProductGroup()== 0  && !refnameFound) {
            refname = QString(pcat[i].getName().c_str());
            refprodID = i;
            refpos = xcount;
            refnameFound = true;

        }else if (pcat[i].getProductGroup()==0){
            getreideIDs.push_back(i);
            getreidePEs.push_back(pcat[i].getPriceExpectation());
            getreidePrices.push_back(pcat[i].getPrice());
            continue;
        }

        ++xcount;
        prodNames.push_back(QString(pcat[i].getName().c_str()));
        prodPrices.push_back(pcat[i].getPrice());
        prodIDs.push_back(i);
        prodPEs.push_back(pcat[i].getPriceExpectation());
        prodUnits.push_back(pcat[i].getUnit().c_str());
        //prodRefPEs.push_back(100);
    }
    prodRefPEs = prodPEs;
    prodRefPrices = prodPrices;
    naivPEs = prodPrices;
    adaptPEs = prodPEs;
    manPEs = prodPEs;
    numPEs = prodIDs.size();

    if (ITER==1) //0-te Iteration kein LA
        allPrices.push_back(prodPrices);
    allPrices.push_back(prodPrices);
    calRefPrices();

    nRowPE = xcount; //25;
    nColPE = 6;
    peSS = new SpreadSheet2(nRowPE, nColPE, true, vs, false);
    peSS->setArt(2);
    peSS->setTabKeyNavigation(false);

    QSignalMapper *sigmapPE = new QSignalMapper;

    peVar = peVarCBox->currentIndex();
    //if (peVar==0) {
        for (int r=0; r<nRowPE; ++r){
            QDoubleSpinBox *dsb = new QDoubleSpinBox();
            //QLineEdit *dsb = new QLineEdit();

            double max, min;
            max = prodPrices[r]*1.5;
            min = prodPrices[r]*0.7;
            if (max<0) {
                double t = max;
                max = min;
                min = t;
            }

           // MyDoubleValidator *dv = new MyDoubleValidator(min,max,10);
           //dv->setNotation(QDoubleValidator::StandardNotation);
           //dsb->setValidator(dv);
            dsb->setRange(min,max);
            dsb->setAlignment(Qt::AlignRight);
            dsb->setSuffix("  ");
            dsb->setLocale(QLocale::German);
            dsb->setValue(prodPrices[r]);//setText(QLocale().toString(prodPrices[r],'f',2));
            peSS->setCellWidget(r,nColPE-1,dsb);

            peDSBs.push_back(dsb);
            connect(dsb, SIGNAL(valueChanged(double)), sigmapPE, SLOT(map()));
            sigmapPE->setMapping(dsb, r);
            //peSS->cell(r,nColPE-1)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable);
            //peSS->cell(r,nColPE-1)->setBackgroundColor(cellBColor);
        }
    //}
    connect(sigmapPE, SIGNAL(mapped(int)), this, SLOT(updatePE(int)));

    peSS->updateColumn(0, prodNames);
    peSS->updateColumn(1, prodUnits);
    peSS->updateColumn(2, prodPrices);
    peSS->updateColumn(3, prodRefPrices);
    peSS->updateColumn(4, prodRefPEs);
    //peSS->updateColumn(3, prodPEs);

    peWidget=new QWidget;

    //refname=QString("Weizen");
    preisIndex = prodPEs[refpos]/prodPrices[refpos];//1.101;
    pindexLab =  new QLabel(QString("Preisindex f�r alle Marktfr�chte (wie der von %1) : %2").arg(refname, QLocale().toString(preisIndex)));

    //QScrollArea *sa = new QScrollArea;
    //sa->setWidget(s);

    QVBoxLayout *vl = new QVBoxLayout;
    vl->addWidget(peTitle);
    vl->addSpacing(5);

    QGridLayout *gl = new QGridLayout;
    gl->addLayout(hl, 0, 0);
    gl->addLayout(hl1, 0, 1);
    //vl->addLayout(hl);
    //vl->addLayout(hl1);
    //vl->addStretch();
    vl->addLayout(gl);
    vl->addWidget(peSS);
    vl->addWidget(pindexLab);

    // vl->addStretch(1);
    peWidget->setLayout(vl);

}

void BodenDialog::makePeWidget2(){
    refzeitSBox->setMaximum(ITER);

    vector<RegProductInfo> pcat = *(farm0->product_cat);
    int sz = numPEs;//pcat.size();

    for (int i=0; i<sz; ++i){
        prodPrices[i]=pcat[prodIDs[i]].getPrice();
        prodPEs[i]=pcat[prodIDs[i]].getPriceExpectation();
    }
    prodRefPEs = prodPEs;
    prodRefPrices = prodPrices;
    naivPEs = prodPrices;
    adaptPEs = prodPEs;
    manPEs = prodPEs;

    if(proz0==0)
        allPrices.push_back(prodPrices);
    calRefPrices();

    peVar = peVarCBox->currentIndex();
    //if (peVar==0) {
        for (int r=0; r<nRowPE; ++r){
            //cout << "pe2: "<<r<<endl;
            double max, min;
            max = prodPrices[r]*1.5;
            min = prodPrices[r]*0.7;
            if (max<0) {
                double t = max;
                max = min;
                min = t;
            }
            QDoubleSpinBox *dsb=peDSBs[r]; //QDoubleSpinBox *dsb = peDSBs[r];
            dsb->setRange(min,max);
            dsb->setValue(prodPEs[r]);//setText(QLocale().toString(prodPrices[r],'f',2));
         }
    //}

    peSS->updateColumn(2, prodPrices);
    peSS->updateColumn(3, prodRefPrices);
    peSS->updateColumn(4, prodRefPEs);
    //peSS->updateColumn(3, prodPEs);

    preisIndex = prodPEs[refpos]/prodPrices[refpos];
    pindexLab->setText(QString("Preisindex f�r alle Marktfr�chte (wie der von %1) : %2").arg(refname, QLocale().toString(preisIndex)));
}

void BodenDialog::calRefPrices(){
    int sz = prodNames.size();
    int x = refzeitSBox->value();
    prodRefPrices.clear();
    prodRefPrices.resize(sz);
    for (int i=0; i<sz; ++i){
       prodRefPrices[i] = 0;
       for (int k=0; k<x; ++k)  {
            prodRefPrices[i] += allPrices[ITER-k-1][i];
       }
       prodRefPrices[i] /= x;
    }
}

void BodenDialog::updateVar(int i){
   peVar = i;
//peSS->updateColumn(2, prodPEs);
    bool en = false;
    if (i==0) {
        en = true;
    }

    vector<double> tvalues ;
    switch (i) {
    case 0: tvalues = manPEs;break;
    case 1: tvalues = naivPEs; break;
    case 2: tvalues = adaptPEs; break;
    default: ;
    }

    for (int i=0; i<numPEs; ++i){
        peDSBs[i]->setValue(tvalues[i]);

    }

    updateIndexLab();
    /*if (i==0) {
        for (int r=0; r<nRowPE; ++r){
            peSS->cell(r,nColPE-1)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable);
            peSS->cell(r,nColPE-1)->setBackgroundColor(cellBColor);
        }
    }else{
        for (int r=0; r<nRowPE; ++r){
            peSS->cell(r,nColPE-1)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
            peSS->cell(r,nColPE-1)->setBackgroundColor(Qt::white);
        }
    }
 //*/

    for (int r=0; r<nRowPE; ++r){
        static_cast<QDoubleSpinBox*>(peSS->cellWidget(r,nColPE-1))->setEnabled(en);
    }
}

void BodenDialog::makeAreaWidget(){
    Title2* areaTitle = new Title2(Qt::AlignCenter, font0);
    areaTitle->setText("Fl�chenoptionen");

    /*QLabel *stype = new QLabel("Bodentyp");
    soiltypeCBox = new QComboBox;

    for (int i=0; i<nsoils; ++i){
        soiltypeCBox->addItem(soilNames[i].c_str());
    }


    QHBoxLayout *hl = new QHBoxLayout;
    hl->addStretch();
    hl->addWidget(stype);
    hl->addWidget(soiltypeCBox);
    hl->addStretch();

    connect(soiltypeCBox, SIGNAL(currentIndexChanged(int)), this, SLOT(updateSoilType(int)));
    //*/
    vector<QString> vs;
    for (int i=0; i<nsoils; ++i){
        vs.push_back(QString("Fl�chenzuwachs (ha) ")+ gg->NAMES_OF_SOIL_TYPES[i].c_str()); //zuzupachtende Fl�chen (ha)");
    }
    vs.push_back(QString("Gesamtdeckungsbeitrag(GDB) (")+QChar(0x20ac)+"/ha)");
    vs.push_back(QString("")+QChar(0x0394)+"GDB ("+QChar(0x20ac)+ "/ha) (zu status quo)");
    vs.push_back(QString("")+QChar(0x0394)+"Transportkosten ("+QChar(0x20ac)+"/ha)");
    vs.push_back(QString("max. Pachtgebot (")+QChar(0x20ac)+ "/ha)"); //?? Bemerkung !!!

    nRowArea = 4+nsoils;
    nColArea = 6;
    areaSS = new SpreadSheet2(nRowArea, nColArea, false, vs, false);
    areaSS->setArt(1);
    areaSS->updateColumn(0, vs);
    areaSS->setTabKeyNavigation(false);

    areaSS->cell(0,1)->setFormula("status quo");
    areaSS->cell(1,1)->setFormula("status quo");

    //int type = soiltypeCBox->currentIndex();
    for (int i=0; i<nsoils; ++i){
        varAreas.push_back(farm0->getBidAreasOfType(i));
    }

    QSignalMapper *sigmapArea = new QSignalMapper;
 for (int j=0; j<nsoils; ++j){
    vector<QSpinBox*> areas;
    for (int i=2; i<nColArea; ++i){
        QSpinBox *dsb = new QSpinBox();
        //dsb->setMinimum(1);
        dsb->setMaximum(500);
        dsb->setSuffix(" ");
        dsb->setAlignment(Qt::AlignRight);
        dsb->setValue(varAreas[j][i-1]);
        areaSS->setCellWidget(j,i,dsb);
        connect(dsb,SIGNAL(valueChanged(int)), sigmapArea,SLOT(map()));
        sigmapArea->setMapping(dsb,j*nColArea+i);
        areas.push_back(dsb);
        /*areaSS->cell(0,i)->setBackgroundColor(cellBColor);
        areaSS->cell(0,i)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable);;
        areaSS->cell(0,i)->setFormula(QString::number(varAreas[i-1]));
        //*/
    }
    areaDSBs.push_back(areas);
 }
    areaSS->adjSize();

    connect(sigmapArea,SIGNAL(mapped(int)),this, SLOT(updateArea2(int)));
    //updateSoilType(type);
    updateAreas();

    areaWidget=new QWidget;
    QVBoxLayout *vl = new QVBoxLayout;

    vl->addWidget(areaTitle);
    vl->addSpacing(5);
    //vl->addLayout(hl);

    QHBoxLayout *hh0 = new QHBoxLayout;
    hh0->addStretch();
    hh0->addWidget(areaSS);
    hh0->addStretch();
    vl->addLayout(hh0);

    vl->addSpacing(25);
    peButton = new QPushButton("Preiserwartung anpassen");
    showPEwidget=false;
    peWidget->setVisible(showPEwidget);//hide();
    connect(peButton, SIGNAL(clicked()), this, SLOT(blendenPE()));

    QHBoxLayout *hl5 = new QHBoxLayout;
    hl5->addWidget(peButton);
    hl5->addStretch();

    vl->addLayout(hl5);

    QHBoxLayout *hh = new QHBoxLayout;
    hh->addStretch();
    hh->addWidget(peWidget);
    hh->addStretch();
    vl->addLayout(hh);

    vl->addStretch(1);
    areaWidget->setLayout(vl);
}

void BodenDialog::makeAreaWidget2(){
    //int type = soiltypeCBox->currentIndex();
  varAreas.clear();
  for (int i=0; i<nsoils; ++i){
    varAreas.push_back(farm0->getBidAreasOfType(i));
  }
  for (int j=0; j<nsoils; ++j){
    for (int i=2; i<nColArea; ++i){
        QSpinBox *dsb = areaDSBs[j][i-2];
        dsb->setValue(varAreas[j][i-1]);
    }
  }
  //updateSoilType(type);
  updateAreas();
}


void BodenDialog::blendenPE(){
    if (!showPEwidget) {
        /*peSS->updateColumn(0, prodNames);
        peSS->updateColumn(1, prodPrices);
        peSS->updateColumn(2, prodRefPEs);
        //*/
        //peSS->updateColumn(3, prodPEs);
         //peWidget->show();
         peButton->setText("Ausblenden Preiserwartung");
    }else{
         //peWidget->hide();
         peButton->setText("Preiserwartung anpassen");
    }
    peWidget->setVisible(!showPEwidget);
    showPEwidget = !showPEwidget;

    peWidget->parentWidget()->layout()->invalidate();
    QWidget *parent = peWidget->parentWidget();
    while (parent) {
        parent->adjustSize();
        parent = parent->parentWidget();
    }
}

void BodenDialog::neueGebote(){
   neuBidsButton->setEnabled(false);
   bidsWidget->show();
}

void BodenDialog::weiter(){
    if (proz0==0) {
        //TODO
        vector<int> maxFreeAreas;
        maxFreeAreas.push_back(1000); // ?ha
        maxFreeAreas.push_back(1000); // ?ha

        vector<vector<pair<int,int> > > offers_of;
        for (int i = 0; i<nsoils; ++i){
            vector<pair<int,int> > offers;
            offers.push_back(pair<int,int>(maxFreeAreas[i],0));
            offers_of.push_back(offers);
        }
        farm0->offers_of_type = offers_of;
    }

    if(proz>100){
        inited=false;
        Manager->product_cat0 = Manager->Market->getProductCat();
    }
    close();
}

void BodenDialog::hilfeClosed(){
    hilfeButton->setEnabled(true);
}

void BodenDialog::showHilfe(){
    dialBodenHilfe->show();
    hilfeButton->setEnabled(false);
}

void BodenDialog::savePEs(){
    if (proz0==0) it_myPEs.clear();

    switch(peVar) {
    case 0: it_myPEs[proz0] = manPEs; break;
    case 1: it_myPEs[proz0] = naivPEs; break;
    case 2: it_myPEs[proz0] = adaptPEs; break;
    default: ;
    }

    if (proz >=100) all_myPEs.push_back(it_myPEs);
}

void BodenDialog::getBids(){
    savePEs();
    if (!validBids()) return ;

    weiterButton->setEnabled(false);
    if (proz0==0) it_bids.clear();

    vector<vector <pair<int, int> > > offers_of_type;
    for (int i=0; i<nsoils; ++i){
        int maxRow = maxRows[i];
        vector<pair<int, int> > offers;
        for (int k=0; k<=maxRow; ++k){
            int f = areaDSBs_of_type[i][k]->value();
            int p = bidDSBs_of_type[i][k]->value();
            offers.push_back(pair<int, int>(f,p));
        }

        offers_of_type.push_back(offers);
    }

    it_bids[proz0] = offers_of_type;

    farm0->offers_of_type = offers_of_type;
    farm0->initNewOffers();

    if (proz>=100) all_bids.push_back(it_bids);
    accept(); //ohne diese w�re rejected();
    if (proz>=100) {
        inited = false;
        //PE zur�cksetzen
        Manager->product_cat0 = Manager->Market->getProductCat();
    }

    close();
}

void BodenDialog::getBids0(){
    savePEs();
    validBids();
    weiterButton->setEnabled(false);

    if (proz0==0) it_bids.clear();

    vector<vector <pair<int,int> > > offers_of_type;
    for (int i=0; i<nsoils; ++i){
        int maxRow = maxRows[i];
        vector<pair<int, int> > offers;
        for (int k=0; k<=maxRow; ++k){
            double f = bidSheets[i]->cell(k,0)->data(Qt::DisplayRole).toDouble();
            double p = bidSheets[i]->cell(k,1)->data(Qt::DisplayRole).toDouble();
            offers.push_back(pair<int,int>(f,p));
        }

        offers_of_type.push_back(offers);
    }

    it_bids[proz0] = offers_of_type;

    farm0->offers_of_type = offers_of_type;
    farm0->initNewOffers();

    if (proz>=100) all_bids.push_back(it_bids);
    accept(); //ohne diese w�re rejected();
    close();
}

bool BodenDialog::validBids(){
    for (int i=0; i<nsoils; ++i){
        if (!validSoilType(i)) {
            return false;
        }
    }
    return true;
}

void BodenDialog::validSoilType0(int t){
    SpreadSheet2 *s = bidSheets[t];
    int row = s->rowCount();
    vector<double> ds;
    for (int i=0; i<row; ++i){
        ds.push_back((s->cell(i,0))->data(Qt::DisplayRole).toDouble());
    }
    int maxRow=0;
    int p = row-1;
    while (p >=0 ){
        if (ds[p]>0) {
            maxRow = p;
            break;
        }
        --p;
    }
    if (ds[0] <=0)
        QMessageBox::warning(this, QString("Problem beim Bieten"),
        QString("Anfangsfl�chen f�r Bodentyp %1 m�ssen gr��er 0 sein").arg(soilNames[t].c_str()));

    else {
      for (int i=maxRow; i>0; --i){
        if (ds[i] < ds[i-1]) {
            QMessageBox::warning(this, QString("Problem beim Bieten"),
            QString("Fl�chen f�r Bodentyp %1 m�ssen aufsteigend angegeben werden").arg(soilNames[t].c_str()));
            break;
        }
      }
    }

    for (int i=0; i<row; ++i){
        double d = s->cell(i,1)->data(Qt::DisplayRole).toDouble();
        if (d<0) {
            QMessageBox::warning(this, QString("Problem beim Bieten"),
            QString("Gebote f�r Bodentyp %1 m�ssen gr��er 0 sein").arg(soilNames[t].c_str()));
            break;
        }
    }

    maxRows[t] = maxRow;
}

bool BodenDialog::validSoilType(int t){
    SpreadSheet2 *s = bidSheets[t];
    int row = s->rowCount();
    vector<int> ds;
    for (int i=0; i<row; ++i){
        ds.push_back(areaDSBs_of_type[t][i]->value());
    }
    int maxRow=0;
    int p = row-1;
    while (p >=0 ){
        if (ds[p]>0) {
            maxRow = p;
            break;
        }
        --p;
    }
      for (int i=maxRow; i>0; --i){
        if (ds[i] <= ds[i-1]) {
            QMessageBox::warning(this, QString("Problem beim Bieten"),
            QString("Fl�chen f�r Bodentyp %1 m�ssen aufsteigend angegeben werden").arg(soilNames[t].c_str()));
            return false;
            //break;
        }
      }
    maxRows[t] = maxRow;
    return true;
}


void BodenDialog::closeEvent(QCloseEvent * event){
/*
    int n = gg->NO_OF_SOIL_TYPES;

    for (int i=0; i<n; ++i){
        vector<pair<double, double> > tmpOffers;
        tmpOffers.push_back(pair<double,double>(10, 1000));
        tmpOffers.push_back(pair<double,double>(20, 1200));
        tmpOffers.push_back(pair<double,double>(30, 1500));

        offers_of_type.push_back(tmpOffers);
    }


    Manager->Farm0->offers_of_type = offers_of_type;

    for (int i=0; i<n; ++i){
       int ind = Manager->Farm0->offer_index_of_type[i] = 0;
       Manager->Farm0->offer_of_type[i] = offers_of_type[i][ind].second;
    }
//*/

    gg->SpielerHasBids = true;
    if (prozent>=100){
        emit fertig();
        parent->mydatWindow->hide();
        parent->actMyData->setEnabled(true);
        Manager->getGlobals()->SIM_WEITER =true;
    }

    dialBodenHilfe->close();
    QDialog::closeEvent(event);
}

void BodenDialog::dispOffer(int i, double d){
    l1->setText(QString::number(i));
    l2->setText(QString::number(d));
}

void BodenDialog::okPushed(){
    double d1,d2;
    d1 = le1->text().toInt();
    d2 = le2->text().toDouble();
    emit offer(d1,d2);

    //updateFreePlots();
    //updateOffers();

    emit ok();
}

static bool compTransportCosts(RegPlotInfo* p1, RegPlotInfo* p2){
    RegPlotInfo *p = Manager->Farm0->farm_plot;
    double d1 = p->calculateDistanceCosts(p1);
    double d2 = p->calculateDistanceCosts(p2);
    return (d1<=d2);
}

void BodenDialog::updateFreePlots(){
  /*  vector<RegPlotInfo*> fplots = Manager->Region->free_plots;
    int sz = fplots.size();

    for (int i=0; i<sz; ++i){
        int t = fplots[i]->getSoilType();
        free_plots_of_type[t].push_back(fplots[i]);
    }

    int n = gg->NO_OF_SOIL_TYPES;
    for (int i=0; i<n; ++i){
        sort(free_plots_of_type[i].begin(), free_plots_of_type[i].end(), compTransportCosts);
    }
//*/
}

void BodenDialog::updateOffers(){
    Manager->Farm0->offers_of_type = offers_of_type;
}

void BodenDialog::showBoden(int p0, int p){
    parent->mydatWindow->layerComboBox->setCurrentIndex(3); //pachtbilanz
    parent->mydatWindow->show();
    parent->actMyData->setEnabled(false);

    proz0= p0;
    proz = p;

    if(proz0==0)  //aktualisieren
        Manager->product_cat0 = Manager->Market->getProductCat();

    inited=false;
    updateGUI();
    inited=true;
    /*if (proz0==0) {
        updateGUI();
        inited = true;
    }
    //*/
    peSS->setInited(true);
    areaSS->setInited(true);
    for (int i=0; i<nsoils; ++i){
        bidSheets[i]->setInited(true);
    }

    prozent = proz;
    prozentStr = QString("%1\% der freien Fl�chen sind vergeben").arg(proz0);
    prozentLab->setText(prozentStr);
    if (proz0>0){
        prozentLab->show();
        bidsWidget->hide();
        weiterWidget->show();
        ergWidget->show();
    }else{
        prozentLab->hide();
        bidsWidget->show();
        weiterWidget->hide();
        ergWidget->hide();
    }
    weiterButton->setEnabled(true);
    neuBidsButton->setEnabled(true);

    //aktualisiertung status Quo
    for (int i=0; i<nsoils; ++i){
        landinputs[i] = farm0->getLandInputOfType(i);
        QString listr = QLocale().toString(landinputs[i])+" ha";
        labLandinputs[i]->setText(listr);
    }

    updateIndexLab();
    //if (proz>=100) inited = false;
    show();
    //exec();//show();
}

void BodenDialog::updateSoilType(int type){
        soiltype = type;
/*
        defaultAreas = farm0->getBidAreasOfType(type);
        int num=defaultAreas.size();
        int r=0;
        for(int x=1; x<num; ++x){
            areaDSBs[x-1]->setValue(farm0->getBidAreasOfType(type)[x]);
            //areaSS->cell(r,x+1)->setFormula(QLocale().toString(farm0->getGDB(type,x),'f',2));
        }

        r=1;
        for(int x=0; x<num; ++x){
            areaSS->cell(r,x+1)->setFormula(QLocale().toString(farm0->getGDB(type,x),'f',2));
        }

        r=2;
        for(int x=0; x<num; ++x){
            double tt=farm0->getGDB(type,x)-farm0->getGDB(type,0);
            if (x==0)areaSS->cell(r,x+1)->setFormula(QLocale().toString(tt,'f',2));
            else areaSS->cell(r,x+1)->setFormula(QLocale().toString(tt/defaultAreas[x],'f',2));
        }

        r=3;
        for(int x=0; x<num; ++x){
            areaSS->cell(r,x+1)->setFormula(QLocale().toString(farm0->getTrK(type,x),'f',2));
        }

        r=4;
        for(int x=0; x<num; ++x){
            double tt = areaSS->cell(r-2,x+1)->data(Qt::DisplayRole).toDouble()-areaSS->cell(r-1,x+1)->data(Qt::DisplayRole).toDouble();
            areaSS->cell(r,x+1)->setFormula(QLocale().toString(tt));
        }
        //*/
}

void BodenDialog::updateAreas(){
    int r=0;
    int num=0;
    for (int type=0; type<nsoils; ++type){
        varAreas[type] = farm0->getBidAreasOfType(type);
        num=varAreas[type].size();
        for(int x=1; x<num; ++x){
            areaDSBs[type][x-1]->setValue(farm0->getBidAreasOfType(type)[x]);
            //areaSS->cell(r,x+1)->setFormula(QLocale().toString(farm0->getGDB(type,x),'f',2));
        }
    }

        r=2;
        for(int x=0; x<num; ++x){
            areaSS->cell(r,x+1)->setFormula(QLocale().toString(farm0->getGDB(x),'f',2));
        }

        ++r;//=3;
        for(int x=0; x<num; ++x){
            double tt=farm0->getGDB(x)-farm0->getGDB(0);
            double asum=0;
            for (int i=0; i<nsoils; ++i){
                asum += varAreas[i][x];
            }
            if (x==0)areaSS->cell(r,x+1)->setFormula(QLocale().toString(tt));
            else areaSS->cell(r,x+1)->setFormula(QLocale().toString(tt/asum,'f',2));
        }

        ++r;
        for(int x=0; x<num; ++x){
            if (x==0) areaSS->cell(r,x+1)->setFormula(QLocale().toString(farm0->getTrK(x)));
            else areaSS->cell(r,x+1)->setFormula(QLocale().toString(farm0->getTrK(x),'f',2));
        }

        ++r; //=5
        for(int x=0; x<num; ++x){
            double tt = areaSS->cell(r-2,x+1)->data(Qt::DisplayRole).toDouble()-areaSS->cell(r-1,x+1)->data(Qt::DisplayRole).toDouble();
            if (x==0) areaSS->cell(r,x+1)->setFormula(QLocale().toString(tt));
            else areaSS->cell(r,x+1)->setFormula(QLocale().toString(tt, 'f',2));
        }

        //*/
}

void BodenDialog::updateArea2(int pos){
        if (!inited) return;

        //cout << "a: pos: " << pos <<endl;
        int zeile = pos/nColArea;
        int spalte= pos%nColArea;

        vector<int> ds;
        for (int i=0; i<nsoils; ++i){
            areaDSBs[i][spalte-2]->setEnabled(false);
            ds.push_back(areaDSBs[i][spalte-2]->value());
        }

        farm0->setBidAreas(spalte-1,ds);
        emit areaSig2(spalte-1);
        //*/
}

void BodenDialog::updateArea(int pos){
        if (!inited) return;

        //cout << "a: pos: " << pos <<endl;
        int zeile = pos/nColArea;
        int spalte= pos%nColArea;
        areaDSBs[zeile][spalte-2]->setEnabled(false);
        int d = areaDSBs[zeile][spalte-2]->value();
        /*int type = soiltype;
        farm0->setBidAreaOfType(type,pos-1,d);
        emit areaSig(type, pos-1);
        //*/
}

void BodenDialog::updatePE(int row){
        if (!inited) return;
        int sz = prodIDs.size();
        for (int i=0; i<sz; ++i){
            peDSBs[i]->setEnabled(false);
        }

        double d = peDSBs[row]->value();
        oldvalue = Manager->product_cat0[prodIDs[row]].getPriceExpectation();
        Manager->product_cat0[prodIDs[row]].setPriceExpectation(d);

        if (row == refpos) {
            double tind = d/prodPrices[refpos];
            getreidePEs.clear();
            int ts = getreideIDs.size();
            for (int i=0; i<ts; ++i){
                getreidePEs.push_back(Manager->product_cat0[getreideIDs[i]].getPriceExpectation());
            }
            for (int i=0; i<ts; ++i){
                Manager->product_cat0[getreideIDs[i]].setPriceExpectation(getreidePrices[i]*tind);
            }
        }

        //cout << "a: row: " << row <<endl;
        emit peSig(row);
}

void BodenDialog::updateIndexLab(){
    double refpe = peDSBs[refpos]->value();
    double refp = prodPrices[refpos];
    preisIndex = refpe/refp;
    pindexLab->setText(QString("Preisindex f�r alle Marktfr�chte (wie der von %1) : %2").arg(refname, QLocale().toString(preisIndex,'f',3)));
}

void BodenDialog::getPeOK(int r, bool b){
        if (!b) {
            inited = false;
            peDSBs[r]->setValue(oldvalue);
            inited = true;

            if (r==refpos) {
                int ts = getreideIDs.size();
                for (int i=0; i<ts; ++i){
                    Manager->product_cat0[getreideIDs[i]].setPriceExpectation(getreidePEs[i]);
                }
            }

            Manager->product_cat0[prodIDs[r]].setPriceExpectation(oldvalue);
            QMessageBox::warning(this, tr("Solver Information"),
                             tr("Bedingungen verletzt"));

        }
        int sz = prodIDs.size();
        for (int i=0; i<sz; ++i){
            peDSBs[i]->setEnabled(true);
        }

        if (!b) return;

        if (peVar == 0) manPEs[r] = peDSBs[r]->value();

        if (r==refpos) {
            updateIndexLab();
            /*  schon aktualisiert
            int ts = getreideIDs.size();
            for (int i=0; i<ts; ++i){
                ;//Manager->product_cat0[getreideIDs[i]].setPriceExpectation(getreidePrices[getreideIDs[i]]*preisIndex);
            }
            //*/
        }
        //cout << "b: row: " << r <<endl;
        //updateSoilType(soiltype);
        updateAreas();
}

void BodenDialog::getAreaOK(int type, int pos){
        /*if (type != soiltype) return;
        //cout << "b: pos: " << pos <<endl;
        areaDSBs[pos-1]->setEnabled(true);

        int r=1;
        areaSS->cell(r,pos+1)->setFormula(QLocale().toString(farm0->getGDB(type,pos),'f',2));

        r=2;
        double tt=farm0->getGDB(type,pos)-farm0->getGDB(type,0);
        areaSS->cell(r,pos+1)->setFormula(QLocale().toString(tt/areaDSBs[pos-1]->value(),'f',2));

        r=3;
        areaSS->cell(r,pos+1)->setFormula(QLocale().toString(farm0->getTrK(type,pos),'f',2));

        r=4;
        tt = areaSS->cell(r-2,pos+1)->data(Qt::DisplayRole).toDouble()
            -areaSS->cell(r-1,pos+1)->data(Qt::DisplayRole).toDouble();
        areaSS->cell(r,pos+1)->setFormula(QLocale().toString(tt,'f',2));
        //*/
}

void BodenDialog::getAreaOK2(int pos){
        //cout << "b: pos: " << pos <<endl;
    for (int i=0; i<nsoils; ++i){
        areaDSBs[i][pos-1]->setEnabled(true);
    }

    int r=2;
    areaSS->cell(r,pos+1)->setFormula(QLocale().toString(farm0->getGDB(pos),'f',2));

    ++r;
    double tt=farm0->getGDB(pos)-farm0->getGDB(0);
    double asum=0;
    for (int i=0; i<nsoils; ++i){
        asum+= areaDSBs[i][pos-1]->value();
    }
    areaSS->cell(r,pos+1)->setFormula(QLocale().toString(tt/asum,'f',2));

    ++r; //=4;
    areaSS->cell(r,pos+1)->setFormula(QLocale().toString(farm0->getTrK(pos),'f',2));

    ++r;// 5;
    tt = areaSS->cell(r-2,pos+1)->data(Qt::DisplayRole).toDouble()
            -areaSS->cell(r-1,pos+1)->data(Qt::DisplayRole).toDouble();
    areaSS->cell(r,pos+1)->setFormula(QLocale().toString(tt,'f',2));
        //*/
}

Title2::Title2(Qt::AlignmentFlag align, QFont f, QWidget*w):QLabel(w){
    this->setAlignment(align);
    this->setFont(f);
}
