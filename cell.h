#ifndef CELL_H
#define CELL_H

#include <QTableWidgetItem>

class Cell : public QTableWidgetItem
{
public:
    //Cell();
    Cell(bool b=false, bool c=false);

    QTableWidgetItem *clone() const;
    void setData(int role, const QVariant &value);
    QVariant data(int role) const;
    void setFormula(const QString &formula);
    QString formula() const;
    void setDirty();
    void setAlign(Qt::Alignment, Qt::Alignment);

private:
    QVariant value() const;
    QVariant evalExpression(const QString &str, int &pos) const;
    QVariant evalTerm(const QString &str, int &pos) const;
    QVariant evalFactor(const QString &str, int &pos) const;

    Qt::Alignment alignH;
    Qt::Alignment alignV;
    mutable QVariant cachedValue;
    mutable bool cacheIsDirty;

};

#endif
