#ifndef CURVEICON_H
#define CURVEICON_H

#include <QPixmap>
#include <QSize>

#include <qwt_symbol.h>
class QColor;

static const QSize DefaultSize1(24, 24);
static const QSize DefaultSize2(48, 24);

QPixmap canvasIcon(const QColor &color,
                 const QSize &size=DefaultSize1);
QPixmap gridlineIcon(const QColor &color,
                 const QSize &size=DefaultSize1);
QPixmap curveIcon(const QColor &color, QwtSymbol &symbol,
                 const QSize &size=DefaultSize2);
QPixmap curveSymbolIcon(const QColor &color, QwtSymbol &symbol,
                 const QSize &size=DefaultSize1);
#endif // CURVEICON_H
