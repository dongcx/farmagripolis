# generated from ===> Altmark_input.xlsm <===					
					
					
#Matrix links					
					
					
#Links -------					
					
#============	===============				
#	MatrixRow	MatrixCol	 linkType   	value	   factor
#============	===============				
	liquidity/financing_rule	Gerste	  market 	C	0.4
	liquidity/financing_rule	Winterweizen	  market 	C	0.4
	liquidity/financing_rule	Raps	  market 	C	0.4
	liquidity/financing_rule	Zuckerrueben	  market 	C	0.4
	liquidity/financing_rule	Proteinpflanzen	  market 	C	0.4
	liquidity/financing_rule	Maissilage	  market 	C	0.4
	liquidity/financing_rule	Ackergras	  market 	C	0.4
	liquidity/financing_rule	Kartoffeln	  market 	C	0.4
	liquidity/financing_rule	Roggen	  market 	C	0.4
	liquidity/financing_rule	Roggen_Biogas	  market 	C	0.4
	liquidity/financing_rule	GPS	  market 	C	0.2
	liquidity/financing_rule	Grassilage_int	  market 	C	0.2
	liquidity/financing_rule	Grassilage_ext	  market 	C	0.2
	liquidity/financing_rule	Portionsweide	  market 	C	0.2
	liquidity/financing_rule	Umtriebsweide	  market 	C	0.2
	liquidity/financing_rule	Standweide	  market 	C	0.2
	liquidity/financing_rule	Sauen	  market 	C	0.4
	liquidity/financing_rule	Mastschweine	  market 	C	0.27
	liquidity/financing_rule	Bullemast_Milch_int	  market 	C	0.66
	liquidity/financing_rule	Bullenmast_Mutterkuh	  market 	C	0.66
	liquidity/financing_rule	Mutterkuh	  market 	C	0.66
	liquidity/financing_rule	Milchkuh	  market 	C	0.2
	liquidity/financing_rule	Faerse_Mutterkuh	  market 	C	0.66
	liquidity/financing_rule	Faerse_Milchkuh	  market 	C	0.66
	liquidity/financing_rule	BIOGAS_I_150kW_A	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_II_150kW_A	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_III_150kW_A	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_IV_150kW_A	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_I_450kW_A	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_II_450kW_A	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_III_450kW_A	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_IV_450kW_A	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_I_800kW_A	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_II_800kW_A	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_III_800kW_A	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_IV_800kW_A	  market 	C	0.5
	liquidity/financing_rule	BIOGAS150KW_A	  market 	C	0.5
	liquidity/financing_rule	BIOGAS450KW_A	  market 	C	0.5
	liquidity/financing_rule	BIOGAS800KW_A	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_I_150kW_N	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_II_150kW_N	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_III_150kW_N	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_I_450kW_N	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_II_450kW_N	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_III_450kW_N	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_I_800kW_N	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_II_800kW_N	  market 	C	0.5
	liquidity/financing_rule	BIOGAS_III_800kW_N	  market 	C	0.5
	liquidity/financing_rule	BIOGAS150KW_N	  market 	C	0.5
	liquidity/financing_rule	BIOGAS450KW_N	  market 	C	0.5
	liquidity/financing_rule	BIOGAS800KW_N	  market 	C	0.5
	liquidity/financing_rule	Stilllegung	  market 	C	0.4
	liquidity/financing_rule	Verkauf_Milchkalb	  market 	C	1
	liquidity/financing_rule	Kauf_Milchkalb	  market 	C	1
	liquidity/financing_rule	Verkauf_Mutterkuhkalb	  market 	C	1
	liquidity/financing_rule	Kauf_Mutterkuhkalb	  market 	C	1
	liquidity/financing_rule	Verkauf_Guelle	  market 	C	0
	liquidity/financing_rule	Kauf_Guelle	  market 	C	0
	liquidity/financing_rule	Quote_pachten	  market 	C	0
	liquidity/financing_rule	Quote_verpachten	  market 	C	0
	liquidity/financing_rule	COUPLED_PREM_UNMOD	  market 	C	0
	liquidity/financing_rule	DECOUPLED_PREM_UNMOD	  market 	C	0
	liquidity/financing_rule	TOTAL_PREM_MODULATED	  market 	C	0
	liquidity/financing_rule	TRANCH_1	  market 	C	0
	liquidity/financing_rule	TRANCH_2	  market 	C	0
	liquidity/financing_rule	TRANCH_3	  market 	C	0
	liquidity/financing_rule	TRANCH_4	  market 	C	0
	liquidity/financing_rule	TRANCH_5	  market 	C	0
	liquidity/financing_rule	IDLE_ARABLE	  market 	C	0.2
	liquidity/financing_rule	IDLE_GRASS	  market 	C	0.2
	liquidity/financing_rule	EXCESS_LU	  market 	C	0
	dist_prem	TRANCH_1	reference	tranch1deg	
	dist_prem	TRANCH_2	reference	tranch2deg	
	dist_prem	TRANCH_3	reference	tranch3deg	
	dist_prem	TRANCH_4	reference	tranch4deg	
	dist_prem	TRANCH_5	reference	tranch5deg	
