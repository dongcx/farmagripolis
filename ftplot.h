#ifndef _FT_PLOT_H_
#define _FT_PLOT_H_

#include <qwt_plot.h>
#include <vector>
#include <map>
#include <qwt_plot_grid.h>

class Histogram;
class Datawindow;

using namespace std;
class FTPlot: public QwtPlot
{
    Q_OBJECT

public:
    bool withGrid;
    QwtPlotGrid* getGrid();

    FTPlot(vector<string>, vector<QColor>, vector< vector<double> >, Datawindow *datawin,QWidget * = NULL);
    void updatePlot(const vector<string>, const vector<QColor>, const vector< vector<double> >);
    vector< map<int,double> >  bases;
    int serie;

    void updateColor(int,QColor);
    void updateColors();

private:
    void populate();

private Q_SLOTS:
     void showItem(QwtPlotItem *, bool on);
private:
     QwtPlotGrid *grid;
     vector <string> names;
     vector <QColor> colors;
     vector <vector <double> > datas;
     vector<Histogram*> histogs;

     Datawindow *datawin;
};

#endif
