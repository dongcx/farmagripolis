#ifndef _CPLOT_H_
#define _CPLOT_H_

#include <qwt_plot.h>
#include <vector>
#include <qwt_symbol.h>

class QwtPlotCurve;
class QwtPlotMarker;
class QwtPlotGrid;
class Datawindow;
class QwtPlotPicker;
class QMainWindow;

using namespace std;
class Plot: public QwtPlot
{
    Q_OBJECT

public:
    bool withGrid;
    QwtPlotGrid * getGrid();
    Plot(int, QMainWindow *dataw, QWidget *parent);//Datawindow* dataw, QWidget *parent);
    int numCurves();
    QwtPlotItem* getCurve(int);

signals:
    void dichteShow(bool);

public Q_SLOTS:
    void updatePlot();
    void showHideCurve(int);
    void showCurve(QwtPlotItem *item, bool on);


private:
    void makeCurves(int);
    void makeMarkers(int);
    void makeMarkers(int,bool);

    vector<QwtPlotCurve*> curves;
    vector<QwtPlotMarker*> markers;

    QwtPlotGrid *grid;

    int plotn;  //Which Plot
    int nCurves;
    QMainWindow *datawin;//Datawindow *datawin;
    bool hasCurves;

    vector<double> xdata;
    vector<vector<double> > ydatas;
    vector<string> ytitles;
    vector<int> yaxes;
    vector<bool> shows;
    vector<QColor> colors;

    vector<QwtSymbol::Style> styles;

    QwtLegend *legend;
    QwtPlotPicker *d_pickerL, *d_pickerR;
    bool hasMarkers;

};

#endif
