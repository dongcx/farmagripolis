//  functions for reading options from options.txt
/* Dong Changxing, 2011  */
//==================================================
#ifndef AGRIPOLIS_H
#define AGRIPOLIS_H

#include <vector>
#include <string>

#include "RegManager.h"
#include "RegGlobals.h"

extern RegManagerInfo* Manager;
extern RegGlobalsInfo* gg;

using namespace std;

void agpinit(string, string);
int agprun();

void tokenize(const string& str,
                      vector<string>& tokens,
                      const string& delimiters = ": \t=;");
void readoptions();

void setoptions();

void options(string idir);

#endif
