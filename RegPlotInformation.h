#ifndef RegPlotInformationH
#define RegPlotInformationH
class RegPlotInfo;
class RegPlotInformationInfo {
public:
    RegPlotInformationInfo();
    double tc;
    double farm_tac;
    double tac;
    double pe;
    double alternative_search_value;
    RegPlotInfo* plot;
    double costs();
    double alternativeSeachCosts() const;
    bool operator<=(RegPlotInformationInfo&  p1);
    bool operator>(RegPlotInformationInfo&  p1);
};


#endif
