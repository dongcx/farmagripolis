#include "decThread.h"
#include <iostream>

#include "AgriPoliS.h"
#include "RegPlotInformation.h"
#include "RegPlot.h"

using namespace std;

DecThread::DecThread():QThread() {
  stopped = false;
  farm0 = 0;
}

void DecThread::setFarm(RegFarmInfo *f){
    farm0 = f;
}

void DecThread::run(){
//Manager->getGlobals()->DEC_THREAD=qobject_cast<DecThread*>(currentThread());
exec();
}

void DecThread::emitAreaOK(int t, int p){
    emit areaOK(t,p);
}

void DecThread::emitAreaOK2B(int t, int p){
    emit areaOK2B(t,p);
}


void DecThread::getAreaSig2(int pos){
    farm0->calGDB(pos);
    farm0->calTrK(pos);
    emit areaOK2(pos);
}

void DecThread::getAreaSig2B(int typ, int pos){
    farm0->calGDB2B(typ,pos);
    emitAreaOK2B(typ,pos);
}

void DecThread::getAreaSig(int type, int pos){
    //farm0->setBidAreaOfType(type,pos,d);
    farm0->calGDB(type,pos);
    farm0->calTrK(type,pos);
    emit areaOK(type, pos);
}

void DecThread::getPeSig(int r){
    if (farm0->calGDBsTry())
        emit peOK(r, true); //war erfolgreich
    else
        emit peOK(r, false);
}

void DecThread::getPeVarSig(int r){
    if (farm0->calGDBsTry())
        emit peVarOK(r, true); //war erfolgreich
    else
        emit peVarOK(r, false);
}

void DecThread::getClosePeSig(int r){
    if (farm0->farmFutureTry(gg->tIter))
        emit ClosePeOK(r, true); //war erfolgreich
    else
        emit ClosePeOK(r, false);
}

void DecThread::getClosePeVarSig(int r){
    if (farm0->farmFutureTry(gg->tIter))
        emit ClosePeVarOK(r, true); //war erfolgreich
    else
        emit ClosePeVarOK(r, false);
}


void DecThread::simsig(){
    cout << Manager->FarmList.front()->getFarmId()<< "  Was ??" << endl;
}

void DecThread::decboden(){
    emit odecboden();
}

void DecThread::decinvest(){
    emit odecinvest();
}

void DecThread::decclose(){
    emit odecclose();
}

void DecThread::stop(){
    stopped = true;
}

void DecThread::getBodenOk(){
    //free_plots_of_type & offers_of_type were updated in other thread
    int n = gg->NO_OF_SOIL_TYPES;
    for (int i=0; i<n; ++i){
        farm0->newly_rented_of_type[i]=0;
        //farm0->index_of_type[i]=0;
        farm0->offer_index_of_type[i]=0;
        farm0->offer_of_type[i] = farm0->offers_of_type[i][farm0->offer_index_of_type[i]].second;
    }
}

void DecThread::getLAsig(int x, int y){
    int nsoils = gg->NO_OF_SOIL_TYPES;
    double delta  = gg->PLOT_SIZE;
    int numpersoil = 4;
    if (gg->tIter == 1 && x==0) {
        //farm0->GDBs_of_type.resize(nsoils);
        //farm0->TranspCosts_of_type.resize(nsoils);
        farm0->bidAreas_of_type.resize(nsoils);

        vector<int> areas;
        areas.push_back(0);
        areas.push_back(2*delta);
        areas.push_back(4*delta);
        areas.push_back(10*delta);
        areas.push_back(20*delta);

        for (int i=0; i<nsoils; ++i) {

            vector<int> as ;
            int nx = nsoils*numpersoil+1;
            as.resize(nx);
            for (int k=0; k<nx; ++k){
                if (k<=numpersoil*(i+1) && k>numpersoil*i)
                    as[k]=areas[(k-1)%numpersoil + 1];
            }
            farm0->bidAreas_of_type[i]=as;
        }
    }

    // if (x==0) {  //neue iteration
        //farm0->initNewOffers();
        vector<vector<RegPlotInformationInfo> > free_plots_of_type;
        free_plots_of_type = *(farm0->farm_plot->getFreePlots());
        vector<double> pe_of_type;
        for (int i=0; i<nsoils; ++i){
            vector<RegPlotInformationInfo> vecinfos = free_plots_of_type[i];
            int sz = vecinfos.size();
            double av, total=0;
            int num = 0;
            for (int j=0; j<sz; ++j){
                total += vecinfos[j].pe;
                ++num;
            }
            av = total/num;
            pe_of_type.push_back(av);
        }
        farm0->setAvPes(pe_of_type);

        farm0->calGDBs();
        farm0->calGDBs2B();
        farm0->calTrKs();
    //}
    //cout << "vor showboden"<<endl;
    emit showBodenDialog(x, y);
}

void DecThread::getLAsig(int p){
    emit showBodenDialog(p);
}

void DecThread::getFoFsig(int p){
    farm0->farmFuture(p);
    emit showCloseDialog(farm0->closed);
}

void DecThread::getCloseSig(bool b){
    //farm0->farmFutureEnd(b);
    //gg->HasCloseDec = true;
}

void DecThread::getINVsig(){
    //Preparation
    //cout<< "decThread: getINVsig" << endl;
    farm0->doLpInvestBeginn();
    //cout<< "decThread: getINVsig nach dlpB" << endl;
    invDec2();
    emit showInvestDialog();
}

void DecThread::invDec2(){
    vector<RegProductInfo> prod_cat = Manager->product_cat0;
    RegMarketInfo market = (*Manager->Market);
    market.setProductCat(prod_cat);
    market.priceFunction(Manager->evaluator);

    Manager->product_cat0 = market.getProductCat();

    farm0->periodResults();

    Manager->product_cat0 = prod_cat;
}

void DecThread::invDec3(){
    vector<RegProductInfo> prod_cat = Manager->product_cat0;
    RegMarketInfo market = (*Manager->Market);
    market.priceFunction(Manager->evaluator);
    Manager->product_cat0 = market.getProductCat();

    farm0->periodResults();

    Manager->product_cat0 = prod_cat;
}


void DecThread::getInvestSig(int i, int z){
    //Preparation
    bool ok =farm0->doLpInvestTry(i,z);
    if (!ok) {
        emit investOK(i,ok);
        return;
    }

    invDec2();

    emit investOK(i,ok);
}

void DecThread::getInvestPeSig(int r){
    bool ok = farm0->doLpInvestTry();
    if (!ok) {
        emit investPeOK(r, ok);
        return;
    }

    invDec2();
    emit investPeOK(r, true);
}

void DecThread::getInvestPeVarSig(int r){
    bool ok = farm0->doLpInvestTry();
    if (!ok) {
        emit investPeVarOK(r, ok);
        return;
    }

    invDec2();
    emit investPeVarOK(r, true);
}
