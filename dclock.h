#ifndef DIGITALCLOCK_H
#define DIGITALCLOCK_H

#include <QLCDNumber>
#include <QTime>

class DigitalClock : public QLCDNumber
{
    Q_OBJECT

public:
    DigitalClock(QWidget *parent = 0);
    void resetTime();
    void anhalten();

private slots:
    void showTime();

private:
    QTime time0;
    int timesecs;
    bool angehalten;
};

#endif

