#include "Mainwindow.h"

#include <QAction>
#include <QLabel>
#include <QToolBar>
#include <QStatusBar>
#include <QMessageBox>
#include <QMenuBar>
#include <QDir>
#include <QFile>
#include <QTextEdit>
#include <QGraphicsEffect>
#include <QApplication>
#include <QDesktopWidget>
#include <QSplashScreen>
#include <QtWebKit/QWebView>
#include <QTextBrowser>
#include <QTimer>
#include <QTextCodec>
#include <QPixmapCache>
#include <QDesktopServices>
#include <QProgressBar>

#include "optdialog.h"
#include "Landwindow.h"
#include "AgriPoliS.h"
#include "simThread.h"
#include "clock.h"
#include "dclock.h"
#include "pe.h"

static void SetBackgroundImage( QWidget *fp_widget, const QPixmap *fp_image )
    {
       QPalette newPalette = fp_widget->palette ( );
       newPalette.setBrush
       (
          QPalette::Base,
          QBrush ( *fp_image )
       );

       newPalette.setBrush
       (
          QPalette::Background,
          QBrush ( *fp_image )
       );

       fp_widget->setPalette ( newPalette );
    }

class decIcon: public QWidget {
public:
    decIcon(QString txt, QImage img, QWidget* par):text(txt),image(img),parent(par){
            l1 = new QLabel;
            l2 = new QLabel(txt);
            l1->setPixmap(QPixmap::fromImage(img).scaled(170, 60, Qt::KeepAspectRatio));//parent->width(), parent->height()));
            l1->setAlignment(Qt::AlignCenter);
            l2->setAlignment(Qt::AlignCenter);
            l2->setStyleSheet("font-size: 16px;");

            QVBoxLayout *vout = new QVBoxLayout;
            vout->addStretch();
            vout->addWidget(l1);
            vout->addSpacing(1);
            vout->addWidget(l2);
            vout->addStretch();

            setLayout(vout);
    }
    void setStyleSheet(QString str){
        l2->setStyleSheet(str);
    }

protected:
    /*void paintEvent(QPaintEvent *){
        l1->setPixmap(QPixmap::fromImage(image).scaled(parent->width(), parent->height(),Qt::KeepAspectRatio));
    }
//*/
private:
    QString text;
    QImage image;
    QLabel *l1 ;
    QLabel *l2 ;
    QWidget *parent;
};

class MyBrowser : public QTextBrowser {
private:
  QTextDocument *doc;

public:
  MyBrowser(const QString &text, QWidget *parent = 0) : QTextBrowser(parent) {
    doc = new QTextDocument(this);
    doc->setHtml(text);
    setDocument(doc);
  }

  void adjust() {

    QMargins margins = contentsMargins();

    int width = size().width() -
      margins.left() - margins.right() - doc->documentMargin()*2;
    doc->setPageSize(QSizeF(width-20,-1));

    int h = doc->size().height();
    int height = h + margins.top() + margins.bottom();
    setMaximumHeight(height);
    setMinimumHeight(height);
    //*/
  }

  void showEvent(QShowEvent *) {
    adjust();
  }

  void resizeEvent(QResizeEvent *) {
    adjust();
  }
};

//_____________________________________


Mainwindow::Mainwindow(){
    politWindow = 0;
    printer = new QPrinter(QPrinter::HighResolution);
    printer->setOutputFileName("m.pdf");

    //setStyleSheet("background-color : lightyellow; ");
    createActions();
    createMenus();
    createToolBars();
    createStatusBar();

    /*AnalogClock *uuhr = new AnalogClock();
    uuhr->show();
//*/
    makeDocks();
    //addDockWidget(Qt::RightDockWidgetArea, myfarmDock);
    addDockWidget(Qt::LeftDockWidgetArea, simuDock);
    //*/

    landWindow = new Landwindow(0);//(this)
    connect(landWindow, SIGNAL(hi(bool)), this, SLOT(updateToolBar(bool)));

    dataWindow= new Datawindow(this,0);
    connect(dataWindow, SIGNAL(hi(bool)), this, SLOT(updateToolBar1(bool)));

    mydatWindow= new Mydatwindow(0, this);
    connect(mydatWindow, SIGNAL(hi(bool)), this, SLOT(updateToolBar2(bool)));

    //bodenDialog = new BodenDialog(this);
    bodenDialog1 = new BodenDialog1(this);
    investDialog = new InvestDialog(this);
    closeDialog = new CloseDialog(this);

    bodenDialog1->setVisible(false);
    investDialog->setVisible(false);
    closeDialog->setVisible(false);

    //makePolitWindow();
    makeCentralWidget();
    //dataWidget->setStyleSheet("background-image:url(./images/grafiken/FarmAgriPoliS.jpg)");
    setCentralWidget(dataWidget);

    cwidget = DEC;

    SIM_ZUSTAND=NOTREADY;
    actStartOrPause->setEnabled(false);
    actLandschaften->setEnabled(false);
    actStop->setEnabled(false);
    actSectorData->setEnabled(false);
    actMyData->setEnabled(false);
    actPolicy->setEnabled(false);
    actDec->setEnabled(false);

  const int width = QApplication::desktop()->width();
  const int height = QApplication::desktop()->height();
  resize(1270,height-100);
  decs = PROG;
  updateMwidget();
  setWindowTitle("FarmAgriPoliS");
  /*
  QTimer *atimer = new QTimer;
  atimer->start(2000);
  connect(atimer, SIGNAL(timeout()), this, SLOT(getTimeout()));
  whichIcon = 0;
  //*/

  sThread=0;
  dThread=0;
  peWidget=0;
  //SetBackgroundImage(mydatWindow->centralWidget(), new QPixmap(QPixmap::fromImage(QImage(":/images/grafiken/Kuh.png"))));
  //mydatWindow->centralWidget()->setAutoFillBackground(true);
  setWindowIcon(QPixmap(":/images/grafiken/Icon_FarmAgriPoliS_2.png"));
  hilfeBrowser = new QTextBrowser();
  hilfeBrowser->setVisible(false);
  hilfeBrowser->resize(700, 900);
  hilfeDoc = new QTextDocument();
  setMouseTracking(true);
  over=false;
  aTime.start();
  absicherFertig = true;
}

void Mainwindow::getLWmouseMoveEvent(QMouseEvent *e){
    mouseMoveEvent(e);
}

bool Mainwindow::faOver() const{
    return over;
}

void Mainwindow::enterEvent(QEvent *e){
    raiseDecs();
    e->accept();
}

void Mainwindow::leaveEvent(QEvent *e){
    raiseDecs();
    e->accept();//QMainWindow::leaveEvent(e);
}

void Mainwindow::mouseMoveEvent(QMouseEvent *e){
    raiseDecs();
    QMainWindow::mouseMoveEvent(e);
}

void Mainwindow::raiseDecs(){
    if(!sThread) return;
    if(investDialog && investDialog->isVisible())
            investDialog->raise();
    if(bodenDialog1 && bodenDialog1->isVisible())
            bodenDialog1->raise();
    if(closeDialog && closeDialog->isVisible())
            closeDialog->raise();
}

void Mainwindow::closeMessage(QCloseEvent*e){
    if (!absicherFertig) {
        QMessageBox msgBox;
        msgBox.setText("Bitte warten Sie. Daten m�ssen noch abgesichert werden. Schlie�en Sie dieses Fenster.");
        //QPushButton *neinButton = msgBox.addButton("Nein", QMessageBox::RejectRole);
        //QPushButton *jaButton = msgBox.addButton("Ja", QMessageBox::AcceptRole);
        //msgBox.setDefaultButton(neinButton);
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();

        if(e) {
            e->ignore();
        }

        return;
    }

    QMessageBox msgBox;
    msgBox.setText("Wollen Sie FarmAgriPoliS beenden ?");
    QPushButton *neinButton = msgBox.addButton("Nein", QMessageBox::RejectRole);
    QPushButton *jaButton = msgBox.addButton("Ja", QMessageBox::AcceptRole);
    msgBox.setDefaultButton(neinButton);
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.setWindowFlags(Qt::WindowStaysOnTopHint);
    msgBox.setWindowTitle("FarmAgriPoliS beenden");
    msgBox.exec();
    if (msgBox.clickedButton()==jaButton){
        if (e){
            e->accept();
            QMainWindow::closeEvent(e);
        }else
            over=true;
    }  else {
        if (e) {
            e->ignore();
        }
    }
}

void Mainwindow::closeEvent(QCloseEvent*e){
    if(over){
        e->accept();
        QMainWindow::closeEvent(e);
    }else{
        closeMessage(e);
    }
}


void Mainwindow::getTimeout(){
    ++whichIcon;
    whichIcon%=3;
    changeDockColor(whichIcon);
}

void Mainwindow::updateToolBar(bool x){
    actLandschaften->setEnabled(x);
}

void Mainwindow::updateToolBar1(bool x){
    actSectorData->setEnabled(x);
}

void Mainwindow::updateToolBar2(bool x){
    actMyData->setEnabled(x);
}

void Mainwindow::createActions()
{
    actNewRegion = new QAction(tr("Neue Region"),this);
    actNewRegion->setIcon(QIcon(":/images/open.png"));
    connect(actNewRegion, SIGNAL(triggered()), this, SLOT(newRegion()));

    actStartOrPause = new QAction(tr("Start"),this); //start or pause
    actStartOrPause->setIcon(QIcon(":/images/editstart.png"));
    connect(actStartOrPause, SIGNAL(triggered()), this, SLOT(startOrPause()));

    actStop = new QAction(tr("Stop"),this);
    actStop->setIcon(QIcon(":/images/editstop.png"));
    connect(actStop, SIGNAL(triggered()),this,SLOT(stop())),

    actLandschaften = new QAction(tr("Landschaften"), this);
    actLandschaften->setIcon(QIcon(":/images/plotscolor.png"));
    connect(actLandschaften, SIGNAL(triggered()), this, SLOT(showLandschaften()));

    actSectorData = new QAction(tr("Daten Region"),this);
    actSectorData->setIcon(QIcon(":/images/statistics.png"));
    connect(actSectorData, SIGNAL(triggered()), this, SLOT(showSectorDaten()));

    actMyData = new QAction(tr("Mein Betrieb"),this);
    actMyData->setIcon(QIcon(":/images/house-md.png"));//.jpg"));//farmer.jpg"));
    connect(actMyData, SIGNAL(triggered()), this, SLOT(showMyData()));

    actPolicy = new QAction(tr("Politiken"),this);
    actPolicy->setIcon(QIcon(":/images/euroIcon1.png"));
    connect(actPolicy, SIGNAL(triggered()), this, SLOT(showPolicy()));

    actPrint = new QAction(tr("Drucken"),this);
    actPrint->setIcon(QIcon(":/images/drucken.png"));

    connect(actPrint, SIGNAL(triggered()), this, SLOT(drucken()));

    actAbout = new QAction(tr("�ber"), this);
    connect(actAbout, SIGNAL(triggered()), this, SLOT(about()));

    actQuit  = new QAction(tr("beenden"),this);
    connect(actQuit, SIGNAL(triggered()), this, SLOT(beenden()));//close()));

    actHilfeFA = new QAction(tr("FarmAgriPoliS"),this);
    connect(actHilfeFA, SIGNAL(triggered()), this, SLOT(hilfeFA()));

    actHilfeLand = new QAction(tr("Landschaften"),this);
    connect(actHilfeLand, SIGNAL(triggered()), this, SLOT(hilfeLand()));

    actHilfeAblauf = new QAction(tr("Simulationsablauf"),this);
    connect(actHilfeAblauf, SIGNAL(triggered()), this, SLOT(hilfeAblauf()));

    actHilfeBoden = new QAction(tr("BodenMarkt"),this);
    connect(actHilfeBoden, SIGNAL(triggered()), this, SLOT(hilfeBoden()));

    actHilfeInvest = new QAction(tr("Investition"),this);
    connect(actHilfeInvest, SIGNAL(triggered()), this, SLOT(hilfeInvest()));

    actHilfeAusst = new QAction(tr("Ausstieg"),this);
    connect(actHilfeAusst, SIGNAL(triggered()), this, SLOT(hilfeAusst()));


    actDec = new QAction(tr("Spiel"),this);//FarmAgriPoliS"),this);
    actDec->setIcon(QIcon(":/images/infoIcon.png"));//grafiken/Kuh.png"));
    connect(actDec, SIGNAL(triggered()), this, SLOT(showFA()));
}

void Mainwindow::createMenus()
{
    progMenu = menuBar()->addMenu(tr("Programm"));
    progMenu->addAction(actAbout);
    progMenu->addAction(actQuit);

    hilfeMenu = menuBar()->addMenu(tr("Hilfe"));
    hilfeMenu->addAction(actHilfeFA);
    hilfeMenu->addAction(actHilfeLand);
    hilfeMenu->addAction(actHilfeBoden);
    hilfeMenu->addAction(actHilfeInvest);
    hilfeMenu->addAction(actHilfeAusst);
    //hilfeMenu->addAction(actHilfeAblauf);
}

void Mainwindow::createToolBars()
{
    simToolBar = addToolBar(tr("SimFuncs"));
    simToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    simToolBar->addAction(actNewRegion);
    simToolBar->addAction(actStartOrPause);
    //simToolBar->addAction(actStop);

    mainToolBar = addToolBar(tr("mainFunctions"));
    //mainToolBar->setWindowTitle("Landschaften?");
    mainToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

    mainToolBar->addAction(actLandschaften);
    mainToolBar->addAction(actSectorData);

    mainToolBar->addAction(actPolicy);
    mainToolBar->addAction(actMyData);

    mainToolBar->addAction(actDec);
    //mainToolBar->addAction(actPrint);
}

void Mainwindow::createStatusBar()
{
    statusLabel = new QLabel;
    statusLabel->setIndent(3);

    statusLabel->setText("Are you ready ?");
    statusLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    statusBar()->addWidget(statusLabel);
    QPalette *p = new QPalette();
    p->setColor(QPalette::Background,Qt::white);
    statusBar()->setAutoFillBackground(true);
    statusBar()->setPalette(*p);
 }

void Mainwindow::showLandschaften(){
    if (!landWindow->sceneCreated()){
       landWindow->initScene();
       //return;
   }
   saveCWidget(cwidget);

   reActGui(cwidget);
   setCentralWidget(landWindow);
   cwidget = LAND;
   landWindow->update();
   landWindow->show();
   actLandschaften->setEnabled(false);
   updateStatusLabel(cwidget);
}

void Mainwindow::showSectorDaten(){
   //dataWidget->setWindowTitle("updated!");
    saveCWidget(cwidget);

    reActGui(cwidget);
    setCentralWidget(dataWindow);
    cwidget = REG;
   dataWindow->show();
   actSectorData->setEnabled(false);
   updateStatusLabel(cwidget);
}

void Mainwindow::showMyData(){
    saveCWidget(cwidget);

    reActGui(cwidget);
    cwidget = MY;
    setCentralWidget(mydatWindow);

    mydatWindow->show();
    actMyData->setEnabled(false);
    if (closeDialog->isInited())
        closeDialog->enableBSbutton(false);
    if (bodenDialog1->isInited())
        bodenDialog1->enableBSbutton(false);
    //cout << "Mein Farm noch nicht implementiert" << endl;

    updateStatusLabel(cwidget);
}

void Mainwindow::updateStatusLabel(CWIDGET cw){
    switch(cw){
    case LAND:
                statusLabel->setText("Landschaften");
                break;
    case REG:
                statusLabel->setText("Regionale Infos");
                break;
    case MY:
                statusLabel->setText("Infos �ber Ihren eigenen Betrieb");
                break;
    case POL:
                statusLabel->setText("Politiken");
                break;
    case DEC:
                statusLabel->setText("Entscheidung");
                break;
    default:  statusLabel->setText("Keine Ahnung, wo ich bin.");;
    }
}

//Spielinformation FarmAgriPoliS
void Mainwindow::showFA(){
    saveCWidget(cwidget);

    reActGui(cwidget);
    cwidget = DEC;

    setCentralWidget(dataWidget);
    updateMwidget();

    actDec->setEnabled(false);

    updateStatusLabel(cwidget);
}

void Mainwindow::showPolicy(){
    saveCWidget(cwidget);

    reActGui(cwidget);
    cwidget = POL;
    setCentralWidget(politWindow);
    actPolicy->setEnabled(false);
    updateStatusLabel(cwidget);
}

void Mainwindow::about(){
    QMessageBox msgBox;
    msgBox.setFont(QFont("Times", 14));
    msgBox.setText("<center> <h1> <font color='green'> FarmAgriPoliS </font></h1>  </br> Leibniz-Institut f�r Agrarentwicklung in Mittel- und Osteuropa (IAMO), </br>"
"Abteilung BSE,  2011-2013 </center>"
"<p> FarmAgriPoliS ist ein interaktives Spiel, bei dem die Entwicklung einer Agrarregion bestehend aus landwirtschaftlichen Betrieben simuliert wird."
"<p>In dem Spiel wird einer der Betriebe durch einen Spieler gesteuert, w�hrend der Computer die Entscheidungen aller anderen Betriebe �bernimmt. "
"<p>FarmAgriPoliS wurde zu Forschungszwecken im Rahmen des von der Deutschen Forschungsgemeinschaft (DFG) finanzierten Teilprojekts 5 der DFG-Forschungseinheit �Structural Change in Agriculture (SiAg)� programmiert, "
"um das Entscheidungsverhalten von Landwirten auf experimentelle Art zu untersuchen. ");
    msgBox.exec();
}

void Mainwindow::hilfeFA(){
    hilfeFileName=":/html/prog.html";
    updateHilfe();
}

void Mainwindow::hilfeLand(){
    hilfeFileName=":/html/land.html";
    updateHilfe();
}

void Mainwindow::hilfeAblauf(){
    static bool copied = false;
    if (!copied) {
        //QFile::copy (":/pdf/ablauf.pdf", "ablauf_copy.pdf");
        copied=true;
        hilfeUrl=QUrl::fromLocalFile("ablauf_copy.pdf");
    }
    //hilfeUrl=QUrl::fromLocalFile("D:/path/to/f.pdf");
    QDesktopServices::openUrl(hilfeUrl);
}

void Mainwindow::updateHilfe(){
    QFile file(hilfeFileName);
    QString tt;
    if (file.open(QFile::ReadOnly | QFile::Text)){
        tt = QString(file.readAll());
    }
    hilfeDoc->setHtml(tt);
    hilfeBrowser->setDocument(hilfeDoc);
    hilfeBrowser->setFont(QFont("Times", 13));
    hilfeBrowser->show();
}

void Mainwindow::hilfeInvest(){
    hilfeFileName=":/html/inv.html";
    updateHilfe();
}

void Mainwindow::hilfeBoden(){
    hilfeFileName=":/html/bod.html";
    updateHilfe();
}

void Mainwindow::hilfeAusst(){
    hilfeFileName=":/html/clo.html";
    updateHilfe();
}

void Mainwindow::newRegion(){
    OptDialog dialog;
        if (dialog.exec()) {
            inputDir= dialog.optdir;
            policyFile= dialog.policy;
            actStartOrPause->setEnabled(true);
            SIM_ZUSTAND=READY;
            actNewRegion->setEnabled(false);
        } else {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Hinweis �ber Eingabe");
            msgBox.setText("Verzeichnis der Eigabedateien ist pflicht; \nPolitikdatei ist optional mit dem Standardnamen policy_settings.txt");
                           //Option folder is necessary; Policy file is optional.");
            msgBox.exec();
        }
}

void Mainwindow::startOrPause(){
switch (SIM_ZUSTAND) {
case READY:
  SIM_ZUSTAND= RUNNING;

  sThread = new SimThread (inputDir, policyFile);
  dThread = new DecThread();

  connect(sThread, SIGNAL(LAsig(int,int)), bodenDialog1, SLOT(getLAsig(int,int)));
  connect(bodenDialog1, SIGNAL(LAsig(int,int)), dThread, SLOT(getLAsig(int,int)));
  //connect(sThread, SIGNAL(LAsig(int,int)), dThread, SLOT(getLAsig(int,int)));

  connect(sThread, SIGNAL(LAsig(int,int)), this, SLOT(getLAsig(int,int)));
  connect(sThread, SIGNAL(LAsig(int)), dThread, SLOT(getLAsig(int)));
  //connect(dThread, SIGNAL(showBodenDialog(int,int)), bodenDialog, SLOT(showBoden(int,int)));
  connect(dThread, SIGNAL(showBodenDialog(int,int)), bodenDialog1, SLOT(showBoden(int,int)));
  connect(dThread, SIGNAL(showBodenDialog(int,int)), this, SLOT(getShowBoden(int,int)));

  connect(dThread, SIGNAL(showBodenDialog(int)), bodenDialog1, SLOT(showBoden(int)));
  connect(dThread, SIGNAL(showBodenDialog(int)), this, SLOT(getShowBoden(int)));

  connect(dThread, SIGNAL(showCloseDialog(int)), this, SLOT(getShowClose(int)));
  connect(dThread, SIGNAL(showInvestDialog()), this, SLOT(getShowInvest()));

  connect(sThread, SIGNAL(FoFsig(int)), dThread, SLOT(getFoFsig(int)));
  connect(sThread, SIGNAL(FoFsig(int)), this, SLOT(getFoFsig(int)));
  connect(dThread, SIGNAL(showCloseDialog(int)), closeDialog, SLOT(showClose(int)));

  connect(sThread, SIGNAL(INVsig()), investDialog,SLOT(getINVsig()));
  connect(investDialog, SIGNAL(INVsig()), dThread, SLOT(getINVsig()));

  //connect(sThread, SIGNAL(INVsig()), dThread, SLOT(getINVsig()));
  connect(sThread, SIGNAL(INVsig()), this, SLOT(getINVsig()));
  //connect(sThread, SIGNAL(INVsig()), this, SLOT(about()));
  connect(dThread, SIGNAL(showInvestDialog()), investDialog, SLOT(showInvest()));

  //connect(bodenDialog, SIGNAL(areaSig2(int)), dThread, SLOT(getAreaSig2(int)));
  //connect(dThread, SIGNAL(areaOK2(int)), bodenDialog, SLOT(getAreaOK2(int)));
  //connect(bodenDialog, SIGNAL(peSig(int)), dThread, SLOT(getPeSig(int)));
  //connect(dThread, SIGNAL(peOK(int,bool)), bodenDialog, SLOT(getPeOK(int,bool)));

  //bdialog1
  connect(bodenDialog1, SIGNAL(areaSig2(int)), dThread, SLOT(getAreaSig2(int)));
  connect(bodenDialog1, SIGNAL(areaSig2B(int,int)), dThread, SLOT(getAreaSig2B(int,int)));
  connect(dThread, SIGNAL(areaOK2(int)), bodenDialog1, SLOT(getAreaOK2(int)));
  connect(dThread, SIGNAL(areaOK2B(int,int)), bodenDialog1, SLOT(getAreaOK2B(int,int)));

  connect(bodenDialog1, SIGNAL(peSig(int)), dThread, SLOT(getPeSig(int)));
  connect(dThread, SIGNAL(peOK(int,bool)), bodenDialog1, SLOT(getPeOK(int,bool)));

  connect(bodenDialog1, SIGNAL(peVarSig(int)), dThread, SLOT(getPeVarSig(int)));
  connect(dThread, SIGNAL(peVarOK(int,bool)), bodenDialog1, SLOT(getPeVarOK(int,bool)));

  connect(closeDialog, SIGNAL(ClosePeSig(int)), dThread, SLOT(getClosePeSig(int)));
  connect(dThread, SIGNAL(ClosePeOK(int,bool)), closeDialog, SLOT(getPeOK(int,bool)));

  connect(closeDialog, SIGNAL(ClosePeVarSig(int)), dThread, SLOT(getClosePeVarSig(int)));
  connect(dThread, SIGNAL(ClosePeVarOK(int,bool)), closeDialog, SLOT(getPeVarOK(int,bool)));

  connect(investDialog, SIGNAL(investSig(int,int)), dThread, SLOT(getInvestSig(int,int)));
  connect(dThread, SIGNAL(investOK(int,bool)), investDialog, SLOT(getInvestOK(int,bool)));
  connect(investDialog, SIGNAL(investPeSig(int)), dThread, SLOT(getInvestPeSig(int)));
  connect(dThread, SIGNAL(investPeOK(int,bool)), investDialog, SLOT(getPeOK(int,bool)));

  connect(investDialog, SIGNAL(investPeVarSig(int)), dThread, SLOT(getInvestPeVarSig(int)));
  connect(dThread, SIGNAL(investPeVarOK(int,bool)), investDialog, SLOT(getPeVarOK(int,bool)));

  //Landschaften nach Geboten
  connect(bodenDialog1, SIGNAL(firstBids()), this, SLOT(getFirstBids()));

  //dock needs
  connect(bodenDialog1, SIGNAL(accepted()), this, SLOT(getdecsig()));
  connect(bodenDialog1, SIGNAL(rejected()), this, SLOT(getdecsig()));
  connect(closeDialog, SIGNAL(accepted()), this,  SLOT(getdecsig()));
  connect(closeDialog, SIGNAL(rejected()), this,  SLOT(getdecsig()));
  connect(investDialog, SIGNAL(accepted()), this, SLOT(getdecsig()));
  connect(investDialog, SIGNAL(rejected()), this, SLOT(getdecsig()));

  //Entscheidungen schreiben
  connect(bodenDialog1, SIGNAL(ok(int)), this, SLOT(getdecsigBoden(int)));
  connect(closeDialog, SIGNAL(fertig()), this,  SLOT(getdecsigAus()));
  connect(investDialog, SIGNAL(fertig()), this, SLOT(getdecsigInv()));

  /*
  connect(sThread, SIGNAL(simover()), dThread, SLOT(simsig()));
  connect(sThread, SIGNAL(decboden()), dThread, SLOT(decboden()));
  connect(sThread, SIGNAL(decinvest()), dThread, SLOT(decinvest()));
  connect(sThread, SIGNAL(decclose()), dThread, SLOT(decclose()));
  //*/
 connect(sThread, SIGNAL(newplot(PlA*)), landWindow, SLOT(newplot(PlA*)));
 connect(sThread, SIGNAL(delplot(PlA*)), landWindow, SLOT(delplot(PlA*)));

 //connect(landWindow, SIGNAL(lwMouseMove(QMouseEvent*)), this, SLOT(getLWmouseMoveEvent(QMouseEvent*)));
 /*
 connect(dThread,SIGNAL(odecboden()), bodenDialog, SLOT(show()));
 connect(dThread,SIGNAL(odecinvest()), investDialog, SLOT(show()));
 connect(dThread,SIGNAL(odecclose()), closeDialog, SLOT(show()));
 //*/

 //connect(sThread,SIGNAL(decboden()), bodenDialog, SLOT(show()));
 connect(sThread,SIGNAL(decinvest()), investDialog, SLOT(show()));
 connect(sThread,SIGNAL(decclose()), closeDialog, SLOT(show()));

 connect(sThread,SIGNAL(decboden()), this, SLOT(getdecsig()));
 connect(sThread,SIGNAL(decinvest()), this, SLOT(getdecsig()));
 connect(sThread,SIGNAL(decclose()), this, SLOT(getdecsig()));

 connect(this, SIGNAL(zustand(SimZustand)), sThread, SLOT(toZustand(SimZustand)));

 connect(sThread, SIGNAL(initover()), this, SLOT(getInitover()));
 connect(sThread, SIGNAL(itOver(int)), this, SLOT(getItOver(int)));
 connect(sThread, SIGNAL(itBeginn()), this, SLOT(getItBeginn()));
 connect(sThread, SIGNAL(simOver()), this, SLOT(getSimOver()));

 connect(sThread, SIGNAL(hasPeriodResults()), this, SLOT(getPeriodResults()));

 connect(sThread,SIGNAL(busy()),this,SLOT(getBusy()));
 connect(sThread,SIGNAL(produced()),this,SLOT(getProduced()));
 connect(sThread,SIGNAL(PRODsig()),this,SLOT(getPRODsig()));
 connect(sThread,SIGNAL(LAOver()),this,SLOT(getLAOver()));

  //rejected should be subst by accepted()
  //connect(bodenDialog, SIGNAL(fertig()), this, SLOT(updateWeiter()));
  //connect(investDialog, SIGNAL(accepted()), this, SLOT(updateWeiter()));
  //connect(closeDialog, SIGNAL(accepted()), this, SLOT(updateWeiter()));
 //*/

 /*
 connect(bodenDialog, SIGNAL(accepted()), sThread, SLOT(takedecboden()));
 connect(investDialog, SIGNAL(accepted()), sThread, SLOT(takedecinvest()));
 connect(closeDialog, SIGNAL(accepted()), sThread, SLOT(takedecclose()));
 //*/

 connect(mydatWindow, SIGNAL(hi(bool)), closeDialog, SLOT(getMydatwindowHi(bool)));
 connect(mydatWindow, SIGNAL(hi(bool)), bodenDialog1, SLOT(getMydatwindowHi(bool)));

 sThread->start();
 dThread->start();

 //actLandschaften->setEnabled(true);
 actStartOrPause->setText("Pause ");
 actStartOrPause->setIcon(QIcon(":/images/pause.png"));
 break;

case RUNNING:
     SIM_ZUSTAND= PAUSED;
     actStartOrPause->setText("Weiter");
     actStartOrPause->setIcon(QIcon(":/images/next.png"));
     break;
case PAUSED:

     SIM_ZUSTAND= RUNNING;
     gg->SIM_WEITER = true;
     actStartOrPause->setText("Pause ");
     actStartOrPause->setIcon(QIcon(":/images/pause.png"));
     if(decs==ENDE){
        //faOutput();
     }
     break;
case STOPPED:
     SIM_ZUSTAND= NOTREADY;
     actStartOrPause->setText("Start ");
     actStartOrPause->setIcon(QIcon(":/images/editstart.png"));
     actStartOrPause->setEnabled(false);
     break;
default: ;
}
emit zustand(SIM_ZUSTAND);
}

void Mainwindow::stop(){
    SIM_ZUSTAND = STOPPED;

    actStartOrPause->setEnabled(false);
    actStartOrPause->setText("Start ");
    actStartOrPause->setIcon(QIcon(":/images/editstart.png"));
    emit zustand(SIM_ZUSTAND);
}

void Mainwindow::getdecsig(){
    changeDockColor(0);
    dclock->resetTime();
    //dclock->anhalten();
}

void Mainwindow::getdecsigBoden(int p0){
    dtime=aTime.elapsed();
    cout<<"p0: \t "<< p0 <<endl;
    cout<<"Zeit: \t "<< dtime <<endl;
    //PE schreiben
    //Offers schreiben
}

void Mainwindow::getdecsigInv(){
    dtime=aTime.elapsed();
    cout<<"Zeit: \t "<< dtime <<endl;
    //PE schreiben
    //Inv schreiben
}

void Mainwindow::getdecsigAus(){
    dtime=aTime.elapsed();
    cout<<"Zeit: \t "<< dtime <<endl;
    //PE schreiben
    //Aus Schreiben
}

void Mainwindow::updateWeiter(){
     SIM_ZUSTAND= PAUSED;
     actStartOrPause->setText("Weiter");
     actStartOrPause->setIcon(QIcon(":/images/next.png"));
     emit zustand(SIM_ZUSTAND);
}

void Mainwindow::getSimOver(){
   this->decs=ENDE;
   updateMwidget();
   QString opstr = QString(gg->OUTPUTFILEdir.c_str());
   QDir idir= QDir(QString("../")+QString(gg->INPUTFILEdir.c_str()));
   if (!idir.exists(opstr))
        idir.mkpath(opstr);
   this->statusLabel->setText("Bitte warten, ich bin flei�ig am Schreiben...");
   repaint();
   faOutput();
    /*
    if (!file.open(QIODevice::Append)){
        QMessageBox::warning(this,
        tr("FarmAgriPolis"), tr("Fehler! beim �ffnen der Datei %1").arg(file.fileName()));
    }
    QTextStream tstr(&file);
    //*/
    QDateTime dateTime = QDateTime::currentDateTime();
    tstr<<"\n\nSpielende:  \t";
    tstr<<dateTimeString(dateTime)<<"\n";

    tstrBoden<<"\n\n===="<<dateString(dateTime)<<"====\n";
    tstrInv<<"\n\n===="<<dateString(dateTime)<<"====\n";
    tstrAus<<"\n\n===="<<dateString(dateTime)<<"====\n";
    tstrPe<<"\n\n===="<<dateString(dateTime)<<"====\n";

    file.close();
    fileBoden.close();
    fileInv.close();
    fileAus.close();
    filePe.close();

    filesClosed=true;
    //cout<<"sollte outputing...\n"<<endl;
    statusLabel->setText("Fertig.");
}

void Mainwindow::getInitover(){
    dtime=aTime.elapsed();
    landWindow->updateFarms();

    //Farm0 ??
    dThread->setFarm(*(Manager->FarmList.begin()));

    landWindow->initColors();
    landWindow->initLegends();
    landWindow->initColorDialogs();
    landWindow->initInfos();
    landWindow->initScene();
    actLandschaften->setEnabled(true);

    mydatWindow->getLiquidity0();

    updatePolitWindow();
    actPolicy->setEnabled(true);

     QString opstr = QString(gg->OUTPUTFILEdir.c_str());
     QDir idir= QDir(QString("../")+QString(gg->INPUTFILEdir.c_str()));
     if (!idir.exists(opstr))
            idir.mkpath(opstr);
    QDir opdir = QDir(opstr);
    if (!opdir.exists("./DataRegion/"))
        opdir.mkpath(opstr+"DataRegion/");
    if (!opdir.exists("./DataBetrieb/"))
        opdir.mkpath(opstr+"DataBetrieb/");
    if (!opdir.exists("./Entscheid/"))
        opdir.mkpath(opstr+"Entscheid/");

     initFiles();
}

void Mainwindow::closeFiles(){
    QDateTime dateTime = QDateTime::currentDateTime();
    tstrBoden<<"\n\n===="<<dateString(dateTime)<<"====\n";
    tstrInv<<"\n\n===="<<dateString(dateTime)<<"====\n";
    tstrAus<<"\n\n===="<<dateString(dateTime)<<"====\n";
    tstrPe<<"\n\n===="<<dateString(dateTime)<<"====\n";
    file.close();
    fileBoden.close();
    fileInv.close();
    fileAus.close();
    filePe.close();
}

QString Mainwindow::dateTimeString(QDateTime d) const {
    return QLocale().dayName(d.date().dayOfWeek(), QLocale::ShortFormat)
        +d.toString(" dd.MM.yyyy hh:mm:ss");
}

QString Mainwindow::dateString(QDateTime d) const {
    return QLocale().dayName(d.date().dayOfWeek(), QLocale::ShortFormat)
        +d.toString(" dd.MM.yyyy");
}

void Mainwindow::initFiles(){
     decPEdat="Entscheid/decPE.dat";
     decBodenDat="Entscheid/decBoden.dat";
     decInvDat="Entscheid/decInv.dat";
     decAusDat="Entscheid/decAusstieg.dat";
    spielDat="ablauf.dat";
    file.setFileName(QString(gg->OUTPUTFILEdir.c_str())+spielDat);
    if (!file.open(QIODevice::WriteOnly)){
        QMessageBox::warning(this,
        tr("FarmAgriPolis"), tr("Fehler! beim �ffnen der Datei %1").arg(file.fileName()));
    }
    tstr.setDevice(&file);
    QDateTime dateTime = QDateTime::currentDateTime();
    tstr<<"=======FarmAgriPolis========\n"
        <<"=======IAMO 2011-2013=======\n\n"
        << "Spielanfang:\t";
    tstr<<dateTimeString(dateTime)<<"\n\n";
    file.close();
    file.open(QIODevice::Append);
    tstr.setDevice(&file);

    fileBoden.setFileName(QString(gg->OUTPUTFILEdir.c_str())+decBodenDat);
    if (!fileBoden.open(QIODevice::WriteOnly)){
        QMessageBox::warning(this,
        tr("FarmAgriPolis"), tr("Fehler! beim �ffnen der Datei %1").arg(fileBoden.fileName()));
    }
    tstrBoden.setDevice(&fileBoden);
    tstrBoden<<"=======FarmAgriPolis========\n"
        <<"===Entscheidungen Bodenmarkt===\n\n";
    fileBoden.close();fileBoden.open(QIODevice::Append);
    tstrBoden.setDevice(&fileBoden);

    fileInv.setFileName(QString(gg->OUTPUTFILEdir.c_str())+decInvDat);
    if (!fileInv.open(QIODevice::WriteOnly)){
        QMessageBox::warning(this,
        tr("FarmAgriPolis"), tr("Fehler! beim �ffnen der Datei %1").arg(fileInv.fileName()));
    }
    tstrInv.setDevice(&fileInv);
    tstrInv<<"=======FarmAgriPolis========\n"
        <<"===Entscheidungen Investitionen===\n\n";
    fileInv.close();fileInv.open(QIODevice::Append);
    tstrInv.setDevice(&fileInv);

    fileAus.setFileName(QString(gg->OUTPUTFILEdir.c_str())+decAusDat);
    if (!fileAus.open(QIODevice::WriteOnly)){
        QMessageBox::warning(this,
        tr("FarmAgriPolis"), tr("Fehler! beim �ffnen der Datei %1").arg(fileAus.fileName()));
    }
    tstrAus.setDevice(&fileAus);
    tstrAus<<"=======FarmAgriPolis========\n"
        <<"===Entscheidungen Ausstieg===\n\n";
    fileAus.close();fileAus.open(QIODevice::Append);
    tstrAus.setDevice(&fileAus);

    filePe.setFileName(QString(gg->OUTPUTFILEdir.c_str())+decPEdat);
    if (!filePe.open(QIODevice::WriteOnly)){
        QMessageBox::warning(this,
        tr("FarmAgriPolis"), tr("Fehler! beim �ffnen der Datei %1").arg(filePe.fileName()));
    }
    tstrPe.setDevice(&filePe);
    tstrPe<<"======= FarmAgriPolis ========\n"
        <<"====== Preiserwartungen ======\n\n";
    filePe.close();filePe.open(QIODevice::Append);
    tstrPe.setDevice(&filePe);

    filesClosed=false;
}

void Mainwindow::updatePolitWindow(){
    if (!politWindow) makePolitWindow();

    int year = gg->tIter+gg->FirstYear;
    if (gg->Politiken.find(year)==gg->Politiken.end())
        gg->Politiken[year] = "Keine neue Meldungen f�r dieses Jahr.";
    //politItLabel->setText(QString::number(gg->tIter+gg->FirstYear)
    politStrings.push_back("\n<h2 style='color:#00aa00'> " + QString::number(year)+ "</h2>"
                + QString::fromLocal8Bit(gg->Politiken[year].c_str())+"<br>");
    int sz = politStrings.size();
    politHtml = politStrings[0];
    for (int i=1;i<sz;++i) {
        politHtml += politStrings[sz-i];
    }
    politBrowser->setHtml(politHtml);
    //cout<<"Politiken: "<< politHtml.toStdString() << endl;
}

void Mainwindow::drucken(){
//cout << "drucken noch nicht implementiert "<< endl;
mainToolBar->removeAction(actPrint);

QPainter painter(printer);
painter.translate(20,0);
painter.scale(8,8);
this->render(&painter);

mainToolBar->addAction(actPrint);
}

void Mainwindow::faOutput(){
    absicherFertig = false;
    QProgressBar *dateiPBar = new QProgressBar;
    dateiPBar->setMaximum(0);
    dateiPBar->setMinimum(0);
    dateiPBar->show();
    statusBar()->layout()->addWidget(dateiPBar);

    mydatWindow->faOutput();
    dataWindow->faOutput();
    dateiPBar->hide();
    absicherFertig = true;
}

void Mainwindow::beenden(){
    closeMessage();
   if (!over) return;
   if (landWindow->sceneCreated()){
        landWindow->saveColors();
   }

   if (dataWindow->plotsCreated())
        dataWindow->saveConfig();

   if (mydatWindow->plotsCreated()){
        mydatWindow->saveConfig();
        mydatWindow->close();
    }

   if (!bodenDialog1->isHidden())
        bodenDialog1->close();
   if (!closeDialog->isHidden())
        closeDialog->close();
   if (!investDialog->isHidden())
        investDialog->close();

   cout<<endl;
   if (sThread)sThread->wait(1000);
   if (dThread)dThread->wait(1000);
   if (!filesClosed)
        closeFiles();
   hilfeBrowser->close();
   close();
}

void Mainwindow::getItBeginn(){
   QCoreApplication::processEvents();
   if (gg->tIter==1){
        QMessageBox msgBox;
        msgBox.setWindowTitle("Hinweise");
        msgBox.setText("<h1>Zu Ihrer Information</h1>");
        msgBox.setInformativeText("<h2>Ab jetzt treffen Sie die Entscheidungen zu den Pachtgeboten am Bodenmarkt, "
         "zu den Investitionen in St�lle, Maschinen. Sie entscheiden au�erdem, ob Sie Ihren Betrieb aufgeben oder weiterf�hren. "
        "F�r diese Entscheidungen werden Ihnen w�hrend des Programmablaufs relevante Informationen zur Verf�gung gestellt (welche auch �ber den Button \"Spiel\" abrufbar sind). "
        "Gleichzeitig k�nnen Sie jederzeit Informationen �ber die Fl�chenverteilung, �ber die gesamte Region, �ber Ihren eigenen Betrieb und �ber die agrarpolitische Lage mithilfe "
        "der Buttons \"Landschaften\", \"Daten Region\", \"Mein Betrieb\" und \"Politiken\" abrufen.</h2>"
      );
         if (gg->ToPlay) {
            int ret = msgBox.exec();
            updateWeiter();
        }
   }
    if (gg->tIter!=0){
        QDateTime dateTime = QDateTime::currentDateTime();
        tstr<<"\nRunde "<< gg->tIter<<"  \t";
        tstr<<dateTimeString(dateTime)<<"\n";
        
        updatePolitWindow();
        showPolicy();
        if (gg->ToPlay) updateWeiter();
    }
    simuIterLab->setText(QString::number(gg->tIter));
    yearLab->setText(QString::number(gg->tIter+gg->FirstYear));

    if (gg->tIter>=1)
        mydatWindow->updateGraphic(6);
    if(gg->tIter==1) {
        //mydatWindow->updateGraphic(6); //Betriebsspiegel
        mydatWindow->show();
        bodenDialog1->setShowBSpiegel(true);
        actMyData->setEnabled(false);
    }

    gg->SIM_WEITER = true;
    //updateWeiter();
}

void Mainwindow::getItOver(int it){
   //updatePolitWindow();

   //landWindow->updateFarms(); //??
   if (landWindow->layerComboBox->currentIndex()==2){ //farmtypen �ndern sich
        landWindow->initScene();
        landWindow->initLegends();
   }
   landWindow->updateRegionData();
   landWindow->updateFarminfo();
   dataWindow->updateNfarms();
   /*if(Manager->Farm0->getClosed())
        decs = CLODEC1;
   else decs = CLODEC0;
   //*/
   mydatWindow->updateGraphic(3); //gewinn-/verlustrechnung ?

   if (mydatWindow->isHidden() && it!=0){
        mydatWindow->show();
        actMyData->setEnabled(false);
   }
   if (gg->tIter>=gg->RUNS-1)
       decs = ENDE;
   else
       decs = PROG;
   //updateMwidget();
   showFA();
   if (gg->ToPlay && it<gg->RUNS-1 ) pausen();
   else gg->SIM_WEITER = true;
}

void Mainwindow::getFirstBids(){
    showLandschaften();
}

void Mainwindow::getLAOver(){ //land allocation
   //landWindow->updateFarms();
    QCoreApplication::processEvents();
}

void Mainwindow::getLAsig(int p0, int p1){ //land allocation
    aTime.restart();
    QDateTime dateTime = QDateTime::currentDateTime();
    tstr<<QString("\tBodenmarkt(%1%) ").arg(p0)<< " \t";
    tstr<<dateTimeString(dateTime)<<"\n";
}

void Mainwindow::getINVsig(){
    aTime.restart();
    QDateTime dateTime = QDateTime::currentDateTime();
    tstr<<QString("\tInvestition      ")<< " \t";
    tstr<<dateTimeString(dateTime)<<"\n";
}

void Mainwindow::getProduced(){
    /*SIM_ZUSTAND=PAUSED;
    actStartOrPause->setText("Weiter");
    actStartOrPause->setIcon(QIcon(":/images/next.png"));

    emit zustand(SIM_ZUSTAND);
    //*/
    QCoreApplication::processEvents();
}

void Mainwindow::getPRODsig(){
    if(gg->tIter==0) return;
    QDateTime dateTime = QDateTime::currentDateTime();
    tstr<<QString("\tProduktion       ")<< " \t";
    tstr<<dateTimeString(dateTime)<<"\n";
}

void Mainwindow::getFoFsig(int x){
    aTime.restart();
    QDateTime dateTime = QDateTime::currentDateTime();
    tstr<<QString("\tAusstieg         ")<< " \t";
    tstr<<dateTimeString(dateTime)<<"\n";
}

void Mainwindow::getBusy(){
    QCoreApplication::processEvents();
}

void Mainwindow::getPeriodResults(){
    /*SIM_ZUSTAND=PAUSED;
    actStartOrPause->setText("Weiter");
    actStartOrPause->setIcon(QIcon(":/images/next.png"));

    emit zustand(SIM_ZUSTAND);
    //*/
   QCoreApplication::processEvents();

   mydatWindow->updateMydata();
   actMyData->setEnabled(mydatWindow->isHidden()?true:false);
   dataWindow->updateRegdata(); //nach mydatWindow wegen EK share
   actSectorData->setEnabled(dataWindow->isHidden()?true:false);
   gg->SIM_WEITER = true;
}

void Mainwindow::makeDocks(){
    /*myfarmDock = new QDockWidget(tr("Wichtige Infos"));
    QWidget *myFarminfo = new QWidget(myfarmDock);

    QLabel *myIdLabel = new QLabel(tr("Info 1"));
    myIdEdit = new QLineEdit(myFarminfo);
    myIdLabel->setBuddy(myIdEdit);

    QLabel *myRentLandLabel = new QLabel(tr("Info 2"));
    myRentLandEdit = new QLineEdit(myFarminfo);
    myRentLandLabel->setBuddy(myRentLandEdit);

    QLabel *myCapLabel = new QLabel("Oder so");
    myCapComboBox = new QComboBox(myFarminfo);
    myCapComboBox->addItem("1  100");
    myCapComboBox->addItem("2  20");
    myCapLabel->setBuddy(myCapComboBox);

    QVBoxLayout *vlayout = new QVBoxLayout();
    vlayout->addWidget(myIdLabel);
    vlayout->addWidget(myIdEdit);
    vlayout->addStretch(1);

    vlayout->addWidget(myRentLandLabel);
    vlayout->addWidget(myRentLandEdit);
    vlayout->addStretch(1);

    vlayout->addWidget(myCapLabel);
    vlayout->addWidget(myCapComboBox);

    myFarminfo->setLayout(vlayout);
    myfarmDock->setWidget(myFarminfo);
    //*/

    simuDock = new QDockWidget(tr("Simulationsablauf"));//Info zur Simulation"));
    simuDock->setFeatures(QDockWidget::NoDockWidgetFeatures);
    QWidget *simuInfo = new QWidget(simuDock);
    //simuInfo->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    QLabel *simuIterLabname = new QLabel(QString("Periode"));
    simuIterLabname->setStyleSheet("font-size:18px");
    simuIterLabname->setAlignment(Qt::AlignCenter);

    int iter =  0;
    simuIterLab = new QLabel(QString::number(iter));
    simuIterLab->setAlignment(Qt::AlignCenter);
    simuIterLab->setStyleSheet("font-size: 18px; color: blue");

    yearLab = new QLabel;//QString::number(iter+gg->FirstYear));
    yearLab->setAlignment(Qt::AlignCenter);
    yearLab->setStyleSheet("font-size: 25px; color: blue");

    QGroupBox *decGBox = new QGroupBox("Entscheidungen");
    //decGBox->setStyleSheet("color: blue");
    QVBoxLayout *gblout = new QVBoxLayout;

    //lout->addWidget(aframe);
    //lout->addStretch();
    //lout->addSpacing(15);
    decIcon *di0 = new decIcon("FarmAgriPoliS", QImage(":/images/grafiken/Titelbild.png"),simuInfo);
    decIcon *di1 = new decIcon("Bodenmarkt", QImage(":/images/grafiken/Bodenmarkt.png"),simuInfo);
    decIcon *di2 = new decIcon("Investitionen", QImage(":/images/grafiken/Investition.png"),simuInfo);
    decIcon *di3 = new decIcon("Ausstieg", QImage(":/images/grafiken/Ausstieg.png"),simuInfo);

    simuIcons.push_back(di0);
    simuIcons.push_back(di1);
    simuIcons.push_back(di2);
    simuIcons.push_back(di3);

    gblout->addWidget(di0);
    gblout->addWidget(di1);
    gblout->addWidget(di2);
    gblout->addWidget(di3);
    decGBox->setLayout(gblout);

    QVBoxLayout *lout = new QVBoxLayout;
    lout->addWidget(yearLab);
    lout->addSpacing(20);
    QHBoxLayout *ho = new QHBoxLayout;
    ho->addStretch();
    ho->addWidget(simuIterLabname);
    ho->addSpacing(20);
    ho->addWidget(simuIterLab);
    ho->addStretch();
    lout->addLayout(ho);


    lout->addSpacing(20);
    QFrame *aframe = new QFrame;
    aframe->setFrameShape(QFrame::HLine);
    //lout->addWidget(aframe);

    lout->addWidget(decGBox);

    lout->addSpacing(20);
    QFrame *bframe = new QFrame;
    bframe->setFrameShape(QFrame::HLine);

    //lout->addWidget(bframe);

    //lout->addSpacing(20);
    aktDecText="FarmAgriPoliS";//Bodenmarkt";
    decLab = new QLabel(QString("%1\n").arg(aktDecText));//Entscheiden Sie �ber\n %1").arg(aktDecText));//
    decLab->setAlignment(Qt::AlignCenter);

    decLab->setStyleSheet("font-size: 15px; color: blue");

    dclock = new DigitalClock;
    //dclock->anhalten();
    QHBoxLayout *hh = new QHBoxLayout;
    /*hh->addSpacing(5);
    hh->addWidget(dclock);
    hh->addSpacing(15);
    //*/
    lout->addStretch();
    //lout->addWidget(dclock);
    lout->addWidget(decLab);
    lout->addLayout(hh);

    /*
    lout->addSpacing(20);
    AnalogClock *uhr = new AnalogClock(simuInfo);
    uhr->resize(200,200);
    uhr->show();
    lout->addSpacing(20);
    lout->addWidget(uhr);
    //*/

    //lout->addStretch();

    simuInfo->setLayout(lout);

    simuInfo->setMinimumWidth(150);// dynam ?
    simuInfo->setMaximumWidth(200);

    simuDock->setWidget(simuInfo);
    simuDock->setMinimumWidth(simToolBar->width());

    simuIcons[0]->setStyleSheet("font-size: 24px; background-color: yellow; color: blue");

}

void Mainwindow::changeDockColor(int k){
    for (int i=0; i<4; ++i){
        if (i==k)
           simuIcons[i]->setStyleSheet("font-size: 24px; background-color: yellow; color: blue");
        else
           simuIcons[i]->setStyleSheet("font-size: 14px; background-color: ");

    }
    switch(k){
    case 1: aktDecText="Bodenmarkt";break;
    case 2: aktDecText="Investitionen";break;
    case 3: aktDecText="Ausstieg";break;
    default: aktDecText="FarmAgriPoliS";
    }
    if (k==0)
        decLab->setText(QString("%1\n").arg(aktDecText));
    else
        decLab->setText(QString("%1\nBitte treffen Sie\n Ihre Entscheidung\n").arg(aktDecText));
}

void Mainwindow::pausen(){
     SIM_ZUSTAND= PAUSED;
     actStartOrPause->setText("Weiter");
     actStartOrPause->setIcon(QIcon(":/images/next.png"));
     emit zustand(SIM_ZUSTAND);
}

void Mainwindow::getShowBoden(int p0,int p1){
    if (p0==0) {
        decs = BODDEC;
        updateMwidget();
        changeDockColor(1);
        dclock->resetTime();
    }
    //bodenDialog1->activateWindow();//setFocus();
}

void Mainwindow::getShowBoden(int p0){
    getShowBoden(p0,0);
}

void Mainwindow::getShowClose(int){
    closeDialog->setShowBSpiegel(!mydatWindow->isHidden());
    decs = CLODEC;
    updateMwidget();
    changeDockColor(3);
    dclock->resetTime();
    closeDialog->setFocus();
}

void Mainwindow::getShowInvest(){
    decs = INVDEC;
    updateMwidget();
    changeDockColor(2);
    dclock->resetTime();
    investDialog->setFocus();
}

void Mainwindow::updateMwidget(){
    //actDec->setEnabled(false);
    if (cwidget != DEC){
        saveCWidget(cwidget);
        reActGui(cwidget);
        setCentralWidget(dataWidget);
        cwidget = DEC;
        actDec->setEnabled(false);
    }//*/
    mwidgets[0]->setVisible(decs==PROG);
    mwidgets[1]->setVisible(decs==BODDEC);
    mwidgets[2]->setVisible(decs==INVDEC);
    mwidgets[3]->setVisible(decs==CLODEC);
    mwidgets[4]->setVisible(decs==ENDE);//CLODEC0);
    //mwidgets[5]->setVisible(decs==CLODEC1);
    //dataWidget->adjustSize();

    switch(decs){
    case PROG: statusLabel->setText("FarmAgriPoliS: Entspannen Sie sich ...");break;
    case BODDEC:statusLabel->setText("Bodenmarkt: entscheiden Sie ...");break;
    case INVDEC:statusLabel->setText("Investition: entscheiden Sie ...");break;
    case CLODEC:statusLabel->setText("Ausstieg: entscheiden Sie ...");break;
    case ENDE:statusLabel->setText("Fertig.");break;
    default: statusLabel->setText("FarmAgriPoliS: keine Ahnung, wo ich bin.");
    }
}

void Mainwindow::saveCWidget(CWIDGET cw){
    switch(cw){
    case LAND: landWindow = static_cast<Landwindow*> (centralWidget());
               landWindow->setParent(NULL); break;
    case REG:  dataWindow = static_cast<Datawindow*> (centralWidget());
               dataWindow->setParent(NULL); break;
    case MY:   mydatWindow = static_cast<Mydatwindow*> (centralWidget());
               mydatWindow->setParent(NULL); break;
    case POL:  politWindow = static_cast<QScrollArea*>(centralWidget());
               politWindow->setParent(NULL); break;
    case DEC:  dataWidget = static_cast<QScrollArea*> (centralWidget());
               dataWidget->setParent(NULL);break;
    default:;
    }
}

void Mainwindow::reActGui(CWIDGET cw){
    switch(cw){
    case LAND: actLandschaften->setEnabled(true);
                //statusLabel->setText("Landschaften");
                break;
    case REG:   actSectorData->setEnabled(true);
                //statusLabel->setText("Regionale Infos");
                break;
    case MY:    actMyData->setEnabled(true);
                if (closeDialog->isInited())
                    closeDialog->enableBSbutton(true);
                if (bodenDialog1->isInited())
                    bodenDialog1->enableBSbutton(true);
                //statusLabel->setText("Infos �ber Ihren eigenen Betrieb");
                break;
    case POL: actPolicy->setEnabled(true);
                //statusLabel->setText("Politiken");
                break;
    case DEC: actDec->setEnabled(true);
                //statusLabel->setText("Entscheidung");
                break;
    default:  statusLabel->setText("Keine Ahnung, wo ich bin.");;
    }
}

void Mainwindow::makeCentralWidget(){
    dataWidget = new QScrollArea(this);//QFrame;
    //dataWidget->setWidgetResizable(true);
    dataWidget->setWindowTitle("Spiel-Informationen");

    dwidget = new QWidget(dataWidget);

    int k=5;//6;
    QVBoxLayout *vl = new QVBoxLayout;

    for (int i=0;i<k;++i){
        /*QTextEdit *te = new QTextEdit;
        te->setReadOnly(true);
        te->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        QTextImageFormat imgFormat;

        //imgFormat.setHeight(te->height());
        imgFormat.setWidth(QApplication::desktop()->width());

        imgFormat.setName(":/images/grafiken/Titelbild.png");//FarmAgriPoliS.jpg");
        QTextCursor helper = te->textCursor();
        helper.setPosition(0);
        helper.setCharFormat(imgFormat);
        helper.insertImage(imgFormat);

        //*/
        QVBoxLayout *vvl = new QVBoxLayout;
        QPixmap *p=0 ;
        switch(i){
            case 0: p = new QPixmap(":/images/grafiken/Titelbild.png");break;
            case 1: p = new QPixmap(":/images/grafiken/Bodenmarkt.png");break;
            case 2: p = new QPixmap(":/images/grafiken/Investition.png");break;
            case 3: p = new QPixmap(":/images/grafiken/Ausstieg.png");break;
            default: p = new QPixmap(":/images/grafiken/Titelbild.png");
        }
        p->scaledToWidth(900);//QApplication::desktop()->width());
        QLabel *lab = new QLabel;
        lab->setAlignment(Qt::AlignCenter);
        lab->setPixmap(*p);

        //QTextEdit *ted = new QTextEdit;
         /*QTextBrowser *ted = new QTextBrowser;
         ted->setOpenExternalLinks(true);
        //*/ ;

        QString fileName;

        switch(i){
            case 0: fileName=":/html/prog.html"; break; //text = textTitle;break;
            case 1: fileName=":/html/bod.html"; break; //text = textBoden;break;
            case 2: fileName=":/html/inv.html"; break; //text = textInvest;break;
            case 3: fileName=":/html/clo.html"; break; //text = textClose; break;
            case 4: fileName=":/html/end.html"; break;
            //case 5: fileName=":/html/out.html"; break;
            default: fileName=":/html/prog.html"; //text = textTitle;
        }

        //ted->setSource(fileName);
        QFile file(fileName);
        QString tt;
        if (file.open(QFile::ReadOnly | QFile::Text)){
            tt = QString(file.readAll());
        }
        //tt = "<center> <h2 style='color:#00aa00'> HALLLLLLLLO!!!</h2> <p> centering ??? </p></center>";
        MyBrowser *ted = new MyBrowser(tt);//file.readAll());
        //ted->setHtml(file.readAll());
       ted->setFont(QFont("Times", 14));
       ted->setFixedWidth(1060);
        //ted->setReadOnly(true);
        //ted->setHtml(text);
        //ted->setAlignment(Qt::AlignCenter);

        vvl->addWidget(lab);
        vvl->addWidget(ted);

        QWidget *w = new QWidget();
        w->setLayout(vvl);
        mwidgets.push_back(w);

        vl->addWidget(w);//addLayout(hhl);
    }
    /*
    QWebView *view = new QWebView(this);
    view->load(QUrl("http://www.iamo.de"));
    view->show();
    vl->addWidget(view);
    //*/

    dwidget->setLayout(vl);
    dwidget->setFixedHeight(960);
    dataWidget->setWidget(dwidget);//
    /*
    //QWidget *centralWidget = new QWidget(dataWidget);
    QLabel *label = new QLabel(dataWidget);
    label->setText(tr("<h1> Hallo <p>  Hier ist <font color = green>FarmAgriPoliS ! </font> </h1>"));
    QGridLayout *lout = new QGridLayout;
    lout->addWidget(label, 0, 0,1,2,Qt::AlignCenter);

    dataWidget->setLayout(lout);
    dataWidget->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    dataWidget->setFrameStyle(1);
    dataWidget->setFrameShadow(QFrame::Raised);
    dataWidget->setFrameShape(QFrame::Box);

    dataWidget->setAutoFillBackground(true);
    QPalette *p = new QPalette();
    p->setColor(QPalette::Background, Qt::white);
    dataWidget->setPalette(*p);
    //*/
}

void Mainwindow::makePolitWindow(){
    politWindow = new QScrollArea(this);
    politWindow->setWindowTitle("Politiken");

        QVBoxLayout *vvl = new QVBoxLayout;
        QPixmap *p= new QPixmap(":/images/grafiken/Politik.png");

        p->scaledToWidth(1000);//QApplication::desktop()->width());
        QLabel *lab = new QLabel;
        lab->setAlignment(Qt::AlignCenter);
        lab->setPixmap(*p);

        politStrings.push_back("<center> <h1 style='color:#00aa00'> Politikmeldungen </h1> </center>");
        politHtml = politStrings[0];

        politBrowser = new QTextBrowser;
        politBrowser->setHtml(politHtml);
        politBrowser->setFont(QFont("Times", 14));
        politBrowser->setMaximumWidth(1080);
        politBrowser->setMinimumHeight(450);

        //politItLabel = new QLabel("xx");
        //politItLabel->setText(QString::number(gg->tIter+gg->FirstYear));
        //vvl->addWidget(politItLabel);

        vvl->addWidget(lab);
        vvl->addWidget(politBrowser);

        QWidget *w = new QWidget();
        w->setLayout(vvl);
        //w->setFixedHeight(centralWidget()->height()-360);
        w->setMinimumHeight(800);
        politWindow->setWidget(w);//
}

void Mainwindow::setSpielPeProds(vector<bool> sp){
    spielPeProds = sp;
}

vector<bool>  Mainwindow::getSpielPeProds()const{
    return spielPeProds;
}

void Mainwindow::setPeWidget(PEwidget *pw){
    peWidget = pw;
}

PEwidget*  Mainwindow::getPeWidget()const{
    return peWidget;
}
