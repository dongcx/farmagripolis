#ifndef CLOSEDIALOG_H
#define CLOSEDIALOG_H

#include <stdio.h>
#include <stdlib.h>

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>


class RegFarmInfo;
class QSpinBox;
class SpreadSheet2;
class QDoubleSpinBox;

using namespace std;
class CloseDialog : public QDialog {
    Q_OBJECT
public:
    CloseDialog();
    void closeEvent(QCloseEvent*);

public slots:
    void dispGewinns(double,double);
    void okPushed();

    void showClose(int);
    void updateGUI();
     void updateGUI2();

    void makePeWidget();
    void makePeWidget2();
    void makeFinWidget();
    void makeOppWidget();
    void calRefPEs();

    void showHilfe();
    void hilfeClosed();

    void updateVar(int);
    void getPeOK(int,bool);
    void updatePE(int);
    void updateIndexLab();
    void savePEs();
    void updateRef(int);
    void updateFinOpp();

    void aussteigen();
    void bleiben();
    void weiter();

    void blendenPE();

signals:
    void gewinns(double,double);//
    void closeSig(bool);
    void ClosePeSig(int);

private:
    QLabel *l1, *l2;
    QLineEdit *le1, *le2;

    int nsoils;
    vector<string> soilNames;
    RegFarmInfo *farm0;
    int ITER;

    int closeState;
    bool firstGUI;
    QPushButton *weiterButton;
    QWidget* weiterWidget;

    QWidget* decWidget;
    QPushButton *aussteigButton, *bleibButton;
    QPushButton *hilfeButton;

    QDialog *hilfeDialog;

    QWidget *peWidget;
    QComboBox *peVarCBox;
    QSpinBox *refzeitSBox;

    vector<QString> prodNames;
    vector<int> prodIDs;
    vector<double> prodPrices;
    vector<double> prodRefPEs;
    vector<double> prodPEs;
    int numPEs;

    vector<int> getreideIDs;
    vector<double> getreidePrices, getreidePEs;

    vector<double> adaptPEs, naivPEs, manPEs;
    vector<vector<double> > allPEs;  //PEs for all iterations

    int nRowPE, nColPE;
    QLabel *pindexLab;
    double preisIndex;

    int peVar;
    QString refname;
    int refprodID;
    int refpos;

    SpreadSheet2 *peSS;
    vector<QDoubleSpinBox*> peDSBs;

    vector<vector<double> > all_myPEs;
    vector<double>  it_myPEs;

    bool inited;
    double oldvalue;

    QWidget *oppCostWidget;
    QWidget *finWidget;
    SpreadSheet2 *finSS;
    SpreadSheet2 *oppSS;

    QPushButton *peButton;
    double langFK, dispo, GDB, liquidity;

    double expIncome;
    QLabel *expIncomeLab;
    double oppcost;
    QLabel *oppcostLab;
    vector<bool> decisions;

    QLabel *langFKLab, *dispoLab , *GDBLab , *liqLab;
    bool showPEwidget;
};

class Title3 : public QLabel{
    Q_OBJECT
public: explicit Title3(Qt::AlignmentFlag align, QFont font, QWidget* w=0);

};

#endif // CLOSEDIALOG_H
