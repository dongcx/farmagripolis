#include "closedialog.h"
#include "AgriPoliS.h"
#include <QLineEdit>
#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include <QComboBox>
#include <QSpinBox>
#include <QGroupBox>
#include <QSignalMapper>
#include <QMessageBox>

#include "spreadsheet2.h"
#include "cell2.h"

static QFont font0 = QFont("SansSerif",14, QFont::Bold);
static QFont font1 = QFont("Times",11, QFont::Bold);
static QFont font2 = QFont("Times",11, QFont::Normal);//Bold);
static QFont font3 = QFont("Times",10, QFont::Normal);

CloseDialog::CloseDialog():firstGUI(true), inited(false), showPEwidget(false),QDialog(){
   /*
    setWindowTitle(tr("Ausscheiden ?"));
    QLabel *lab1 = new QLabel("Gewinn");
    le1 = new QLineEdit("100000");
    QLabel *lab2 = new QLabel("Opport-Gewinn");
    le2 = new QLineEdit("100005");
    QGridLayout *gl = new QGridLayout;
    QPushButton *pb = new QPushButton("Ok");

    connect(pb,SIGNAL(clicked()),this, SLOT(okPushed()));

    l1 = new QLabel("");
    l2 = new QLabel("");

    gl->addWidget(lab1,0,0);
    gl->addWidget(le1,0,1);
    gl->addWidget(lab2,1,0);
    gl->addWidget(le2,1,1);
    gl->addWidget(pb,2,0,1,2);
    gl->addWidget(l1, 3,0);
    gl->addWidget(l2,3,1);
    setLayout(gl);

    resize(200,500);
    //*/
}

void CloseDialog::closeEvent(QCloseEvent * event){
    Manager->getGlobals()->SIM_WEITER =true;
    gg->HasCloseDec=true;
    hilfeDialog->close();
    inited=false;
    Manager->product_cat0 = Manager->Market->getProductCat();
    QDialog::closeEvent(event);
}

void CloseDialog::updateGUI2(){
    if (closeState==3) weiterWidget->setVisible(true);
    else weiterWidget->setVisible(false);
    if (closeState==3) decWidget->setVisible(false);
    else decWidget->setVisible(true);

    updateFinOpp();
    makePeWidget2();
}

void CloseDialog::makePeWidget2(){
    refzeitSBox->setMaximum(ITER);

    vector<RegProductInfo> pcat = *(farm0->product_cat);
    int sz = numPEs;//pcat.size();

    for (int i=0; i<sz; ++i){
        prodPrices[i]=pcat[prodIDs[i]].getPrice();
        prodPEs[i]=pcat[prodIDs[i]].getPriceExpectation();
    }
    prodRefPEs = prodPEs;
    naivPEs = prodPrices;
    adaptPEs = prodPEs;
    manPEs = prodPEs;

    allPEs.push_back(adaptPEs);
    calRefPEs();

    peVar = peVarCBox->currentIndex();
    for (int r=0; r<nRowPE; ++r){
            //cout << "pe2: "<<r<<endl;
            double max, min;
            max = prodPrices[r]*1.5;
            min = prodPrices[r]*0.7;
            if (max<0) {
                double t = max;
                max = min;
                min = t;
            }
            QDoubleSpinBox *dsb=peDSBs[r]; //QDoubleSpinBox *dsb = peDSBs[r];
            dsb->setRange(min,max);
            dsb->setValue(prodPEs[r]);//setText(QLocale().toString(prodPrices[r],'f',2));
    }

    peSS->updateColumn(1, prodPrices);
    peSS->updateColumn(2, prodRefPEs);
    //peSS->updateColumn(3, prodPEs);

    preisIndex = prodPEs[refpos]/prodPrices[refpos];
    pindexLab->setText(QString("Preisindex für alle Marktfrüchte (wie der von %1) : %2").arg(refname, QLocale().toString(preisIndex)));
    inited = true;
}

void CloseDialog::updateGUI(){
    ITER = gg->tIter;

    farm0 = Manager->Farm0;

    soilNames = gg->NAMES_OF_SOIL_TYPES;
    nsoils = gg->NO_OF_SOIL_TYPES;

    langFK=farm0->getLangFKclose();//>getLtBorrowedCapital();
    dispo=farm0->getDispoClose();//>getStBorrowedCapital();
    liquidity=farm0->getLiqClose();//>getLiquidity();
    GDB=farm0->getGDBclose();

    if (!firstGUI) {
        updateGUI2();
        return;
    }
    QString title("Ausstieg");

    Title3 *titleLab = new Title3(Qt::AlignCenter, font0);
    titleLab->setText(title);

    QVBoxLayout *v0 = new QVBoxLayout; //out
    v0->addWidget(titleLab);

    weiterButton = new QPushButton("weiter");
    connect(weiterButton, SIGNAL(clicked()), this, SLOT(weiter()));

    weiterWidget  = new QWidget;

    Title3 *ausLab = new Title3(Qt::AlignCenter, font1);
    ausLab->setText("Aussteigen");
    Title3 *detailLab = new Title3(Qt::AlignCenter, font3);
    detailLab->setText("Sie müssen aufgrund von Illiquidität Ihren Betrieb aufgeben !");
    //Wegen negativem Finanzzustand musst du aussteigen.");

    QHBoxLayout *hh = new QHBoxLayout;
    hh->addStretch();
    hh->addWidget(weiterButton);
    hh->addStretch();

    QVBoxLayout *v1 = new QVBoxLayout;
    //v1->addWidget(ausLab);
    v1->addSpacing(15);
    v1->addWidget(detailLab);
    v1->addLayout(hh);//addWidget(weiterButton);
    weiterWidget->setLayout(v1);

    v0->addWidget(weiterWidget);
    //v0->addStretch(1);


    decWidget = new QWidget;

    Title3 *des = new Title3(Qt::AlignCenter, font1);
    des->setText("Die Entscheidung liegt bei dir");

    aussteigButton = new QPushButton("Ich steige aus");
    bleibButton = new QPushButton("Ich mache weiter");

    connect(aussteigButton, SIGNAL(clicked()), this, SLOT(aussteigen()));
    connect(bleibButton, SIGNAL(clicked()), this, SLOT(bleiben()));

    connect(this, SIGNAL(rejected()), this, SLOT(bleiben()));

    QHBoxLayout *hl3 = new QHBoxLayout;
    hilfeButton = new QPushButton("Hilfe zur Entscheidung");
    hl3->addWidget(hilfeButton);
    hl3->addStretch();

    QHBoxLayout *hh1 = new QHBoxLayout;
    hh1->addStretch();
    hh1->addWidget(aussteigButton);
    hh1->addWidget(bleibButton);
    hh1->addStretch();

    QVBoxLayout *v2 = new QVBoxLayout;
    v2->addWidget(des);
    v2->addSpacing(10);
    v2->addLayout(hh1);
    //v2->addWidget(aussteigButton);
    //v2->addWidget(bleibButton);
    v2->addSpacing(20);
    v2->addLayout(hl3);
    decWidget->setLayout(v2);

    v0->addWidget(decWidget);


    hilfeDialog = new QDialog();
    hilfeDialog->setWindowTitle("Opportunitätskosten und Finanzen für angepaßte Preiserwartung");

    connect(hilfeButton, SIGNAL(clicked()), this, SLOT(showHilfe()));
    connect(hilfeDialog, SIGNAL(rejected()), this, SLOT(hilfeClosed()));

    makePeWidget();
    makeFinWidget();
    makeOppWidget();

    QVBoxLayout *vl4 = new QVBoxLayout;

    QHBoxLayout *hh4 = new QHBoxLayout;
    hh4->addStretch();
    hh4->addWidget(finWidget);
    //hh4->addWidget(oppCostWidget);
    hh4->addStretch();

    peButton = new QPushButton("Preiserwartung anpassen");
    showPEwidget=false;
    peWidget->setVisible(showPEwidget);//hide();
    connect(peButton, SIGNAL(clicked()), this, SLOT(blendenPE()));

    QHBoxLayout *hh5 = new QHBoxLayout;
    hh5->addWidget(peButton);
    hh5->addStretch();

    QHBoxLayout *hh6 = new QHBoxLayout;
    hh6->addStretch();
    hh6->addWidget(oppCostWidget);
    hh6->addStretch();

    vl4->addLayout(hh6);
    vl4->addLayout(hh4);
    vl4->addSpacing(20);
    vl4->addLayout(hh5);//addWidget(peButton);
    vl4->addSpacing(10);
    vl4->addWidget(peWidget);

    hilfeDialog->setLayout(vl4);

    setLayout(v0);
    firstGUI = false;

    if (closeState==3) weiterWidget->setVisible(true);
    else weiterWidget->setVisible(false);
    if (closeState==3) decWidget->setVisible(false);
    else decWidget->setVisible(true);

    //resize(400,500);
}

void CloseDialog::showHilfe(){
    hilfeDialog->show();
    hilfeButton->setEnabled(false);
}

void CloseDialog::savePEs(){
    it_myPEs.clear();

    switch(peVar) {
    case 0: it_myPEs = manPEs; break;
    case 1: it_myPEs = naivPEs; break;
    case 2: it_myPEs = adaptPEs; break;
    default: ;
    }

    all_myPEs.push_back(it_myPEs);
}

void CloseDialog::hilfeClosed(){
    hilfeButton->setEnabled(true);
}

void CloseDialog::makeOppWidget(){
    oppCostWidget = new QWidget;

    QGroupBox *compGBox = new QGroupBox;

    Title3 *oppKostTitle = new Title3(Qt::AlignCenter, font1);
    oppKostTitle->setText("Außerhalb Landwirtschaft");//Opportunitätskosten");
    Title3 *landWirtschaftTitle = new Title3(Qt::AlignCenter, font1);
    landWirtschaftTitle->setText("Landwirtschaft");//Opportunitätskosten");

    expIncome = farm0->getExpIncome();
    expIncomeLab = new QLabel;
    expIncomeLab->setText(QString("Erwartetes Einkommen: %1")
                .arg(QLocale().toString(expIncome,'f',2))+" "+QChar(0x20ac));
    expIncomeLab->setFont(font2);

    oppcost = farm0->getOppcost();
    oppcostLab = new QLabel;
    oppcostLab->setText(QString("Opportunitätskosten: %1")
            .arg(QLocale().toString(oppcost,'f',2))+ " "+ QChar(0x20ac));
    oppcostLab->setFont(font2);

    QGridLayout *gl = new QGridLayout;
    gl->addWidget(oppKostTitle, 0, 2);
    gl->addWidget(landWirtschaftTitle, 0, 0);
    gl->addWidget(expIncomeLab, 1,0);
    gl->addWidget(oppcostLab, 1,2);
    gl->setHorizontalSpacing(30);

    compGBox->setLayout(gl);

    QVBoxLayout *vl = new QVBoxLayout;
    vl->addWidget(compGBox);
    oppCostWidget->setLayout(vl);


    /*
    Title3 *oppLab = new Title3(Qt::AlignCenter, font3);
    oppLab->setText("Einkommen bei Arbeit");

    Title3 *oppLab2 = new Title3(Qt::AlignCenter, font3);
    oppLab2->setText("außerhalb der Landwirtschaft:");

    oppcost = farm0->getOppcost();
    Title3 *oppCostLab = new Title3(Qt::AlignCenter, font3);
    oppCostLab->setText(QLocale().toString(oppcost,'f',2)+" "+QChar(0x20ac));

    QVBoxLayout *vv = new QVBoxLayout;
    vv->addWidget(oppKostLab);
    vv->addSpacing(15);
    vv->addWidget(oppLab);
    vv->addWidget(oppLab2);
    vv->addSpacing(15);
    vv->addWidget(oppCostLab);
    vv->addStretch();
    oppcostWidget->setLayout(vv);
    //*/
/*
    vector<QString> tv;

    oppcost = farm0->getOppcost();
    tv.push_back(QString("Einkommen bei Arbeit"));
    tv.push_back("außerhalb der Landwirtschaft:");
    tv.push_back("");
    tv.push_back(QLocale().toString(oppcost,'f',2)+" "+QChar(0x20ac));

    oppSS = new SpreadSheet2(4,1,false, tv,false);

    oppSS->updateColumn(0,tv);
    oppSS->cell(3,0)->setAlign(Qt::AlignCenter, Qt::AlignCenter);

    QVBoxLayout *vf = new QVBoxLayout;
    vf->addWidget(oppKostLab);
    vf->addWidget(oppSS);

    oppCostWidget->setLayout(vf);
    //*/
}

void CloseDialog::makeFinWidget(){
    finWidget = new QWidget;
    Title3* finTitle = new Title3(Qt::AlignCenter, font1);
    finTitle->setText("Finanzen");

    vector<QString> tv;
    finSS = new SpreadSheet2(4,2,false,tv,false);

    tv.push_back(QString("langfristiges FK"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Dispo"));//+QChar(0x20ac)+")");
    tv.push_back(QString("GDB"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Liquidität"));//+QChar(0x20ac)+")");

    /*
    QLabel *langFKLabT = new QLabel("langfristiges FK");
    QLabel *dispoLabT  = new QLabel("Dispo");
    QLabel *GDBLabT = new QLabel ("GDB");
    QLabel *liqLabT = new QLabel ("Liquidität");

    langFKLab = new QLabel;
    dispoLab  = new QLabel;
    GDBLab = new QLabel;
    liqLab = new QLabel;

    langFKLab->setText(QLocale().toString(langFK,'f',2));
    dispoLab->setText(QLocale().toString(dispo,'f',2));
    GDBLab->setText(QLocale().toString(GDB,'f',2));
    liqLab->setText(QLocale().toString(liquidity,'f',2));

    QGridLayout *gl = new QGridLayout;
    gl->addWidget(langFKLabT, 0, 0);
    gl->addWidget(langFKLab,0,1);
    gl->addWidget( dispoLabT, 1,0);
    gl->addWidget(dispoLab, 1,1);
    gl->addWidget( GDBLabT,2,0);
    gl->addWidget(GDBLab,2,1);
    gl->addWidget( liqLabT, 3,0);
    gl->addWidget(liqLab,3,1);

    vector<double> td;
    td.push_back(langFK);
    td.push_back(dispo);
    td.push_back(GDB);
    td.push_back(liquidity);
    //*/

    vector<QString> td;
    td.push_back(QLocale().toString(langFK,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(dispo,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(GDB,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(liquidity,'f',2)+" "+QChar(0x20ac));

    finSS->updateColumn(0,tv);
    finSS->updateColumn(1,td);

    QVBoxLayout *vf = new QVBoxLayout;
    vf->addWidget(finTitle);
    vf->addWidget(finSS);

    finWidget->setLayout(vf);
}

void CloseDialog::blendenPE(){
    if (!showPEwidget) {
        peButton->setText("Ausblenden Preiserwartung");
    }else{
        peButton->setText("Preiserwartung anpassen");
    }
    peWidget->setVisible(!showPEwidget);
    showPEwidget = !showPEwidget;

    //alle vorgänger
    peWidget->parentWidget()->layout()->invalidate();
    QWidget *parent = peWidget->parentWidget();
    while (parent) {
        parent->adjustSize();
        parent = parent->parentWidget();
    }
}


void CloseDialog::makePeWidget(){
    Title3* peTitle = new Title3(Qt::AlignCenter, font0);
    peTitle->setText("Preiserwartung");

    QLabel *vartype = new QLabel("Variante");
    peVarCBox = new QComboBox;
    peVarCBox->addItem(QString("Manuell"));
    peVarCBox->addItem(QString("Naiv"));
    peVarCBox->addItem(QString("Adaptiv"));

    connect(peVarCBox, SIGNAL(currentIndexChanged(int)), this, SLOT(updateVar(int)));

    QHBoxLayout *hl = new QHBoxLayout;
    hl->addStretch();
    hl->addWidget(vartype);
    hl->addWidget(peVarCBox);
    hl->addStretch();

    QLabel *refLab = new QLabel("Referenz-Zeit");
    refzeitSBox = new QSpinBox;
    refzeitSBox->setMaximum(ITER+1);
    refzeitSBox->setValue(1);
    refzeitSBox->setMinimum(1);
    QLabel *jahrLab = new QLabel("Jahre");

    connect(refzeitSBox, SIGNAL(valueChanged(int)), this, SLOT(updateRef(int)));

    QHBoxLayout *hl1 = new QHBoxLayout;
    hl1->addStretch();
    hl1->addWidget(refLab);
    hl1->addWidget(refzeitSBox);
    hl1->addWidget(jahrLab);
    hl1->addStretch();

    //*/

    vector<QString> vs;
    vs.push_back("Produktname");
    vs.push_back(QString("Preis (")+QChar(0x20ac)+")");
    vs.push_back(QString("Ref. Preiserwartung (")+QChar(0x20ac)+")");
    vs.push_back(QString("Meine Preiserwartung (")+QChar(0x20ac)+")");
    vs.push_back(QString("Preisindex"));

    int xcount = 0;
    vector<RegProductInfo> pcat = *(farm0->product_cat);
    int sz = pcat.size();
    bool refnameFound = false;

    for (int i=0; i<sz; ++i){
        //if (pcat[i].getProductGroup()<0) continue;
        if (pcat[i].getProductGroup()== 0  && !refnameFound) {
            refname = QString(pcat[i].getName().c_str());
            refprodID = i;
            refpos = xcount;
            refnameFound = true;

        }else if (pcat[i].getProductGroup()==0){
            getreideIDs.push_back(i);
            getreidePEs.push_back(pcat[i].getPriceExpectation());
            getreidePrices.push_back(pcat[i].getPrice());
            continue;
        }
        ++xcount;
        prodNames.push_back(QString(pcat[i].getName().c_str()));
        prodPrices.push_back(pcat[i].getPrice());
        prodIDs.push_back(i);
        prodPEs.push_back(pcat[i].getPriceExpectation());
        //prodRefPEs.push_back(100);
    }
    prodRefPEs = prodPEs;
    naivPEs = prodPrices;
    adaptPEs = prodPEs;
    manPEs = prodPEs;
    numPEs = prodIDs.size();

    allPEs.push_back(adaptPEs);
    calRefPEs();

    nRowPE = xcount; //25;
    nColPE = 4;
    peSS = new SpreadSheet2(nRowPE, nColPE, true, vs, false);
    peSS->setArt(2);
    peSS->setTabKeyNavigation(false);

    QSignalMapper *sigmapPE = new QSignalMapper;

    peVar = peVarCBox->currentIndex();
    //if (peVar==0) {
        for (int r=0; r<nRowPE; ++r){
            QDoubleSpinBox *dsb = new QDoubleSpinBox();
            //QLineEdit *dsb = new QLineEdit();

            double max, min;
            max = prodPrices[r]*1.5;
            min = prodPrices[r]*0.7;
            if (max<0) {
                double t = max;
                max = min;
                min = t;
            }

           // MyDoubleValidator *dv = new MyDoubleValidator(min,max,10);
           //dv->setNotation(QDoubleValidator::StandardNotation);
           //dsb->setValidator(dv);
            dsb->setRange(min,max);
            dsb->setAlignment(Qt::AlignRight);
            dsb->setSuffix("  ");
            dsb->setLocale(QLocale::German);
            dsb->setValue(prodPrices[r]);//setText(QLocale().toString(prodPrices[r],'f',2));
            peSS->setCellWidget(r,nColPE-1,dsb);

            peDSBs.push_back(dsb);
            connect(dsb, SIGNAL(valueChanged(double)), sigmapPE, SLOT(map()));
            sigmapPE->setMapping(dsb, r);
            //peSS->cell(r,nColPE-1)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable);
            //peSS->cell(r,nColPE-1)->setBackgroundColor(cellBColor);
        }
    //}
    connect(sigmapPE, SIGNAL(mapped(int)), this, SLOT(updatePE(int)));

    peSS->updateColumn(0, prodNames);
    peSS->updateColumn(1, prodPrices);
    peSS->updateColumn(2, prodRefPEs);
    //peSS->updateColumn(3, prodPEs);

    peWidget=new QWidget;

    //refname=QString("Weizen");
    preisIndex = prodPEs[refpos]/prodPrices[refpos];//1.101;
    pindexLab =  new QLabel(QString("Preisindex für alle Marktfrüchte (wie der von %1) : %2").arg(refname, QLocale().toString(preisIndex)));

    QVBoxLayout *vl = new QVBoxLayout;
    vl->addWidget(peTitle);
    vl->addSpacing(5);

    QGridLayout *gl = new QGridLayout;
    gl->addLayout(hl, 0, 0);
    gl->addLayout(hl1, 0, 1);
    //vl->addLayout(hl);
    //vl->addLayout(hl1);
    //vl->addStretch();
    vl->addLayout(gl);
    vl->addWidget(peSS);
    vl->addWidget(pindexLab);

    // vl->addStretch(1);
    peWidget->setLayout(vl);
    inited = true;
}

void CloseDialog::updateRef(int x){
    calRefPEs();
    peSS->updateColumn(2, prodRefPEs);
}

void CloseDialog::updateVar(int i){
   peVar = i;

    bool en = false;
    if (i==0) {
        en = true;
    }

    vector<double> tvalues ;
    switch (i) {
    case 0: tvalues = manPEs;break;
    case 1: tvalues = naivPEs; break;
    case 2: tvalues = adaptPEs; break;
    default: ;
    }

    for (int i=0; i<numPEs; ++i){
        peDSBs[i]->setValue(tvalues[i]);

    }

    updateIndexLab();

    for (int r=0; r<nRowPE; ++r){
        static_cast<QDoubleSpinBox*>(peSS->cellWidget(r,nColPE-1))->setEnabled(en);
    }
}

void CloseDialog::okPushed(){
    double d1,d2;
    d1 = le1->text().toDouble();
    d2 = le2->text().toDouble();
    emit gewinns(d1,d2);
}

void CloseDialog::dispGewinns(double i, double d){
    l1->setText(QString::number(i));
    l2->setText(QString::number(d));
}

void CloseDialog::calRefPEs(){
    int sz = prodNames.size();
    int x = refzeitSBox->value();
    prodRefPEs.clear();
    prodRefPEs.resize(sz);
    for (int i=0; i<sz; ++i){
       prodRefPEs[i] = 0;
       for (int k=0; k<x; ++k)  {
            prodRefPEs[i] += allPEs[ITER-k][i];
       }
       prodRefPEs[i] /= x;
    }
}

void CloseDialog::updatePE(int row){
        if (!inited) return;
        int sz = prodIDs.size();
        for (int i=0; i<sz; ++i){
            peDSBs[i]->setEnabled(false);
        }

        double d = peDSBs[row]->value();
        oldvalue = Manager->product_cat0[prodIDs[row]].getPriceExpectation();
        Manager->product_cat0[prodIDs[row]].setPriceExpectation(d);

        if (row == refpos) {
            double tind = d/prodPrices[refpos];
            getreidePEs.clear();
            int ts = getreideIDs.size();
            for (int i=0; i<ts; ++i){
                getreidePEs.push_back(Manager->product_cat0[getreideIDs[i]].getPriceExpectation());
            }
            for (int i=0; i<ts; ++i){
                Manager->product_cat0[getreideIDs[i]].setPriceExpectation(getreidePrices[i]*tind);
            }
        }

        //cout << "a: row: " << row <<endl;
        emit ClosePeSig(row);
}

void CloseDialog::updateIndexLab(){
    double refpe = peDSBs[refpos]->value();
    double refp = prodPrices[refpos];
    preisIndex = refpe/refp;
    pindexLab->setText(QString("Preisindex für alle Marktfrüchte (wie der von %1) : %2").arg(refname, QLocale().toString(preisIndex,'f',3)));
}

void CloseDialog::getPeOK(int r, bool b){
        if (!b) {
            inited = false;
            peDSBs[r]->setValue(oldvalue);
            inited = true;
            if (r==refpos) {
                int ts = getreideIDs.size();
                for (int i=0; i<ts; ++i){
                    Manager->product_cat0[getreideIDs[i]].setPriceExpectation(getreidePEs[i]);
                }
            }

            Manager->product_cat0[prodIDs[r]].setPriceExpectation(oldvalue);
            QMessageBox::warning(this, tr("Solver Information"),
                             tr("Bedingungen verletzt"));

        }
        int sz = prodIDs.size();
        for (int i=0; i<sz; ++i){
            peDSBs[i]->setEnabled(true);
        }

        if (!b) return;

        if (peVar == 0) manPEs[r] = peDSBs[r]->value();

        updateFinOpp();

        if (r==refpos) {
            updateIndexLab();
        }
        //cout << "b: row: " << r <<endl;
}

void CloseDialog::updateFinOpp(){
    langFK=farm0->getLangFKclose();//>getLtBorrowedCapital();
    dispo=farm0->getDispoClose();//>getStBorrowedCapital();
    liquidity=farm0->getLiqClose();//>getLiquidity();
    GDB=farm0->getGDBclose();

    vector<QString> td;
    td.push_back(QLocale().toString(langFK,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(dispo,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(GDB,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(liquidity,'f',2)+" "+QChar(0x20ac));

    finSS->updateColumn(1,td);

    oppcost = farm0->getOppcost();
    expIncome = farm0->getExpIncome();
    expIncomeLab->setText(QString("Erwartetes Einkommen: %1")
                .arg(QLocale().toString(expIncome,'f',2))+" "+QChar(0x20ac));
    oppcostLab->setText(QString("Opportunitätskosten: %1")
            .arg(QLocale().toString(oppcost,'f',2))+ " "+ QChar(0x20ac));


    //oppSS->cell(3,0)->setFormula(QLocale().toString(oppcost,'f',2)+" "+QChar(0x20ac));
}

void CloseDialog::aussteigen(){
    QDialog *absicherDialog = new QDialog(this);
    QString title("Steigen Sie wirklich aus ?");

    Title3 *titleLab = new Title3(Qt::AlignCenter, font1);
    titleLab->setText(title);

    QVBoxLayout *v0 = new QVBoxLayout; //out
    v0->addWidget(titleLab);

    QPushButton *jaButton = new QPushButton("Ja");
    connect(jaButton, SIGNAL(clicked()), absicherDialog, SLOT(accept()));

    QPushButton *noButton = new QPushButton("Nein, zurück");
    connect(noButton, SIGNAL(clicked()), absicherDialog, SLOT(reject()));

    QHBoxLayout *hh = new QHBoxLayout;
    hh->addStretch();
    hh->addWidget(jaButton);
    hh->addSpacing(30);
    hh->addWidget(noButton);
    hh->addStretch();

    v0->addSpacing(10);
    v0->addLayout(hh);
    absicherDialog->setLayout(v0);

    if (!absicherDialog->exec()) return;

    farm0->farmFutureEnd(true);
    accept();
    close();
}

void CloseDialog::bleiben(){
    farm0->farmFutureEnd(false);
    accept();
    close();
}

void CloseDialog::weiter(){
    farm0->farmFutureEnd(false);
    accept();
    close();
}

//TODOs
void CloseDialog::showClose(int closed){
    closeState=closed;
    updateGUI();

    show();
    //exec();

    //Tempt
    //cout << "closed: "<< closed << endl;
    //Manager->getGlobals()->SIM_WEITER = true;
    //gg->HasCloseDec=true;
}

Title3::Title3(Qt::AlignmentFlag align, QFont f, QWidget*w):QLabel(w){
    this->setAlignment(align);
    this->setFont(f);
}
