#include "closedialog1.h"
#include "AgriPoliS.h"
#include <QLineEdit>
#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include <QComboBox>
#include <QSpinBox>
#include <QGroupBox>
#include <QSignalMapper>
#include <QMessageBox>
#include <qaction.h>
#include <QCoreApplication>

#include "spreadsheet2.h"
#include "cell2.h"
#include "Mainwindow.h"

static QFont font0 = QFont("SansSerif",14, QFont::Bold);
static QFont font1 = QFont("Times",11, QFont::Bold);
static QFont font2 = QFont("Times",11, QFont::Normal);//Bold);
static QFont font3 = QFont("Times",10, QFont::Normal);

CloseDialog::CloseDialog(Mainwindow* par):firstGUI(true), inited(false), showPEwidget(false),QDialog(){
  showBSpiegel = false;
  parent = par;
  setWindowIcon(QPixmap(":/images/grafiken/Traktor.png"));
  setWindowFlags(Qt::WindowStaysOnTopHint);
  setWindowTitle("Aufgabe oder weiter");
  toClose=false;
}

bool CloseDialog::isInited(){
    return !firstGUI;
}

void CloseDialog::setShowBSpiegel(bool b){
    showBSpiegel= b;
}

void CloseDialog::abspeichern(){
    vector<bool> spiels=parent->getSpielPeProds();
    int sz=prodIDs.size();
    int t=parent->aTime.elapsed();
    if (gg->tIter==1) { // first iter
        parent->tstrAus<<QString("=\t=\tBleiben\tEink_Land(")+QChar(0x20ac)
                +")\tEink_Außerhalb("+QChar(0x20ac)+")\n";
        //erwart. Einkommen aus Landwirtschaft")+QChar(0x20ac)
        //  +"\terwart. Einkommen(außerhalb Landwirtschaft)"+QChar(0x20ac)+"\n\n";
    }
    parent->tstrAus<<"\nRunde: \t"<<gg->tIter<<"\t"<<QLocale().toString(t/1000.,'f',2)<<" Sekunden\n";
    parent->tstrAus<<"\t\t"<<(decisions[gg->tIter-1]?"ja":"nein")<<"\t"
            <<QLocale().toString(expIncome,'f',2)<<"\t"<<QLocale().toString(oppcost,'f',2)<<"\n";

    parent->tstrPe<<"\t Ausstieg       \t";
    for (int i=0;i<sz;++i){
        if (spiels[i]) {
            parent->tstrPe<<QLocale().toString((prodRefPEs[i]-prodNertrags[i])/prodErtrags[i],'f',2)+"\t"
                    <<QLocale().toString((it_myPEs[i]-prodNertrags[i])/prodErtrags[i],'f',2)+"\t";
        }
    }parent->tstrPe<<"\n";
}

void CloseDialog::closeEvent(QCloseEvent * event){
    if (!( parent->faOver()||toClose) ){
        event->ignore();
        return;
    }
    //Manager->getGlobals()->SIM_WEITER =true;
    if (!parent->faOver())
        abspeichern();//emit fertig();
    gg->HasCloseDec=true;
    //savePEs();
    hilfeDialog->close();
    inited=false;
    Manager->product_cat0 = Manager->Market->getProductCat();
    QDialog::closeEvent(event);
}

void CloseDialog::updateGUI2(){
    toClose=false;
    if (closeState==3) weiterWidget->setVisible(true);
    else weiterWidget->setVisible(false);
    if (closeState==3) decWidget->setVisible(false);
    else decWidget->setVisible(true);

    if (showBSpiegel)
        spiegelButton->setText("Betriebsspiegel ausblenden");
    else
        spiegelButton->setText("Betriebsspiegel einblenden");

    updateFinOpp();
    makePeWidget2();
}

void CloseDialog::makePeWidget2(){
    peWidget->updateIter(ITER);

    vector<RegProductInfo> pcat = *(farm0->product_cat);
    int sz = numPEs;//pcat.size();

    for (int i=0; i<sz; ++i){
        prodPrices[i]=pcat[prodIDs[i]].getPrice();
        prodPEs[i]=pcat[prodIDs[i]].getPriceExpectation();
    }
    prodRefPEs = prodPEs;
    //prodRefPrices = prodPrices;
    naivPEs = prodPrices;
    adaptPEs = prodPEs;
    manPEs = prodPEs;

    allPrices.push_back(prodPrices);

    vector<double> pes ;
    switch(peVar){
    case 0: pes=manPEs;  break;
    case 1: pes=naivPEs; break;
    case 2: pes=adaptPEs; break;
    default:pes=manPEs;
    }
    #define PROCEVENTS
    peWidget->updateData(prodPrices, prodRefPEs, pes, true);PROCEVENTS
    peWidget->updateData1a(getreidePrices, getreidePEs);PROCEVENTS
    peWidget->updateRefs();
    #undef PROCEVENTS
    inited = true;
}

void CloseDialog::updateGUI(){
    ITER = gg->tIter;

    farm0 = Manager->Farm0;

    soilNames = gg->NAMES_OF_SOIL_TYPES;
    nsoils = gg->NO_OF_SOIL_TYPES;

    langFK=farm0->getLangFKclose();//>getLtBorrowedCapital();
    dispo=farm0->getDispoClose();//>getStBorrowedCapital();
    liquidity=farm0->getLiqClose();//>getLiquidity();
    GDB=farm0->getGDBclose();
    //profit=farm0->getProfClose();
    //income=farm0->getIncClose();

    if (!firstGUI) {
        updateGUI2();
        return;
    }
    QString title("Ausstieg");

    Title3 *titleLab = new Title3(Qt::AlignCenter, font0);
    titleLab->setText(title);

    QVBoxLayout *v0 = new QVBoxLayout; //out
    v0->addWidget(titleLab);

    weiterButton = new QPushButton("weiter");
    connect(weiterButton, SIGNAL(clicked()), this, SLOT(weiter()));

    weiterWidget = new QWidget;

    Title3 *ausLab = new Title3(Qt::AlignCenter, font1);
    ausLab->setText("Aussteigen");
    Title3 *detailLab = new Title3(Qt::AlignCenter, font3);
    detailLab->setText("Sie müssen aufgrund von Illiquidität Ihren Betrieb aufgeben !");
    //Wegen negativem Finanzzustand musst du aussteigen.");

    QHBoxLayout *hh = new QHBoxLayout;
    hh->addStretch();
    hh->addWidget(weiterButton);
    hh->addStretch();

    QVBoxLayout *v1 = new QVBoxLayout;
    //v1->addWidget(ausLab);
    v1->addSpacing(15);
    v1->addWidget(detailLab);
    v1->addLayout(hh);//addWidget(weiterButton);
    weiterWidget->setLayout(v1);

    v0->addWidget(weiterWidget);
    //v0->addStretch(1);


    decWidget = new QWidget;

    Title3 *des = new Title3(Qt::AlignCenter, font1);
    des->setText("Die Entscheidung liegt bei Ihnen");

    aussteigButton = new QPushButton("Ich steige aus");
    bleibButton = new QPushButton("Ich mache weiter");

    connect(aussteigButton, SIGNAL(clicked()), this, SLOT(aussteigen()));
    connect(bleibButton, SIGNAL(clicked()), this, SLOT(bleiben()));

    connect(this, SIGNAL(rejected()), this, SLOT(bleiben()));

    QHBoxLayout *hl3 = new QHBoxLayout;
    hilfeButton = new QPushButton("Hilfe zur Entscheidung");

    hl3->addWidget(hilfeButton);
    hl3->addStretch();

    QHBoxLayout *hl3a = new QHBoxLayout;
    if (showBSpiegel)
        spiegelButton = new QPushButton("Betriebsspiegel ausblenden");
    else
        spiegelButton = new QPushButton("Betriebsspiegel einblenden");
    hl3a->addWidget(spiegelButton);
    hl3a->addStretch();

    QHBoxLayout *hh1 = new QHBoxLayout;
    hh1->addStretch();
    hh1->addWidget(aussteigButton);
    hh1->addWidget(bleibButton);
    hh1->addStretch();

    QVBoxLayout *v2 = new QVBoxLayout;
    v2->addWidget(des);
    v2->addSpacing(10);
    v2->addLayout(hh1);
    //v2->addWidget(aussteigButton);
    //v2->addWidget(bleibButton);
    v2->addSpacing(20);
    v2->addLayout(hl3);
    v2->addSpacing(20);
    v2->addLayout(hl3a);
    decWidget->setLayout(v2);

    v0->addWidget(decWidget);

    hilfeDialog = new QDialog();
    hilfeDialog->setWindowTitle("Opportunitätskosten und Finanzen für angepaßte Preiserwartung");

    connect(hilfeButton, SIGNAL(clicked()), this, SLOT(showHilfe()));
    connect(spiegelButton, SIGNAL(clicked()), this, SLOT(blendenBSpiegel()));
    connect(hilfeDialog, SIGNAL(rejected()), this, SLOT(hilfeClosed()));

    makePeWidget();
    makeFinWidget();
    makeOppWidget();

    QVBoxLayout *vl4 = new QVBoxLayout;

    QHBoxLayout *hh4 = new QHBoxLayout;
    hh4->addStretch();
    hh4->addWidget(finWidget);
    //hh4->addWidget(oppCostWidget);
    hh4->addStretch();

    peButton = new QPushButton("Preiserwartung anpassen");
    showPEwidget=true;//false;
    peWidget->setVisible(showPEwidget);//hide();
    connect(peButton, SIGNAL(clicked()), this, SLOT(blendenPE()));

    /*
    QHBoxLayout *hh5 = new QHBoxLayout;
    hh5->addWidget(peButton);
    hh5->addStretch();
    //*/

    QHBoxLayout *hh6 = new QHBoxLayout;
    hh6->addStretch();
    hh6->addWidget(oppCostWidget);
    hh6->addStretch();

    vl4->addLayout(hh6);
    //vl4->addLayout(hh4);
    //vl4->addSpacing(20);
    //vl4->addLayout(hh5);//addWidget(peButton);
    vl4->addSpacing(10);
    vl4->addWidget(peWidget);

    vl4->setSizeConstraint(QLayout::SetFixedSize);
    hilfeDialog->setLayout(vl4);

    setLayout(v0);
    firstGUI = false;

    if (closeState==3) weiterWidget->setVisible(true);
    else weiterWidget->setVisible(false);
    if (closeState==3) decWidget->setVisible(false);
    else decWidget->setVisible(true);

    //setFixedSize(sizeHint());
    //setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    //resize(400,500);
}

void CloseDialog::showHilfe(){
    hilfeDialog->show();
    hilfeButton->setEnabled(false);
}

void CloseDialog::enableBSbutton(bool b){
    spiegelButton->setEnabled(b);
}

void CloseDialog::blendenBSpiegel(){
    if (!showBSpiegel) {
        spiegelButton->setText("Betriebsspiegel ausblenden");
        parent->actMyData->setDisabled(true);
    }else{
        spiegelButton->setText("Betriebsspiegel einblenden");
        parent->actMyData->setEnabled(true);
    }
    parent->mydatWindow->updateGraphic(6);
    //layerComboBox->setCurrentIndex(5); //betriebsspiegel
    parent->mydatWindow->setVisible(!showBSpiegel);
    showBSpiegel = !showBSpiegel;
}

void CloseDialog::savePEs(){
    it_myPEs.clear();

    switch(peVar) {
    case 0: it_myPEs = manPEs; break;
    case 1: it_myPEs = naivPEs; break;
    case 2: it_myPEs = adaptPEs; break;
    default: ;
    }

    all_myPEs.push_back(it_myPEs);
}

void CloseDialog::hilfeClosed(){
    hilfeButton->setEnabled(true);
}

void CloseDialog::makeOppWidget(){
    oppCostWidget = new QWidget;

    QGroupBox *compGBox = new QGroupBox;

    Title3 *oppKostTitle = new Title3(Qt::AlignCenter, font1);
    oppKostTitle->setText("Außerhalb Landwirtschaft");//Opportunitätskosten");
    Title3 *landWirtschaftTitle = new Title3(Qt::AlignCenter, font1);
    landWirtschaftTitle->setText("Landwirtschaft");//Opportunitätskosten");

    expIncome = farm0->getExpIncome();
    expIncomeLab = new QLabel;
    expIncomeLab->setText(QString("Erwartetes Haushaltseinkommen: %1")
                .arg(QLocale().toString(expIncome,'f',2))+" "+QChar(0x20ac));
    expIncomeLab->setFont(font2);

    oppcost = farm0->getOppcost();
    oppcostLab = new QLabel;
    oppcostLab->setText(QString("Erwartetes Haushaltseinkommen: %1")  //Opportunitätskosten: %1")
            .arg(QLocale().toString(oppcost,'f',2))+ " "+ QChar(0x20ac));
    oppcostLab->setFont(font2);
    QLabel *anmerkLab = new QLabel("Das Haushaltseinkommen außerhalb der Landwirtschaft umfasst die Löhne"
    " der Familienarbeitskräfte, die Zinsen für die \nLiquidität, Pacht für Eigenland und Pacht für Milchquote(bis 2015).");
    anmerkLab->setStyleSheet("color: blue");
    anmerkLab->setIndent(20);

    QGridLayout *gl = new QGridLayout;
    gl->addWidget(oppKostTitle, 0, 2);
    gl->addWidget(landWirtschaftTitle, 0, 0);
    gl->addWidget(expIncomeLab, 1,0);
    gl->addWidget(oppcostLab, 1,2);
    gl->setHorizontalSpacing(30);
    gl->addWidget(anmerkLab, 2, 0, 1, 3);
    compGBox->setLayout(gl);

    QVBoxLayout *vl = new QVBoxLayout;
    vl->addWidget(compGBox);
    oppCostWidget->setLayout(vl);


    /*
    Title3 *oppLab = new Title3(Qt::AlignCenter, font3);
    oppLab->setText("Einkommen bei Arbeit");

    Title3 *oppLab2 = new Title3(Qt::AlignCenter, font3);
    oppLab2->setText("außerhalb der Landwirtschaft:");

    oppcost = farm0->getOppcost();
    Title3 *oppCostLab = new Title3(Qt::AlignCenter, font3);
    oppCostLab->setText(QLocale().toString(oppcost,'f',2)+" "+QChar(0x20ac));

    QVBoxLayout *vv = new QVBoxLayout;
    vv->addWidget(oppKostLab);
    vv->addSpacing(15);
    vv->addWidget(oppLab);
    vv->addWidget(oppLab2);
    vv->addSpacing(15);
    vv->addWidget(oppCostLab);
    vv->addStretch();
    oppcostWidget->setLayout(vv);
    //*/
/*
    vector<QString> tv;

    oppcost = farm0->getOppcost();
    tv.push_back(QString("Einkommen bei Arbeit"));
    tv.push_back("außerhalb der Landwirtschaft:");
    tv.push_back("");
    tv.push_back(QLocale().toString(oppcost,'f',2)+" "+QChar(0x20ac));

    oppSS = new SpreadSheet2(4,1,false, tv,false);

    oppSS->updateColumn(0,tv);
    oppSS->cell(3,0)->setAlign(Qt::AlignCenter, Qt::AlignCenter);

    QVBoxLayout *vf = new QVBoxLayout;
    vf->addWidget(oppKostLab);
    vf->addWidget(oppSS);

    oppCostWidget->setLayout(vf);
    //*/
}

void CloseDialog::makeFinWidget(){
    finWidget = new QWidget;
    Title3* finTitle = new Title3(Qt::AlignCenter, font1);
    finTitle->setText("Finanzen");

    vector<QString> tv;
    finSS = new SpreadSheet2(4,2,false,tv,false);

    tv.push_back(QString("langfristiges FK"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Dispo"));//+QChar(0x20ac)+")");
    tv.push_back(QString("GDB"));//+QChar(0x20ac)+")");
    tv.push_back(QString("Liquidität"));//+QChar(0x20ac)+")");
    //tv.push_back(QString("Gewinn"));//+QChar(0x20ac)+")");
    //tv.push_back(QString("Haushaltseinkommen"));//+QChar(0x20ac)+")");

    /*
    QLabel *langFKLabT = new QLabel("langfristiges FK");
    QLabel *dispoLabT  = new QLabel("Dispo");
    QLabel *GDBLabT = new QLabel ("GDB");
    QLabel *liqLabT = new QLabel ("Liquidität");

    langFKLab = new QLabel;
    dispoLab  = new QLabel;
    GDBLab = new QLabel;
    liqLab = new QLabel;

    langFKLab->setText(QLocale().toString(langFK,'f',2));
    dispoLab->setText(QLocale().toString(dispo,'f',2));
    GDBLab->setText(QLocale().toString(GDB,'f',2));
    liqLab->setText(QLocale().toString(liquidity,'f',2));

    QGridLayout *gl = new QGridLayout;
    gl->addWidget(langFKLabT, 0, 0);
    gl->addWidget(langFKLab,0,1);
    gl->addWidget( dispoLabT, 1,0);
    gl->addWidget(dispoLab, 1,1);
    gl->addWidget( GDBLabT,2,0);
    gl->addWidget(GDBLab,2,1);
    gl->addWidget( liqLabT, 3,0);
    gl->addWidget(liqLab,3,1);

    vector<double> td;
    td.push_back(langFK);
    td.push_back(dispo);
    td.push_back(GDB);
    td.push_back(liquidity);
    //*/

    vector<QString> td;
    td.push_back(QLocale().toString(langFK,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(dispo,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(GDB,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(liquidity,'f',2)+" "+QChar(0x20ac));
    //td.push_back(QLocale().toString(profit,'f',2)+" "+QChar(0x20ac));
    //td.push_back(QLocale().toString(income,'f',2)+" "+QChar(0x20ac));

    finSS->updateColumn(0,tv);
    finSS->updateColumn(1,td);

    QVBoxLayout *vf = new QVBoxLayout;
    vf->addWidget(finTitle);
    vf->addWidget(finSS);

    finWidget->setLayout(vf);
}

void CloseDialog::blendenPE(){
    if (!showPEwidget) {
        peButton->setText("Ausblenden Preiserwartung");
    }else{
        peButton->setText("Preiserwartung anpassen");
    }
    peWidget->setVisible(!showPEwidget);
    showPEwidget = !showPEwidget;

    //alle vorgänger
    peWidget->parentWidget()->layout()->invalidate();
    QWidget *parent = peWidget->parentWidget();
    while (parent) {
        parent->adjustSize();
        parent = parent->parentWidget();
    }
}

void CloseDialog::makePeWidget(){
    //bool hasPeWidget=false;
    /*
    if(parent->getPeWidget()){
        peWidget=parent->getPeWidget();

        disconnect(peWidget, SIGNAL(peSig(int)), parent->investDialog, SLOT(getPeSig(int)));
        disconnect(peWidget, SIGNAL(peVarSig(int)), parent->investDialog, SLOT(getPeVarSig(int)));
        disconnect(parent->investDialog, SIGNAL(peOK(int,bool)), peWidget, SLOT(getPeOK(int,bool)));
        disconnect(peWidget, SIGNAL(ausBlenden()), parent->investDialog, SLOT(getAusblenden()));

        hasPeWidget=true;
    }
    //*/

    vector<QString> vs;
    vs.push_back("Produktname");
    vs.push_back(QString("Einheit "));
    vs.push_back(QString("Preis "));
    vs.push_back(QString("")+QChar(0x0D8)+QString("Preis\nder Ref-Zeit"));
    vs.push_back(QString("PE\nvon Modell"));
    vs.push_back(QString("Meine PE"));
    vs.push_back(QString("Deckungsbeitrag"));
    vs.push_back(QString("Einheit von DB"));
    //vs.push_back(QString("Preisindex"));

    int xcount = 0;
    vector<RegProductInfo> pcat = *(farm0->product_cat);
    int sz = pcat.size();
    bool refnameFound = false;

    vector<bool> peSpiels;
    for (int i=0; i<sz; ++i){
        //if (pcat[i].getProductGroup()<0) continue;
        if (!pcat[i].getPreisSchwankung()) continue;
        if (pcat[i].getProductGroup()== 0  && !refnameFound) {
            refname = QString(pcat[i].getName().c_str());
            refprodID = i;
            refpos = xcount;
            refnameFound = true;

        }else if (pcat[i].getProductGroup()==0){
            getreideIDs.push_back(i);
            getreidePEs.push_back(pcat[i].getPriceExpectation());
            getreidePrices.push_back(pcat[i].getPrice());
            continue;
        }else if (pcat[i].getProductGroup()==-1 && pcat[i].getClass().compare("GRASSLAND")!=0) // nur f. Altmark ??
             continue;

        ++xcount;

        if (parent->spielPeProds.size()==0) {
        string aName = pcat[i].getName();
        bool aFind=false;
        for (int ti=0;ti<gg->MyPeProducts.size();++ti){
            if (gg->MyPeProducts[ti]==aName) aFind = true;
        }
        peSpiels.push_back(aFind);
        }

        prodNames.push_back(QString(pcat[i].getName().c_str()));
        prodPrices.push_back(pcat[i].getPrice());
        prodCosts.push_back(pcat[i].getVarCost());
        prodIDs.push_back(i);
        prodPEs.push_back(pcat[i].getPriceExpectation());
        prodUnits.push_back(pcat[i].getUnit().c_str());
        prodGMunits.push_back(pcat[i].getGMunit().c_str());
        prodErtrags.push_back(pcat[i].getErtrag());
        prodNertrags.push_back(pcat[i].getNebenErtrag());
        //prodRefPEs.push_back(100);
    }
    if (parent->spielPeProds.size()==0)
        parent->setSpielPeProds(peSpiels);
    else
        peSpiels = parent->getSpielPeProds();


    prodRefPEs = prodPEs;
    //prodRefPrices = prodPrices;
    naivPEs = prodPrices;
    adaptPEs = prodPEs;
    manPEs = prodPEs;
    numPEs = prodIDs.size();

    allPrices.push_back(adaptPEs);

    #define PROCEVENTS
    peVar = 0;
    peWidget = new PEwidget(ITER, &peVar, refname, vs, prodNames, prodUnits, prodErtrags, prodNertrags,prodGMunits);PROCEVENTS
    peWidget->setPeSpiels(peSpiels);
    peWidget->updateCosts(prodCosts);
    peWidget->updateData(prodPrices,prodRefPEs, prodPEs);PROCEVENTS
    peWidget->updateData1(getreideIDs,getreidePrices, getreidePEs);PROCEVENTS
    peWidget->updateData2(refpos, &manPEs, adaptPEs, naivPEs);PROCEVENTS
    peWidget->updateData3(prodIDs, &allPrices);PROCEVENTS
    peWidget->updateRefs();
    peWidget->setPeInited(true);
    #undef PROCEVENTS

    connect(peWidget, SIGNAL(peSig(int)), this, SLOT(getPeSig(int)));
    connect(peWidget, SIGNAL(peVarSig(int)), this, SLOT(getPeVarSig(int)));
    connect(this, SIGNAL(peOK(int,bool)), peWidget, SLOT(getPeOK(int,bool)));

    if (!parent->getPeWidget())
        parent->setPeWidget(peWidget);
}

void CloseDialog::getPeVarOK(int r, bool b){
    updateFinOpp();
}

vector<double> CloseDialog::getManPEs() const{
return manPEs;
}

vector<double> CloseDialog::getEffPEs() {
return it_myPEs;
}

void CloseDialog::getPeVarSig(int v){
    emit ClosePeVarSig(v);
}

void CloseDialog::getPeSig(int r){
    emit ClosePeSig(r);
}

void CloseDialog::getPeOK(int r, bool b){
        emit peOK(r,b);
        if (!b) {
            Manager->product_cat0[prodIDs[r]].setPriceExpectation(oldvalue);
            QMessageBox::warning(this, tr("Warnung"),
                             tr("Erwartete Preise zu extrem"));//Bedingungen verletzt"));
        }
        if (!b) return;

        updateFinOpp();
}

void CloseDialog::updateFinOpp(){
    /*langFK=farm0->getLangFKclose();//>getLtBorrowedCapital();
    dispo=farm0->getDispoClose();//>getStBorrowedCapital();
    liquidity=farm0->getLiqClose();//>getLiquidity();
    GDB=farm0->getGDBclose();
    //profit=farm0->getProfClose();
    //income=farm0->getIncClose();

    vector<QString> td;
    td.push_back(QLocale().toString(langFK,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(dispo,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(GDB,'f',2)+" "+QChar(0x20ac));
    td.push_back(QLocale().toString(liquidity,'f',2)+" "+QChar(0x20ac));
    //td.push_back(QLocale().toString(profit,'f',2)+" "+QChar(0x20ac));
    //td.push_back(QLocale().toString(income,'f',2)+" "+QChar(0x20ac));

    finSS->updateColumn(1,td);
    //*/

    oppcost = farm0->getOppcost();
    expIncome = farm0->getExpIncome();
    expIncomeLab->setText(QString("Erwartetes Einkommen: %1")
                .arg(QLocale().toString(expIncome,'f',2))+" "+QChar(0x20ac));
    oppcostLab->setText(QString("Opportunitätskosten: %1")
            .arg(QLocale().toString(oppcost,'f',2))+ " "+ QChar(0x20ac));


    //oppSS->cell(3,0)->setFormula(QLocale().toString(oppcost,'f',2)+" "+QChar(0x20ac));
}

void CloseDialog::aussteigen(){
    QDialog *absicherDialog = new QDialog(this);
    QString title("Steigen Sie wirklich aus ?");

    Title3 *titleLab = new Title3(Qt::AlignCenter, font1);
    titleLab->setText(title);

    QVBoxLayout *v0 = new QVBoxLayout; //out
    v0->addWidget(titleLab);

    QPushButton *jaButton = new QPushButton("Ja");
    connect(jaButton, SIGNAL(clicked()), absicherDialog, SLOT(accept()));

    QPushButton *noButton = new QPushButton("Nein, zurück");
    connect(noButton, SIGNAL(clicked()), absicherDialog, SLOT(reject()));

    QHBoxLayout *hh = new QHBoxLayout;
    hh->addStretch();
    hh->addWidget(jaButton);
    hh->addSpacing(30);
    hh->addWidget(noButton);
    hh->addStretch();

    v0->addSpacing(10);
    v0->addLayout(hh);
    absicherDialog->setLayout(v0);

    if (!absicherDialog->exec()) return;

    decisions.push_back(false);
    farm0->farmFutureEnd(true);
    accept();
    toClose=true;
    savePEs();
    close();
}

void CloseDialog::bleiben(){
    decisions.push_back(true);
    farm0->farmFutureEnd(false);
    accept();
    toClose=true;
    savePEs();
    close();
}

void CloseDialog::weiter(){
    farm0->farmFutureEnd(false);
    accept();
    toClose=true;
    savePEs();
    close();
    parent->mydatWindow->setVisible(false);
    spiegelButton->setText("Betriebsspiegel einblenden");
    showBSpiegel = false;
}

void CloseDialog::getMydatwindowHi(bool t){
    if(!gg->ToPlay) return;
    if (!t) return;
    if (!firstGUI)
        spiegelButton->setText("Betriebsspiegel einblenden");
    else {
        if (isInited())
            spiegelButton->setText("Betriebsspiegel ausblenden");
    }
    showBSpiegel = false;
}

void CloseDialog::showClose(int closed){
    closeState=closed;
    updateGUI();

    /*
    if (showBSpiegel) {
        parent->mydatWindow->layerComboBox->setCurrentIndex(6); //betriebsspiegel
        parent->mydatWindow->show();
        parent->actMyData->setEnabled(false);
    }
    //*/

    show();
    //exec();

    parent->aTime.restart();
    //Manager->getGlobals()->SIM_WEITER = true;
    //gg->HasCloseDec=true;
}

Title3::Title3(Qt::AlignmentFlag align, QFont f, QWidget*w):QLabel(w){
    this->setAlignment(align);
    this->setFont(f);
}
