//#include <QGUI>
#include "optdialog.h"
#include <QHBoxLayout>
#include "AgriPoliS.h"
#include <QDir>
#include <QFileDialog>
#include <QDebug>

OptDialog::OptDialog(QWidget *parent): QDialog(parent) {

    optLabel = new QLabel("Eingabeverzeichnis:");//Folder of input files:");
    optEdit = new QLineEdit(this);
    QString curDir = QDir::currentPath()+"/inputfiles";
    QString testDir = "D:/_testFA/inputfiles";
    if (QDir(testDir).exists())
        optEdit->setText("D:/_testFA/inputfiles");//\\_testFA\ \inputfiles");
    else optEdit->setText(curDir);//"D:/_testFA/inputfiles");//\\_testFA\\inputfiles");

    optLabel->setBuddy(optEdit);

    polLabel = new QLabel("Politik:");//File of policy settings:");
    polEdit = new QLineEdit(this);
    polEdit->setText("policy_settings.txt");
    polLabel->setBuddy(polEdit);

    okButton = new QPushButton("Ok");
    cancelButton = new QPushButton("Abbrechen");//Cancel");

    QPushButton* dirButton = new QPushButton("Verzeichnis w�hlen");//folder...");
    QPushButton* fileButton =new QPushButton("Politikdatei w�hlen");//policy...");

    QHBoxLayout *layout1 =new QHBoxLayout();
    layout1->addWidget(optLabel);
    layout1->addWidget(optEdit);
    layout1->addWidget(dirButton);

    QHBoxLayout *layout2= new QHBoxLayout();
    layout2->addWidget(polLabel);
    layout2->addWidget(polEdit);
    layout2->addWidget(fileButton);

    QGridLayout *glout = new QGridLayout();
    glout->addWidget(optLabel,0,0);
    glout->addWidget(optEdit,0,1);
    glout->addWidget(dirButton,0,2);
    glout->addWidget(polLabel,1,0);
    glout->addWidget(polEdit,1,1);
    glout->addWidget(fileButton,1,2);

    QHBoxLayout *layout3 = new QHBoxLayout();
    layout3->addStretch();
    layout3->addWidget(okButton);
    layout3->addWidget(cancelButton);
    layout3->addStretch();

    QVBoxLayout *layout = new QVBoxLayout();
    //layout->addLayout(layout1);
    //layout->addLayout(layout2);
    layout->addLayout(glout);
    layout->addStretch();
    layout->addLayout(layout3);

    connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(close()));

    connect(dirButton,SIGNAL(clicked()),this,SLOT(getInDir()));
    connect(fileButton,SIGNAL(clicked()),this,SLOT(getPol()));

    setLayout(layout);
    setWindowTitle("Region w�hlen");

    //QFileDialog::getOpenFileName(this, "Policysetting", QDir::currentPath());
    //QFileDialog::getExistingDirectory(this,"Folder of inputfiles",QDir::currentPath());
 }

void OptDialog::getInDir(){
    //qDebug()<<QDir::currentPath();
    /*QString theDir= QFileDialog::getExistingDirectory(this,"Verzeichnis der Eingabedateien",QDir::currentPath());
        //QFileDialog::getExistingDirectory(this,"Verzeichnis der Eingabedateien");//Folder of inputfiles");
        //cout<<"Folder is : "<<theDir.toStdString()<<endl;
        if (!theDir.isEmpty())
            optEdit->setText(theDir);
    //*/
    QFileDialog *fdialog = new QFileDialog(this, Qt::Dialog);
    fdialog->setFileMode(QFileDialog::Directory);
    fdialog->setOption(QFileDialog::ShowDirsOnly,true);
    fdialog->setOption(QFileDialog::HideNameFilterDetails, true);
    fdialog->setNameFilterDetailsVisible (false);
    fdialog->setOption(QFileDialog::DontUseNativeDialog, true);
    connect(fdialog,SIGNAL(fileSelected(QString)),this, SLOT(getDir(QString)));
    fdialog->exec();
    //*/
}

void OptDialog::getDir(QString theDir){
    //QString theDir= fdialog->getExistingDirectory(this,"Verzeichnis der Eingabedateien");
            //QFileDialog::getExistingDirectory(this,"Verzeichnis der Eingabedateien");//Folder of inputfiles");
    //cout<<"Folder is : "<<theDir.toStdString()<<endl;
    if (!theDir.isEmpty())
        optEdit->setText(theDir);
}

void OptDialog::getFile(QString thePol){
    if (!thePol.isEmpty()){
        thePol.replace("\\", "/");
        QString stemPol = thePol.section("/",-1,-1);
        //cout<<"STEMPOL: "<<stemPol.toStdString()<<endl;
        polEdit->setText(stemPol);
        //polEdit->setText(thePol);
    }
}

void OptDialog::getPol(){
    QFileDialog *fdialog = new QFileDialog(this, Qt::Dialog);
    connect(fdialog,SIGNAL(fileSelected(QString)),this, SLOT(getFile(QString)));
    fdialog->exec();
    //*/
    //qDebug()<<QDir::currentPath();
    /*QString thePol = QFileDialog::getOpenFileName(this, "Politik", QDir::currentPath()); //Policy settings "
    if (!thePol.isEmpty()){
        thePol.replace("\\", "/");
        QString stemPol = thePol.section("/",-1,-1);
        //cout<<"STEMPOL: "<<stemPol.toStdString()<<endl;
        polEdit->setText(stemPol);
    }
    //*/
}

void OptDialog::accept(){
    optdir = optEdit->text();
    //cout<<"VORHER:"<<optdir.toStdString()<<endl;
    optdir.replace("\\", "/");
    //cout<<"WEITER:"<<optdir.toStdString()<<endl;
    if (!optdir.size()) {
        QDialog::close();
        return;
    }
    QString thePol = polEdit->text();
    //cout<<"VORHER:"<<thePol.toStdString()<<endl;
    thePol.replace("\\", "/");
    QString stemPol = thePol.section("/",-1,-1);
    //cout<<"STEMPOL: "<<stemPol.toStdString()<<endl;

    policy = stemPol;
    QDialog::accept();
}
