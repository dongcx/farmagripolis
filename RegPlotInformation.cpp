#include "RegPlotInformation.h"
    RegPlotInformationInfo::RegPlotInformationInfo() {
        tc=0;
        tac=0;
        farm_tac=0;
        pe=0;
        plot=0;
        alternative_search_value=0;
    }
    double
    RegPlotInformationInfo::costs() {
        return tc + farm_tac;
    };
    double
    RegPlotInformationInfo::alternativeSeachCosts() const{
        return tc + alternative_search_value;
    };
    
	bool
    RegPlotInformationInfo::operator<=(RegPlotInformationInfo& p1) {
        return costs() <= p1.costs();
    }
    bool
    RegPlotInformationInfo::operator>(RegPlotInformationInfo& p1) {
        return costs() > p1.costs();
    }


