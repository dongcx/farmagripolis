#ifndef BODENDIALOG_H
#define BODENDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QDoubleValidator>


#include "RegPlot.h"

class RegFarmInfo;
class SpreadSheet2;
class Title2;
class QComboBox;
class QSpinBox;
class QDoubleSpinBox;
class Mainwindow;

class BodenDialog : public QDialog {
Q_OBJECT
public:
    BodenDialog(Mainwindow* parent=0);

    void updateFreePlots();
    void updateOffers();

    void closeEvent(QCloseEvent*);
public slots:
    void dispOffer(int,double);
    void okPushed();
    void showBoden(int, int);

    void updateGUI();
    void updateGUI2();

    void getBids();
    void getBids0();
    bool validBids();
    bool validSoilType(int);
    void validSoilType0(int);
    void validBidDSB(int);

    void showHilfe();
    void hilfeClosed();
    void weiter();
    void neueGebote();

    void makePeWidget();
    void makeAreaWidget();

    void makePeWidget2();
    void makeAreaWidget2();

    void blendenPE();
    void updateVar(int);
    void updateSoilType(int);

    void updateArea(int);
    void updateAreas();
    void updateArea2(int);
    void getAreaOK(int,int);
    void getAreaOK2(int);
    void getPeOK(int,bool);

    void updatePE(int);
    void updateIndexLab();

    void savePEs();
    void updateRef(int);

    void updateFreeArea(int);
    void updateErg();

signals:
    void offer(int,double); //soiltype, offer
    void ok();
    void fertig();

    void areaSig(int,int);
    void areaSig2(int);
    void peSig(int);

private:
    void makeEditable(SpreadSheet2 *);
    void calRefPrices();

    QLabel *l1, *l2;
    QLineEdit *le1, *le2;

    vector<vector<pair<int,int> > > offers_of_type;

    int prozent;
    vector<SpreadSheet2*> bidSheets;
    int nsoils;
    vector<string> soilNames;
    RegFarmInfo *farm0;
    vector<int> maxRows;
    vector<double> landinputs;
    vector<QLabel*> labLandinputs;

    int nRowBid, nColBid;

    QString prozentStr;
    Title2 *prozentLab;

    QWidget *bidsWidget, *weiterWidget;
    QPushButton *neuBidsButton, *weiterButton;

    QPushButton *hilfeButton;
    QDialog *dialBodenHilfe;

    QWidget *peWidget, *areaWidget;
    QComboBox *soiltypeCBox;
    int soiltype;
    QComboBox *peVarCBox;

    QSpinBox *refzeitSBox;
    int refzeit;
    int ITER;  //max refzeit
    int peVar;
    QString refname;
    int refprodID;
    int refpos;
    double preisIndex;
    QLabel *pindexLab;

    QPushButton *peButton;
    bool showPEwidget;

    int nRowPE, nColPE;
    int nRowArea, nColArea;
    SpreadSheet2 *peSS, *areaSS;

    vector<vector<QSpinBox*> > areaDSBs; //nach bodentyp
    vector<QDoubleSpinBox*> peDSBs;

    QColor cellBColor;
    vector<QString> prodNames;
    vector<int> prodIDs;
    vector<double> prodPrices;
    vector<double> prodRefPEs;
    vector<double> prodRefPrices;
    vector<double> prodPEs;
    int numPEs;

    vector<int> getreideIDs;
    vector<double> getreidePrices, getreidePEs;

    vector<double> adaptPEs, naivPEs, manPEs;
    vector<vector<double> > allPrices;  //PEs for all iterations

    vector<vector<int> > varAreas;  //nach soiltype
    bool inited;
    double oldvalue;

    bool firstGUI;
    double proz0, proz;

    //it, proz
    vector<map<double, vector<double> > > all_myPEs;

    map<double, vector<double> > it_myPEs;

    //it, proz, soil, bids
    vector<map<int, vector<vector<pair<int, int> > > > > all_bids;

    //proz, soil, bids
    map<int, vector<vector<pair<int, int> > > > it_bids;

    vector<vector<QSpinBox*> > areaDSBs_of_type;
    vector<vector<QSpinBox*> > bidDSBs_of_type;
    bool BidIsValid;

    Mainwindow *parent;
    QSpinBox *radSBox;
    vector<int> freeAreas, radFreeAreas;

    SpreadSheet2 *freeAreaSS;
    SpreadSheet2 *ergSS;
    QWidget *ergWidget;
    vector<int> ergAreas;

    vector<QString> prodUnits;
};

class Title2 : public QLabel{
    Q_OBJECT
public: explicit Title2(Qt::AlignmentFlag align, QFont font, QWidget* w=0);

};

class MyDoubleValidator : public QDoubleValidator
{
public:
    MyDoubleValidator( double bottom, double top, int decimals, QObject*
        parent = 0, const char* name = 0 )
        : QDoubleValidator( bottom, top, decimals, parent)
    {}

    QValidator::State validate ( QString & input, int &pos ) const
    {
        if (this->bottom() < 0 && (input.isEmpty() || input == "." || input == "-" || input == "-.")
                    || this->bottom() >= 0 && (input.isEmpty() || input == "."))
            return Intermediate;

        //if ( input.isEmpty() || input == "." )
        //    return Intermediate;
        if ( QDoubleValidator::validate( input, pos ) != Acceptable ) {
            return Invalid;
        }
        return Acceptable;
    }
};
#endif // BODENDIALOG_H
