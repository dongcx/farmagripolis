#ifndef INVESTDIALOG_H
#define INVESTDIALOG_H

#include <QDialog>
#include <QCloseEvent>
#include <QLabel>
#include <QLineEdit>
#include <spreadsheet2.h>

class RegFarmInfo;
class Title4;
class QDoubleSpinBox;
class QSpinBox;
class QComboBox;
struct Komb;
class QScrollArea;
class QHBoxLayout;

class InvestDialog : public QDialog {
    Q_OBJECT
public:
    InvestDialog(QWidget* parent=0);
    void closeEvent(QCloseEvent*);
public slots:
    void dispInvest(int,int);
    void okPushed();

    void showInvest();
    void updateGUI();
    void updateGUI2();

    void calRefPEs();
    void updateVar(int);
    void getPeOK(int,bool);
    void updatePE(int);
    void updateIndexLab();
    void savePEs();
    void updateRef(int);
    void blendenPE();

    void makePeWidget();
    void makePeWidget2();
    void updatePeWidget(int);

    void makeFinWidget();
    void makeInvWidget();
    void updateFinWidget(int);
    void updateFinWidget();
    void updateFins(int);
    void updateFins();
    void updateInvWidget();
    void updateInvWidget(int);
    void updateHead();

    QWidget* createFinCopy(struct Komb,int);
    QWidget* makeFinNamen();

    void weiter();

    void updateInv(int);
    void getInvestOK(int,bool);

    void updateComb(int);
    void updateComb(QString);
    int combInd(int);
    int indComb(int);

    void neuKomb();
    void delKomb();
    void updateCombs();
    void saveComb(int);

    void saveCombInv(int);
    void saveCombPe(int);
    void saveCombFin(int);


signals:
    void invest(int,int); //inv, anzahl

    void investPeSig(int);
    void investSig(int,int);


private:
    QLabel *l1, *l2;
    QLineEdit *le1, *le2;

    RegFarmInfo *farm0;

    int aktComb, numComb;

    bool firstGUI, inited, showPEwidget;

    QWidget *peWidget;
    QComboBox *peVarCBox;
    QSpinBox *refzeitSBox;

    vector<QString> prodNames;
    vector<int> prodIDs;
    vector<double> prodPrices;
    vector<double> prodRefPEs;
    vector<double> prodPEs;
    int numPEs;

    vector<int> getreideIDs;
    vector<double> getreidePrices, getreidePEs;

    vector<double> adaptPEs, naivPEs, manPEs;
    vector<vector<double> > allPEs;  //PEs for all iterations

    int nRowPE, nColPE;
    QLabel *pindexLab;
    double preisIndex;

    int peVar;
    QString refname;
    int refprodID;
    int refpos;

    SpreadSheet2 *peSS, *finSS, *invSS;
    vector<QDoubleSpinBox*> peDSBs;

    vector<vector<double> > all_myPEs;
    vector<double>  it_myPEs;

    double oldvalue;

    vector<QString> invNames;
    vector<int> invCaps;
    vector<int> invRefs;
    vector<int> myInvs;

    int nRowInv, nColInv;
    vector<QSpinBox*> invDSBs;

    QWidget *finWidget, *invWidget;
    double langFK, dispo, GDB, liquidity;
    double profit, income;

    QPushButton *peButton, *newKombButton, *weiterButton;
    QPushButton *delButton;
    QWidget* weiterWidget;
    QLineEdit *anzKomb;
    QSpinBox *nrSBox;
    QLineEdit *aktLE;

    QScrollArea *sa;
    //QWidget *saWidget;
    QHBoxLayout *saHLout;
    vector <Komb> combs;
    vector<SpreadSheet2*> fins;
    vector<QWidget*> wins;
    int finwidth;
    QWidget *finW;

    int ITER;
    bool nurHead;
    bool inComb;

    vector<int> plans;
    int maxPlan;
    vector<Title4*> titles;
};

struct Komb {
    vector<double> fins;
    vector<double> pes;
    vector<int> invs;
    vector<int> invRefs;
};

class Title4 : public QLabel{
    Q_OBJECT
public: explicit Title4(Qt::AlignmentFlag align, QFont font, QWidget* w=0);

};
#endif // INVESTDIALOG_H
